!> Module testing our integration (cubature) implementation
module test_integration
#include "../src/preprocessor.inc"
  use fruit
  use fruit_util
  use kind
  implicit none
contains
  subroutine test_d01ahf_style_function
    call assert_equals(12._DBL, func(2._DBL), 'Function behaving weird')
  end subroutine test_d01ahf_style_function

  real(DBL) function func(i)
    real(DBL), intent(in) :: i ! input
    func = i**2 + i**3
  end function func

  subroutine integrand(dim_in, f_in, f_data, dim_out, f_out, ifail_func)
    use kind
    integer, intent(in) :: dim_in, dim_out
    real(kind=dbl), dimension(:), intent(in) :: f_in !dim_in
    real(kind=dbl), dimension(:), intent(inout) :: f_data
    real(kind=dbl), dimension(:), intent(out) :: f_out !dim_out
    integer, intent(inout) :: ifail_func

    f_out = func(f_in(1))
  end subroutine integrand

  function func_2d(i)
    real(DBL), intent(in) :: i ! input
    real(DBL), dimension(2) :: func_2d
    func_2d(1) = i**2 + i**3
    func_2d(2) = i**3 + i**4
  end function func_2d

  subroutine integrand_2d(dim_in, f_in, f_data, dim_out, f_out, ifail_func)
    use kind
    integer, intent(in) :: dim_in, dim_out
    real(kind=dbl), dimension(:), intent(in) :: f_in !dim_in
    real(kind=dbl), dimension(:), intent(inout) :: f_data
    real(kind=dbl), dimension(:), intent(out) :: f_out !dim_out
    integer, intent(inout) :: ifail_func

    f_out = func_2d(f_in(1))
  end subroutine integrand_2d

end module test_integration
