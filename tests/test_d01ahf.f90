!> Module testing our NAG implementation
module test_d01ahf
#include "../src/preprocessor.inc"
  use fruit
  use fruit_util
  use kind
  implicit none
contains
  subroutine test_d01ahf_style_function
    implicit none
    real(DBL) :: func
    call assert_equals(12._DBL, func(2._DBL), 'Function behaving weird')
  end subroutine test_d01ahf_style_function

  subroutine test_d01ahf_style_nag_1din_1dout
    implicit none

    real(DBL) :: A, B, EPR, RELERR
    integer :: IFAIL, NL, NPTS

    real(DBL) :: res
    external d01ahf
    real(DBL) :: d01ahf

    real(DBL) :: func

    A = -1
    B = 1
    EPR = 1e-2
    NL = -1
    IFAIL = -1
    res = -99999
    res = d01ahf(A,B,EPR,NPTS,RELERR,func,NL,IFAIL)

    !print *, 'Used ', npts, ' evalution to reach precision of', relerr

    call assert_equals(0, IFAIL, 'Integration routine failed')
    call assert_equals(2._DBL/3, res, EPR, 'Expected different result from analytical integration')
  end subroutine test_d01ahf_style_nag_1din_1dout

  subroutine test_d01ahf_style_integrate_1din_1dout
    use datcal, only: general_integrand
    use mod_integration, only: integration_options, integration, nag1d_options
    implicit none

    external integrand

    procedure(general_integrand) :: f
    integer :: dim_in, dim_out
    real(DBL), dimension(1) :: int_lbound, int_ubound
    class(integration_options), allocatable :: poly_opts
    integer :: ifail
    real(DBL), dimension(0) :: f_data
    real(kind=DBL), dimension(1) :: result, result_err

    allocate(nag1d_options::poly_opts)
    dim_out = 1
    dim_in = 1
    int_lbound = -1
    int_ubound = 1
    poly_opts%reqRelError = 1e-2
    call integration(dim_out, integrand, dim_in, int_lbound, int_ubound, poly_opts, result, result_err, f_data, ifail)

    !print *, 'Reached precision of', result_err

    call assert_equals(0, IFAIL, 'Integration routine failed')
    call assert_equals(2._DBL/3, result(1), poly_opts%reqRelError, 'Expected different result from analytical integration')
  end subroutine test_d01ahf_style_integrate_1din_1dout

  subroutine test_d01ahf_style_integrate_1din_2dout
    use datcal, only: general_integrand
    use mod_integration, only: integration_options, integration, nag1d_options
    implicit none

    external integrand_2d

    procedure(general_integrand) :: f
    integer :: dim_in, dim_out
    real(DBL), dimension(1) :: int_lbound, int_ubound
    class(integration_options), allocatable :: poly_opts
    integer :: ifail
    real(DBL), dimension(0) :: f_data
    real(kind=DBL), dimension(2) :: result, result_err

    allocate(nag1d_options::poly_opts)
    dim_out = 2
    dim_in = 1
    int_lbound = -1
    int_ubound = 1
    poly_opts%reqRelError = 1e-2
    call integration(dim_out, integrand_2d, dim_in, int_lbound, int_ubound, poly_opts, result, result_err, f_data, ifail)

    !print *, 'Reached precision of', result_err

    call assert_equals(0, IFAIL, 'Integration routine failed')
    call assert_equals(2._DBL/3, result(1), poly_opts%reqRelError, 'Expected different result from analytical integration')
    call assert_equals(2._DBL/5, result(2), poly_opts%reqRelError, 'Expected different result from analytical integration')
  end subroutine test_d01ahf_style_integrate_1din_2dout


end module test_d01ahf
