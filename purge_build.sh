#!/usr/bin/env bash
#
# Author: Karel van de Plassche, <karelvandeplassche@gmail.com>
# Based on TUBS purge, repurposed for QuaLiKiz
set -u

function usage() {
  printf "\n"
  printf "Usage: `basename $0`\n"
  printf "\n"
  printf "\nOptions:\n"
  printf "  -h       print this usage information and exit\n"
  printf "  -q       do not print non-error messages\n"
  printf "  -f       purge all without asking.\n"
  printf "  -r       remove *.a and binaries too (default remove .o and .mod).\n"
  printf "\n"
  printf "\nOptional arguments:\n"
  printf "  dir      directory of which the subdirectories will be purged. (default autodetect)\n"
  printf "\n!!! Autodetect will purge $rootDir !!!\n"
}

# Initialize defaults
quiet=0
force=0
realclean=0
tubsDir="$(dirname "$(realpath "$0")")";
makefile="$(realpath "$tubsDir/Makefile")";
rootDir="$(dirname "$makefile")"

OPTIND=1
# Resetting OPTIND is necessary if getopts was used previously in the script.
# It is a good idea to make OPTIND local if you process options in a function.

# Read user input (if any)
while getopts ":hqfr" opt; do
  case $opt in
    h)
      usage
      exit 0
      ;;
    q)
      quiet=1
      ;;
    f)
      force=1
      ;;
    r)
      realclean=1
      ;;
    \?)
      printf "Invalid option: -$opt\n" >&2
      usage
      exit 1
      ;;
    :)
      printf "Option -$OPTARG requires an argument.\n" >&2
      usage
      exit 1
      ;;
  esac
done

# Discard the options and sentinel --
# Everything that's left in "$@" is a non-option.  In our case, a DIR to process.
shift "$((OPTIND-1))"
if [[ $# > 1 ]]; then
    printf "Can only clean one dir at the time\n"
    exit 1
elif [[ $# = 1 ]]; then
    dirArg="$1"
else
    dirArg=""
fi

# Find directory to purge
if [[ $dirArg == "" ]]; then
    # Check if given Makefile is a TUBS makefile
    if ! grep -e 'include .*tubs/main.make' "${makefile}" >/dev/null 2>&1; then
        printf "No TUBS-importing Makefile found, abort!\n" >&2;
        exit 1
    else
        purgeDir=$(dirname "$makefile")
        if [ $quiet = 0 ]; then
            printf "Purging TUBS dir '$purgeDir'\n"
        fi
    fi
else
    purgeDir=$dirArg
    if [ $quiet = 0 ]; then
        printf "Explicit dir '$purgeDir' given to purge, skip TUBS safety check\n"
    fi
fi

# Find files to purge
if [ $realclean = 1 ]; then
    IFS='\n' readarray -t  files < <(find "$(dirname "$makefile")" \( -name '*.o' -or -name '*.a' -or -name '*.mod' -or -name '*.exe' \) -print)
else
    IFS='\n' readarray -t  files < <(find "$(dirname "$makefile")" \( -name '*.o' -or -name '*.mod' \) -print)
fi
if [ "${#files[@]}" -eq 0 ]; then
    if [ $quiet = 0 ]; then
        printf "No purgable files found, nothing to do\n"
    fi
    exit 0
fi

if [ $quiet = 0 ]; then
    printf "Found purgable files:\n"
    printf '%s\n' "${files[*]}"
fi

# Get user input what to do
if [ $force != 1 ]; then
    read -p "Purge files [Y/n] " yno
    if [ -z "$yno" ]; then
      printf "Y\n"
      yno=Y
    fi
else
    yno=Y
fi

case "$yno" in
  Y | y)
    if [ $quiet = 0 ]; then
        printf "Purging files\n"
    fi
    rm "${files[@]}"
    ;;
  N | n)
    if [ $quiet = 0 ]; then
        printf "Not purging files\n"
    fi
    exit 0
    ;;
esac
