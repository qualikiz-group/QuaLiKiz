ifeq ($(JETTO_FIRST_RUN),1)
  $(info Applying JETTO build pre-TUBS modification)
  ifeq ($(TOOLCHAIN),intel)
    override FFLAGS := $(FFLAGS:-O2=-O1)
  endif
  # Limit pgi optimisation (QLK commit 322bda33), leads to missing TEM modes when coexisting with ITG
  ifeq ($(TOOLCHAIN),pgi)
    $(info overwriting)
    override FFLAGS := $(FFLAGS:-O2=-O1)
    $(info FFLAGS=$(FFLAGS))
  endif
  JETTO_FIRST_RUN=0
else
$(info Applying JETTO build post-TUBS modification)
# Import main JETTO build include file
include $(JSRCPATH)/include.mk

  ## Set the local environment
  ## ------------------------------------
  # Overwrite JETTO pattern rules.
%.mod %.o: %.f90
	# Keep this line WITH the tab

# Import 'transport folder' build include file
include $(QLKROOT_)/../include.mk


$(LIBNAME): $(PROJECT_SUB_QLK_TGT) $(PROJECT_SUB_QLK_OBJS)
	@echo Building $(LIBNAME)
	@echo The objects were contained in $(PROJECT_SUB_QLK_TGT)
	@echo Namely $(PROJECT_SUB_QLK_OBJS)
	ar vr $(LIBNAME) $(PROJECT_SUB_QLK_OBJS)
endif
