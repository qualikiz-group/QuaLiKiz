# Building Docker images
This `Dockerfile` uses 'layers' to produce smaller images. To build
and run a single tagged layer, for example the intel build layer:

``` bash
export img=intel-build
sudo docker build --tag $img --target $img .
sudo docker run --tty --interactive $img
```

Or as a one-liner in fish:

``` fish
export img=intel-build; and sudo docker build -t $img --target $img . ; and sudo docker run -ti $img
```
This will build the layers one-by-one

You can also build the image as stored in the gitlab.com
[container registery](https://gitlab.com/qualikiz-group/QuaLiKiz/container_registry)
Do not use cache to avoid any Docker cache mistakes in my Docker file.

``` bash
sudo docker build --no-cache --tag registry.gitlab.com/qualikiz-group/qualikiz:latest .
```

Check out the registry on how to push and log in. When developing,
keep in mind the
[best practices for writing docker files](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
, the [Dockerfile reference](https://docs.docker.com/engine/reference/builder/), and
[using multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/)

# Using docker images
As we work with tagged images, first make sure you have the right one. This will
check if you have it locally first, so if you build locally, no internet
communication is done

``` bash
sudo docker pull registry.gitlab.com/qualikiz-group/qualikiz:latest
```

Then run this image. Usually you want to mount your local QuaLiKiz install.
Be warned, "out of the box" Docker runs as root, so files will be owned by root.
This mounts the QuaLiKiz folder like it is in gitlab CI, e.g. on
`/builds/qualikiz-group/QuaLiKiz`

``` bash
sudo docker run --volume='/home/karel/working/QuaLiKiz/:/builds/qualikiz-group/QuaLiKiz' --tty --interactive --workdir=/builds/qualikiz-group/QuaLiKiz --hostname=docker-debian registry.gitlab.com/qualikiz-group/qualikiz
```

This gives you a bash shell with all compilers, e.g.:
```
ifort --version
gfortran --version
pgf90 --version
mpif90 # This uses gfortran and Open MPI by default
ompi_info # This gives info for Open MPI, see the top for versions
mpiifort # This uses ifort and intel MPI by default
```

# Advanced
Stop all Docker containers
``` bash
sudo docker container ls -aq | xargs -I {} sudo docker container stop {}
```

Remove all stopped containers
``` bash
sudo docker container ls -aq | xargs -I {} sudo docker container rm {}
```

Clean up any resources - images, containers, volumes, and networks - that are dangling (not associated with a container):
``` bash
sudo docker system prune
```
