#!/bin/bash
# Script based on update_jintrac_mirrors.sh by F.J. Casson
#
# Script used to update the QuaLiKiz repository at JET
#
#   Mirror to    * git.ccfe.ac.uk
#
# Prerequisites:  
# * Accounts on git.ccfe.ac.uk and an ssh key pair
#   shared between local key ~/.ssh (private) and git.ccfe.ac.uk (public)
# * The ssh key needs to be created without passphrase if you want the
#   script to run without requiring your interaction.
# * Read access to all the relevant QuaLikiz repositories
# * Write access to all the relevant JET repositories
#
# If you leave the created local folders in place you can save time for
# subsequent synchronisations, but it is also fine to remove them.
# Do not use the local copies created for anything other than
# maintaining the mirror connection.
#
# While the QuaLiKiz repositories continue as the masters, the JET repos
# should only be updated via this script.
#
# K.L. van de Plassche - Jan 2019
# F.J. Casson - Mar 2017, updated Nov 2018



SYNC_MIRRORS_LOCAL=~/sync_mirrors
TARGET_SSH_KEY=$HOME/.ssh/id_rsa_jet_docker

# Cache the SSH key for 2 minutes (assumes default key is the one to use)
# on hpc-app1, /usr/bin/ssh does not recognise newer key formats
# but /usr/local/bin/ssh does....
function start_agent {
  echo "Initialising new SSH agent..."
  #/usr/bin/ssh-agent -t 120 | sed 's/^echo/#echo/' 
  eval "$(ssh-agent -t 120)"
  echo agent started
  ssh-add -t 120 $TARGET_SSH_KEY
  echo adding SSH_PRIVATE_KEY
  # Key was base64 encoded: cat ~/.ssh/key | base64 -w0
  ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 --decode)
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
  ssh-add -L
 }

# 1) Clone a target repo, for example at gitlab.com (as a bare mirror) into a local folder
#    (or update the repository if the folder already exists)
# 2) Push everything (as a mirror) to the remote repo, for example JET
function clone_or_update_and_push {
   source_gitlab_link=$1
   source_namespace=$2
   source_repository_name=$3
   target_gitlab_link=$4
   target_namespace=$5
   target_repository_name=$6
   source_branch=$7
   target_branch=$8

   cd $SYNC_MIRRORS_LOCAL
   echo Updating $target_repository_name ...

   #If already cloned as mirror, just update to match source origin
   if  [ -d $target_repository_name ]; then    
     cd $target_repository_name
     #git remote update
     # This also remove branches no longer on source remote
     git fetch origin --prune

     #do not continue if first connection attempt fails, leads to IP banning by gitlab
     if [ $? -ne 0 ]; then
       echo "git source connection error, aborting"
       ssh-agent -k
       exit 1
     fi

   # Clone as a bare mirror if not existing
   else
     #git clone --mirror git@$source_gitlab_link:$source_namespace/$source_repository_name.git $target_repository_name
     git clone -v --bare https://$source_gitlab_link/$source_namespace/$source_repository_name.git $target_repository_name
     cd $target_repository_name
     git remote -v add target git@$target_gitlab_link:$target_namespace/$target_repository_name.git
   fi

   #mirror exactly, unless $source_branch argument is present
   if [ -z $source_branch ]; then
     # push all branches and tags, but not pull_requests (may get refused by stash)
     #GIT_SSH_COMMAND="ssh" git push -v target $(find refs/heads -type f) $(find refs/tags -type f)
     git push -v --mirror target

     # Alternate below will also prune branches from target repo not on the source, but this is
     # dangerous because it will remove any work pushed directly to target repo.
     # There is no way to both cleanup branches automatically and protect against this,
     # so discarded branches have to be removed manually at target
     # git push --prune target +refs/heads/* +refs/tags/*

     if [ $? -ne 0 ]; then
       echo "git target connection error, aborting"
       ssh-agent -k
       exit 1
     fi
   else
     #push single named branch to single named branch
     git push target $source_branch:$target_branch
   fi

 }

start_agent
#export GIT_SSH_COMMAND="/usr/local/bin/ssh -v -o IdentitiesOnly=yes"
export GIT_SSH_COMMAND="ssh -o IdentitiesOnly=yes -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

# load latest git (ok to reload it)
# module load git

if  [ ! -d $SYNC_MIRRORS_LOCAL ]; then
  mkdir $SYNC_MIRRORS_LOCAL
fi
# Keep contents private by default, since server read permissions are unknown
chmod 700 $SYNC_MIRRORS_LOCAL

# QuaLiKiz
clone_or_update_and_push gitlab.com qualikiz-group QuaLiKiz git.ccfe.ac.uk kplass QuaLiKiz

ssh-agent -k
