src_dir: ./src
output_dir: ./doc
project: QuaLiKiz
project_website: http://qualikiz.com
project_download: https://gitlab.com/qualikiz-group/QuaLiKiz
summary: Rapid quasilinear gyrokinetic tokamak transport model
author: QuaLiKiz-group
author_description: Based at the Dutch Institude For Fundamental Energy Research ([DIFFER](https://www.differ.nl/)) and CEA, but accepting improvements from all over the world. Most of us work in the Integrated Modelling and Transport ([IMT](https://www.differ.nl/research/integrated-modelling-and-transport)) group, where we create, support and use different models for full-device modelling of Tokamaks.
email: j.citrin@differ.nl
fpp_extensions: f90
predocmark: >
media_dir: ./media
docmark_alt: #
predocmark_alt: <
display: public
         protected
         private
source: true
graph: true
coloured_edges: true
search: true
macro: MPI
warn: true
page_dir: .

Please refer to the [wiki](https://gitlab.com/qualikiz-group/QuaLiKiz/wikis) for more usage-type documentation. This page contains detailed information about all methods and types defined in QuaLiKiz
