$(call SUBPROJECT_depend, QLK)

$(call SUBPROJECT_reset_local)
$(call SUBPROJECT_set_local_dir_here)

# lto gives trouble with fruit
ifeq ($(TOOLCHAIN),gcc)
  QLK_EXTRA_FFLAGS+=-fno-lto
endif
#$(eval include $(QLKROOT_)/flags.make)
$(call SUBPROJECT_set_local_fflags, gcc, \
	-cpp \
	$(QLK_EXTRA_FFLAGS) \
)
$(call SUBPROJECT_set_local_fflags, intel, \
	-fpp \
	$(QLK_EXTRA_FFLAGS) \
)
$(call SUBPROJECT_set_local_fflags, pgi, \
	-Mpreprocess \
	$(QLK_EXTRA_FFLAGS) \
)

$(call SUBPROJECT_set_linkflags, gcc, \
	$(QLK_EXTRA_LFLAGS_) \
)
$(call SUBPROJECT_set_linkflags, intel, \
	$(QLK_EXTRA_LFLAGS_) \
)
$(call SUBPROJECT_set_linkflags, pgi, \
	$(QLK_EXTRA_LFLAGS_) \
)
#
#$(call SUBPROJECT_set_linkflags, gcc, \
#	$(QLKNN_EXTRA_LFLAGS) \
#)

$(call LOCAL_add, \
	lib/src/fruit/src/fruit.f90 \
)

ifeq ($(TUBSCFG_MPI),1)
$(call LOCAL_add, \
	lib/src/fruit/src/fruit_mpi.f90 \
)
$(call LOCAL_mod_dep, tests/test_d01ahf.f90, fruit_mpi.mod fruit.mod)
$(call LOCAL_mod_dep, tests/test_integration.f90, fruit_mpi.mod fruit.mod)
else
$(call LOCAL_mod_dep, tests/test_d01ahf.f90, fruit_mpi.mod)
$(call LOCAL_mod_dep, tests/test_integration.f90, fruit_mpi.mod)
endif

$(call LOCAL_add, \
	tests/test_integration.f90 \
)

ifeq ($(QLK_HAVE_NAG),1)
$(call LOCAL_add, \
	tests/test_d01ahf.f90 \
)
endif
