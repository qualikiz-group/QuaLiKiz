QLKROOT_ := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

# Set up pre-tubs JETTO-specific overwrites
ifeq ($(TARGET_ENV),jetto)
  JETTO_FIRST_RUN=1
  include jetto.make
endif

# Include all tubs definitions
include $(QLKROOT_)/tubs/main.make


# Copy generated QuaLiKiz binary to cannonical 'QuaLiKiz' path
all:
	rm -f QuaLiKiz
	ln -s $(PROJECT_SUB_QuaLiKiz_TGT) QuaLiKiz

# Boilerplate: if invoked directly (and not via the root Makefile), generate
# the neccessary standard rules
ifeq ($(TCI_MAIN_MAKE_FIRST),1)
$(call PROJECT_setup)
endif

# Set link flags for NAG.
ifeq ($(origin QLK_HAVE_NAG), $(filter $(origin QLK_HAVE_NAG),default undefined))
  QLK_HAVE_NAG := 1
endif
include $(QLKROOT_)/nag_link.make

$(call SUBPROJECT_open,QLK,staticlib)
include $(QLKROOT_)/transport_module_mpi.make
$(call SUBPROJECT_close)

ifeq ($(TOOLCHAIN), pgi)
  PGI_MAJOR_VERSION:=$(shell pgfortran --version 2> /dev/null | head -n 2 | cut -d' ' -f 2 | cut -d'.' -f 1)
  ifeq ($(shell expr $(PGI_MAJOR_VERSION) \<= 14), 1)
    $(warning Old PGI compiler detected! Cannot build unit tests!)
    DEPRECATED_COMPILER := 1
  endif
endif

ifneq ($(DEPRECATED_COMPILER),1)
  $(call SUBPROJECT_open,fruit,staticlib)
  include $(QLKROOT_)/fruit.make
  $(call SUBPROJECT_close)
endif

include $(QLKROOT_)/qlk_python.make

$(call SUBPROJECT_open,QuaLiKiz,app)
include $(QLKROOT_)/qlk_standalone.make
$(call SUBPROJECT_close)

# Set up post-tubs JETTO-specific overwrites
ifeq ($(TARGET_ENV),jetto)
  $(info JETTO build detected, loading JETTO settings)
  include jetto.make
endif
# Boilerplate: if invoked directly (and not via the root Makefile), generate
# the build rules here
ifeq ($(TCI_MAIN_MAKE_FIRST),1)
$(call PROJECT_finalize)
endif

# Dump all variables
# Based on https://www.cmcrossroads.com/article/dumping-every-makefile-variable
# Basically, for every defined variable the expanded and defined values are
# are printed out. Some variables are skipped, as on expansion they actually
# run functions.
.PHONY: printvars dependency_tree
ignore_printvar=PROJECT_finalize PROJECT_setup SUBPROJECT_close SUBPROJECT_close_impl_ SUBPROJECT_open SUBPROJECT_close_impl_ SUBPROJECT_open_impl_ UTIL_build UTIL_link UTIL_target_of
printvars:
	@$(foreach V,$(sort $(.VARIABLES)), $(if $(filter-out .VARIABLES $(ignore_printvar) $      (filter-out environment% default automatic,$(origin $V)), $V),$(info $V=$($V) ($(value $V      )))))
	@$(foreach V,$(ignore_printvar), $(info Ignored $V))

dependency_tree:
	@for name in $(PROJECT_SUB_QLK_SRCS); do \
		./tubs/makedepend -t $$name | sort | uniq; \
	  echo;\
	done;

tubs_mod_dependency_tree:
	$(eval makevars := $(foreach V, $(PROJECT_SUB_QLK_SRCS), FORT_MOD_DEPEND_$V $(FORT_MOD_DEPEND_$V) break))
	@for name in $(makevars); do \
		if [ $$name == "break" ]; then \
			echo; \
		else \
			echo $$name; \
		fi; \
	done;

# Recipes for fruitsh
QLK_TEST_LIBS := $(PROJECT_SUB_QLK_TGT) $(PROJECT_SUB_fruit_TGT)
QLK_TEST_OBJS := $(PROJECT_SUB_fruit_OBJS)

test%.f90.o: test%.f90
	$(V_)$(FC) $(BAKEDFFLAGS) $(SUB_LOCALFLAGS_$(TOOLCHAIN)_.f90) -c $< -o $@

test%: test%.f90.o $(QLK_TEST_LIBS)
	$(V_)$(LINK) $(BAKEDLFLAGS) -o $@ $< $(LNKGRPBEG) $(PROJECT_SUB_EXTRALNK_$(TOOLCHAIN)) $(patsubst lib%.a,-l%,$(notdir $(QLK_TEST_LIBS))) $(LNKGRPEND) $(MKL_LDFLAGS)
