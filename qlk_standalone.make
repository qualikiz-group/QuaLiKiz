$(call SUBPROJECT_depend, QLK)

$(call SUBPROJECT_reset_local)
$(call SUBPROJECT_set_local_dir_here)

include $(QLKROOT_)/flags.make

$(call SUBPROJECT_set_linkflags, gcc, \
	$(QLK_EXTRA_LFLAGS_) \
)
$(call SUBPROJECT_set_linkflags, intel, \
	$(QLK_EXTRA_LFLAGS_) \
)
$(call SUBPROJECT_set_linkflags, pgi, \
	$(QLK_EXTRA_LFLAGS_) \
)

$(call LOCAL_add,\
	src/qlk_standalone.f90 \
)
