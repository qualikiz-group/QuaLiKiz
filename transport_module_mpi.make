$(call SUBPROJECT_reset_local)
$(call SUBPROJECT_set_local_dir_here)
ifeq ($(origin QLKROOT_), undefined)
QLKROOT_ := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
endif

# Set up build-time version information
# Potentially breaks if QLK version 10 or higher is released
QLK_CLOSEST_RELEASE:="'$(shell cd $(QLKROOT_) && git describe --tags --abbrev=0 --match [[:digit:]].*)'"
dummy:=$(shell git branch --no-color | grep [*])
QLK_GITBRANCHNAME:="'$(shell a="$(dummy)"; echo $${a:2})'"
QLK_DIRTY=$(shell [[ $$(git diff --shortstat --ignore-submodules 2> /dev/null | tail -n1) != "" ]] && echo -dirty)
QLK_GITSHAKEY:="'$(shell git --git-dir=$(QLKROOT_)/.git rev-parse --verify HEAD)$(QLK_DIRTY)'"
QLK_COMPILE_DATE:="'$(shell date "+%Y-%m-%d %T %Z")'"
QLK_COMPILER_VERSION:="'$(shell $(FC) --version | sed '/^$$/d' | head -n 1)'"

# Local QuaLiKiz flags
# Do not allow warnings in core QuaLiKiz
ifeq ($(TOOLCHAIN),gcc)
  QLK_EXTRA_FFLAGS += -Werror
endif
include $(QLKROOT_)/flags.make

# Fortran files for QUALIKIZ
$(call LOCAL_add,\
	src/asymmetry.f90 \
	src/calcroutines.f90 \
	src/callpassQLints.f90 \
	src/callpassints.f90 \
	src/calltrapQLints.f90 \
	src/calltrapints.f90 \
	src/datcal.f90 \
	src/datmat.f90 \
	src/diskio.f90 \
	src/dispfuncs.f90 \
	src/FLRterms.f90 \
	src/hcub.f90 \
	src/kind.f90 \
	src/mod_contour.f90 \
	src/mod_fluidsol.f90 \
	src/mod_fonct.f90 \
	src/mod_make_io.f90 \
	src/mod_saturation.f90 \
	src/nanfilter.f90 \
	src/passints.f90 \
	src/QLflux.f90 \
	src/mod_qualikiz.f90 \
	src/trapints.f90 \
)

ifeq ($(TOOLCHAIN), pgi)
  PGI_MAJOR_VERSION:=$(shell pgfortran --version 2> /dev/null | head -n 2 | cut -d' ' -f 2 | cut -d'.' -f 1)
  ifeq ($(shell expr $(PGI_MAJOR_VERSION) \<= 14), 1)
    $(warning Old PGI compiler detected! Cannot build mod_integration!)
    DEPRECATED_COMPILER := 1
  endif
endif

ifneq ($(DEPRECATED_COMPILER),1)
    $(call LOCAL_add,\
      src/mod_integration.f90 \
    )
    $(call LOCAL_mod_dep, src/mod_integration.f90, datcal.mod kind.mod hcub.mod)
endif

$(call LOCAL_mod_dep, src/mod_qualikiz.f90, asymmetry.mod calcroutines.mod datcal.mod datmat.mod kind.mod mod_make_io.mod mod_saturation.mod )
$(call LOCAL_mod_dep, src/callpassQLints.f90, kind.mod datcal.mod datmat.mod mod_integration.mod callpassints.mod passints.mod )
$(call LOCAL_mod_dep, src/callpassints.f90, kind.mod passints.mod)
$(call LOCAL_mod_dep, src/calltrapQLints.f90, kind.mod datcal.mod datmat.mod mod_integration.mod trapints.mod calltrapints.mod)
$(call LOCAL_mod_dep, src/calltrapints.f90, kind.mod trapints.mod)
$(call LOCAL_mod_dep, src/QLflux.f90, kind.mod datmat.mod datcal.mod callpassqlints.mod calltrapqlints.mod )
$(call LOCAL_mod_dep, src/calcroutines.f90, kind.mod datcal.mod datmat.mod flrterms.mod mod_fluidsol.mod mod_contour.mod mod_make_io.mod mod_fonct.mod mod_integration.mod qlflux.mod asymmetry.mod nanfilter.mod )
$(call LOCAL_mod_dep, src/nanfilter.f90, kind.mod )
$(call LOCAL_mod_dep, src/mod_fonct.f90, kind.mod datcal.mod datmat.mod dispfuncs.mod trapints.mod passints.mod mod_integration.mod callpassints.mod calltrapints.mod )
$(call LOCAL_mod_dep, src/flrterms.mod, FLRterms.mod )
$(call LOCAL_mod_dep, src/passints.f90, datcal.mod datmat.mod dispfuncs.mod kind.mod )
$(call LOCAL_mod_dep, src/trapints.f90, datcal.mod datmat.mod dispfuncs.mod kind.mod )
$(call LOCAL_mod_dep, src/dispfuncs.f90, kind.mod datcal.mod datmat.mod )
$(call LOCAL_mod_dep, src/FLRterms.f90, kind.mod datcal.mod datmat.mod mod_integration.mod )
$(call LOCAL_mod_dep, src/mod_fluidsol.f90, kind.mod datcal.mod datmat.mod mod_integration.mod )
$(call LOCAL_mod_dep, src/mod_contour.f90, kind.mod datcal.mod )
$(call LOCAL_mod_dep, src/asymmetry.f90, kind.mod datmat.mod datcal.mod mod_integration.mod )
$(call LOCAL_mod_dep, src/datcal.f90, kind.mod )
$(call LOCAL_mod_dep, src/datmat.f90, kind.mod )
$(call LOCAL_mod_dep, src/mod_make_io.f90, kind.mod datmat.mod datcal.mod )
$(call LOCAL_mod_dep, src/mod_saturation.f90, kind.mod datmat.mod datcal.mod )
$(call LOCAL_mod_dep, src/hcub.f90, kind.mod )

# Allow warnings in libraries
ifeq ($(TOOLCHAIN),gcc)
  QLK_EXTRA_FFLAGS += -Wno-error
endif
# This == 1 if the NAG compilation rules can be found in $(QLKROOT_)/lib/src/nag/rules.make. Include the rules how to make NAG if it is there.
ifeq ($(QLK_LOCAL_NAG),1)
	include $(QLKROOT_)/lib/src/nag/rules.make
endif
include $(QLKROOT_)/lib/src/fukushima/rules.make
include $(QLKROOT_)/lib/src/slatec/rules.make
include $(QLKROOT_)/lib/src/specfun/rules.make

$(BAKEDBUILDDIR)/qlk_version.f90.FORCE : | meta-tool-setup

$(BAKEDBUILDDIR)/qlk_version.f90.check : $(BAKEDBUILDDIR)/qlk_version.f90.FORCE
	$(V_)grep QLK_GITSHAKEY $(basename $@) 2>/dev/null | cut -f2 -d\'| [ x"$$(cat -)" == x"$(QLK_GITSHAKEY)" ] || (echo Repository state changed! && touch $@ && touch $(QLKROOT_)/src/qlk_standalone.f90)

$(BAKEDBUILDDIR)/qlk_version.f90: $(BAKEDBUILDDIR)/qlk_version.f90.check | meta-tool-setup
	$(call make_version_file, $@)

$(QLKROOT_)/src/diskio.f90: $(call UTIL_obj_of, $(BAKEDBUILDDIR)/qlk_version.f90)

$(call SUBPROJECT_add_artefact, $(BAKEDBUILDDIR)/qlk_version.f90.check)
$(call SUBPROJECT_add_artefact, $(BAKEDBUILDDIR)/qlk_version.f90)
$(call SUBPROJECT_add, $(BAKEDBUILDDIR)/qlk_version.f90)
$(call LOCAL_mod_dep, src/diskio.f90, qlk_version.mod )

define make_version_file
  @echo "module qlk_version" > $(1)
  @echo "  implicit none" >> $(1)
  @echo "  character (len=*), parameter :: QLK_CLOSEST_RELEASE="$(QLK_CLOSEST_RELEASE) >> $(1)
  @echo "  character (len=*), parameter :: QLK_GITBRANCHNAME="$(QLK_GITBRANCHNAME) >> $(1)
  @echo "  character (len=*), parameter :: QLK_GITSHAKEY="$(QLK_GITSHAKEY) >> $(1)
  @echo "  character (len=*), parameter :: QLK_COMPILER_VERSION="$(QLK_COMPILER_VERSION) >> $(1)
  @echo "  character (len=*), parameter :: QLK_REPOSITORY_ROOT=""'$(QLKROOT_)'" >> $(1)
  @echo "  character (len=*), parameter :: QLK_HOSTFULL=""'$(HOSTFULL)'" >> $(1)
  @echo end module qlk_version >> $(1)
endef


# Set dependencies to preprocessor file
#$(call LOCAL_src_dep, src/asymmetry.f90, preprocessor.inc)
#$(call LOCAL_src_dep, src/mod_fluidsol.f90, preprocessor.inc)
