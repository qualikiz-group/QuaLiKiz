# The following flags (-ffree-line-length-none, -fdefault-*-8) are from the
# "official" QuaLiKiz makefiles, specifically:
# https://github.com/QuaLiKiz-group/QuaLiKiz/blob/master/src/make.inc/Makefile.archlinux
# 
# Note: -warn nointerface is required, as ifort will otherwise emit a hard
# error. This *probably* should be looked at.
$(call SUBPROJECT_set_local_fflags, gcc, \
	-fdefault-real-8 \
	-fdefault-double-8 \
	-Wno-unused-dummy-argument \
	-Wno-unused-parameter \
	-Wno-maybe-uninitialized \
	-Wno-compare-reals \
	-Wno-conversion \
	-Wno-array-temporaries \
	-Wno-intrinsic-shadow \
	-Wno-target-lifetime \
	-ffree-line-length-256 \
	-cpp \
	$(QLK_EXTRA_FFLAGS) \
)
# -check shape gives compiler errors on JET CI
$(call SUBPROJECT_set_local_fflags, intel, \
	-warn nointerface \
	-real-size 64 \
	-diag-disable 6717 \
	-diag-disable 7712 \
	-fpp \
	-fp-model precise \
	-check noshape \
	$(QLK_EXTRA_FFLAGS) \
)
$(call SUBPROJECT_set_local_fflags, pgi, \
	-r8 \
	-Mpreprocess \
	$(QLK_EXTRA_FFLAGS) \
)
#
#EOF vim:syntax=make:foldmethod=marker:ts=4:noexpandtab:
