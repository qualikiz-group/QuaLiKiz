MODULE trapints
#include "preprocessor.inc"
  USE kind
  USE datcal
  USE datmat, ONLY: ceik, ceie, mshift, mwidth, nwg, omega2bar, omffk, pffk, Zi, Tix, coefi, Ati, Ani, Ate, Ane, alphax, &
       Joe2p, Joi2p, Tex, Nex, Nix, smag, Rhoeff, Ro, anue, aui, machi, ft, qx, gammaE, cthe, pFFk, epsilon
  USE datmat
  USE dispfuncs

  IMPLICIT NONE

CONTAINS
  COMPLEX(KIND=DBL) FUNCTION FFki(kk,caseflag,nion,rotation,v)
    !! The returned integrand depends on the calculation flag, used
    !! in case we only want certain coefficient output
    !! Integrand for trapped ions without rotatin
    !! caseflag = 1, full output
    !! caseflag = 2, return factor in front of At (kgt)
    !! caseflag = 3, return factor in front of An (kgn)
    !! caseflag = 4, return curvature term (kgc)
    !! caseflag = 5, return full output for energy integral (higher v exponent)
    !! caseflag = 6, return At coefficient for energy integral
    !! caseflag = 7, return An coefficient for energy integral
    !! caseflag = 8, return curvature term  for energy integral
    !!
    !! If rotation is True, one can also return
    !! caseflag = 9, return factor in front of Au (roto-diffusion)
    !! caseflag = 10, return factor in front of Au (roto-diffusion) for energy integral
    !! caseflag = 11, return full output for ang momentum integral (higher v exponent)
    REAL(KIND=DBL), INTENT(IN) :: kk
    INTEGER, INTENT(IN) :: caseflag, nion
    COMPLEX(KIND=DBL) :: Fik, zik, fk, FFki_base
    COMPLEX(KIND=DBL) :: zik2, Zgik
    COMPLEX(KIND=DBL) :: Aiz, Biz, Ciz
    REAL(KIND=DBL)    :: ya, k2
    REAL(KIND=DBL)    :: fki, Eg, Kg, floorvar
    INTEGER :: ifailloc
    COMPLEX(KIND=DBL) :: Fik_norot

    LOGICAL :: rotation, collisions
    COMPLEX(KIND=DBL) :: ompFFk
    REAL(KIND=DBL)    :: gau2mshift, E2g
    REAL(KIND=DBL)    :: nwe,vpar2

    REAL(KIND=DBL), INTENT(IN), OPTIONAL :: v
    COMPLEX(KIND=DBL) :: Aez, Bez, Bez1, Bez2, bbe
    REAL(KIND=DBL)    :: v2, v3, v4, v5
    REAL(KIND=DBL)    :: nwge
    REAL(KIND=DBL)    :: delta, Anuen, Anuent

    REAL(KIND=DBL)    :: Zs, Ts, coefs, Jos2p, Ats, Ans
   
    k2 = kk * kk
    ya = 1. - k2
    IF (nion > 0) THEN
      Kg = ceik(ya)
      Eg = ceie(ya)
      Zs = Zi(pFFk,nion)
      Ts = Tix(pFFk,nion)
      collisions = .FALSE.
      coefs = coefi(pFFk,nion)
      Jos2p = Joi2p(nion)
      Ats = Ati(pFFk,nion)
      Ans = Ani(pFFk,nion)
    ELSEIF (nion == 0) THEN
      Kg = ceik(ya)
      Eg = ceie(ya)
      Zs = Ze
      Ts = Tex(pFFk)
      IF (present(v)) THEN
        collisions = .TRUE.
      ELSE
        collisions = .FALSE.
      ENDIF
      coefs = Nex(pFFk)
      Jos2p = Joe2p
      Ats = Ate(pFFk)
      Ans = Ane(pFFk)
    ELSE
      ERRORSTOP(.TRUE., 'Incorrect ion index requested for trapint')
    ENDIF
    !Specialized form of incomplete 2nd elliptic integral. Used for bounce average of Vpar^2

    floorvar=kk
    IF (ABS(floorvar) < epsint)  floorvar = SIGN(epsint,floorvar) ! to avoid divisions near zero. epsint in datcal 
    E2g = 1. / floorvar * (Eg - Kg * (1. - k2)) 

    ! The term weighting the vertical drift of the trapped (fk) is calculated
    ! The formulation with elliptic integrals is used

    floorvar=Kg
    IF (ABS(floorvar) < epsint)  floorvar = SIGN(epsint,floorvar) ! to avoid divisions near zero. epsint in datcal
    
    fki = -1. + (smag(pFFk) * 4. + 4. / 3._DBL * alphax(pFFk)) * &
         (k2 - 1. + Eg / floorvar) + 2. * Eg / floorvar * &
         (1. - 4. / 3._DBL * k2 * alphax(pFFk))

    IF (ABS(fki) < minfki)  fki = SIGN(minfki,fki)
    fk = CMPLX(fki, 0)

    IF (rotation) THEN
      ! Alternative formulation of gau2mshift (equivalent)
      !gau2mshift = REAL(mshift) + AIMAG(mshift)*AIMAG(mwidth**2)/REAL(mwidth**2) !Alternative formulation
      gau2mshift = REAL(mshift) + 2. * AIMAG(mshift) * AIMAG(mwidth) * &
           REAL(mwidth) / (REAL(mwidth)**2 - AIMAG(mwidth)**2)
      ! account for gamma_E=-grad(E_r)/B*R/Cref
      ! nwE means here nwE'=-k_theta*grad(Er)/B=k_theta*gammaE(as def in code)*Cref/R normalized to nwd=-ktheta T(1keV)/(eBR)
      nwE = -gammaE(pFFk) / Rhoeff(pFFk)
      ompFFk = omFFk - nwE * gau2mshift ! gau2mshift is shift of |gau(x)|
      !Bounce average of vpar^2 / (vts^2 * v^2)
      vpar2 = 2. * epsilon(pFFk) * kk / floorvar * E2g
    ENDIF

    IF (rotation .AND. collisions) THEN
      zik2 = -ompFFk * (Zs / Ts) !makes the normalization of omega to nwge instead of nwg
    ELSEIF (rotation .AND. .NOT. collisions) THEN
      zik2 = -ompFFk / fk * (Zs / Ts)
    ELSEIF (.NOT. rotation .AND. collisions) THEN
      zik2 = -omFFk * (Zs / Ts) !makes the normalization of omega to nwge instead of nwg
    ELSEIF (.NOT. rotation .AND. .NOT. collisions) THEN
      zik2 = -omFFk / fk * (Zs / Ts)
    ENDIF

    ! The square root whose imaginary part is positive is chosen
    ! for compatibility with the Fried-Conte function
    zik  = SQRT(zik2)
    IF (AIMAG(zik) < 0) zik = -zik

    IF (collisions) THEN
       nwge = nwg * (-Tex(pFFk) / Ze) !

      floorvar=Kg * nwge * qx(pFFk) * Ro(pFFk)
      IF (ABS(floorvar) < epsint)  floorvar = SIGN(epsint,floorvar) ! to avoid divisions near zero. epsint in datcal
       
      bbe = CMPLX(cthe(pFFk) * omega2bar/floorvar, 0) !Only for no rotation?
      ! Calculation of the electron collision frequency
      ! odd function, integration not ok if Anuen =0, so put Anuen = 1.0e-14
      ! we normalize the collision frequency to -nw_ge
      Anuen = Anue(pFFk) * (-Ze) / (Tex(pFFk) * nwg)
      IF (rotation) THEN
        ! Krook operator for collisions. From M. Kotschenreuther, G. Rewoldt and W.M. Tang, Computer Physics Communications 88 (1995) 128-140
        delta = 12.0*(ABS(ompFFk) * nwg / (Anue(pFFk)))**1.5_DBL
      ELSE
        delta = 12.0*(ABS(omFFk) * nwg / (Anue(pFFk)))**1.5_DBL
      ENDIF

      Anuent = Anuen / ((2. * k2 - 2)**2 + epscoll ) * (0.111 * delta + 1.31) / (11.79 * delta + 1)

      IF ( ABS(Anuent) < epsD ) Anuent = epsD

      v2 = v  *v
      v3 = v2 * v
      v4 = v3 * v
      v5 = v4 * v

      SELECT CASE (caseflag)
      CASE (1, 5)
        Aez = (zik2 + 1.5 * Ats - Ans) * v3 - Ats * v5
      CASE (2, 6)
        Aez =  1.5 * v3 - v5
      CASE (3, 7)
        Aez = -v3
      CASE (4, 8)
        Aez = zik2 * v3
      CASE (9, 10)
        Aez = 0.
      CASE (11)
        !       Aez = 2.* omega2bar/Kg*v4*(-Aue(pFFk)+Mache(pFFk)*(zik2 + 2.5 * Ats - Ans-Ats*v2))
        Aez = 0. !Tiny quantity since Mache, Aue, and epsilon all small
      END SELECT
      Bez =  zik2 * v3 - v5 * fk + ci * Anuent
      Bez1 =  zik2 * v3  - bbe * v4 - v5 * fk + ci * Anuent  !Only for no rotation?
      Bez2 =  zik2 * v3  + bbe * v4 - v5 * fk + ci * Anuent  !Only for no rotation?
    ELSE
      nwge = nwg*Tex(pFFk)
      !The plasma dispersion function is calculated (analytical energy integration)
      ifailloc = 0
      Zgik = ci * sqrtpi * wofzweid(zik)

      !Now the function is calculated based on the dispersion function calculation
      Aiz = 1. + zik * Zgik
      Biz = 0.5 + zik2 * Aiz
      Ciz =  0.75 + zik2 * Biz
    ENDIF

    if (.NOT. collisions) THEN
      SELECT CASE (caseflag)
      CASE (1)
        Fik_norot = 2./fk * ((-zik2 * fk - 1.5 * Ats + Ans) * Aiz + Ats * Biz)
      CASE (2)
        Fik_norot = 2./fk * (-1.5 * Aiz + Biz)
      CASE (3)
        Fik_norot = 2./fk * Aiz
      CASE (4)
        Fik_norot = -2. * zik2 * Aiz
      CASE (5)
        Fik_norot = 2./fk * ((-zik2 * fk - 1.5 * Ats+Ans) * Biz + Ats*Ciz)
      CASE (6)
        Fik_norot = 2./fk * (-1.5 * Biz + Ciz)
      CASE (7)
        Fik_norot = 2./fk * Biz
      CASE (8)
        Fik_norot = -2.* zik2 * Biz
      END SELECT
    ENDIF
    IF (collisions) THEN
      Fik = 4. / sqrtpi * v2 * EXP(-v2) * Aez / Bez
    ELSEIF (.NOT. collisions .AND. (.NOT. rotation .OR. nion == 0)) THEN
      Fik = Fik_norot
    ELSEIF (.NOT. collisions .AND. rotation .AND. nion /= 0) THEN
      !Only terms up to Mach**2 are kept, where Vpar^2 and Aui are ordered like Mach.
      SELECT CASE (caseflag)
      CASE (1)
        Fik = 2./fk *( &
             (1 - Machi(pFFk,nion)**2) * fk / 2._DBL * Fik_norot + &
             (Machi(pFFk,nion)**2*Ats - 2.*Machi(pFFk,nion)*Aui(pFFk,nion))*Aiz + &
             4.*Machi(pFFk,nion)*Aui(pFFk,nion)*vpar2*Biz &
             )
      CASE (2)
        Fik = ((1. - Machi(pFFk,nion)**2) * Fik_norot + &
             2./fk * Machi(pFFk,nion)**2 * Aiz)
      CASE (3,4,7,8)
        Fik = Fik_norot * (1.-Machi(pFFk,nion)**2)
      CASE (5)
        Fik = 2./fk * (  &
             (1. - Machi(pFFk,nion)**2) * fk / 2._DBL * Fik_norot + &
             (Machi(pFFk,nion)**2 * Ats - 2. * Machi(pFFk,nion) * Aui(pFFk,nion)) * Biz + &
             4. * Machi(pFFk,nion) * Aui(pFFk,nion) * vpar2 * Ciz &
             )
      CASE (6)
        Fik = 2./fk *( (1.- Machi(pFFk,nion)**2)* fk/2._DBL * Fik_norot + Machi(pFFk,nion)**2*Biz)
      CASE (9)
        Fik = -4./fk * Machi(pFFk,nion)*Aiz + 8./fk*Machi(pFFk,nion)*vpar2*Biz
      CASE (10)
        Fik = -4./fk * Machi(pFFk,nion)*Biz + 8./fk*Machi(pFFk,nion)*vpar2*Ciz
      CASE (11) ! integral for ang momentum
        Fik = 4./fk * vpar2 * (Biz*(Aui(pFFk,nion)+Machi(pFFk,nion)*(Ans-2.5*Ats-zik2*fk) ) + &
             & Ciz*Machi(pFFk,nion)*Ats   )
      END SELECT
    ENDIF

    FFki_base = kk * Kg * ft(pFFk) * coefs * Fik  * Jos2p
    IF (caseflag < 5 .OR. (caseflag == 9) .OR. (caseflag == 11)) THEN
      FFki = FFki_base
    ELSEIF (collisions) THEN
      FFki = FFki_base * Ts * v2
    ELSE
      FFki = FFki_base * Ts
    ENDIF
    IF (ABS(FFki) < SQRT(epsD)) FFki=0.
  END FUNCTION FFki
END MODULE trapints
