!Calculate integrals
MODULE mod_fonct
#include "preprocessor.inc"
  USE kind
  USE datcal
  USE datmat
  USE dispfuncs
  USE trapints
  USE calltrapints
  USE passints
  USE callpassints
  USE mod_integration

  IMPLICIT NONE

CONTAINS

  SUBROUTINE calcfonctp( p, nu, omega, fonctp, rotation )
    !-----------------------------------------------------------
    ! Calculate the trapped particle integrals
    ! Includes collisions for the electron terms
    !-----------------------------------------------------------   
    INTEGER, INTENT(IN)  :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    LOGICAL, INTENT(IN) :: rotation
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctp

    REAL(KIND=DBL), DIMENSION(1) :: int_1d_lbound, int_1d_ubound
    REAL(KIND=DBL), DIMENSION(2) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(5) :: fdata
    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_1d, opts_2d
    REAL(KIND=DBL)     :: cc, dd
    REAL(KIND=DBL), DIMENSION(2) :: a,b, int_err_2d
    INTEGER :: ifailloc, num_dim
    REAL(KIND=DBL) :: rfonctpiz, ifonctpiz, adiabatic_scale

    REAL(KIND=DBL), DIMENSION(nf) :: intout

    !set the omega and p to be seen by all integration subroutines
    omFFk = omega
    pFFk = p
    nuFFk = nu

    !Set integration limits. (1) for kappa and (2) for v (for the electrons)
    a(1) = 0.0d0
    b(1) = 1.0d0 - barelyavoid
    a(2) = 0.0d0
    b(2) = vuplim

    cc = a(1)
    dd = b(1)

    ! The kappa integral is calculated. The real component with rFFkiz,
    ! and the imaginary component with iFFkiz, for all ions
    dim_out = 2
    dim_in = 1
    int_1d_lbound = a(1)
    int_1d_ubound = b(1)

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
      allocate(nag2d_options::opts_2d)
      adiabatic_scale = 1
    CASE (1)
      allocate(hcubature_options::opts_1d)
      allocate(hcubature_options::opts_2d)
      adiabatic_scale = abs(Ac(p))
    CASE DEFAULT
      !TODO: Adding this line borks efi*_GB somehow.. Memory corruption?
      !ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT

    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
    CLASS IS (hcubature_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
      opts_1d%minEval = 0
      opts_1d%norm = 2
      opts_1d%reqAbsError = absacc1
    END SELECT

    SELECT TYPE (opts_2d)
    CLASS IS (nag2d_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%lenwrk = lenwrk
    CLASS IS (hcubature_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%norm = 2
      opts_2d%reqAbsError = absacc2
    END SELECT

    fdata(1) = -1 ! caseflag
    fdata(2) = -1 ! Do not care
    fdata(3) = logic2real(rotation)
    fdata(4) = 1 ! Sum over ions
    fdata(5) = adiabatic_scale ! Adiabatic scale factor

    ! Total sum over ions
    CALL integration(dim_out, calc_QL_FFki, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnuomega('FFkiz', ifailloc, verbose, p, nu, omega)
    int_res = int_res * adiabatic_scale
    rfonctpiz = int_res(1)
    ifonctpiz = int_res(2)

    !Only calculate nonadiabatic part for electrons if el_type == 1
    fdata(4) = 0 ! Do not sum over ions anymore, these are electrons
    ifailloc = 1
    IF (el_type == 1) THEN 
      IF ( ABS(coll_flag) > epsD) THEN !Collisional simulation, do double integral
        fdata(2) = 0 ! Electrons
        num_dim = 2
        CALL integration(dim_out, calc_QL_FFke, num_dim, a, b, opts_2d, intout, int_err_2d, fdata, ifailloc)
        CALL debug_msg_pnuomega('FFke', ifailloc, verbose, p, nu, omega)
       ELSE !Collisionless simulation, revert to faster single integral
          fdata(2) = 0 ! Electrons
          CALL integration(dim_out, calc_QL_FFke, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_pnuomega('FFke_nocol', ifailloc, verbose, p, nu, omega)
       ENDIF
    ELSE
       intout(1)=0
       intout(2)=0
    ENDIF
    intout = intout * adiabatic_scale
    fonctp = intout(1) + rfonctpiz + ci * intout(2) + ci * ifonctpiz

  END SUBROUTINE calcfonctp

  SUBROUTINE calcfonctc( p, nu, omega, fonctc, rotation)
    !-----------------------------------------------------------
    ! Calculate the passing particle integrands
    !-----------------------------------------------------------   
    INTEGER, INTENT(IN)  :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    LOGICAL, INTENT(IN)  :: rotation
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctc

    REAL(KIND=DBL), DIMENSION(ndim) :: a, b
    REAL(KIND=DBL), DIMENSION(nf) :: intout

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_2d
    REAL(KIND=DBL), DIMENSION(2) :: int_err
    REAL(KIND=DBL), DIMENSION(4) :: fdata
    REAL(KIND=DBL) :: adiabatic_scale
    INTEGER :: ifailloc

    IF (rotation) THEN
      a(:) = -rkuplim ! with rotation from -inf to +inf
      b(:) =  rkuplim
    ELSE
      !Note the 0 lower limit, different from callpassQLints.
      !This leads to a factor 4 (even symmetry and 2*2) prefactor for the integrals here
      a(:) = 0
      b(:) = rkuplim
    ENDIF

    omFFk = omega
    pFFk = p
    nuFFk = nu

    dim_out = 2
    dim_in = 2

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag2d_options::opts_2d)
      adiabatic_scale = 1
    CASE (1)
      allocate(hcubature_options::opts_2d)
      adiabatic_scale = abs(Ac(p))
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcfonctc')
    END SELECT

    SELECT TYPE (opts_2d)
    CLASS IS (nag2d_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%lenwrk = lenwrk
    CLASS IS (hcubature_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%norm = 2
      opts_2d%reqAbsError = absacc2
    END SELECT

    fdata(1) = 0 ! caseflag
    fdata(2) = -1 ! ion: Do not care
    fdata(3) = logic2real(rotation)
    fdata(4) = adiabatic_scale

    !The k* and rho* double integral is calculated
    !The real and imaginary parts are calculated separately due to NAG function limitations
    ccount=0

    ifailloc = 1
    CALL integration(dim_out, calc_fonctc, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
    CALL debug_msg_integration('fonctc', ifailloc, verbose, p, nu, ion, omega)
    intout = intout * adiabatic_scale

    fonctc = intout(1) + ci * intout(2)

  END SUBROUTINE calcfonctc

END MODULE mod_fonct
