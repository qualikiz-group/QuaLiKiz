MODULE passints
#include "preprocessor.inc"
  USE kind
  USE datcal
  USE datmat
  USE dispfuncs

  IMPLICIT NONE

CONTAINS

!*************************************************************************************
! Fkstarrstari and Fkstarrstare without rotation
! Fkstarrstarirot and Fkstarrstarerot with rotation
!*************************************************************************************

  COMPLEX(KIND=DBL) FUNCTION Fkstarrstari(xx, caseflag, nion, rotation)
    !! Calculates the passing ion k*, r* integrands
    !! The returned integrand depends on the calculation flag, used
    !! in case we only want certain coefficient output
    !! caseflag = 1, full output
    !! caseflag = 2, return factor in front of At (kgt)
    !! caseflag = 3, return factor in front of An (kgn)
    !! caseflag = 4, return curvature term (kgc)
    !! caseflag = 5, return full output for energy integral (higher v exponent)
    !! caseflag = 6, return At coefficient for energy integral
    !! caseflag = 7, return An coefficient for energy integral
    !! caseflag = 8, return curvature term  for energy integral
    !! caseflag = 9, return factor in front of Au (roto-diffusion)
    !! caseflag = 10, return factor in front of Au (roto-diffusion) for energy integral
    !! caseflag = 11, return full output for ang momentum integral (higher v exponent)

    REAL(KIND=DBL), DIMENSION(2), INTENT(IN) :: xx
    INTEGER , INTENT(IN) :: caseflag, nion
    LOGICAL, INTENT(IN) :: rotation

    INTEGER :: ndim
    REAL(KIND=DBL)    :: Athir
    COMPLEX(KIND=DBL) :: bbi, cci, ddi, sqrtdi, Vmi, Vpi
    COMPLEX(KIND=DBL) :: alphai, ompFFk
    COMPLEX(KIND=DBL) :: faci
    REAL(KIND=DBL)    :: nwge
    REAL(KIND=DBL)    :: rstar, kstar, teta, fkstar
    REAL(KIND=DBL)    :: var2,var3,bessm2 !new terms for Bessel directly inside passints
    REAL(KIND=DBL)    :: nwE
    REAL(KIND=DBL)    :: xFkr
    COMPLEX(KIND=DBL) :: inti3, inti4, inti5, inti6, inti7
    COMPLEX(KIND=DBL) :: Fikstarrstar
    REAL(KIND=DBL) :: Zs, Ts, coefs, Ats, Ans, Rhos, ktetaRhos, Aths

    IF (nion /= 0) THEN
       Zs = Zi(pFFk,nion)
       Ts = Tix(pFFk,nion)
       coefs = coefi(pFFk,nion)
       Ats = Ati(pFFk,nion)
       Ans = Ani(pFFk,nion)
       Rhos = Rhoi(pFFk,nion)
       ktetaRhos = ktetaRhoi(nion)
       Aths = Athi(nion)
    ELSE
       Zs = Ze
       Ts = Tex(pFFk)
       coefs = Nex(pFFk)
       Ats = Ate(pFFk)
       Ans = Ane(pFFk)
       Rhos = Rhoe(pFFk)
       ktetaRhos = ktetaRhoe
       Aths = Athe
    ENDIF

    kstar = xx(1)
    rstar = xx(2)
    ndim = 2

    IF (rotation) THEN
       teta = (kstar * d / SQRT(2._DBL) / SQRT(REAL(mwidth**2)) + &
            AIMAG(mshift) * d / REAL(mwidth**2))
    ELSE
       teta = kstar * d / REAL(mwidth) / SQRT(2._DBL) !warning not sure if should be real(mwidth) or rather keep teta complex?
    ENDIF

    ! in order to have 1/2pi exp( -(kstar^2+rstar^2)/2) Jonathan Citrin changed the normalizations compare to Pierre Cottier QLK_mom's version. Keep JC norm.
    !Vertical drift term
    fkstar = 4. / 3._DBL * (COS(teta) + (smag(pFFk) * teta - alphax(pFFk) * SIN(teta)) * SIN(teta))
    IF (ABS(fkstar) < minfki) fkstar = SIGN(minfki, fkstar)

    nwge = nwg * (-Ts / Zs)

    var2 = (teta / d * Rhos)**2  !!1st argument of Bessel fun
    var3 = (ktetaRhos)**2               !!2nd argument of Bessel fun
    bessm2 = BESEI0(var2 + var3)

    IF (rotation) THEN
       xFkr = rstar/SQRT(2._DBL) + REAL(mshift)/SQRT(REAL(mwidth**2))
       !         & AIMAG(mwidth)/REAL(mwidth)*(kstar/SQRT(2.)+AIMAG(mshift)/SQRT(REAL(mwidth**2)))
       nwE = -gammaE(pFFk)/Rhoeff(pFFk)*SQRT(REAL(mwidth**2))
       ompFFk = omFFk-nwE*xFkr
    ELSE
       xFKr = rstar / SQRT(2._DBL)
    ENDIF

    !Transit frequency
    Athir = Aths * xFKr
    IF (ABS(Athir) < epsint) Athir = SIGN(epsint, Athir) !set minimum of transit frequency
    
    ! Analytical form of velocity integration written with 
    ! Fried-Conte functions, generalizations of the simple plasma dispersion function
    bbi = CMPLX(Athir/(nwge*fkstar),0.) ! in Pierre's case QLK_mom it was "-"Athir/(nwgi*fkstar) only in Fkstarrstar not in Fek... nor Fvk... nor Fkstarrstargni, etc
    IF (rotation) THEN
       cci = ompFFk * (Zs / Ts) / fkstar
    ELSE
       cci = omFFk * (Zs / Ts) / fkstar
    ENDIF

    ddi = bbi**2 - 4. * cci
    sqrtdi = SQRT(ddi)

    Vmi = (-bbi - sqrtdi) / 2.
    Vpi = (-bbi + sqrtdi) / 2.

    IF (rotation) THEN
       faci = 2. / (fkstar * (Vpi - Vmi) + epsint)
    ELSE
       faci = 2. / (fkstar * (Vpi - Vmi) + epsint)
    ENDIF

    IF ((caseflag < 5) .OR. (caseflag == 9)) THEN !differentiate between particle, energy, momentum integrals, here particles
       inti3 = faci * (Z1(Vpi) - Z1(Vmi))
       inti5 = faci * (Z2(Vpi) - Z2(Vmi))
       IF (rotation) THEN
          inti4 = faci * (Vpi * Z1(Vpi) - Vmi * Z1(Vmi))
          inti6 = faci * (Vpi * Z2(Vpi) - Vmi * Z2(Vmi))
          inti7 = faci * (Z3(Vpi) - Z3(Vmi))
       ENDIF
    ELSEIF (caseflag == 11) THEN ! for ang momentum
       inti3 = faci * (Vpi * Z1(Vpi) - Vmi * Z1(Vmi))
       inti4 = faci * (Z2(Vpi) - Z2(Vmi))
       inti5 = faci * (Vpi * Z2(Vpi) - Vmi * Z2(Vmi))
       inti6 = faci * (Z3(Vpi) - Z3(Vmi))
       inti7 = faci * (Vpi * Z3(Vpi) - Vmi * Z3(Vmi))
    ELSE ! for energy
       inti3 = faci * (Z2(Vpi) - Z2(Vmi))
       inti5 = faci * (Z3(Vpi) - Z3(Vmi))
       IF (rotation) THEN
          inti4 = faci * (Vpi * Z2(Vpi) - Vmi * Z2(Vmi))
          inti6 = faci * (Vpi * Z3(Vpi) - Vmi * Z3(Vmi))
          inti7 = faci * (Z4(Vpi) - Z4(Vmi))
       ENDIF
    ENDIF


    ! VERY DIRTY FIX! Otherwise QL integrals do not converge for some unexplained reason (maybe a NAG bug)
    IF (nion /= 0) THEN
       IF ( rotation .AND. Machi(pFFk,nion) < 1d-4) Machi(pFFk,nion) = 1d-4
    ENDIF

    rot: IF (rotation) THEN
       ion: IF (nion /= 0) THEN
          IF (caseflag == 1) THEN !full term for particle  
             alphai = ompFFk*(Zi(pFFk,nion)/Tix(pFFk,nion))+Ani(pFFk,nion)-1.5*Ati(pFFk,nion)
             Fikstarrstar = inti3 * ( alphai - 2.*Machi(pFFk,nion)*Aui(pFFk,nion)-Machi(pFFk,nion)**2*(alphai-Ati(pFFk,nion))) + &
                  & inti4*2.*alam1 * &
                  & (Aui(pFFk,nion)+Machi(pFFk,nion)*(alphai - Ati(pFFk,nion))-3.*Machi(pFFk,nion)**2*Aui(pFFk,nion) ) + &
                  & inti5*(Ati(pFFk,nion) + 4.*alam2*Machi(pFFk,nion)*Aui(pFFk,nion)+2.*Machi(pFFk,nion)**2*alam2 * &
                  & (alphai - (3.5+0.5/alam2)*Ati(pFFk,nion)))+ &
                  & inti6*(2.*alam1*Machi(pFFk,nion)*Ati(pFFk,nion)+4.*Machi(pFFk,nion)**2*alam3*Aui(pFFk,nion)) + &
                  & inti7*alam2*2.*Machi(pFFk,nion)**2*Ati(pFFk,nion)

          ELSEIF (caseflag == 2) THEN !At factor only particle transport
             alphai = -1.5
             Fikstarrstar = inti3 * ( alphai - Machi(pFFk,nion)**2*(alphai-1.)) + &
                  & inti4*2.*alam1*(Machi(pFFk,nion)*(alphai - 1.)) + &
                  & inti5*(1.+2.*Machi(pFFk,nion)**2*alam2*(alphai - (3.5+0.5/alam2)))+ &
                  & inti6*(2.*alam1*Machi(pFFk,nion)) + &
                  & inti7*alam2*2.*Machi(pFFk,nion)**2
          ELSEIF (caseflag == 3) THEN !An factor only particle transport
             alphai = 1.
             Fikstarrstar = inti3 * ( alphai -Machi(pFFk,nion)**2*alphai) + &
                  & inti4*2.*alam1*Machi(pFFk,nion)*alphai + &
                  & inti5*2.*Machi(pFFk,nion)**2*alam2*alphai
          ELSEIF (caseflag == 4) THEN !Curvature term particle transport
             alphai = ompFFk*(Zi(pFFk,nion)/Tix(pFFk,nion))
             Fikstarrstar = inti3 * ( alphai -Machi(pFFk,nion)**2*alphai) + &
                  & inti4*2.*alam1*Machi(pFFk,nion)*alphai + &
                  & inti5*2.*Machi(pFFk,nion)**2*alam2*alphai
          ELSEIF (caseflag == 9) THEN !Au term particle transport

             Fikstarrstar = -inti3 * 2.*Machi(pFFk,nion) + &
                  & inti4*2.*alam1*(1.-3.*Machi(pFFk,nion)**2 ) + &
                  & inti5*4.*alam2*Machi(pFFk,nion) + &
                  & inti6*4.*Machi(pFFk,nion)**2*alam3  

          ELSEIF (caseflag == 5) THEN !full term for energy 
             alphai = ompFFk*(Zi(pFFk,nion)/Tix(pFFk,nion))+Ani(pFFk,nion)-1.5*Ati(pFFk,nion)

             Fikstarrstar = inti3 * ( alphai - 2.*Machi(pFFk,nion)*Aui(pFFk,nion)-Machi(pFFk,nion)**2*(alphai-Ati(pFFk,nion))) + &
                  & inti4*2.*alam1 * &
                  & (Aui(pFFk,nion)+Machi(pFFk,nion)*(alphai - Ati(pFFk,nion))-3.*Machi(pFFk,nion)**2*Aui(pFFk,nion)) + &
                  & inti5*(Ati(pFFk,nion) + 4.*alam2*Machi(pFFk,nion)*Aui(pFFk,nion)+2.*Machi(pFFk,nion)**2*alam2 * &
                  & (alphai - (3.5+0.5/alam2)*Ati(pFFk,nion)))+ &
                  & inti6*(2.*alam1*Machi(pFFk,nion)*Ati(pFFk,nion)+4.*Machi(pFFk,nion)**2*alam3*Aui(pFFk,nion)) + &
                  & inti7*alam2*2.*Machi(pFFk,nion)**2*Ati(pFFk,nion)

          ELSEIF (caseflag == 6) THEN !At factor only energy transport
             alphai = -1.5
             Fikstarrstar = inti3 * ( alphai - Machi(pFFk,nion)**2*(alphai-1.)) + &
                  & inti4*2.*alam1*(Machi(pFFk,nion)*(alphai - 1.)) + &
                  & inti5*(1.+2.*Machi(pFFk,nion)**2*alam2*(alphai - (3.5+0.5/alam2)))+ &
                  & inti6*(2.*alam1*Machi(pFFk,nion)) + &
                  & inti7*alam2*2.*Machi(pFFk,nion)**2
          ELSEIF (caseflag == 7) THEN !An factor only energy transport
             alphai = 1.
             Fikstarrstar = inti3 * ( alphai -Machi(pFFk,nion)**2*alphai) + &
                  & inti4*2.*alam1*Machi(pFFk,nion)*alphai + &
                  & inti5*2.*Machi(pFFk,nion)**2*alam2*alphai    
          ELSEIF (caseflag == 8) THEN !Curvature term energy transport
             alphai = ompFFk*(Zi(pFFk,nion)/Tix(pFFk,nion))
             Fikstarrstar = inti3 * ( alphai -Machi(pFFk,nion)**2*alphai) + &
                  & inti4*2.*alam1*Machi(pFFk,nion)*alphai + &
                  & inti5*2.*Machi(pFFk,nion)**2*alam2*alphai
          ELSEIF (caseflag == 10) THEN !Au term energy transport
             Fikstarrstar = -inti3 * 2.*Machi(pFFk,nion) + &
                  & inti4*2.*alam1*(1.-3.*Machi(pFFk,nion)**2 ) + &
                  & inti5*4.*alam2*Machi(pFFk,nion) + &
                  & inti6*4.*Machi(pFFk,nion)**2*alam3

          ELSEIF (caseflag == 11) THEN !full term for ang momentum

             alphai = ompFFk*(Zi(pFFk,nion)/Tix(pFFk,nion))+Ani(pFFk,nion)-1.5*Ati(pFFk,nion)

             Fikstarrstar = inti3 * alam1 * &
                  & ( alphai - 2.*Machi(pFFk,nion)*Aui(pFFk,nion)-Machi(pFFk,nion)**2*(alphai-Ati(pFFk,nion))) + &
                  & inti4*2.*alam2 * &
                  & (Aui(pFFk,nion)+Machi(pFFk,nion)*(alphai - Ati(pFFk,nion))-3.*Machi(pFFk,nion)**2*Aui(pFFk,nion)) + &
                  & inti5*(alam1*Ati(pFFk,nion) + 4.*alam3*Machi(pFFk,nion)*Aui(pFFk,nion)+2.*Machi(pFFk,nion)**2*alam3 * &
                  & (alphai - (3.5+alam1*0.5/alam3)*Ati(pFFk,nion)))+ &
                  & inti6*(2.*alam2*Machi(pFFk,nion)*Ati(pFFk,nion)+4.*Machi(pFFk,nion)**2*alam4*Aui(pFFk,nion)) + &
                  & inti7*alam3*2.*Machi(pFFk,nion)**2*Ati(pFFk,nion)
          ENDIF
       ELSE ion
          IF (caseflag == 1) THEN !full term for particle
             alphai = ompFFk*Zs/Ts+Ans-1.5*Ats
             Fikstarrstar = inti3 * alphai + inti5*Ats
          ELSEIF (caseflag == 2) THEN !At factor only particle transport
             alphai = -1.5
             Fikstarrstar = inti3 * alphai + inti5
          ELSEIF (caseflag == 3) THEN !An factor only particle transport
             alphai = 1.
             Fikstarrstar = inti3 * alphai
          ELSEIF (caseflag == 4) THEN !Curvature term particle transport
             alphai = ompFFk*Zs/Ts
             Fikstarrstar = inti3 * alphai
          ELSEIF (caseflag == 9) THEN !Au term particle transport
             Fikstarrstar = 0.
          ELSEIF (caseflag == 5) THEN !full term for energy
             alphai = ompFFk*Zs/Ts+Ans-1.5*Ats
             Fikstarrstar = inti3 * alphai + inti5*Ats
          ELSEIF (caseflag == 6) THEN !At factor only energy transport
             alphai = -1.5
             Fikstarrstar = inti3 * alphai + inti5
          ELSEIF (caseflag == 7) THEN !An factor only energy transport
             alphai = 1.
             Fikstarrstar = inti3 * alphai
          ELSEIF (caseflag == 8) THEN !Curvature term energy transport
             alphai = ompFFk*Zs/Ts
             Fikstarrstar = inti3 * alphai
          ELSEIF (caseflag == 10) THEN !Au term energy transport
             Fikstarrstar = 0
          ELSEIF (caseflag == 11) THEN !full term for ang momentum
             alphai = ompFFk*Zs/Ts+Ans-1.5*Ats
             Fikstarrstar = alam1 * inti3 * alphai + inti5*Ats
          ENDIF
       ENDIF ion
    ELSE rot
       IF ( (caseflag == 1) .OR. (caseflag == 5) ) THEN !full term for particle or energy
          alphai = omFFk * (Zs / Ts) + Ans - 1.5 * Ats
          Fikstarrstar = inti3 * alphai + inti5 * Ats
       ELSEIF ( (caseflag == 2) .OR. (caseflag == 6) ) THEN !At factor only particle transport
          alphai = -1.5
          Fikstarrstar = inti3 * alphai + inti5
       ELSEIF ( (caseflag == 3) .OR. (caseflag == 7) ) THEN !An factor only particle transport
          alphai = 1.
          Fikstarrstar = inti3 * alphai
       ELSEIF ( (caseflag == 4) .OR. (caseflag == 8) ) THEN !Curvature term particle transport
          alphai = omFFk * (Zs / Ts)
          Fikstarrstar = inti3 * alphai
       ENDIF
    ENDIF rot

    IF ( (caseflag < 5).OR. (caseflag == 9) .OR. (caseflag == 11)) THEN !differentiate between particle, energy, ang. mom. integrals, here particle or momentum
!!$       Fkstarrstarirot = fc(pFFk)/twopi * EXP(-(kstar**2+rstar**2)/2) * coefi(pFFk,nion) * Fikstarrstar * Joi2c(nion) 
       Fkstarrstari = fc(pFFk)/twopi * EXP(-(kstar**2+rstar**2)/2) * coefs * Fikstarrstar * bessm2 
       ! in order to have 1/2pi exp( -(kstar^2+rstar^2)/2) Jonathan Citrin changed the normalizations compare to Pierre Cottier QLK_mom's version. Keep JC norm.
    ELSE !energy
!!$       Fkstarrstarirot = fc(pFFk)/twopi * EXP(-(kstar**2+rstar**2)/2) * coefi(pFFk,nion) * Tix(pFFk,nion) * Fikstarrstar *Joi2c(nion) 
       Fkstarrstari = fc(pFFk) * EXP(-(kstar**2+rstar**2)/2) * coefs * Ts * Fikstarrstar * bessm2 /twopi
       ! in order to have 1/2pi exp( -(kstar^2+rstar^2)/2) Jonathan Citrin changed the normalizations compare to Pierre Cottier QLK_mom's version. Keep JC norm.
    ENDIF

    IF (ABS(Fkstarrstari) < SQRT(epsD)) Fkstarrstari=0

  END FUNCTION Fkstarrstari
END MODULE passints
