! --------------------------------------------------------------
! PURPOSE: READ ALL INPUT PARAMETERS, CREATE DERIVED QUANTITIES
! --------------------------------------------------------------

MODULE mod_make_io
#include "preprocessor.inc"
  USE kind
  USE datcal
  USE datmat
  USE mpi

  IMPLICIT NONE

CONTAINS

  SUBROUTINE make_input(sizes, in_regular)

    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular

    INTEGER:: p,nu !counter for loop over coordinates. p is radius (or general scan), nu is wavenumber
    INTEGER:: i !counter for loops
    REAL(kind=DBL), DIMENSION(:), ALLOCATABLE :: Lambe, Nue !physical quantities used for the derivations, then discarded
    CHARACTER(len=128) :: error_msg ! Buffer for displaying error messages

    ! POPULATE GLOBAL VARIABLES
    dimx      = sizes%dimx
    dimn      = sizes%dimn
    nions     = sizes%nions
    numsols   = sizes%numsols

    phys_meth = in_regular%phys_meth
    coll_flag = in_regular%coll_flag
    rot_flag  = in_regular%rot_flag
    IF (in_regular%verbose > 0) verbose = .TRUE.
    IF (in_regular%verbose == 0) verbose = .FALSE.
    IF (in_regular%separateflux == 1) separateflux = .TRUE.
    IF (in_regular%separateflux == 0) separateflux = .FALSE.

    el_type = in_regular%el_type

    maxruns              = in_regular%maxruns
    maxpts               = in_regular%maxpts
    relacc1              = in_regular%relacc1
    relacc2              = in_regular%relacc2
    timeout              = in_regular%timeout
    relaccQL1            = in_regular%relacc1
    relaccQL2            = in_regular%relacc2
    ETGmult              = in_regular%ETGmult
    collmult             = in_regular%collmult
    integration_routine  = in_regular%integration_routine
    absacc2              = in_regular%absacc2
    absacc1              = in_regular%absacc1
    rhomin               = in_regular%rhomin
    rhomax               = in_regular%rhomax

    !lenwrk=(ndim+2+1)*(1+maxptsin/(2**ndim+2*ndim*ndim+2*ndim+1)) !Set array size for 2D integration routine
    lenwrk = 1e5

    !Input array allocation
    ALLOCATE(kthetarhos(dimn)); kthetarhos   = in_regular%kthetarhos
    ALLOCATE(x(dimx)); x                     = in_regular%x
    ALLOCATE(rho(dimx)); rho                 = in_regular%rho
    ALLOCATE(Ro(dimx)); Ro                   = in_regular%Ro
    ALLOCATE(Rmin(dimx)); Rmin               = in_regular%Rmin
    ALLOCATE(Bo(dimx)); Bo                   = in_regular%Bo
    ALLOCATE(qx(dimx)); qx                   = in_regular%q
    ALLOCATE(smag(dimx)); smag               = in_regular%smag
    ALLOCATE(alphax(dimx)); alphax           = in_regular%alpha
    ALLOCATE(Machtor(dimx)); Machtor         = in_regular%Machtor ! Mach number in toroidal direction Mach                                     = v_tor / C_ref, C_ref thermal velocity of D at 1keV sqrt(1keV/m_p)
    ALLOCATE(Autor(dimx)); Autor             = in_regular%Autor ! Normalized toroidal velocity gradient: - R\nabla U_\tor / C_ref
    ALLOCATE(Machpar(dimx)); Machpar         = in_regular%Machpar ! Mach number in parallel direction Mach                                     = v_par / C_ref, C_ref thermal velocity of D at 1keV sqrt(1keV/m_p)
    ALLOCATE(Aupar(dimx)); Aupar             = in_regular%Aupar ! Normalized parallel velocity gradient: - R\nabla U_\parallel / C_ref
    ALLOCATE(gammaE(dimx)); gammaE           = in_regular%gammaE ! normalized gammaE: - R \nablaE_r/B /C_ref (B here is B total not B_\varphi)
    ALLOCATE(Tex(dimx)); Tex                 = in_regular%Te
    ALLOCATE(Nex(dimx)); Nex                 = in_regular%Ne
    ALLOCATE(Ate(dimx)); Ate                 = in_regular%Ate
    ALLOCATE(Ane(dimx)); Ane                 = in_regular%Ane
    ALLOCATE(anise(dimx)); anise             = in_regular%anise
    ALLOCATE(danisedr(dimx)); danisedr       = in_regular%danisedr
    ALLOCATE(Ai(dimx,nions)); Ai             = in_regular%Ai
    ALLOCATE(Zi(dimx,nions)); Zi             = in_regular%Zi
    ALLOCATE(Tix(dimx,nions)); Tix           = in_regular%Ti
    ALLOCATE(ninorm(dimx,nions)); ninorm     = in_regular%normni
    ALLOCATE(Ati(dimx,nions)); Ati           = in_regular%Ati
    ALLOCATE(Ani(dimx,nions)); Ani           = in_regular%Ani
    ALLOCATE(ion_type(dimx,nions)); ion_type = in_regular%ion_type
    ALLOCATE(anis(dimx,1:nions)); anis       = in_regular%anis
    ALLOCATE(danisdr(dimx,1:nions)); danisdr = in_regular%danisdr

    ! Set radial dependent rotation flag
    ALLOCATE(rotflagarray(dimx)); 

   
    IF (rot_flag == 0) THEN
       rotflagarray(:)=0
    ELSE
       rotflagarray(:)=1
    ENDIF

    IF (rot_flag == 2) THEN !Do not use rotation version (much slower) for rho < 0.4
       WHERE (rho < 0.4) rotflagarray = 0
       ALLOCATE(Machparorig(dimx)); Machparorig = Machpar
       ALLOCATE(Auparorig(dimx)); Auparorig = Aupar
       ALLOCATE(gammaEorig(dimx)); gammaEorig = gammaE
       ALLOCATE(Machiorig(dimx,nions))
       ALLOCATE(Auiorig(dimx,nions))

       ALLOCATE(Machparmod(dimx)); Machparmod=1d-14
       ALLOCATE(Auparmod(dimx)) ; Auparmod=1d-14
       ALLOCATE(gammaEmod(dimx)) ; gammaEmod=1d-14
       ALLOCATE(Machimod(dimx,nions));  Machimod=1d-14
       ALLOCATE(Auimod(dimx,nions)); Auimod=1d-14
       ALLOCATE(filterprof(dimx))

       DO i=1,dimx ! define filterprof
          IF (rho(i) < 0.4) THEN 
             filterprof(i)=1d-14
          ELSEIF (rho(i) > 0.6) THEN
             filterprof(i)=1.
          ELSE
             filterprof(i) = (rho(i)-0.4)/(0.6 - 0.4)
          ENDIF
       ENDDO
      
       WHERE(filterprof<1d-14) filterprof=1d-14
       gammaEmod=gammaE*filterprof
       Auparmod=Aupar*filterprof
       Machparmod=Machpar*filterprof

    ENDIF

    ! Check input sanity
    ERRORSTOP(dimn > 99, 'dimn > 99, reduce kthetarhos grid')
    WHERE(x < 0.05) x = 0.05 !filter points near magnetic axis
    ERRORSTOP(ANY(Bo < 0), 'Negative magnetic field not allowed in input! Abandon ship...')
    ERRORSTOP(ANY(qx < 0), 'Negative q-profile not allowed in input! Abandon ship...')
    WHERE(ABS(smag) < 0.1) smag = SIGN(0.1,smag) !filter very low magnetic shear which is not valid under QLK assumptions (locality and strong ballooning)
    WHERE(smag < -0.3) smag = -0.3               !filter very negative magnetic shear which is not presently valid under QLK assumptions (we have no slab modes)
    WHERE( (smag - alphax) < 0.2 ) alphax = -0.2 + smag !filter high alpha where s-alpha goes into invalid regime under QLK assumptions (we have no slab modes)
    WHERE( alphax < 0.) alphax = 0. ! For negative magnetic shear: make sure pressure gradients are not positive due to above constraint

    DO p=1,dimx
       IF (rho(p) > rhomax) CYCLE ! CYCLE loop if beyond rhomax
       IF (Tex(p) < 1e-3) THEN
          WRITE(error_msg,*) 'Invalid input! Te below 1eV at p=',p
          ERRORSTOP(.TRUE., error_msg)
       ENDIF
       IF (Nex(p) < 0) THEN
          WRITE(error_msg,*) 'Invalid input! Negative density at p=',p
          ERRORSTOP(.TRUE., error_msg)
       ENDIF

       DO i=1,nions
          IF (Tix(p,i) < 1e-3) THEN
             WRITE(error_msg,*) 'Invalid input! Ti below 1eV for nion=',i,' at p=',p
             ERRORSTOP(.TRUE., error_msg)
          ENDIF
          IF (ninorm(p,i) < 0) THEN
             WRITE(error_msg,*) 'Invalid input! normalized density below 0 for nion=',i,'at p=',p
             ERRORSTOP(.TRUE., error_msg)
          ENDIF
          IF (ninorm(p,i) > 1) THEN
             WRITE(error_msg,*) 'Invalid input! normalized density above 1 for nion=',i,'at p=',p
             ERRORSTOP(.TRUE., error_msg)
          ENDIF
       ENDDO
    ENDDO

    ! Define all quantities derived in the code from the input arrays

    ! These next three are temporary and are deallocated following the calculations
    ALLOCATE(Lambe(dimx))
    ALLOCATE(Nue(dimx))
    ALLOCATE(epsilon(dimx))

    ! Array allocation
    ALLOCATE(tau(dimx,nions))
    ALLOCATE(Nix(dimx,nions))
    ALLOCATE(Zeffx(dimx))
    ALLOCATE(Nustar(dimx))
    ALLOCATE(qprim(dimx))
    ALLOCATE(Anue(dimx))
    ALLOCATE(wg(dimx))
    ALLOCATE(Ac(dimx))
    ALLOCATE(csou(dimx))
    ALLOCATE(cref(dimx)) ! ref velocity used for input of gamma_E, U_par and \nabla U_par : sqrt(1keV/m_p) i.e. thermal velocity of D at 1keV
    ALLOCATE(cthe(dimx))
    ALLOCATE(cthi(dimx,nions))
    ALLOCATE(omegator(dimx))
    ALLOCATE(domegatordr(dimx))
    ALLOCATE(Rhoe(dimx))
    ALLOCATE(Rhoi(dimx,nions))
    ALLOCATE(de(dimx))
    ALLOCATE(di(dimx,nions))
    ALLOCATE(ktetasn(dimx)) 
    ALLOCATE(rhostar(dimx))
    ALLOCATE(Rhoeff(dimx))
    ALLOCATE(ft(dimx))
    ALLOCATE(fc(dimx))
    ALLOCATE(Machi(dimx,nions))
    ALLOCATE(Machitemp(dimx,nions))
    ALLOCATE(Mache(dimx))
    ALLOCATE(Aui(dimx,nions))
    ALLOCATE(Aue(dimx))
    ALLOCATE(th(ntheta))
    ALLOCATE(phi(dimx,ntheta))
    ALLOCATE(dphidr(dimx,ntheta))
    ALLOCATE(dphidth(dimx,ntheta))
    ALLOCATE(npol(dimx,ntheta,nions))
    ALLOCATE(ecoefs(dimx,0:nions,numecoefs)); ecoefs(:,:,:)=0. !includes electrons
    ALLOCATE(ecoefsgau(dimx,dimn,0:nions,0:9)); ecoefsgau(:,:,:,:)=0. !includes electrons
    ALLOCATE(cftrans(dimx,nions,numicoefs)); cftrans(:,:,:)=0. !includes 7 transport coefficients, only for ions
    ALLOCATE(nepol(dimx,ntheta))
    ALLOCATE(Anipol(dimx,ntheta,nions))
    ALLOCATE(Anepol(dimx,ntheta))
    ALLOCATE(tpernorme(dimx,ntheta))
    ALLOCATE(tpernormi(dimx,ntheta,nions))
    ALLOCATE(tpernormifunc(nions))

    ALLOCATE(coefi(dimx,nions))
    ALLOCATE(ntor (dimx, dimn) )   
    ALLOCATE(mi(dimx,nions))
    ALLOCATE(ETG_flag(dimn))
    ALLOCATE(edgemode(dimx, dimn))

    !These next ones are calculate at each p inside calcroutines
    ALLOCATE(Athi(nions))
    ALLOCATE(ktetaRhoi(nions))
    ALLOCATE(Joi2(nions))
    ALLOCATE(Jobani2(nions))
    ALLOCATE(Joi2p(nions))

    mi(:,:) = Ai(:,:)*mp

    !STYLE NOTE: The (:) is not necessary but useful as a reminder for which 
    !variables are scalars and which are arrays

    ! Some auxiliary definitions
    epsilon(:) = Rmin(:)*x(:)/Ro(:)
    ft(:) = 2.*(2.*epsilon(:))**0.5/pi; !trapped particle fraction
    fc(:) = 1. - ft(:) !passing particle fraction
    qprim(:)=smag(:)*qx(:)/(Rmin(:)*x(:))

    th=(/((i * pi/(ntheta-1)),i=0,ntheta-1)/) !set poloidal angle from [0,pi]

    ! Thermal velocities
    csou(:) = SQRT(qe*Tex(:)*1.d3/mi(:,1)) !Calculated with respect to main ions (assumed index 1)
    cref(:) = SQRT(qe*1.d3/mp) !Cref=sqrt(2x1keV/mD)=sqrt(1keV/mp) used to normalized gammaEin, Machin, Auparin
    cthe(:) = SQRT(2._DBL*qe*Tex(:)*1.d3/me)

    Zeffx(:)=0. !Initialize Zeff
    Ac(:) = Nex(:) !Adiabatic term, electrons
    DO i = 1,nions
       tau(:,i) = Tex(:)/Tix(:,i) !Temperature ratios

       WHERE (ion_type(:,i) == 4) 
          Zeffx(:) = Zeffx(:)+ninorm(:,i)*Zi(:,i)**2 
          ion_type(:,i) = 3 !Set ion type to pure tracer for rest of code. 
       ENDWHERE

       !Set tracer density. Arbitrary 1e-5, in case ninorm was 0 in input. Does not affect physics since in the end we divide fluxes by nz
       WHERE (ion_type(:,i) == 3) ninorm(:,i)=1d-5 

    ENDDO

    IF (nions > 1) THEN
       DO i = 1,dimx !impose quasineutrality due to potential ion_type=3
          IF (nions == 2) THEN
             ninorm(i,1) = (1. - Zi(i,2)*ninorm(i,2)) /Zi(i,1)
          ELSE
             ninorm(i,1) = (1. - SUM(Zi(i,2:nions)*ninorm(i,2:nions))) /Zi(i,1)
          ENDIF
       ENDDO
    ENDIF
   
    DO i = 1,nions
       Nix(:,i) = (ninorm(:,i)+epsD)*Nex(:) !Ion densities
       coefi(:,i) = Zi(:,i)*Zi(:,i) * Nix(:,i) * tau(:,i)/(ninorm(:,i)+epsD) !Ion coefficients throughout equations. Normalised by ninorm to avoid small numbers. Later unnormalised
       cthi(:,i) = SQRT(2._DBL*qe*Tix(:,i)*1.d3/mi(:,i)) !Thermal velocities
       Rhoi(:,i) = SQRT(2._DBL*qe*Tix(:,i)*1.d3*mi(:,i))/(qe*Zi(:,i)*Bo(:)) !Larmor radii
       di(:,i) = qx(:)/SQRT(2._DBL*(epsilon(:)+eps))*Rhoi(:,i) !Ion banana width

       Machi(:,i) = Machpar(:)*cref(:)/cthi(:,i) !Mach number in parallel direction
      
       !Reduce effective Mach number for impurities to a level that doesn't break Mach ordering. 
       WHERE(Ai(:,i)>mass_cutoff) Machi(:,i) = Machi(:,i) * SQRT(mass_cutoff/Ai(:,i))
       Machitemp = Machi ! Original, unaltered due to filterprof Mach number      
       IF (rot_flag == 2) THEN
          Machiorig(:,i) = Machparorig(:)*cref(:)/cthi(:,i) !Mach number in parallel direction
          Machimod(:,i) = Machparmod(:)*cref(:)/cthi(:,i) !Mach number in parallel direction
          !Reduce effective Mach number for impurities to a level that doesn't break Mach ordering
          WHERE(Ai(:,i)>mass_cutoff) Machiorig(:,i) = Machiorig(:,i) * SQRT(mass_cutoff/Ai(:,i))
          WHERE(Ai(:,i)>mass_cutoff) Machimod(:,i)  = Machimod(:,i) * SQRT(mass_cutoff/Ai(:,i))   
       ENDIF

       WHERE (ion_type(:,i) .NE. 3) 
          Ac(:) = Ac(:) + Zi(:,i)**2*Nix(:,i)*tau(:,i) !Rest of adiabatic term
          Zeffx(:) = Zeffx(:)+ninorm(:,i)*Zi(:,i)**2 !Zeff
       ENDWHERE
    ENDDO
   
    Lambe(:) = 15.2_DBL - 0.5_DBL*LOG(0.1_DBL*Nex(:)) + LOG(Tex(:))  !Coulomb constant and collisionality. Wesson 2nd edition p661-663
    Nue(:) = 1._DBL/(1.09d-3) *Zeffx(:)*Nex(:)*Lambe(:)/(Tex(:))**1.5_DBL*collmult 
    Nustar(:) = Nue(:)*qx(:)*Ro(:)/epsilon(:)**1.5/(SQRT(Tex(:)*1.d3*qe/me))

    ! Collisionality array
    IF (coll_flag .NE. 0.0) THEN
       Anue(:) = Nue(:)/epsilon(:) 
    ELSE
       Anue(:) = 0._DBL
    ENDIF

    !DEBUGGING
!!$    OPEN(unit=700, file="input/Zeffx.dat", action="write", status="replace")
!!$    WRITE(700,'(G15.7)') (Zeffx(i),i=1,dimx) ; CLOSE(700)
!!$    OPEN(unit=700, file="input/Anue.dat", action="write", status="replace")
!!$    WRITE(700,'(G15.7)') (Anue(i),i=1,dimx) ; CLOSE(700)
!!$    OPEN(unit=700, file="input/Ac.dat", action="write", status="replace")
!!$    WRITE(700,'(G15.7)') (Ac(i),i=1,dimx) ; CLOSE(700)


    ! Normalisation factor
    wg(:) = qx(:)*1.d3/(Ro(:)*Bo(:)*Rmin(:)*(x(:)+eps))

    ! Larmor radii
    Rhoe(:) = SQRT(2._DBL*qe*Tex(:)*1.d3*me)/(qe*Bo(:))

    ! Banana widths
    de(:) = qx(:)/SQRT(2._DBL*(epsilon(:)+eps))*Rhoe(:) 

    !Rotation terms
    Mache(:) = Machpar(:)*cref(:)/cthe(:)

    !Set ETG_flag for when kthetarhos > 2 
    WHERE (kthetarhos > ETGk) 
       ETG_flag = .TRUE.
    ELSEWHERE
       ETG_flag = .FALSE.
    ENDWHERE

    Aue(:) = Aupar(:)*cref(:)/cthe(:)      
    DO i = 1,nions
       Aui(:,i) = Aupar(:)*cref(:)/cthi(:,i)
       !Reduce effective Mach number for impurities to a level that doesn't break Mach ordering
       WHERE(Ai(:,i) > mass_cutoff) Aui(:,i) = Aui(:,i) * SQRT(mass_cutoff/Ai(:,i))
       IF (rot_flag == 2) THEN
          Auiorig(:,i) = Auparorig(:)*cref(:)/cthi(:,i)
          Auimod(:,i) = Auparmod(:)*cref(:)/cthi(:,i)

         !Reduce effective Mach number for impurities to a level that doesn't break Mach ordering
         WHERE(Ai(:,i) > mass_cutoff) Auiorig(:,i) = Auiorig(:,i) * SQRT(mass_cutoff/Ai(:,i))
         WHERE(Ai(:,i) > mass_cutoff) Auimod(:,i) = Auimod(:,i) * SQRT(mass_cutoff/Ai(:,i))
         
       ENDIF
    ENDDO

    !   omega=cref(irad)*Mach(irad)/Ro(irad)*SQRT(1+(epsilon(irad)/qx(irad))**2) !angular toroidal velocity of main ions. We assume that all ions rotate with same omega
    !   domegadr = -Aupar(irad)*cref(irad)/Ro(irad)**2*SQRT(1+(epsilon(irad)/qx(irad))**2) !rotation angular velocity 

    omegator=cref(:)*Machtor(:)/Ro(:) !angular toroidal velocity of main ions. Assumed that all ions rotate at same velocity
    domegatordr = -Aupar(:)*cref(:)/Ro(:)**2 !rotation angular velocity 

    ! Final
    ktetasn(:) = qx(:)/(Rmin(:)*(x(:)+eps)) 
    rhostar(:) = 1./Rmin(:)*SQRT(Tex(:)*1.d3*qe/mi(:,1))/(Zi(:,1)*qe*Bo(:)/mi(:,1)) !With respect to main ion
    Rhoeff(:) = SQRT(qe*1.e3*mp)/(qe*Bo(:)) ! This is the deuterium Larmor radius for Ti = 1keV.  used to normalize nwE, gammaE

    ! ntor is the toroidal wave-number grid for each position of the scan
    DO p=1,dimx
       DO nu=1,dimn
          ntor(p,nu) = kthetarhos(nu)*x(p)/(qx(p)*rhostar(p))
       ENDDO
    ENDDO

    !Integer ntor can sometimes cause problems at low x when ntor is very low
    !ntor=ANINT(ntor)

    DEALLOCATE(Lambe)
    DEALLOCATE(Nue)

  END SUBROUTINE make_input

  SUBROUTINE allocate_output()
    !Complex 1D arrays. These ion arrays are not the final output but used in an intermediate step
    ALLOCATE(fonxcirci(nions)); fonxcirci=0.; fonxcirce=0.
    ALLOCATE(fonxpiegi(nions)); fonxpiegi=0.; fonxpiege=0.
    ALLOCATE(fonxecirci(nions)); fonxecirci=0.; fonxecirce=0.
    ALLOCATE(fonxepiegi(nions)); fonxepiegi=0.; fonxepiege=0.
    ALLOCATE(fonxvcirci(nions)); fonxvcirci=0.; 
    ALLOCATE(fonxvpiegi(nions)); fonxvpiegi=0.; 

    ALLOCATE( krmmuITG (dimx) ); krmmuITG=0;
    ALLOCATE( krmmuETG (dimx) ); krmmuETG=0;

    !Real 2D arrays
    ALLOCATE( kperp2 (dimx, dimn) ); kperp2=0.
    ALLOCATE( modewidth (dimx, dimn) ); modewidth=0.
    ALLOCATE( modeshift (dimx, dimn) ); modeshift=0.
    ALLOCATE( distan (dimx, dimn) ); distan=0.
    ALLOCATE( FLRep (dimx, dimn) ); FLRep=0.
    !Real 3D arrays with 3rd dimension equal to number of ions
    ALLOCATE( FLRip (dimx, dimn,nions) ); FLRip=0.
    !Real 3D arrays with 3rd dimension equal to number of solutions searched for
    ALLOCATE( gamma (dimx, dimn, numsols) ); gamma=0.
    ALLOCATE( Ladia (dimx, dimn, numsols) ); Ladia=0.
    !Complex 2D arrays 
    ALLOCATE( ommax (dimx, dimn) ) ; ommax=0.
    ALLOCATE( solflu (dimx, dimn) ); solflu=0.
    !DEBUGGING FOR FLUID SOLUTIONS
    ALLOCATE(jon_solflu(dimx,dimn)); jon_solflu=0.
    ALLOCATE(jon_modewidth(dimx,dimn)); jon_modewidth=0.
    ALLOCATE(jon_modeshift(dimx,dimn)); jon_modeshift=0.
    ALLOCATE(ana_solflu(dimx,dimn)); ana_solflu=0.


    !Complex 3D arrays with 3rd dimension equal to number of solutions searched for
    ALLOCATE( sol (dimx, dimn, numsols) ); sol=0.
    ALLOCATE( fdsol (dimx, dimn, numsols) ); fdsol=0.
    ALLOCATE( Lcirce (dimx, dimn, numsols) ); Lcirce=0.
    ALLOCATE( Lpiege (dimx, dimn, numsols) ); Lpiege=0.
    ALLOCATE( Lecirce (dimx, dimn, numsols) ); Lecirce=0.
    ALLOCATE( Lepiege (dimx, dimn, numsols) ); Lepiege=0.
    !Complex 4D arrays with 3rd dimension equal to nions and 4th to numsols
    ALLOCATE( Lcirci (dimx, dimn, nions, numsols) ); Lcirci=0.
    ALLOCATE( Lpiegi (dimx, dimn, nions, numsols) ); Lpiegi=0.
    ALLOCATE( Lecirci (dimx, dimn, nions, numsols) ); Lecirci=0.
    ALLOCATE( Lepiegi (dimx, dimn, nions, numsols) ); Lepiegi=0.
    ALLOCATE( Lvcirci (dimx, dimn, nions, numsols) ); Lvcirci=0.
    ALLOCATE( Lvpiegi (dimx, dimn, nions, numsols) ); Lvpiegi=0.
    !To save memory space, the following output arrays are only produced when specifically requested
    IF (phys_meth /= 0.0) THEN
       !Intermediate 1D complex arrays
       ALLOCATE(fonxcircgti(nions)); fonxcircgti=0.; fonxcircgte=0.
       ALLOCATE(fonxpieggti(nions)); fonxpieggti=0.; fonxpieggte=0.
       ALLOCATE(fonxcircgni(nions)); fonxcircgni=0.; fonxcircgne=0.
       ALLOCATE(fonxpieggni(nions)); fonxpieggni=0.; fonxpieggne=0.
       ALLOCATE(fonxcircgui(nions)); fonxcircgui=0.; 
       ALLOCATE(fonxpieggui(nions)); fonxpieggui=0.; 
       ALLOCATE(fonxcircci(nions)); fonxcircci=0.; fonxcircce=0.
       ALLOCATE(fonxpiegci(nions)); fonxpiegci=0.; fonxpiegce=0.
       !Complex 3D arrays with 3rd dimension equal to number of solutions searched for
       ALLOCATE( Lcircgte (dimx, dimn, numsols) ); Lcircgte=0.
       ALLOCATE( Lpieggte (dimx, dimn, numsols) ); Lpieggte=0.
       ALLOCATE( Lcircgne (dimx, dimn, numsols) ); Lcircgne=0.
       ALLOCATE( Lpieggne (dimx, dimn, numsols) ); Lpieggne=0.
       ALLOCATE( Lcircce (dimx, dimn, numsols) ); Lcircce=0.
       ALLOCATE( Lpiegce (dimx, dimn, numsols) ); Lpiegce=0.
       !Complex 4D arrays with 3rd dimension equal to nions and 4th to numsols
       ALLOCATE( Lcircgti (dimx, dimn, nions, numsols) ); Lcircgti=0.
       ALLOCATE( Lpieggti (dimx, dimn, nions, numsols) ); Lpieggti=0.
       ALLOCATE( Lcircgni (dimx, dimn, nions, numsols) ); Lcircgni=0.
       ALLOCATE( Lpieggni (dimx, dimn, nions, numsols) ); Lpieggni=0.
       ALLOCATE( Lcircgui (dimx, dimn, nions, numsols) ); Lcircgui=0.
       ALLOCATE( Lpieggui (dimx, dimn, nions, numsols) ); Lpieggui=0.
       ALLOCATE( Lcircci (dimx, dimn, nions, numsols) ); Lcircci=0.
       ALLOCATE( Lpiegci (dimx, dimn, nions, numsols) ); Lpiegci=0.
!!!
       IF (phys_meth == 2) THEN
          !Intermediate 1D complex arrays
          ALLOCATE(fonxecircgti(nions)); fonxecircgti=0.; fonxecircgte=0.
          ALLOCATE(fonxepieggti(nions)); fonxepieggti=0.; fonxepieggte=0.
          ALLOCATE(fonxecircgni(nions)); fonxecircgni=0.; fonxecircgne=0.
          ALLOCATE(fonxepieggni(nions)); fonxepieggni=0.; fonxepieggne=0.
          ALLOCATE(fonxecircgui(nions)); fonxecircgui=0.; 
          ALLOCATE(fonxepieggui(nions)); fonxepieggui=0.; 
          ALLOCATE(fonxecircci(nions)); fonxecircci=0.; fonxecircce=0.
          ALLOCATE(fonxepiegci(nions)); fonxepiegci=0.; fonxepiegce=0.
          !Complex 3D arrays with 3rd dimension equal to number of solutions searched for
          ALLOCATE( Lecircgte (dimx, dimn, numsols) ); Lecircgte=0.
          ALLOCATE( Lepieggte (dimx, dimn, numsols) ); Lepieggte=0.
          ALLOCATE( Lecircgne (dimx, dimn, numsols) ); Lecircgne=0.
          ALLOCATE( Lepieggne (dimx, dimn, numsols) ); Lepieggne=0.
          ALLOCATE( Lecircce (dimx, dimn, numsols) ); Lecircce=0.
          ALLOCATE( Lepiegce (dimx, dimn, numsols) ); Lepiegce=0.
          !Complex 4D arrays with 3rd dimension equal to nions and 4th to numsols
          ALLOCATE( Lecircgti (dimx, dimn, nions, numsols) ); Lecircgti=0.
          ALLOCATE( Lepieggti (dimx, dimn, nions, numsols) ); Lepieggti=0.
          ALLOCATE( Lecircgni (dimx, dimn, nions, numsols) ); Lecircgni=0.
          ALLOCATE( Lepieggni (dimx, dimn, nions, numsols) ); Lepieggni=0.
          ALLOCATE( Lecircgui (dimx, dimn, nions, numsols) ); Lecircgui=0.
          ALLOCATE( Lepieggui (dimx, dimn, nions, numsols) ); Lepieggui=0.
          ALLOCATE( Lecircci (dimx, dimn, nions, numsols) ); Lecircci=0.
          ALLOCATE( Lepiegci (dimx, dimn, nions, numsols) ); Lepiegci=0.
       ENDIF
    ENDIF
  END SUBROUTINE allocate_output

  SUBROUTINE deallocate_all()

    DEALLOCATE(x)
    DEALLOCATE(rho)
    DEALLOCATE(Ro)
    DEALLOCATE(Rmin)
    DEALLOCATE(rotflagarray)
    DEALLOCATE(Bo)
    DEALLOCATE(kthetarhos)
    DEALLOCATE(qx)
    DEALLOCATE(smag)
    DEALLOCATE(Tex)
    DEALLOCATE(Tix)
    DEALLOCATE(Nex)
    DEALLOCATE(Nustar)
    DEALLOCATE(anise)
    DEALLOCATE(danisedr)
    DEALLOCATE(anis)
    DEALLOCATE(danisdr) 
    DEALLOCATE(ion_type)
    DEALLOCATE(Ate)
    DEALLOCATE(Ati)
    DEALLOCATE(Ane)
    DEALLOCATE(Ani)
    DEALLOCATE(alphax)
    DEALLOCATE(Machtor)
    DEALLOCATE(Autor)
    DEALLOCATE(gammaE)
    DEALLOCATE(Machpar)
    DEALLOCATE(Aupar)
    DEALLOCATE(ninorm)
    DEALLOCATE(Ai)
    DEALLOCATE(Zi)

    IF (rot_flag == 2) THEN
       DEALLOCATE(gammaEmod,Auparmod,Machparmod,gammaEorig,Auparorig,Machparorig,Machiorig,Auiorig,Machimod,Auimod,filterprof)
    ENDIF

    DEALLOCATE(Machi)
    DEALLOCATE(Machitemp)
    DEALLOCATE(Mache)
    DEALLOCATE(Aue)
    DEALLOCATE(Aui)
    DEALLOCATE(ft)
    DEALLOCATE(fc)
    DEALLOCATE(npol)
    DEALLOCATE(ecoefs)
    DEALLOCATE(ecoefsgau)
    DEALLOCATE(cftrans)
    DEALLOCATE(nepol)
    DEALLOCATE(Anipol)
    DEALLOCATE(Anepol)
    DEALLOCATE(th)
    DEALLOCATE(tpernormi)
    DEALLOCATE(tpernormifunc)
    DEALLOCATE(tpernorme)
    DEALLOCATE(phi)    
    DEALLOCATE(dphidr)    
    DEALLOCATE(dphidth)    
    DEALLOCATE(epsilon)
    DEALLOCATE(qprim)
    DEALLOCATE(mi)
    DEALLOCATE(ETG_flag)
    DEALLOCATE(tau)
    DEALLOCATE(Nix)
    DEALLOCATE(Anue)
    DEALLOCATE(wg)
    DEALLOCATE(Zeffx)  
    DEALLOCATE(Ac)
    DEALLOCATE(csou)
    DEALLOCATE(cref)
    DEALLOCATE(cthe)
    DEALLOCATE(cthi)
    DEALLOCATE(omegator)
    DEALLOCATE(domegatordr)
    DEALLOCATE(Rhoe)
    DEALLOCATE(Rhoi)
    DEALLOCATE(de)
    DEALLOCATE(di)
    DEALLOCATE(ktetasn) 
    DEALLOCATE(rhostar)
    DEALLOCATE(Rhoeff)
    DEALLOCATE( ntor )
    DEALLOCATE( edgemode )    
    DEALLOCATE( coefi )
    DEALLOCATE( ktetaRhoi )
    DEALLOCATE(Joi2)
    DEALLOCATE(Jobani2)
    DEALLOCATE(Joi2p)
    DEALLOCATE( krmmuITG)
    DEALLOCATE( krmmuETG)
    DEALLOCATE( kperp2 )
    DEALLOCATE( modewidth )
    DEALLOCATE( modeshift )
    DEALLOCATE( distan )
    DEALLOCATE( Athi )
    DEALLOCATE( FLRip )
    DEALLOCATE( FLRep )

    DEALLOCATE( gamma )
    DEALLOCATE( Ladia )

    DEALLOCATE( ommax )
    DEALLOCATE( solflu )
    DEALLOCATE(jon_solflu)
    DEALLOCATE(jon_modewidth)
    DEALLOCATE(jon_modeshift)
    DEALLOCATE(ana_solflu)

    DEALLOCATE( sol )
    DEALLOCATE( fdsol )
    DEALLOCATE(fonxcirci)
    DEALLOCATE(fonxpiegi)
    DEALLOCATE(fonxecirci)
    DEALLOCATE(fonxepiegi)
    DEALLOCATE(fonxvcirci)
    DEALLOCATE(fonxvpiegi)

    DEALLOCATE( Lcirce )
    DEALLOCATE( Lpiege )
    DEALLOCATE( Lcirci )
    DEALLOCATE( Lpiegi )
    DEALLOCATE( Lepiege )
    DEALLOCATE( Lecirce )
    DEALLOCATE( Lecirci )
    DEALLOCATE( Lepiegi )
    DEALLOCATE( Lvcirci )
    DEALLOCATE( Lvpiegi )

    IF (phys_meth /= 0) THEN
       DEALLOCATE(fonxcircgti)
       DEALLOCATE(fonxpieggti)
       DEALLOCATE(fonxcircgni)
       DEALLOCATE(fonxpieggni)
       DEALLOCATE(fonxcircgui)
       DEALLOCATE(fonxpieggui)
       DEALLOCATE(fonxcircci)
       DEALLOCATE(fonxpiegci)
       DEALLOCATE( Lcircgte )
       DEALLOCATE( Lpieggte )
       DEALLOCATE( Lcircgti )
       DEALLOCATE( Lpieggti )
       DEALLOCATE( Lcircgne )
       DEALLOCATE( Lpieggne )
       DEALLOCATE( Lcircgni )
       DEALLOCATE( Lpieggni )
       DEALLOCATE( Lcircgui )
       DEALLOCATE( Lpieggui )
       DEALLOCATE( Lcircce )
       DEALLOCATE( Lpiegce )
       DEALLOCATE( Lcircci )
       DEALLOCATE( Lpiegci )
       IF (phys_meth == 2) THEN
          DEALLOCATE(fonxecircgti)
          DEALLOCATE(fonxepieggti)
          DEALLOCATE(fonxecircgni)
          DEALLOCATE(fonxepieggni)
          DEALLOCATE(fonxecircgui)
          DEALLOCATE(fonxepieggui)
          DEALLOCATE(fonxecircci)
          DEALLOCATE(fonxepiegci)
          DEALLOCATE( Lecircgte )
          DEALLOCATE( Lepieggte )
          DEALLOCATE( Lecircgti )
          DEALLOCATE( Lepieggti )
          DEALLOCATE( Lecircgne )
          DEALLOCATE( Lepieggne )
          DEALLOCATE( Lecircgni )
          DEALLOCATE( Lepieggni )
          DEALLOCATE( Lecircgui )
          DEALLOCATE( Lepieggui )
          DEALLOCATE( Lecircce )
          DEALLOCATE( Lepiegce )
          DEALLOCATE( Lecircci )
          DEALLOCATE( Lepiegci )
       ENDIF
    ENDIF

  END SUBROUTINE deallocate_all

  SUBROUTINE save_qlfunc(p,nu,j,issol)
    !Arguments
    LOGICAL, INTENT(IN) :: issol
    INTEGER, INTENT(IN) :: p,nu,j
    !Local variables

    IF (issol .EQV. .FALSE.) THEN
       fonxad=Ac(p)
       fonxcirce=(0.,0.)
       fonxpiege=(0.,0.)
       fonxcirci(:)=(0.,0.)
       fonxpiegi(:)=(0.,0.)
       ! to save memory space, this condition has been introduced, Ale 10/08
       IF (phys_meth .NE. 0.0) THEN
          fonxcircgte=(0.,0.)
          fonxpieggte=(0.,0.)
          fonxcircgti(:)=(0.,0.)
          fonxpieggti(:)=(0.,0.)
          fonxcircgne=(0.,0.)
          fonxpieggne=(0.,0.)
          fonxcircgni(:)=(0.,0.)
          fonxpieggni(:)=(0.,0.)
          fonxcircgui(:)=(0.,0.)
          fonxpieggui(:)=(0.,0.)
          fonxcircce=(0.,0.)
          fonxpiegce=(0.,0.)
          fonxcircci(:)=(0.,0.)
          fonxpiegci(:)=(0.,0.)
          IF (phys_meth == 2) THEN
             fonxecircgte=(0.,0.)
             fonxepieggte=(0.,0.)
             fonxecircgti(:)=(0.,0.)
             fonxepieggti(:)=(0.,0.)
             fonxecircgne=(0.,0.)
             fonxepieggne=(0.,0.)
             fonxecircgni(:)=(0.,0.)
             fonxepieggni(:)=(0.,0.)
             fonxecircgui(:)=(0.,0.)
             fonxepieggui(:)=(0.,0.)
             fonxecircce=(0.,0.)
             fonxepiegce=(0.,0.)
             fonxecircci(:)=(0.,0.)
             fonxepiegci(:)=(0.,0.)
          ENDIF
       ENDIF
       fonxecirce=(0.,0.)
       fonxepiege=(0.,0.)
       fonxecirci(:)=(0.,0.)
       fonxepiegi(:)=(0.,0.)
       fonxvcirci(:)=(0.,0.)
       fonxvpiegi(:)=(0.,0.)
    ELSE
       Ladia(p,nu,j) = fonxad
       Lcirce(p,nu,j) = AIMAG(fonxcirce)
       Lpiege(p,nu,j) = AIMAG(fonxpiege)
       Lcirci(p,nu,:,j) = AIMAG(fonxcirci(:))
       Lpiegi(p,nu,:,j) = AIMAG(fonxpiegi(:))
       ! to save memory space, this condition has been introduced, Ale 10/08
       IF (phys_meth .NE. 0.0) THEN
          Lcircgte(p,nu,j) = AIMAG(fonxcircgte)
          Lpieggte(p,nu,j) = AIMAG(fonxpieggte)
          Lcircgti(p,nu,:,j) = AIMAG(fonxcircgti(:))
          Lpieggti(p,nu,:,j) = AIMAG(fonxpieggti(:))
          Lcircgne(p,nu,j) = AIMAG(fonxcircgne)
          Lpieggne(p,nu,j) = AIMAG(fonxpieggne)
          Lcircgni(p,nu,:,j) = AIMAG(fonxcircgni(:))
          Lpieggni(p,nu,:,j) = AIMAG(fonxpieggni(:))
          Lcircgui(p,nu,:,j) = AIMAG(fonxcircgui(:))
          Lpieggui(p,nu,:,j) = AIMAG(fonxpieggui(:))
          Lcircce(p,nu,j) = AIMAG(fonxcircce)
          Lpiegce(p,nu,j) = AIMAG(fonxpiegce)
          Lcircci(p,nu,:,j) = AIMAG(fonxcircci(:))
          Lpiegci(p,nu,:,j) = AIMAG(fonxpiegci(:))
          IF (phys_meth == 2) THEN
             Lecircgte(p,nu,j) = AIMAG(fonxecircgte)
             Lepieggte(p,nu,j) = AIMAG(fonxepieggte)
             Lecircgti(p,nu,:,j) = AIMAG(fonxecircgti(:))
             Lepieggti(p,nu,:,j) = AIMAG(fonxepieggti(:))
             Lecircgne(p,nu,j) = AIMAG(fonxecircgne)
             Lepieggne(p,nu,j) = AIMAG(fonxepieggne)
             Lecircgni(p,nu,:,j) = AIMAG(fonxecircgni(:))
             Lepieggni(p,nu,:,j) = AIMAG(fonxepieggni(:))
             Lecircgui(p,nu,:,j) = AIMAG(fonxecircgui(:))
             Lepieggui(p,nu,:,j) = AIMAG(fonxepieggui(:))
             Lecircce(p,nu,j) = AIMAG(fonxecircce)
             Lepiegce(p,nu,j) = AIMAG(fonxepiegce)
             Lecircci(p,nu,:,j) = AIMAG(fonxecircci(:))
             Lepiegci(p,nu,:,j) = AIMAG(fonxepiegci(:))
          ENDIF
       ENDIF
       Lecirce(p,nu,j) = AIMAG(fonxecirce)
       Lepiege(p,nu,j) = AIMAG(fonxepiege)
       Lecirci(p,nu,:,j) = AIMAG(fonxecirci(:))
       Lepiegi(p,nu,:,j) = AIMAG(fonxepiegi(:))
       Lvcirci(p,nu,:,j) = AIMAG(fonxvcirci(:))
       Lvpiegi(p,nu,:,j) = AIMAG(fonxvpiegi(:))
    ENDIF

  END SUBROUTINE save_qlfunc

  SUBROUTINE collectarrays()
    ! Collect all output into all cores
    INTEGER :: ierr,myrank, nproc

    REAL(KIND=DBL), DIMENSION(dimx,dimn) :: distantmp,FLReptmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,numsols) :: gammatmp, Ladiatmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,nions) :: FLRiptmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,0:nions,0:9) :: ecoefsgautmp
    COMPLEX(KIND=DBL), DIMENSION(dimx,dimn) :: modewidthtmp, modeshifttmp,jon_modewidthtmp, jon_modeshifttmp
    COMPLEX(KIND=DBL), DIMENSION(dimx,dimn) :: ommaxtmp, solflutmp,jon_solflutmp,ana_solflutmp
    COMPLEX(KIND=DBL), DIMENSION(dimx,dimn,numsols) :: soltmp, fdsoltmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,numsols) :: Lcircetmp, Lpiegetmp, Lecircetmp, Lepiegetmp, Lcircgtetmp, Lpieggtetmp, &
         Lcircgnetmp, Lpieggnetmp,  Lcirccetmp, Lpiegcetmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,numsols) :: Lecircgtetmp, Lepieggtetmp, Lecircgnetmp, Lepieggnetmp, Lecirccetmp, Lepiegcetmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,nions,numsols) :: Lcircitmp, Lpiegitmp, Lecircitmp, Lepiegitmp, Lvcircitmp, Lvpiegitmp, &
         Lcircgtitmp, Lpieggtitmp, Lcircgnitmp, Lpieggnitmp, Lcircguitmp, Lpiegguitmp, Lcirccitmp, Lpiegcitmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,nions,numsols) :: Lecircgtitmp, Lepieggtitmp, Lecircgnitmp, Lepieggnitmp, Lecircguitmp, &
         Lepiegguitmp, Lecirccitmp, Lepiegcitmp

    CALL mpi_comm_rank(mpi_comm_world,myrank,ierr)
    CALL mpi_comm_size(mpi_comm_world,nproc,ierr)

    CALL MPI_AllReduce(distan,distantmp,dimx*dimn,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(modewidth,modewidthtmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(modeshift,modeshifttmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(jon_modewidth,jon_modewidthtmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(jon_modeshift,jon_modeshifttmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(FLRep,FLReptmp,dimx*dimn,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(gamma,gammatmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Ladia,Ladiatmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(FLRip,FLRiptmp,dimx*dimn*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ommax,ommaxtmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(solflu,solflutmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(jon_solflu,jon_solflutmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ana_solflu,ana_solflutmp,dimx*dimn,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(sol,soltmp,dimx*dimn*numsols,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(fdsol,fdsoltmp,dimx*dimn*numsols,MPI_DOUBLE_COMPLEX,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(Lcirce,Lcircetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lpiege,Lpiegetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lecirce,Lecircetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lepiege,Lepiegetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lcirci,Lcircitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lpiegi,Lpiegitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lecirci,Lecircitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lepiegi,Lepiegitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lvcirci,Lvcircitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(Lvpiegi,Lvpiegitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(ecoefsgau,ecoefsgautmp,dimx*dimn*(nions+1)*10,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    IF (phys_meth /= 0) THEN
       CALL MPI_AllReduce(Lcircgte,Lcircgtetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpieggte,Lpieggtetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lcircgne,Lcircgnetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpieggne,Lpieggnetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lcircce,Lcirccetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpiegce,Lpiegcetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

       CALL MPI_AllReduce(Lcircgti,Lcircgtitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpieggti,Lpieggtitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lcircgni,Lcircgnitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpieggni,Lpieggnitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lcircgui,Lcircguitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpieggui,Lpiegguitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lcircci,Lcirccitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(Lpiegci,Lpiegcitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
!!!
       IF (phys_meth == 2) THEN
          CALL MPI_AllReduce(Lecircgte,Lecircgtetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepieggte,Lepieggtetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lecircgne,Lecircgnetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepieggne,Lepieggnetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lecircce,Lecirccetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepiegce,Lepiegcetmp,dimx*dimn*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

          CALL MPI_AllReduce(Lecircgti,Lecircgtitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepieggti,Lepieggtitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lecircgni,Lecircgnitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepieggni,Lepieggnitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lecircgui,Lecircguitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepieggui,Lepiegguitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lecircci,Lecirccitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(Lepiegci,Lepiegcitmp,dimx*dimn*nions*numsols,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       ENDIF
    ENDIF

    modewidth=modewidthtmp
    modeshift=modeshifttmp
    jon_modewidth=jon_modewidthtmp
    jon_modeshift=jon_modeshifttmp

    distan=distantmp
    FLRep=FLReptmp
    gamma=gammatmp
    Ladia=Ladiatmp
    FLRip=FLRiptmp
    ommax=ommaxtmp
    solflu=solflutmp
    jon_solflu=jon_solflutmp
    ana_solflu=ana_solflutmp
    sol=soltmp
    fdsol=fdsoltmp
    Lcirce=Lcircetmp
    Lpiege=Lpiegetmp
    Lecirce=Lecircetmp
    Lepiege=Lepiegetmp
    Lcirci=Lcircitmp
    Lpiegi=Lpiegitmp
    Lecirci=Lecircitmp
    Lepiegi=Lepiegitmp
    Lvcirci=Lvcircitmp
    Lvpiegi=Lvpiegitmp
    ecoefsgau=ecoefsgautmp

    IF (phys_meth /= 0) THEN
       Lcircgte=Lcircgtetmp
       Lpieggte=Lpieggtetmp
       Lcircgne=Lcircgnetmp
       Lpieggne=Lpieggnetmp
       Lcircce=Lcirccetmp
       Lpiegce=Lpiegcetmp
       Lcircgti=Lcircgtitmp
       Lpieggti=Lpieggtitmp
       Lcircgni=Lcircgnitmp
       Lpieggni=Lpieggnitmp
       Lcircgui=Lcircguitmp
       Lpieggui=Lpiegguitmp
       Lcircci=Lcirccitmp
       Lpiegci=Lpiegcitmp   
       IF (phys_meth == 2) THEN
          Lecircgte=Lecircgtetmp
          Lepieggte=Lepieggtetmp
          Lecircgne=Lecircgnetmp
          Lepieggne=Lepieggnetmp
          Lecircce=Lecirccetmp
          Lepiegce=Lepiegcetmp
          Lecircgti=Lecircgtitmp
          Lepieggti=Lepieggtitmp
          Lecircgni=Lecircgnitmp
          Lepieggni=Lepieggnitmp
          Lecircgui=Lecircguitmp
          Lepieggui=Lepiegguitmp
          Lecircci=Lecirccitmp
          Lepiegci=Lepiegcitmp   
       ENDIF
    ENDIF

  END SUBROUTINE collectarrays

  SUBROUTINE reduceoutput()
    ! collect all output into all cores for parallel writing
    INTEGER :: ierr,myrank, nproc

    REAL(KIND=DBL), DIMENSION(dimx) :: epf_SItmp, eef_SItmp, epf_GBtmp, eef_GBtmp
    REAL(KIND=DBL), DIMENSION(dimx) :: krmmuITGtmp, krmmuETGtmp
    REAL(KIND=DBL), DIMENSION(dimx) :: dfe_SItmp, vte_SItmp, vce_SItmp, dfe_GBtmp, vte_GBtmp, vce_GBtmp, cketmp
    REAL(KIND=DBL), DIMENSION(dimx) :: chiee_SItmp, vene_SItmp, vece_SItmp, chiee_GBtmp, vene_GBtmp, vece_GBtmp, ceketmp
    REAL(KIND=DBL), DIMENSION(dimx,nions) :: ipf_SItmp, ief_SItmp, ivf_SItmp, ipf_GBtmp, ief_GBtmp, ivf_GBtmp
    REAL(KIND=DBL), DIMENSION(dimx,nions) :: dfi_SItmp, vti_SItmp, vci_SItmp, vri_SItmp
    REAL(KIND=DBL), DIMENSION(dimx,nions) :: dfi_GBtmp, vti_GBtmp, vci_GBtmp, vri_GBtmp, ckitmp
    REAL(KIND=DBL), DIMENSION(dimx,nions) :: chiei_SItmp, veni_SItmp, veci_SItmp, veri_SItmp
    REAL(KIND=DBL), DIMENSION(dimx,nions) :: chiei_GBtmp, veni_GBtmp, veci_GBtmp, veri_GBtmp, cekitmp

    REAL(KIND=DBL), DIMENSION(dimx,dimn) :: epf_cmtmp, eef_cmtmp, kperp2tmp
    REAL(KIND=DBL), DIMENSION(dimx,dimn,nions) :: ipf_cmtmp, ief_cmtmp, ivf_cmtmp

    REAL(KIND=DBL), DIMENSION(dimx,nions,numicoefs) :: cftranstmp

    CALL mpi_comm_rank(mpi_comm_world,myrank,ierr)
    CALL mpi_comm_size(mpi_comm_world,nproc,ierr)

    CALL MPI_AllReduce(epf_SI,epf_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(eef_SI,eef_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(epf_GB,epf_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(eef_GB,eef_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(krmmuITG,krmmuITGtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(krmmuETG,krmmuETGtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(epf_cm,epf_cmtmp,dimx*dimn,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(eef_cm,eef_cmtmp,dimx*dimn,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(kperp2,kperp2tmp,dimx*dimn,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(ipf_SI,ipf_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ief_SI,ief_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ivf_SI,ivf_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ipf_GB,ipf_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ivf_GB,ivf_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ief_GB,ief_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(ipf_cm,ipf_cmtmp,dimx*dimn*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ief_cm,ief_cmtmp,dimx*dimn*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
    CALL MPI_AllReduce(ivf_cm,ivf_cmtmp,dimx*dimn*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    CALL MPI_AllReduce(cftrans,cftranstmp,dimx*nions*numicoefs,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

    IF (phys_meth /= 0.0) THEN
       CALL MPI_AllReduce(dfe_SI,dfe_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vte_SI,vte_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vce_SI,vce_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(dfe_GB,dfe_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vte_GB,vte_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vce_GB,vce_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

       CALL MPI_AllReduce(cke,cketmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(dfi_SI,dfi_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vti_SI,vti_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vri_SI,vri_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vci_SI,vci_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(dfi_GB,dfi_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vti_GB,vti_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vri_GB,vri_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(vci_GB,vci_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       CALL MPI_AllReduce(cki,ckitmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)

       IF (phys_meth == 2) THEN
          CALL MPI_AllReduce(chiee_SI,chiee_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(vene_SI,vene_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(vece_SI,vece_SItmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(ceke,ceketmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(chiei_SI,chiei_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veni_SI,veni_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veri_SI,veri_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veci_SI,veci_SItmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(ceki,cekitmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(chiee_GB,chiee_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(vene_GB,vene_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(vece_GB,vece_GBtmp,dimx,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(chiei_GB,chiei_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veni_GB,veni_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veri_GB,veri_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
          CALL MPI_AllReduce(veci_GB,veci_GBtmp,dimx*nions,MPI_DOUBLE_PRECISION,MPI_SUM,mpi_comm_world,ierr)
       ENDIF
    ENDIF

    epf_SI=epf_SItmp
    eef_SI=eef_SItmp
    epf_GB=epf_GBtmp
    eef_GB=eef_GBtmp
    krmmuITG=krmmuITGtmp
    krmmuETG=krmmuETGtmp

    epf_cm=epf_cmtmp
    eef_cm=eef_cmtmp

    kperp2=kperp2tmp

    ipf_SI=ipf_SItmp
    ief_SI=ief_SItmp
    ivf_SI=ivf_SItmp
    ipf_GB=ipf_GBtmp
    ivf_GB=ivf_GBtmp
    ief_GB=ief_GBtmp

    ipf_cm=ipf_cmtmp
    ief_cm=ief_cmtmp
    ivf_cm=ivf_cmtmp

    cftrans=cftranstmp

    IF (phys_meth /= 0.0) THEN

       dfe_SI=dfe_SItmp
       vte_SI=vte_SItmp
       vce_SI=vce_SItmp
       cke=cketmp
       dfi_SI=dfi_SItmp
       vti_SI=vti_SItmp
       vri_SI=vri_SItmp
       vci_SI=vci_SItmp
       cki=ckitmp

       dfe_GB=dfe_GBtmp
       vte_GB=vte_GBtmp
       vce_GB=vce_GBtmp

       dfi_GB=dfi_GBtmp
       vti_GB=vti_GBtmp
       vri_GB=vri_GBtmp
       vci_GB=vci_GBtmp

       IF (phys_meth == 2) THEN

          chiee_SI=chiee_SItmp
          vene_SI=vene_SItmp
          vece_SI=vece_SItmp
          ceke=ceketmp
          chiei_SI=chiei_SItmp
          veni_SI=veni_SItmp
          veri_SI=veri_SItmp
          veci_SI=veci_SItmp
          ceki=cekitmp
          chiee_GB=chiee_GBtmp
          vene_GB=vene_GBtmp
          vece_GB=vece_GBtmp
          chiei_GB=chiei_GBtmp
          veni_GB=veni_GBtmp
          veri_GB=veri_GBtmp
          veci_GB=veci_GBtmp
       ENDIF
    ENDIF
  END SUBROUTINE reduceoutput

  subroutine allocate_output_meth_0(dimxin, dimnin, nionsin, nthetain, numecoefsin, numicoefsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin, nthetain, numecoefsin, numicoefsin
    type(qlk_output_meth_0), intent(inout) :: output

    allocate(output%pfe_cm(dimxin, dimnin), &
             output%pfi_cm(dimxin, dimnin, nionsin), &
             output%efe_cm(dimxin, dimnin), &
             output%efi_cm(dimxin, dimnin, nionsin), &
             output%vfi_cm(dimxin, dimnin, nionsin), &
             output%ecoefs(dimxin, 0:nionsin, numecoefsin), &
             output%npol(dimxin, nthetain, nionsin), &
             output%cftrans(dimxin, nionsin, numicoefsin), &
             output%phi(dimxin, nthetain), &
             output%Zeff(dimxin), &
             output%Nustar(dimxin))
  end subroutine allocate_output_meth_0


  subroutine allocate_output_meth_0_sep_0(dimxin, dimnin, nionsin, numsolsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin, numsolsin
    type(qlk_output_meth_0_sep_0), intent(inout) :: output

    allocate(output%gam(dimxin, dimnin, numsolsin), &
             output%ome(dimxin, dimnin, numsolsin), &
             output%pfe(dimxin), &
             output%pfi(dimxin, nionsin), &
             output%efe(dimxin), &
             output%efi(dimxin, nionsin), &
             output%vfi(dimxin, nionsin), &
             output%efeETG(dimxin))
  end subroutine allocate_output_meth_0_sep_0

  subroutine allocate_output_meth_0_sep_1(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_0_sep_1), intent(inout) :: output

    allocate(output%efeITG(dimxin), &
             output%efeTEM(dimxin), &
             output%efiITG(dimxin, nionsin), &
             output%efiTEM(dimxin, nionsin), &
             output%pfeITG(dimxin), &
             output%pfeTEM(dimxin), &
             output%pfiITG(dimxin, nionsin), &
             output%pfiTEM(dimxin, nionsin), &
             output%vfiITG(dimxin, nionsin), &
             output%vfiTEM(dimxin, nionsin))
  end subroutine allocate_output_meth_0_sep_1

  subroutine allocate_output_meth_1(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_1), intent(inout) :: output

    allocate(output%cke(dimxin), &
             output%cki(dimxin, nionsin))
  end subroutine allocate_output_meth_1

  subroutine allocate_output_meth_1_sep_0(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_1_sep_0), intent(inout) :: output

    allocate(output%dfe(dimxin), &
             output%dfi(dimxin, nionsin), &
             output%vte(dimxin), &
             output%vti(dimxin, nionsin), &
             output%vce(dimxin), &
             output%vci(dimxin, nionsin), &
             output%vri(dimxin, nionsin))
  end subroutine allocate_output_meth_1_sep_0

  subroutine allocate_output_meth_1_sep_1(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_1_sep_1), intent(inout) :: output

    allocate(output%dfeITG(dimxin), &
             output%dfeTEM(dimxin), &
             output%dfiITG(dimxin, nionsin), &
             output%dfiTEM(dimxin, nionsin), &
             output%vriITG(dimxin, nionsin), &
             output%vriTEM(dimxin, nionsin), &
             output%vteITG(dimxin), &
             output%vteTEM(dimxin), &
             output%vtiITG(dimxin, nionsin), &
             output%vtiTEM(dimxin, nionsin), &
             output%vceITG(dimxin), &
             output%vceTEM(dimxin), &
             output%vciITG(dimxin, nionsin), &
             output%vciTEM(dimxin, nionsin))
  end subroutine allocate_output_meth_1_sep_1

  subroutine allocate_output_meth_2(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_2), intent(inout) :: output

    allocate(output%ceke(dimxin), &
             output%ceki(dimxin, nionsin))
  end subroutine allocate_output_meth_2

  subroutine allocate_output_meth_2_sep_0(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_2_sep_0), intent(inout) :: output

    allocate(output%chiee(dimxin), &
             output%chiei(dimxin, nionsin), &
             output%vece(dimxin), &
             output%veci(dimxin, nionsin), &
             output%vene(dimxin), &
             output%veni(dimxin, nionsin), &
             output%veri(dimxin, nionsin))
  end subroutine allocate_output_meth_2_sep_0

  subroutine allocate_output_meth_2_sep_1(dimxin, nionsin, output)
    integer, intent(in) :: dimxin, nionsin
    type(qlk_output_meth_2_sep_1), intent(inout) :: output

    allocate(output%chieeITG(dimxin), &
             output%chieeTEM(dimxin), &
             output%chieiITG(dimxin, nionsin), &
             output%chieiTEM(dimxin, nionsin), &
             output%veceITG(dimxin), &
             output%veceTEM(dimxin), &
             output%veciITG(dimxin, nionsin), &
             output%veciTEM(dimxin, nionsin), &
             output%veneITG(dimxin), &
             output%veneTEM(dimxin), &
             output%veniITG(dimxin, nionsin), &
             output%veniTEM(dimxin, nionsin), &
             output%veriITG(dimxin, nionsin), &
             output%veriTEM(dimxin, nionsin), &
             output%chieeETG(dimxin), &
             output%veceETG(dimxin), &
             output%veneETG(dimxin))
  end subroutine allocate_output_meth_2_sep_1

  subroutine allocate_primi_meth_0(dimxin, dimnin, nionsin, numsolsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin, numsolsin
    type(qlk_primi_meth_0), intent(inout) :: output

    allocate(output%Lcirce(dimxin, dimnin, numsolsin), &
             output%Lcirci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lecirce(dimxin, dimnin, numsolsin), &
             output%Lecirci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepiege(dimxin, dimnin, numsolsin), &
             output%Lpiege(dimxin, dimnin, numsolsin), &
             output%Lpiegi(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepiegi(dimxin, dimnin, nionsin, numsolsin), &
             output%Lvcirci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lvpiegi(dimxin, dimnin, nionsin, numsolsin), &
             output%fdsol(dimxin, dimnin, numsolsin), &
             output%modeshift(dimxin, dimnin), &
             output%modewidth(dimxin, dimnin), &
             output%sol(dimxin, dimnin, numsolsin), &
             output%solflu(dimxin, dimnin), &
             output%ntor(dimxin, dimnin), &
             output%distan(dimxin, dimnin), &
             output%kperp2(dimxin, dimnin), &
             output%kymaxETG(dimxin), &
             output%kymaxITG(dimxin))
  end subroutine allocate_primi_meth_0

  subroutine allocate_primi_meth_1(dimxin, dimnin, nionsin, numsolsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin, numsolsin
    type(qlk_primi_meth_1), intent(inout) :: output

    allocate(output%Lcircgne(dimxin, dimnin, numsolsin), &
             output%Lcircgni(dimxin, dimnin, nionsin, numsolsin), &
             output%Lcircgte(dimxin, dimnin, numsolsin), &
             output%Lcircgti(dimxin, dimnin, nionsin, numsolsin), &
             output%Lcircgui(dimxin, dimnin, nionsin, numsolsin), &
             output%Lcircce(dimxin, dimnin, numsolsin), &
             output%Lcircci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lpiegce(dimxin, dimnin, numsolsin), &
             output%Lpiegci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lpieggte(dimxin, dimnin, numsolsin), &
             output%Lpieggti(dimxin, dimnin, nionsin, numsolsin), &
             output%Lpieggui(dimxin, dimnin, nionsin, numsolsin), &
             output%Lpieggne(dimxin, dimnin, numsolsin), &
             output%Lpieggni(dimxin, dimnin, nionsin, numsolsin))
  end subroutine allocate_primi_meth_1

  subroutine allocate_primi_meth_2(dimxin, dimnin, nionsin, numsolsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin, numsolsin
    type(qlk_primi_meth_2), intent(inout) :: output

    allocate(output%Lecircce(dimxin, dimnin, numsolsin), &
             output%Lecircci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lecircgne(dimxin, dimnin, numsolsin), &
             output%Lecircgni(dimxin, dimnin, nionsin, numsolsin), &
             output%Lecircgte(dimxin, dimnin, numsolsin), &
             output%Lecircgti(dimxin, dimnin, nionsin, numsolsin), &
             output%Lecircgui(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepiegce(dimxin, dimnin, numsolsin), &
             output%Lepiegci(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepieggne(dimxin, dimnin, numsolsin), &
             output%Lepieggni(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepieggui(dimxin, dimnin, nionsin, numsolsin), &
             output%Lepieggte(dimxin, dimnin, numsolsin), &
             output%Lepieggti(dimxin, dimnin, nionsin, numsolsin))
  end subroutine allocate_primi_meth_2

  subroutine allocate_qlk_in_regular(dimxin, dimnin, nionsin, output)
    integer, intent(in) :: dimxin, dimnin, nionsin
    type(qlk_in_regular), intent(inout) :: output

    allocate(output%Ane(dimxin), &
             output%Ate(dimxin), &
             output%Aupar(dimxin), &
             output%Autor(dimxin), &
             output%Machpar(dimxin), &
             output%Machtor(dimxin), &
             output%x(dimxin), &
             output%Bo(dimxin), &
             output%gammaE(dimxin), &
             output%ne(dimxin), &
             output%q(dimxin), &
             output%Ro(dimxin), &
             output%Rmin(dimxin), &
             output%smag(dimxin), &
             output%Te(dimxin), &
             output%alpha(dimxin), &
             output%rho(dimxin), &

             output%anise(dimxin), &
             output%danisedr(dimxin), &

             output%kthetarhos(dimnin), &

             output%anis(dimxin, nionsin), &
             output%danisdr(dimxin, nionsin), &

             output%ion_type(dimxin, nionsin), &

             output%Ai(dimxin, nionsin), &
             output%Ani(dimxin, nionsin), &
             output%Ati(dimxin, nionsin), &
             output%Zi(dimxin, nionsin), &
             output%typei(dimxin, nionsin), &
             output%normni(dimxin, nionsin), &
             output%Ti(dimxin, nionsin))
  end subroutine allocate_qlk_in_regular

  subroutine allocate_qlk_in_newt(dimxin, dimnin, numsolsin, output)
    integer, intent(in) :: dimxin, dimnin, numsolsin
    type(qlk_in_newt), intent(inout) :: output

    allocate(output%oldsol(dimxin, dimnin, numsolsin), &
             output%oldfdsol(dimxin, dimnin, numsolsin))
  end subroutine allocate_qlk_in_newt

  subroutine zeroout_output_meth_0(output)
    type(qlk_output_meth_0), intent(inout) :: output

    output%pfe_cm = 0
    output%pfi_cm = 0
    output%efe_cm = 0
    output%efi_cm = 0
    output%vfi_cm = 0
    output%ecoefs = 0
    output%npol   = 0
    output%cftrans = 0
    output%phi = 0
    output%Zeff = 0
    output%Nustar = 0
  end subroutine zeroout_output_meth_0


  subroutine zeroout_output_meth_0_sep_0(output)
    type(qlk_output_meth_0_sep_0), intent(inout) :: output

    output%gam = 0
    output%ome = 0
    output%pfe = 0
    output%pfi = 0
    output%efe = 0
    output%efi = 0
    output%vfi = 0
    output%efeETG = 0
  end subroutine zeroout_output_meth_0_sep_0

  subroutine zeroout_output_meth_0_sep_1(output)
    type(qlk_output_meth_0_sep_1), intent(inout) :: output

    output%efeITG = 0
    output%efeTEM = 0
    output%efiITG = 0
    output%efiTEM = 0
    output%pfeITG = 0
    output%pfeTEM = 0
    output%pfiITG = 0
    output%pfiTEM = 0
    output%vfiITG = 0
    output%vfiTEM = 0
  end subroutine zeroout_output_meth_0_sep_1

  subroutine zeroout_output_meth_1(output)
    type(qlk_output_meth_1), intent(inout) :: output

    output%cke = 0
    output%cki = 0
  end subroutine zeroout_output_meth_1

  subroutine zeroout_output_meth_1_sep_0(output)
    type(qlk_output_meth_1_sep_0), intent(inout) :: output

    output%dfe = 0
    output%dfi = 0
    output%vte = 0
    output%vti = 0
    output%vce = 0
    output%vci = 0
    output%vri = 0
  end subroutine zeroout_output_meth_1_sep_0

  subroutine zeroout_output_meth_1_sep_1(output)
    type(qlk_output_meth_1_sep_1), intent(inout) :: output

    output%dfeITG = 0
    output%dfeTEM = 0
    output%dfiITG = 0
    output%dfiTEM = 0
    output%vriITG = 0
    output%vriTEM = 0
    output%vteITG = 0
    output%vteTEM = 0
    output%vtiITG = 0
    output%vtiTEM = 0
    output%vceITG = 0
    output%vceTEM = 0
    output%vciITG = 0
    output%vciTEM = 0
  end subroutine zeroout_output_meth_1_sep_1

  subroutine zeroout_output_meth_2(output)
    type(qlk_output_meth_2), intent(inout) :: output

    output%ceke = 0
    output%ceki = 0
  end subroutine zeroout_output_meth_2

  subroutine zeroout_output_meth_2_sep_0(output)
    type(qlk_output_meth_2_sep_0), intent(inout) :: output

    output%chiee = 0
    output%chiei = 0
    output%vece = 0
    output%veci = 0
    output%vene = 0
    output%veni = 0
    output%veri = 0
  end subroutine zeroout_output_meth_2_sep_0

  subroutine zeroout_output_meth_2_sep_1(output)
    type(qlk_output_meth_2_sep_1), intent(inout) :: output

    output%chieeITG = 0
    output%chieeTEM = 0
    output%chieiITG = 0
    output%chieiTEM = 0
    output%veceITG = 0
    output%veceTEM = 0
    output%veciITG = 0
    output%veciTEM = 0
    output%veneITG = 0
    output%veneTEM = 0
    output%veniITG = 0
    output%veniTEM = 0
    output%veriITG = 0
    output%veriTEM = 0
    output%chieeETG = 0
    output%veceETG = 0
    output%veneETG = 0
  end subroutine zeroout_output_meth_2_sep_1

  subroutine zeroout_primi_meth_0(output)
    type(qlk_primi_meth_0), intent(inout) :: output

    output%Lcirce = 0
    output%Lcirci = 0
    output%Lecirce = 0
    output%Lecirci = 0
    output%Lepiege = 0
    output%Lpiege = 0
    output%Lpiegi = 0
    output%Lepiegi = 0
    output%Lvcirci = 0
    output%Lvpiegi = 0
    output%fdsol = 0
    output%modeshift = 0
    output%modewidth = 0
    output%sol = 0
    output%solflu = 0
    output%ntor = 0
    output%distan = 0
    output%kperp2 = 0
    output%kymaxETG = 0
    output%kymaxITG = 0
  end subroutine zeroout_primi_meth_0

  subroutine zeroout_primi_meth_1(output)
    type(qlk_primi_meth_1), intent(inout) :: output

    output%Lcircgne = 0
    output%Lcircgni = 0
    output%Lcircgte = 0
    output%Lcircgti = 0
    output%Lcircgui = 0
    output%Lcircce = 0
    output%Lcircci = 0
    output%Lpiegce = 0
    output%Lpiegci = 0
    output%Lpieggte = 0
    output%Lpieggti = 0
    output%Lpieggui = 0
    output%Lpieggne = 0
    output%Lpieggni = 0
  end subroutine zeroout_primi_meth_1

  subroutine zeroout_primi_meth_2(output)
    type(qlk_primi_meth_2), intent(inout) :: output

    output%Lecircce = 0
    output%Lecircci = 0
    output%Lecircgne = 0
    output%Lecircgni = 0
    output%Lecircgte = 0
    output%Lecircgti = 0
    output%Lecircgui = 0
    output%Lepiegce = 0
    output%Lepiegci = 0
    output%Lepieggne = 0
    output%Lepieggni = 0
    output%Lepieggui = 0
    output%Lepieggte = 0
    output%Lepieggti = 0
  end subroutine zeroout_primi_meth_2

  subroutine zeroout_qlk_in_regular(output)
    type(qlk_in_regular), intent(inout) :: output

    output%Ane = 0
    output%Ate = 0
    output%Aupar = 0
    output%Autor = 0
    output%Machpar = 0
    output%Machtor = 0
    output%x = 0
    output%Bo = 0
    output%gammaE = 0
    output%ne = 0
    output%q = 0
    output%Ro = 0
    output%Rmin = 0
    output%smag = 0
    output%Te = 0
    output%alpha = 0
    output%rho = 0

    output%anise = 0
    output%danisedr = 0

    output%kthetarhos = 0

    output%anis = 0
    output%danisdr = 0

    output%ion_type = 0

    output%Ai = 0
    output%Ani = 0
    output%Ati = 0
    output%Zi = 0
    output%typei = 0
    output%normni = 0
    output%Ti = 0
  end subroutine zeroout_qlk_in_regular

  subroutine zeroout_qlk_in_newt(output)
    type(qlk_in_newt), intent(inout) :: output

    output%oldsol = 0
    output%oldfdsol = 0
  end subroutine zeroout_qlk_in_newt

END MODULE mod_make_io
