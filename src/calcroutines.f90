MODULE calcroutines
#include "preprocessor.inc"
  USE kind
  USE datcal
  USE datmat
  USE FLRterms !module contain functions defining all the FLR terms 
  USE mod_fluidsol !module which calculates the fluid growth rate and frequencies
  USE mod_contour !module which contains contour routines
  USE mod_make_io
  USE mod_fonct
  USE mod_integration, ONLY: integration_options, nag1d_options, integration, debug_msg_pnu
  USE QLflux
  USE asymmetry
  USE nanfilter

  IMPLICIT NONE

CONTAINS
  SUBROUTINE calc(p, nu)
    INTEGER, INTENT(IN) :: p,nu

    ! Variables for fluid solution
    REAL(kind=DBL) :: ana_gamma

    ! Variables for defining contour locations
    REAL(kind=DBL) :: ma, wpi, wpe
    COMPLEX(kind=DBL) :: om0
    COMPLEX(kind=DBL) :: C, Centre
    REAL(kind=DBL) :: L, Cshift_elmode, Cshift_ionmode

    ! Variables for collecting the solutions, and their status flags
    COMPLEX(kind=DBL), DIMENSION(numsols) :: soll, fdsoll, solltmp, fdsolltmp
    COMPLEX(kind=DBL) :: fonout,newsol,newfdsol
    REAL(kind=DBL),    DIMENSION(numsols) :: isol,rsol,ifdsol,rfdsol,tmpsol
    INTEGER :: NN, i,j, ifailloc,minlocind, dim_out, dim_in
    REAL(KIND=DBL), DIMENSION(1) :: int_lbound, int_ubound, int_res, int_err
    REAL(KIND=DBL), DIMENSION(0) :: fdata
    INTEGER, DIMENSION(1) :: minloci
    LOGICAL :: issol
    REAL(kind=DBL) :: kteta,maxdia, centerwidth
    REAL(KIND=DBL) :: maxklam, minklam
    class(integration_options), allocatable :: opts

    LOGICAL :: point_outside_contour

    !INITIALIZATION OF VARIABLES************************

    !IF ( ( p /= 1 ) .OR. ( nu /= 1)) THEN
    !STOP
    !ENDIF

    !Initialize prefactor
    nwg = ntor(p,nu)*wg(p)

    !Initialize distance between rational surfaces
    d = ABS(1./(ntor(p,nu)*(epsD+ABS(qprim(p)))))
    distan(p,nu) = d !save for output

    !Initialize variables used in calcfonctp and FLR module
    kteta     = ntor(p,nu)*ktetasn(p)
    ktetaRhoe = kteta*Rhoe(p)

    ktetaRhoi(:) = kteta*Rhoi(p,:)

    pnFLR = p !save scan index for use in FLRterms module

    !Initialize variables for calculating the mode width

    Joe2 = BESEI0(ktetaRhoe * ktetaRhoe)
    Jobane2 = BESEI0 (kteta*kteta*de(p)*de(p))

    DO i = 1,nions
       Joi2(i) = BESEI0 (ktetaRhoi(i) * ktetaRhoi(i))
       Jobani2(i) = BESEI0 (kteta*kteta*di(p,i)*di(p,i))
    ENDDO

    normkr = normkrfac*ntor(p,nu) !sets boundary in kr integrations

    !**************************************************

    !CALCULATION PHASE. TWO OPTIONS: calculate from scratch, or start directly from newton solver with inputs from a previous run

    !Calculate the fluid frequency and growth rates. Still testing phase where various approaches tried

    ! Set the ktheta dependent width tuning factors. The 0.075 prefactor was chosen on the base of optimizing a scan
    widthtuneITG = (0.075/kthetarhos(nu));
    widthtuneETG = (0.075/( kthetarhos(nu)*SQRT(me/mi(p,1)) ) );

    ! Calculates growth rate based on analytical fluid interchange / slab formulation. in units of nwg
    CALL ana_fluidsol(p,nu,ana_gamma)! with or without rotation ana_gamma used only for contour limit

    !diamagnetic frequencies used to set real part of "analytical" fluid solution (used for contour)
    wpi = wg(p)*Tix(p,1)/Zi(p,1)* (ABS(Ati(p,1)) + ABS(Ani(p,1)))  !ABS is used in case of hollow profiles where otherwise cancellations can lead to small wpi/wpe and missed eigenvalues
    wpe = wg(p)*Tex(p)/Ze * (ABS(Ate(p)) + ABS(Ane(p)))

    ! Set maximum absolute diamagnetic frequency for limit of contour search
    IF ( ABS(wpi) > ABS(wpe)) THEN
       maxdia = wpi
    ELSE
       maxdia=wpe
    ENDIF

    IF ( ETG_flag(nu) .EQV. .FALSE. ) THEN !ITG/TEM case
       ana_solflu(p,nu) = CMPLX(maxdia/wg(p),ana_gamma)
    ELSE !ETG case
       ana_solflu(p,nu) = CMPLX(wpe/wg(p),ana_gamma)
    ENDIF

    solflu(p,nu) = omeflu

    IF (ETG_flag(nu) .EQV. .TRUE.) THEN
       CALL jon_fluidsol_ele(p,nu)
    ELSEIF (rotflagarray(p) == 0) THEN 
       IF (rot_flag == 2) THEN
          CALL jon_fluidsol(p,nu,Machiorig(p,:),Auiorig(p,:),gammaEorig(p)) ! run in any case for QL momentum transport if rot_flag=2
          mwidth_rot=mwidth
          mshift_rot=mshift
       ENDIF
       CALL jon_fluidsol_norot(p,nu)
    ELSEIF (rotflagarray(p) ==1)  THEN
       IF (rot_flag == 2) THEN
          CALL jon_fluidsol(p,nu,Machiorig(p,:),Auiorig(p,:),gammaEorig(p)) ! run in any case for QL momentum transport if rot_flag=2
          mwidth_rot=mwidth
          mshift_rot=mshift

          Machi(p,:) = Machimod(p,:)
          Aui(p,:) = Auimod(p,:)
          gammaE(p) = gammaEmod(p)
          CALL jon_fluidsol(p,nu,Machimod(p,:),Auimod(p,:),gammaEmod(p)) ! run with modified profiles
       ELSE
          CALL jon_fluidsol(p,nu,Machi(p,:),Aui(p,:),gammaE(p))
       ENDIF
    ENDIF
  
    jon_solflu(p,nu) = omeflu / (ntor(p,nu)*wg(p))
    jon_modewidth(p,nu) = mwidth 
    jon_modeshift(p,nu) = mshift 

    !Write out fluid solution

    !Calculate old mode width and shift from before rotation times
    mshift = 0.0 

    ! Keeping old results the same

    solflu(p,nu) = ana_solflu(p,nu)

   
    !! choose which one to actually keep for use in the code
    modewidth(p,nu) = jon_modewidth(p,nu)
    mwidth = modewidth(p,nu)
    modeshift(p,nu) = jon_modeshift(p,nu)
    mshift = modeshift(p,nu); 

    !Calculate the width corresponding to |phi(x)| (in case eigenfunction is complex)
    widthhat = ABS(mwidth)**2 / SQRT(REAL(mwidth**2))

    !    mshift=CMPLX(-0.1E-02,0.2E-02)
    !    mshift=CMPLX(0.1E-02,-0.2E-02)

    !    mshift=CMPLX(REAL(mshift)/100,AIMAG(mshift))

    !    mshift=CMPLX(0.,0.)

    ! Calculate flux-surface-averaging of poloidal asymmetry coefficients including eigenmode
    CALL makeecoefsgau(p,nu)

    !! Carry out FLR calculation with Bessel function integrations over kr
    IF (rotflagarray(p) == 1) THEN
       CALL makeFLRtermsrot(p,nu)
       FLRep(p,nu) = Joe2p !output
       FLRip(p,nu,:) = Joi2p(:)
    ELSE
       CALL makeFLRterms(p,nu)
       FLRep(p,nu) = Joe2p !output
       FLRip(p,nu,:) = Joi2p(:)
    ENDIF

    !used to pass variables in <Vpar^n> calculations below
    plam = p
    nulam = nu
    maxklam =   ABS(pi/(distan(p,nu)*normkr)) 
    minklam = - maxklam

    alamnorm = fc(p) !to be consistent with passing particle fraction

    dim_out = 1
    dim_in = 1
    int_lbound = 0
    int_ubound = 1 - 2. * epsilon(p)
    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts)
    CASE (1)
      allocate(hcubature_options::opts)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT
    SELECT TYPE (opts)
    CLASS IS (nag1d_options)
      opts%reqRelError = relacc1
      opts%maxEval = lw
    CLASS IS (hcubature_options)
      opts%reqRelError = relacc1
      opts%maxEval = lw
      opts%minEval = 0
      opts%norm = 1
      opts%reqAbsError = absacc1
    END SELECT


    ifailloc = 1
    CALL integration(dim_out, alam1int, dim_in, int_lbound, int_ubound, opts, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('alam1int', ifailloc, verbose, p, nu)

    alam1 = int_res(1) / alamnorm

    ifailloc = 1
    CALL integration(dim_out, alam2int, dim_in, int_lbound, int_ubound, opts, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('alam2int', ifailloc, verbose, p, nu)

    alam2 = int_res(1) / alamnorm

    ifailloc = 1
    CALL integration(dim_out, alam3int, dim_in, int_lbound, int_ubound, opts, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('alam3int', ifailloc, verbose, p, nu)

    alam3 = int_res(1) / alamnorm

    ifailloc = 1
    CALL integration(dim_out, alam4int, dim_in, int_lbound, int_ubound, opts, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('alam4int', ifailloc, verbose, p, nu)

    alam4 = int_res(1) / alamnorm

    ifailloc = 1
    CALL integration(dim_out, alam5int, dim_in, int_lbound, int_ubound, opts, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('alam5int', ifailloc, verbose, p, nu)

    alam5 = int_res(1) / alamnorm

    !Set the transit frequency
    !    qRd = qx(p)*Ro(p)*d*SQRT(3._DBL)
    qRd = qx(p)*Ro(p)*d*1./alam1

    Athe=widthhat*cthe(p)/qRd 
    Athi(:)=widthhat*cthi(p,:)/qRd

    !Set the bounce frequency
    omega2bar = pi/2.*SQRT(x(p)*Rmin(p)/(2.*Ro(p)))

    !Check if in L-mode edge regime and set parameters for appropriate contour modification
    edgemode(p,nu) = .FALSE. ! initialize edgemode boolean
    Cshift_elmode = 1.
    Cshift_ionmode = 1.
    IF ( (x(p) > 0.9) .AND. ( (Ate(p) + Ane(p)) > 30.)) THEN !contour ceiling correction for possible highly driven edge region (electron modes)
       Cshift_elmode = 3. 
       edgemode(p,nu) = .TRUE.
    ENDIF

    IF ( (x(p) > 0.9) .AND. ( (Ati(p,1) + Ani(p,1)) > 30.))  THEN !contour ceiling correction for possible highly driven edge region (electron modes)
       Cshift_ionmode = 3.
       edgemode(p,nu) = .TRUE.
    ENDIF

    ! maintain same ceiling for both types of mode for now, but provide flexibility for future dev
    Cshift_ionmode = MAX(Cshift_elmode, Cshift_ionmode)
    Cshift_elmode = Cshift_ionmode 
    
    !Initialize ma value used in contour choice based on fluid solution
    ma = MAX(AIMAG(solflu(p,nu)),10.*Tex(p)/8.)  ! The Tex/8 term is to scale the fallback maximum of the contour, compared to Te=8 where testing was carried out
    om0 = ma*ci
    C    = om0 *0.5 

    !Set maximum bound of contour locations based on diamagnetic frequency
    IF (edgemode(p,nu) .EQV. .FALSE.) THEN
       ommax(p,nu) = om0 + REAL(solflu(p,nu))
    ELSE
       ommax(p,nu) = om0*MAX(Cshift_ionmode,Cshift_elmode) + REAL(solflu(p,nu))
    ENDIF
    omegmax=ommax(p,nu) ! used in calculsol for maximum boundary of allowed solution

    IF (runcounter /=0) THEN !launch newton solver from previous run
       DO j=1,numsols
          IF ( ABS(AIMAG(oldsol(p,nu,j))) < epsD ) THEN !There was no solution before, so stay with 0
             soll(j)   = (0.,0.)
             fdsoll(j) = (0.,0.)
          ELSE

             IF ( (gkw_is_nan(AIMAG(oldsol(p,nu,j)))) .OR. (gkw_is_nan(REAL(oldsol(p,nu,j))))) THEN
                IF (verbose .EQV. .TRUE.) THEN 
                   WRITE(stderr,'(A,I7,A,I2,A)') &
                        'Jump to Newton phase: old solution had a NaN (how did that happen)! ' &
                        // 'Skipping solution. (p,nu)=(',p,',',nu,')'
                ENDIF
                soll(j)   = (0.,0.)
                fdsoll(j)   = (0.,0.)
                CYCLE
             ENDIF

             IF ((AIMAG(oldsol(p,nu,j)) < 0. ) .OR. outside_contour(p, nu, j, oldsol, ommax)) THEN
                IF (verbose .EQV. .TRUE.) THEN
                   WRITE(stderr,'(A,I7,A,I2,A)') &
                        'Jump to Newton phase: old solution outside of allowed contour range (how did that happen?) ' &
                        // 'Skipping solution. (p,nu)=(',p,',',nu,')'
                ENDIF
                soll(j)   = (0.,0.)
                fdsoll(j)   = (0.,0.)
                CYCLE
             ENDIF

             CALL calcfonct(p, nu, oldsol(p,nu,j), fonout) !Get new distance from solution from old solution and new input parameters
             CALL newton(p, nu, oldsol(p,nu,j), fonout, newsol, newfdsol) !Now refine the solution to the new solution
             !CALL broyden(p, nu, oldsol(p,nu,j), fonout, newsol, newfdsol) !Now refine the solution to the new solution

             !If, after the Newton refinement, the solution is too coarse, or outside the preset boundaries, then we abandon the solution
             IF (ABS(newfdsol)> 0.5  .OR. &
                  ABS(AIMAG(newsol))>(ABS(AIMAG(omegmax))) .OR. &
                  ABS(REAL(newsol)) > (ABS(REAL(omegmax)))) THEN
                soll(j)   = (0.,0.)
                fdsoll(j) = (0.,0.)
                IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I3,A,I3,A,G15.7,A,2G15.7,A)') &
                     'Jumped straight to Newton for p=',p,' nu=,',nu, &
                    ', ABS(newfdsol)=',ABS(newfdsol),' with newsol=',newsol,' and the solution was abandoned'
                !If the solution is stable, then the solution is zeroed out. We can't have stable solutions since we haven't included the analytic continuation
             ELSEIF (AIMAG(newsol)<0.) THEN
                soll(j)   = (0.,0.)
                fdsoll(j) = (0.,0.)
                IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I3,A,I3,A)') 'For p=',p,' nu=,',nu, &
                     ' , a spurious stable solution was found and zeroed out'
             ELSE
                soll(j) = newsol
                fdsoll(j) = newfdsol
             END IF
          ENDIF
       ENDDO

    ELSE !launch contour solutions

       !CHOICE OF THE CENTER OF THE FIRST CONTOUR in which the kinetic solution is found.
       !The imaginary coordinate of the center is half the max fluid growth rate
       !In this manner, the upper limit of the kinetic growth rate is the (overestimated)
       !fluid growth rate. The lower limit of the "fluid growth rate" is set at 10, 
       !in case any fluid solution roots were not found


       !Find the location on the real axis for the contours. the contours will be
       !placed in multiples of rint on the real axis
       !For now this is disabled due to low values of real(C) leading to much contour overlap
       !CALL limx(C,rint)

       !This line below has led to a critical speedup. The contour overlap is optimized
       !and also the centerpoint in the imaginary plane has been slightly reduced from the 
       !fluid solution

!!$       !DEFAULT
!!$       C=C/1.5; rint=ABS(REAL(solflu(p,nu)))/5. !The divisor sets the number of contours tried per solution NEW

       !standard contour setting
       C=C/1.5; rint=ABS(REAL(solflu(p,nu)))/10. !The divisor sets the number of contours tried per solution

       !!FOR TESTING ONLY
       !    C=C/1.5; rint=AIMAG(C)*0.8 !The overlap factor for rint sets the degree of overlap between contours   
       !    C=C/3; rint=AIMAG(C)*1.6 !The overlap factor for rint sets the degree of overlap between contours

       !DEBUGGING*****
       !WRITE(stdout,*) 'Rint and C are:', rint, AIMAG(C)
       !**************

       !Variable initialization
       NN = 0 !number of solutions found
       soll = (0.,0.) !solution array
       fdsoll = (0.,0.) !value of fonct(soll) (should be close to zero, as soll are roots)

       ! If the mode distance is greater than the gradient lengths,
       ! then the local limit is not satisfied and we do not calculate
       IF ( d > Ro(p)/MAX( ABS( Ane(p)),ABS(Ate(p)) ) ) THEN
          fdsoll = (-1.,-1.) !Flag that the local limit was not satisfied
          IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') &
               'Local approximation not satisfied. Skipping solution. (p,nu)=(',p,',',nu,')'
       ELSE 
          L = 1.0 !begin the calculation
          ! LAUNCH VARIOUS CONTOURS SCANNING THE REAL AXIS 
          ! The loop exits when the contour center extends
          ! beyond the defined maximum on the real axis

          DO WHILE ((L+0.5)*rint < ABS(REAL(omegmax)))
             !Loop over both positive and negative frequencies in solution search

             IF ((MPI_Wtime()-calltimeinit) > timeout) THEN
                timeoutflag = .TRUE.
                EXIT
             ENDIF

             !Flags are in place in case we are certain that solutions found only in one side
             DO i = 0,2              
                IF ((MPI_Wtime()-calltimeinit) > timeout) THEN
                   timeoutflag = .TRUE.
                   EXIT
                ENDIF
                IF ( (i==0) .AND. (L>1.) ) CYCLE  !we only carry out a narrow small contour 
                IF ( ( (onlyelec .EQV. .TRUE.) .OR. (kthetarhos(nu) > 2. ) ) .AND. (i == 1) ) CYCLE  !Skips ions also for ETG scales
                IF ( (onlyion .EQV. .TRUE.) .AND. (i == 2) ) CYCLE
                !Define center of this contour

                IF (i==0) THEN !Set narrow center contour (numerically more difficult near real axis, so would rather avoid this region for a solution contour)
                   IF (edgemode(p,nu) .EQV. .FALSE.) THEN
                      Centre = C
                   ELSE
                      Centre = C*MAX(Cshift_elmode,Cshift_ionmode)
                   ENDIF
                   centerwidth = centerwidth8*Tex(p)/8.
                   rint = centerwidth*1.5
                ELSEIF (i==1) THEN
                   rint=ABS(REAL(solflu(p,nu)))/5.
                   IF (edgemode(p,nu) .EQV. .FALSE.) THEN
                      Centre = -(L * rint + centerwidth*(1-centeroverlapfac))+ C !Set contour center, with slight overlap with center contour if L=1
                   ELSE
                      Centre = -(L * rint + centerwidth*(1-centeroverlapfac))+ C*Cshift_ionmode !Set contour center, with slight overlap with center contour if L=1
                   ENDIF
                ELSEIF (i==2) THEN
                   rint=ABS(REAL(solflu(p,nu)))/5.
                   IF (edgemode(p,nu) .EQV. .FALSE.) THEN
                      Centre = (L * rint + centerwidth*(1-centeroverlapfac))+ C !Set contour center, with slight overlap with center contour if L=1
                   ELSE
                      Centre = (L * rint + centerwidth*(1-centeroverlapfac))+ C*Cshift_elmode !Set contour center, with slight overlap with center contour if L=1
                   ENDIF
                ENDIF

                !DEBUGGING CODE
                !WRITE(stdout,'(2G13.5,A,I0,A,F7.3,A,F5.2)') Centre, ' i=', INT(i), ' L=',L,' Rint = ',rint

                !Solutions are now saught inside this specific contour
                !The vast bulk of QuaLiKiz computation is within this procedure

                CALL calculsol(p, nu, Centre, NN, solltmp, fdsolltmp)

                !Solution cleanup: all solutions within soldel*100 percent. soldel found in datcal
                !of a previously found solution (from another contour) is set to zero             
                IF (NN > 0) THEN
                   DO j = 1,numsols
                      IF (ABS(soll(j)) > epsD) THEN !only compare to non-zero solutions in soll
                         WHERE (ABS(solltmp-soll(j))/ABS(soll(j)) < soldel)                       
                            solltmp = (0.,0.)
                            fdsolltmp = (0.,0.)
                         END WHERE
                      ENDIF
                   ENDDO
                ENDIF

                DO j=1,numsols 
                  point_outside_contour = (ABS(AIMAG(solltmp(j))) > ABS(AIMAG(ommax(p,nu)))) .OR. &
                       (ABS(REAL(solltmp(j))) > ABS(REAL(ommax(p,nu))))
                   IF ( (AIMAG(solltmp(j)) < 0. ) .OR. point_outside_contour) THEN
                      IF (verbose .EQV. .TRUE.) THEN 
                         WRITE(stderr,'(A,I7,A,I2,A)') 'Solution found but outside of allowed contour range. ' &
                              // 'Skipping solution. (p,nu)=(',p,',',nu,')'
                      ENDIF
                      solltmp(j) = (0.,0.)
                      fdsolltmp(j) = (0.,0.)
                   ENDIF
                ENDDO

                ! If any solutions survive, they are saved together with any previous solutions
                ! If numsols is not high enough to save all solutions, then the largest growth rates are saved first
                DO j=1,numsols 
                   IF ( ABS(solltmp(j)) > epsD ) THEN
                      minloci = MINLOC(AIMAG(soll))
                      minlocind = minloci(1)
                      IF ( (ABS(soll(minlocind)) > epsD) .AND. (verbose .EQV. .TRUE.) ) THEN
                         WRITE(stdout,'(A,I2,A,I2,A)') 'Valid instability discarded due to limited number of numsols ' &
                              // 'at (p,nu)=(',p,',',nu,')'
                      ENDIF
                      IF (AIMAG(solltmp(j)) > AIMAG(soll(minlocind))) THEN
                         soll(minlocind)   = solltmp(j)
                         fdsoll(minlocind) = fdsolltmp(j)                        
                      ENDIF
                   ENDIF
                ENDDO
             ENDDO
             !Shift the contours for the next iteration
             L = L+2. - overlapfac

          ENDDO
       ENDIF
    ENDIF

    ! The solution for this (p,nu) pair is now sorted and saved

    !Reorder (descending) the solutions
    isol = AIMAG(soll)
    rsol = REAL(soll)
    ifdsol = AIMAG(fdsoll)
    rfdsol = REAL(fdsoll)

    tmpsol = AIMAG(soll)
    CALL dsort(tmpsol,isol,numsols,-2)
    tmpsol = AIMAG(soll)
    CALL dsort(tmpsol,rsol,numsols,-2)
    tmpsol = AIMAG(soll)
    CALL dsort(tmpsol,ifdsol,numsols,-2)
    tmpsol = AIMAG(soll)
    CALL dsort(tmpsol,rfdsol,numsols,-2)

    DO j=1,numsols
       sol(p,nu,j) = CMPLX(rsol(j),isol(j)) !output array
       fdsol(p,nu,j) = CMPLX(rfdsol(j),ifdsol(j)) !output array

       IF ( soll(j) /= (0.,0.) ) THEN
          !calculate integrals for quasilinear flux. Small percentage of total calculation

          IF ( (AIMAG(sol(p,nu,j)) < 0. ) .OR. outside_contour(p, nu, j, sol, ommax)) THEN
             IF (verbose .EQV. .TRUE.) THEN 
               WRITE(stderr,'(A,I7,A,I2,A)') 'Before makeQLflux: solution outside of allowed contour range (how did that happen?)'&
                     // 'Skipping QL integrals. (p,nu)=(',p,',',nu,')'
             ENDIF
             CYCLE
          ENDIF
          IF (timeoutflag .EQV. .FALSE.) THEN
             CALL make_QLflux(p,nu,sol(p,nu,j))
          ENDIF

          issol = .TRUE.
       ELSE
          issol = .FALSE.
       ENDIF
       CALL save_qlfunc(p,nu,j,issol)
       !Important IF statement for integrated modelling applications
       !If previously we had a solution, and now it's been zeroed out, we keep old solution to maintain search in the next run. This is important if we're near
       !threshold and we fluctuate between stability and instability. If we don't have this step below, then once we're stable we never recover the instability
       IF (ALLOCATED(oldsol)) THEN
          IF ( (runcounter /= 0) .AND. ( AIMAG(sol(p,nu,j)) < epsD ) .AND. ( AIMAG(oldsol(p,nu,j)) > epsD ) ) THEN
             sol(p,nu,j) = oldsol(p,nu,j) 
          ENDIF
       ENDIF
    ENDDO

    !Save growth rates to output array (normalized to nwg)
    gamma(p,nu,:) = sol(p,nu,:)

  END SUBROUTINE calc

  SUBROUTINE calculsol( p, nu, Centre, NN, soll, fdsoll)  
    ! -------------------------------------------------------------------
    ! Finds the zeros of the function "fonct" within the given contour
    ! B. Davies, J. Compt. Phys. 66, 36 (1986)
    ! -------------------------------------------------------------------
    INTEGER, INTENT(IN) :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN) :: Centre 
    INTEGER,               INTENT(OUT) :: NN
    COMPLEX(KIND=DBL), DIMENSION(numsols), INTENT(OUT) :: soll, fdsoll

    ! Local variables
    INTEGER :: i, j, k, ind
    INTEGER :: M ! Local number of contour segments
    INTEGER :: ndeg,NNbck

    REAL(KIND=DBL)    :: maxgradanglespi, maxdifalphan
    REAL(KIND=DBL)    :: Nenv, thetatemp, realintans, imagintans, mincont
    COMPLEX(KIND=DBL) :: omega, solli, fdsolli, nsolli, nfdsolli
    COMPLEX(KIND=DBL) :: fonx, foncttemp, omtemp, varztemp

    COMPLEX(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: fonct, fex, om, om2, varz
    COMPLEX(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: foncti, omi
    COMPLEX(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: Sint, A, csolint
    COMPLEX(KIND=DBL), DIMENSION(:,:), ALLOCATABLE :: exprn

    REAL(KIND=DBL), DIMENSION(:,:), ALLOCATABLE :: solint
    REAL(KIND=DBL), DIMENSION(:,:), ALLOCATABLE :: Areal
    REAL(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: thetai
    REAL(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: ww, exprnreal, exprnimag
    REAL(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: difalphan
    REAL(KIND=DBL), DIMENSION(:),   ALLOCATABLE :: theta, alpha, alphan, alphaex, alphanex
    LOGICAL :: anomflag, omega_outside_contour
    INTEGER :: ifailloc

    soll(:)=0.
    fdsoll(:)=0.
    M=MM
    mincont = mincont8*Tex(p)/8. ! Scale minimum value in imaginary axis of contour. Was tested at 8 keV
    
    !Allocate arrays with dimension of the number of contour bits
    ALLOCATE (theta(M)) 
    ALLOCATE (varz(M)) 
    ALLOCATE (om(M)) 
    ALLOCATE (om2(M)) 
    ALLOCATE (fonct(M)) !The main player - the dispersion function!
    ALLOCATE (alpha(M)) !fonct angles
    ALLOCATE (alphan(M)) !unwrapped fonct angles
    ALLOCATE (difalphan(M-1)) !unwrapped fonct angle differences

    ! DEBUGGING
!!$    DO i=1,M
!!$       ! We follow the contour defined by the variable "om"
!!$       theta(i)    = 2.*pi*REAL(i-1)/REAL(M-1)
!!$       varz(i)     = EXP(ci*theta(i))
!!$       CALL squircle(Centre, varz(i), omega, rint, mincont, verbose)
!!$       om(i) = omega
!!$    ENDDO
!!$
!!$    IF ((p == 1) .AND. (nu == 1)) THEN
!!$       INQUIRE(file="rom.dat", exist=exist)
!!$       IF (exist) THEN
!!$          OPEN(700, file="rom.dat", status="old", position="append", action="write")
!!$       ELSE
!!$          OPEN(700, file="rom.dat", status="new", action="write")
!!$       END IF
!!$
!!$       INQUIRE(file="iom.dat", exist=exist)
!!$       IF (exist) THEN
!!$          OPEN(701, file="iom.dat", status="old", position="append", action="write")
!!$       ELSE
!!$          OPEN(701, file="iom.dat", status="new", action="write")
!!$       END IF
!!$
!!$       WRITE(700,'(17G15.7)') (REAL(om(i)),i=1,M) ; CLOSE(700)
!!$       WRITE(701,'(17G15.7)') (AIMAG(om(i)),i=1,M) ; CLOSE(701)
!!$
!!$    ENDIF
    fonct(:)=0.
    DO i=1,M
       ! We follow the contour defined by the variable "om"
       theta(i)    = 2.*pi*REAL(i-1)/REAL(M-1)
       varz(i)     = EXP(ci*theta(i))
       CALL squircle(Centre, varz(i), omega, rint, mincont, verbose)
       om(i) = omega
       ! Calculate the kinetic response at each point on the contour
       ! The root finder part of the code can be tested on a simpler case.
       ! For example:
       ! fonct(i) = (om(i)-(3.+2.*ci))*(om(i)-(-2.+4.*ci))
       omega_outside_contour = (ABS(AIMAG(omega)) > ABS(2.*AIMAG(ommax(p,nu)))) .OR. &
            (ABS(REAL(omega)) > ABS(REAL(2.*ommax(p,nu))))
       IF ((AIMAG(omega) < 0. ) .OR. omega_outside_contour) THEN
          IF (verbose .EQV. .TRUE.) THEN 
             WRITE(stderr,'(A,2G15.7,A,I7,A,I2,A)') 'In contours: omega outside of allowed contour range (how did that happen?) ' &
                  // 'Skipping solution. Omega=,',omega,'. (p,nu)=(',p,',',nu,')'
          ENDIF
          anomflag = .TRUE.
          fonct(i) = 0. 
       ELSE
          anomflag = .FALSE.
          CALL calcfonct(p, nu, omega, fonx)
          !test timeout
          IF ((MPI_Wtime()-calltimeinit) > timeout) THEN
             timeoutflag = .TRUE.
             EXIT
          ENDIF

          fonct(i) = fonx
       ENDIF

    END DO
    DEALLOCATE (varz)

    ! The function angle is calculated at this point on the contour
    alpha(:) = ATAN2(AIMAG(fonct),REAL(fonct))
    ! The angle outputs are all placed within the same -pi +pi interval
    CALL unwrap( alpha, M, alphan) 
    ! The differences in angle are calculated
    DO i=1,(M-1)
       difalphan(i) = ABS ( alphan(i+1) - alphan(i) )
    END DO
    ! The maximum jump is found
    maxdifalphan = MAXVAL ( difalphan )
    maxgradanglespi = maxdifalphan / pi ! Maximum angle is normalized by pi

    ! Refinement of the contour segmentation. 
    ! The refinement stops once the maximum angle is less than maxangle,
    ! or if the number of segments is above maxM (both defined parameters in datcal)
    DO WHILE ( ( maxdifalphan > maxangle  ) .AND. ( M < maxM ) .AND. (anomflag .EQV. .FALSE.) )
       ! Leave loop if timed out
       IF ((MPI_Wtime()-calltimeinit) > timeout) THEN
          timeoutflag = .TRUE.
          EXIT
       ENDIF
       !Reallocate and repopulate more refined arrays
       ALLOCATE (thetai(M)) 
       ALLOCATE (omi(M)) 
       ALLOCATE (foncti(M)) 

       foncti = fonct
       omi    = om
       thetai = theta

       DEALLOCATE (theta) 
       DEALLOCATE (om) 
       DEALLOCATE (fonct) 

       ALLOCATE (theta(2*M)) 
       ALLOCATE (om(2*M)) 
       ALLOCATE (fonct(2*M)) 
       !Fill in new points in more refined array 
       DO i = 1, M
          !Find new median values of theta and the frequency on the complex plane
          thetatemp = thetai(i) + ( thetai(2) - thetai(1) ) / 2.
          varztemp  = EXP(ci * thetatemp)
          CALL squircle(Centre , varztemp, omtemp, rint, mincont, verbose)
          !New values for refined contour calculated


          omega_outside_contour = (ABS(AIMAG(omtemp)) > ABS(2.*AIMAG(ommax(p,nu)))) .OR. &
               (ABS(REAL(omtemp)) > ABS(REAL(2.*ommax(p,nu))))
          IF ((AIMAG(omtemp) < 0. ) .OR. omega_outside_contour) THEN
             IF (verbose .EQV. .TRUE.) THEN 
                WRITE(stderr,'(A,2G15.7,A,I7,A,I2,A)') 'In refined contours: omega outside of allowed contour range ' &
                     // '(how did that happen?) Skipping solution. Omega=,',omega,'. (p,nu)=(',p,',',nu,')'
             ENDIF
             anomflag = .TRUE.
             foncttemp = 0. 
          ELSE
             CALL calcfonct (p, nu, omtemp, foncttemp)
          ENDIF

          ind = 2*i
          !Refined arrays corresponding to the refined contour are populated
          fonct(ind-1) = foncti(i)
          fonct(ind)   = foncttemp
          theta(ind-1) = thetai(i)
          theta(ind)   = thetatemp
          om(ind-1)    = omi(i)
          om(ind)      = omtemp
       END DO

       DEALLOCATE (thetai) 
       DEALLOCATE (omi) 
       DEALLOCATE (foncti) 

       M=2*M

       !DEBUGGING
       IF (M==maxM) THEN
          IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,I3,A,I7,A,I2)") 'Warning, maxM reached! M=',M,' for p=',p,' and nu=',nu
          !WRITE(stdout,*) alphan
       ENDIF

       DEALLOCATE (alpha)
       DEALLOCATE (alphan)
       DEALLOCATE (difalphan)

       ALLOCATE (alpha(M))
       ALLOCATE (alphan(M)) 
       ALLOCATE (difalphan(M-1)) 

       !Test if the new contour is sufficiently refined
       alpha(:) = ATAN2(AIMAG(fonct),REAL(fonct))
       CALL unwrap( alpha, M, alphan) 

       DO i=1,(M-1)
          difalphan(i)    = ABS ( alphan(i+1) - alphan(i) )
       END DO

       !DEBUGGING
       !IF (M==maxM) THEN
       !OPEN(unit=700, file="difalphan.dat", action="write", status="replace")
       !WRITE(700,'(G15.7)') (difalphan(i),i=1,M-1) ; CLOSE(700)
       !OPEN(unit=700, file="rom.dat", action="write", status="replace")
       !WRITE(700,'(G15.7)') (REAL(om(i)),i=1,M) ; CLOSE(700)
       !OPEN(unit=700, file="iom.dat", action="write", status="replace")
       !WRITE(700,'(G15.7)') (AIMAG(om(i)),i=1,M) ; CLOSE(700)
       !STOP
       !ENDIF

       !The new maximum angle jump is calculated
       maxdifalphan = MAXVAL ( difalphan )
       maxgradanglespi = maxdifalphan / pi

       Nenv = ABS( (alphan(M) - alphan(1) ) / 2. / pi )
       !test timeout
       IF ((MPI_Wtime()-calltimeinit) > timeout) THEN
          anomflag = .TRUE.
       ENDIF

       !DEBUG CODE
       !WRITE(stdout,*) 'M=',M,'and Nenv=',Nenv

       !In most cases, if Nenv isn't near one after the first iteration then
       !the contour is going over a problematic region where the phase jumps back and forth
       !This solution is then abandoned. This choice saves typically ~20-30% computation time
       IF (Nenv < 0.5) anomflag=.TRUE.

    END DO
    DEALLOCATE (difalphan) 

    !Count the number of zeros found in the contour
    Nenv = ABS( (alphan(M) - alphan(1) ) / 2. / pi )

    !Abandon solution of D(omega)=0 anywhere on contour
    IF (ANY(ABS(fonct)<epsD)) THEN
       IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I2,A,I2,A)') 'Main contour phase: contour had D(omega)=0! Skipping solution. ' &
       // '(p,nu)=(',p,',',nu,')'
       anomflag = .TRUE.
    ENDIF

    IF (anomflag .EQV. .TRUE.) Nenv = 0.
    NN = NINT( Nenv ) 

    !DEBUG CODE********************
!!$    WRITE(stdout,*) 'For p=',p,'and nu= ',nu,', final M = ', M
!!$    WRITE(stdout,*) 'Nenv =', Nenv
    !******************************

    !If, despite refining the segments, the angle jumps are too high,
    !and also the phase is not close to an integer, then the contour was problematic
    !and the exact solution is not sought out
    IF ( (maxgradanglespi > maxangle) .AND. ( ABS(Nenv-NN) > 0.01 ) )  THEN 
       Nsolrat = Nsolrat+NN; NN = 0
    END IF

    !If there is more than one solution but the phase is not close to an integer, drop the last solution
    IF ( (NN > 1) .AND. (ABS(Nenv-NN)>0.1) ) THEN
       NN=FLOOR(Nenv)
       IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Main contour phase: solutions > 1 ' &
            // 'and partial integer found in single contour! Flooring solution down to lower integer. (p,nu)=(',p,',',nu,')'
    ENDIF

    !If somehow NN>numsols, floor NN to numsols
    IF ( (NN > numsols) ) THEN
       NN=numsols
       IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Main contour phase: solutions > numsols in single contour! ' &
            // 'Flooring solutions down to numsols. (p,nu)=(',p,',',nu,')'
    ENDIF
    
    IF (NN == 0) THEN
       ! No solutions sought for!
    ELSE
       ! There are solutions and they will be found with the Davies method
       ALLOCATE ( fex(M) )
       ALLOCATE ( exprn(M,NN) )

       !Modified dispersion relation function. This is done to remove the branch cut when taking
       !the logarithm of fex. The values of the Sn 'argument principle' integrals are unchanged
       !since the imposed roots are at z=0 (see Davies 1986)
       DO i=1,M
          fex(i) = fonct(i) * EXP( -ci * NN * theta(i) ) 
       END DO

       ALLOCATE ( alphaex(M) )
       ALLOCATE ( alphanex(M) )

       !Find argument of modified dispersion relation
       alphaex(:) = ATAN2(AIMAG(fex),REAL(fex))

       !Unwrap the arguments to be in ascending order in pi
       CALL unwrap( alphaex, M, alphanex)

       !Integrand defined for 'argument principle' integrals (equation 3.3 in Davies 1986)
       DO i=1,M       
          DO j=1,NN
             exprn(i,j) = -REAL(j)/(2.*pi) * EXP(ci*REAL(j)*theta(i)) &
                  *  ( LOG ( ABS ( fex(i) ) ) + ci*alphanex(i) ) 
          END DO
       END DO

       DEALLOCATE ( fex )
       DEALLOCATE ( alphanex )

       !Argument principle integrals (Sn) are calculated
       ALLOCATE ( Sint(NN) )
       ALLOCATE ( exprnreal(M) )
       ALLOCATE ( exprnimag(M) )
       DO j=1,NN
          exprnreal = REAL(exprn(:,j))
          exprnimag = AIMAG(exprn(:,j))
          CALL davint(theta,exprnreal, M, 0._DBL, 2.*pi , realintans, ifailloc,1)
          CALL davint(theta,exprnimag, M, 0._DBL, 2.*pi , imagintans, ifailloc,2)
          Sint(j) = CMPLX(realintans,imagintans)
       END DO

       DEALLOCATE ( exprnreal )
       DEALLOCATE ( exprnimag )
       DEALLOCATE ( theta )       
       DEALLOCATE ( exprn )

       ALLOCATE ( A(0:NN) )
       !Setup the system of equations for the Davies method
       !This works as follows. From the argument principle we have:
       ! Sint(1) is sum(z) (sum of all solutions)
       ! Sint(2) is sum(z^2) (sum of all squares of solutions) 
       ! Sint(3) is sum(z^3) (sum of all cubes of solutions) ... and so on
       ! Based on this information, a polynomial is constructed with the form below,
       ! which can be proven that the roots of the polynomial are the solutions
       A(0) = (1.,0.)
       DO j=1,NN
          A(j) = 0.
          DO k=0,(j-1)
             ind = j-k
             A(j) = A(j) + A(k) * Sint(ind)
          END DO
          A(j) = -A(j) / j
       END DO
       DEALLOCATE ( Sint )

       !Construct arrays for root finding in polynomial
       ndeg = NN
       ALLOCATE ( Areal(2,1:NN+1) )
       ALLOCATE ( solint(2,NN) )
       ALLOCATE ( ww(2*NN*(NN+1)) )

       DO j = 0,NN
          Areal(1,j+1) = REAL( A(j) )
          Areal(2,j+1) = AIMAG( A(j) )
       END DO

       DEALLOCATE ( A )
       !The roots of the polynomials are found, which are the solutions!
       CALL CPQR79 (ndeg, Areal, solint, ifailloc, ww)

       DEALLOCATE ( Areal )       
       ALLOCATE ( csolint(NN) )

       NNbck=NN
       DO j=1,NN
          !Each solution is rewritten into a complex variable
          csolint(j) = CMPLX( solint(1,j), solint(2,j) )
          !We now verify that a true root was indeed found
          !CALL contour(Centre, csolint(j), solli)

          CALL squircle(Centre, csolint(j), solli, rint, mincont, verbose)

          !DEBUG
          !WRITE(*,'(A,I3,I3,A,2G15.7)') 'p/nu/j=',p,nu,'. solli = ',solli

          IF ( (gkw_is_nan(AIMAG(solli))) .OR. (gkw_is_nan(REAL(solli)))) THEN
             IF (verbose .EQV. .TRUE.) THEN 
                WRITE(stderr,'(A,I7,A,I2,A)') 'Main contour phase: solution had a NaN! Skipping solution. (p,nu)=(',p,',',nu,')'
             ENDIF
             soll(j)   = (0.,0.)
             fdsoll(j)   = (0.,0.)
             CYCLE
          ENDIF

          IF ( (AIMAG(solli) < 0. )) THEN 
             IF (verbose .EQV. .TRUE.) THEN 
                WRITE(stderr,'(A,I7,A,I2,A)') 'Main contour phase: solution is negative growth rate (stable eigenmode). ' &
                     // 'Skipping solution. (p,nu)=(',p,',',nu,')'
             ENDIF
             soll(j)   = (0.,0.)
             fdsoll(j)   = (0.,0.)
             CYCLE
          ENDIF

          IF ( (ABS(AIMAG(solli)) > ABS(AIMAG(ommax(p,nu)))) .OR. (ABS(REAL(solli)) > ABS(REAL(ommax(p,nu))))  ) THEN
             IF (verbose .EQV. .TRUE.) THEN 
                WRITE(stderr,'(A,I7,A,I2,A)') 'Main contour phase: solution out of allowed contour range (how did that happen?) ' &
                     // 'Skipping solution. (p,nu)=(',p,',',nu,')'
             ENDIF
             soll(j)   = (0.,0.)
             fdsoll(j)   = (0.,0.)
             CYCLE
          ENDIF

          CALL calcfonct(p, nu, solli, fdsolli)

          !DEBUGGING SKIPPING THE NEWTON STAGE***
          ! soll(j) = solli
          ! fdsoll(j) = fdsolli
          ! EXIT

          !If in fact our 'solution' is nowhere close to a root, then
          !we skip the Newton refinement stage and abandon the solution
          IF (ABS(fdsolli) > nearlysol) THEN
             soll(j)   = (0.,0.)
             fdsoll(j)   = (0.,0.)
             IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I3,A,G15.7,A)') 'For p=',p,' nu=,',nu, &
                  ' , ABS(fdsolli)=',ABS(fdsolli),' and the solution was abandoned'
             NNbck=NNbck-1
          ELSE !We have a close solution, now refined with a Newton step
             !DEGBUGGING 
             !WRITE(stdout,*) 'BEFORE NEWTON', solli
             !WRITE(stdout,*) 'NUMBER OF CONTOUR SEGMENTS', M
!!!!!
             CALL newton(p, nu, solli, fdsolli, nsolli, nfdsolli)
             !CALL broyden(p, nu, solli, fdsolli, nsolli, nfdsolli)

             !WRITE(stdout,*) 'AFTER NEWTON', nsolli

             !If, even after the Newton refinement, the solution is still too coarse, or outside 
             !the preset boundaries, then we abandon the solution
             IF (ABS(nfdsolli)> 0.5  .OR. &
                  ABS(AIMAG(nsolli))>(1.5*ABS(AIMAG(omegmax))) .OR. &
                  ABS(REAL(nsolli)) > (2.*ABS(REAL(omegmax)))) THEN
                soll(j)   = (0.,0.)
                fdsoll(j) = (0.,0.)
                IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I3,A,G15.7,A,2G15.7,A)') 'For p=',p,' nu=,',nu, &
                     ' , ABS(nfdsolli)=',ABS(nfdsolli),' with nsolli=',nsolli,' and the solution was abandoned'
                NNbck=NNbck-1
                !If the solution is stable, then the solution is zeroed out. We can't have stable solutions since we haven't included the analytic continuation
             ELSEIF (AIMAG(nsolli)<0.) THEN
                soll(j)   = (0.,0.)
                fdsoll(j) = (0.,0.)
                IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I3,A)') 'For p=',p,' nu=,',nu, &
                     ' , a spurious stable solution was found and zeroed out'
                NNbck=NNbck-1
             ELSE  
                soll(j) = nsolli
                fdsoll(j) = nfdsolli
             END IF
          END IF
       END DO
       NN=NNbck

       !Solution cleanup: all solutions within soldel*100 percent. soldel found in datcal
       !of a previously found solution (from same contour) is set to zero             
       IF (NN > 0) THEN
          DO j = 1,numsols-1
             DO k = j+1,numsols
             IF (ABS(soll(j)) > epsD) THEN !only compare to non-zero solutions in soll
                IF (ABS(soll(k)-soll(j))/ABS(soll(j)) < soldel) THEN
                   soll(k) = (0.,0.)
                   fdsoll(k) = (0.,0.)
                   IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I3,A)') 'For p=',p,' nu=,',nu, &
                        '. Warning: multiple identical eigenfrequencies found in same contour (and only one kept)'
                ENDIF
             ENDIF
             ENDDO
          ENDDO
       ENDIF


       DEALLOCATE ( csolint )
       DEALLOCATE ( solint )
       DEALLOCATE ( ww )
       DEALLOCATE ( alphaex )
       DEALLOCATE ( fonct )
       DEALLOCATE ( alphan )
       DEALLOCATE ( alpha )
       DEALLOCATE ( om )
       DEALLOCATE ( om2 )

    END IF


    IF (ALLOCATED(theta)) DEALLOCATE(theta)
    IF (ALLOCATED(om)) DEALLOCATE(om)
    IF (ALLOCATED(om2)) DEALLOCATE(om2)
    IF (ALLOCATED(fonct)) DEALLOCATE(fonct)
    IF (ALLOCATED(fonct)) DEALLOCATE(fonct)
    IF (ALLOCATED(alpha)) DEALLOCATE(alpha)
    IF (ALLOCATED(alphan)) DEALLOCATE(alphan)
    IF (ALLOCATED(difalphan)) DEALLOCATE(difalphan)


  END SUBROUTINE calculsol

  SUBROUTINE calcfonct(p, nu, omega, fonx)
    ! -------------------------------------------------------------------
    ! Calculates the integral function for which we search the roots
    ! This function is comprised of the adiabatic (Ac), passing (fonctc)
    ! and trapped (fonctp) terms
    ! -------------------------------------------------------------------
    INTEGER, INTENT(in)  :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonx

    COMPLEX(KIND=DBL) :: fonctc
    COMPLEX(KIND=DBL) :: fonctp

    IF ( ( rotflagarray(p) == 1 ) .AND. ( ETG_flag(nu) .EQV. .FALSE. ) ) THEN
       ! replace mwidth by real(mwidth) in such comparaisons since now mwidth is complex. Warning: is it correct or should take module?
       IF ( ( fc(p)==0. ) .OR. ( REAL(mwidth)<d/4.) .OR. ( calccirc .EQV. .FALSE. ) ) THEN
          fonctc = 0.
          IF (verbose .EQV. .TRUE.) WRITE(stdout,*) 'Warning: REAL(mwidth)<d/4 for p/nu=',p,'/',nu
       ELSE
          CALL calcfonctc (p, nu, omega, fonctc, .TRUE.)
       END IF

       IF (ft(p)==0. .OR. (calctrap .EQV. .FALSE.) ) THEN
          fonctp = 0.    
       ELSE      
          CALL calcfonctp ( p, nu, omega, fonctp, .TRUE. )
       END IF

       fonx = CMPLX(Ac(p),0.) - fonctc - fonctp

    ELSE

       IF ( ( fc(p)==0. ) .OR. ( REAL(mwidth)<d/4.) .OR. ( calccirc .EQV. .FALSE. ) ) THEN
          fonctc = 0.
          IF (verbose .EQV. .TRUE.) WRITE(stdout,*) 'Warning: REAL(mwidth)<d/4 for p/nu=',p,'/',nu
       ELSE
          CALL calcfonctc ( p, nu, omega, fonctc, .FALSE. )
       END IF

       IF (ft(p)==0. .OR. (calctrap .EQV. .FALSE.) ) THEN
          fonctp = 0.    
       ELSE      
          CALL calcfonctp ( p, nu, omega, fonctp, .FALSE. )
       END IF

       fonx = CMPLX(Ac(p),0.) - fonctc - fonctp

    ENDIF

  END SUBROUTINE calcfonct

  SUBROUTINE newton( p, nu, sol, fsol, newsol, fnewsol)  
    !Newton method for refining the solutions found from the contour integrals
    !Basic 2D Newton method. Demands that both u(z) and v(z) go to zero, where F(z)=u(z)+i*v(z)
    !Function derivative defined with ndif parmameter
    INTEGER, INTENT(IN) :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN) :: sol, fsol

    COMPLEX(KIND=DBL), INTENT(OUT) :: newsol, fnewsol

    INTEGER :: niter
    REAL(KIND=DBL) :: err, detMa, maxnerrint, ndif
    REAL(KIND=DBL), DIMENSION(2,2) :: Ma, invMa
    REAL(KIND=DBL), DIMENSION(2,1) :: uo, deltau
    COMPLEX(KIND=DBL) :: fzo, fzpd, fzmd, fzpid, fzmid
    COMPLEX(KIND=DBL) :: dfsurdx, dfsurdy
    COMPLEX(KIND=DBL) :: zo, zpd, zmd, zpid, zmid, delom, delomi 

    ndif = ndif8*Tex(p)/8.*Nex(p)/ne5 ! scale differentiation parameter to normalized omega scale
    delom  = CMPLX(ndif,0.)
    delomi = CMPLX(0.,ndif)
    niter  = 0
    zo     = sol
    fzo    = fsol
    err    = ABS(fzo)

    IF (runcounter /= 0) THEN
       maxnerrint = maxnerr*Nex(p)/ne5
    ELSE
       maxnerrint = maxnerr2*Nex(p)/ne5
    ENDIF

    DO
       IF ( (err < maxnerrint) .OR. (niter > maxiter) ) EXIT

       zpd = zo+delom
       zmd = zo-delom

       CALL calcfonct (p, nu, zpd, fzpd)
       CALL calcfonct (p, nu, zmd, fzmd)

       dfsurdx = (fzpd-fzmd)/(2.*delom)

       zpid=zo+delomi
       zmid=zo-delomi

       CALL calcfonct (p, nu, zpid, fzpid)
       CALL calcfonct (p, nu, zmid, fzmid)

       dfsurdy = (fzpid-fzmid)/(2.*delom)

       Ma(1,1) = REAL(dfsurdx)
       Ma(1,2) = REAL(dfsurdy)
       Ma(2,1) = AIMAG(dfsurdx)
       Ma(2,2) = AIMAG(dfsurdy)

       uo(1,1) = -REAL(fzo)
       uo(2,1) = -AIMAG(fzo)
       detMa = Ma(1,1)*Ma(2,2)-Ma(1,2)*Ma(2,1)
       !FOR DEBUGGING UNCOMMENT BELOW
       !WRITE(stderr,*) detMa, niter, err
       IF (ABS(detMa)<epsD) EXIT
       invMa(1,1) =  Ma(2,2)/detMa
       invMa(1,2) = -Ma(1,2)/detMa
       invMa(2,1) = -Ma(2,1)/detMa
       invMa(2,2) =  Ma(1,1)/detMa

       deltau = MATMUL(invMa,uo)

       IF (edgemode(p,nu) .EQV. .FALSE.) THEN
          zo = zo+CMPLX(deltau(1,1),deltau(2,1))
       ELSE
          zo = zo+CMPLX(deltau(1,1),deltau(2,1))/5. ! reduce newton step for strongly driven edge modes, which can have large steps
       ENDIF
       
       IF ( (gkw_is_nan(AIMAG(zo))) .OR. (gkw_is_nan(REAL(zo)))) THEN
          IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Newton: solution had a NaN! Skipping solution. ' &
               // '(p,nu)=(',p,',',nu,')'
          zo   = (0.,0.)
          fzo  = (0.,0.)
          EXIT
       ENDIF

       IF ( (AIMAG(zo) < 0. ) .OR. (ABS(AIMAG(zo)) > ABS(AIMAG(ommax(p,nu)))) .OR. (ABS(REAL(zo)) > ABS(REAL(ommax(p,nu))))  ) THEN
          IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Newton: solution outside of allowed contour range! ' &
               // 'Skipping solution. (p,nu)=(',p,',',nu,')'
          zo   = (0.,0.)
          fzo  = (0.,0.)
          EXIT
       ENDIF

       CALL calcfonct (p, nu, zo, fzo)

       err = ABS(fzo)
       !err = ABS(CMPLX(deltau(1,1), deltau(2,1))/zo)

       niter = niter+1
    END DO
    !FOR DEBUGGING*************
    !WRITE(stdout,*) 'Number of iterations in Newton solver = ', niter
    !**************************
    newsol  = zo
    fnewsol = fzo   

  END SUBROUTINE newton

  SUBROUTINE broyden( p, nu, sol, fsol, newsol, fnewsol)
    !! Broyden method for refining the solutions found from the contour integrals
    !! Basic 2D Broyden method. Demands that both u(z) and v(z) go to zero, where F(z)=u(z)+i*v(z)
    !! Function derivative defined with ndif parmameter
    INTEGER, INTENT(IN) :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN) :: sol, fsol
    COMPLEX(KIND=DBL), INTENT(OUT) :: newsol, fnewsol
    INTEGER :: niter, i
    INTEGER :: newt_conv = 0
    REAL(KIND=DBL) :: err, detMa, maxnerrint, ndif
    REAL(KIND=DBL), DIMENSION(2,2) :: Ma, invMa, I0
    REAL(KIND=DBL), DIMENSION(2,1) :: uo, deltau, deltaf
    REAL(KIND=DBL), DIMENSION(1,1) :: denom
    COMPLEX(KIND=DBL) :: fzo, fz_old, fzpd, fzmd, fzpid, fzmid
    COMPLEX(KIND=DBL) :: dfsurdx, dfsurdy
    COMPLEX(KIND=DBL) :: zo, zpd, zmd, zpid, zmid, delom, delomi

    ndif = ndif8*Tex(p)/8.*Nex(p)/ne5 ! scale differentiation parameter to normalized omega scale
    delom  = CMPLX(ndif,0.)
    delomi = CMPLX(0.,ndif)
    niter  = 0
    zo     = sol
    fzo    = fsol

    !Idenitty matrix
    I0(:,:) = 0.
    forall(i=1:2) I0(i,i) = 1.

    err = 1._DBL
    IF (runcounter /= 0) THEN
      maxnerrint = maxnerr*Nex(p)/ne5
    ELSE
      maxnerrint = maxnerr2*Nex(p)/ne5
    ENDIF
    DO
    IF ( (err < maxnerrint) .OR. (niter > maxiter) ) EXIT
      uo(1,1) = -REAL(fzo)
      uo(2,1) = -AIMAG(fzo)
      deltaf(1,1) = REAL(fzo) - REAL(fz_old)
      deltaf(2,1) = AIMAG(fzo) - AIMAG(fz_old)

      IF(niter.EQ.0) THEN
        ! Only calculate the Jacobian once
        ! Afterwards, we approximately update it
        zpd = zo+delom
        zmd = zo-delom
        CALL calcfonct(p, nu, zpd, fzpd)
        CALL calcfonct(p, nu, zmd, fzmd)
        dfsurdx = (fzpd-fzmd)/(2.*delom)
        zpid=zo+delomi
        zmid=zo-delomi
        CALL calcfonct(p, nu, zpid, fzpid)
        CALL calcfonct(p, nu, zmid, fzmid)
        dfsurdy = (fzpid-fzmid)/(2.*delom)
        Ma(1,1) = REAL(dfsurdx)
        Ma(1,2) = REAL(dfsurdy)
        Ma(2,1) = AIMAG(dfsurdx)
        Ma(2,2) = AIMAG(dfsurdy)
        detMa = Ma(1,1)*Ma(2,2)-Ma(1,2)*Ma(2,1)
        !FOR DEBUGGING UNCOMMENT BELOW
        !WRITE(stderr,*) detMa, niter, err
        IF (ABS(detMa)<epsD) EXIT
        invMa(1,1) =  Ma(2,2)/detMa
        invMa(1,2) = -Ma(1,2)/detMa
        invMa(2,1) = -Ma(2,1)/detMa
        invMa(2,2) =  Ma(1,1)/detMa
      ELSE
        !Update invMa
        !This minimizes the Frobenius norm of Ma_new - Ma_old, i.e. the "good" update
        denom = MATMUL(TRANSPOSE(deltau), MATMUL(invMa, deltaf))
        IF(ABS(denom(1,1))<epsD) EXIT
        invMa = MATMUL(I0 + 1./denom(1,1) * MATMUL(deltau - MATMUL(invMa, deltaf), TRANSPOSE(deltau)), invMa)
      END IF

      deltau = MATMUL(invMa,uo)
      zo = zo+CMPLX(deltau(1,1),deltau(2,1))
      IF ( (gkw_is_nan(AIMAG(zo))) .OR. (gkw_is_nan(REAL(zo)))) THEN
        IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Broyden: solution had a NaN! Skipping solution. (p,nu)=(',p,',',nu,')'
        zo   = (0.,0.)
        fzo  = (0.,0.)
        EXIT
      ENDIF
      IF ( (AIMAG(zo) < 0. ) .OR. (ABS(AIMAG(zo)) > ABS(AIMAG(ommax(p,nu)))) .OR. (ABS(REAL(zo)) > ABS(REAL(ommax(p,nu))))  ) THEN
        IF (verbose .EQV. .TRUE.) WRITE(stderr,'(A,I7,A,I2,A)') 'Broyden: solution outside of allowed contour range! Skipping solution. (p,nu)=(',p,',',nu,')'
        zo   = (0.,0.)
        fzo  = (0.,0.)
        EXIT
      ENDIF
      fz_old = fzo
      CALL calcfonct(p, nu, zo, fzo)
      !Change Broyden convergence test
      IF(newt_conv.EQ.0) THEN
        err = ABS(fzo)
      ELSE IF(newt_conv.EQ.1) THEN
        err = ABS(CMPLX(deltau(1,1), deltau(2,1)))
      ELSE IF(newt_conv.EQ.2) THEN
        err = ABS(CMPLX(deltau(1,1), deltau(2,1))/zo)
      END IF
      niter = niter+1
    END DO
    !FOR DEBUGGING*************
    !WRITE(stdout,*) 'Number of iterations in Broyden solver = ', niter
    !**************************
    !WRITE(stderr, *) "In Broyden for p = ", p, " and nu = ", nu, ", niter = ", niter
    newsol  = zo
    fnewsol = fzo
  END SUBROUTINE

  REAL(KIND=DBL) FUNCTION alamnormint(lamin)
    !integrand for pinch angle iaverage of Vpar for passing particles
    REAL(KIND=DBL), INTENT(IN) :: lamin
    REAL(KIND=DBL) :: Tlam
    INTEGER :: ifail_slatec,i
    REAL(KIND=DBL), DIMENSION(ntheta) :: theta,Tint

    theta=(/((i * pi/(ntheta-1)),i=0,ntheta-1)/) !set theta = [0,pi]
    Tint = 2./SQRT(1-lamin*(1+2.*epsilon(plam)*SIN(theta/2.)**2))

    ifail_slatec=0
    CALL davint(theta,Tint,ntheta,0.,pi,Tlam,ifail_slatec,2) !calculate transit time

    IF (ifail_slatec /= 1) THEN
       IF (verbose .EQV. .TRUE.) WRITE(stderr,"(A,I0,A,I7)") 'ifail_slatec = ',ifail_slatec,&
            &'. Abnormal termination of alam1 solution T integration at p=',plam
    ENDIF

    alamnormint = 1./(4.*pi)*Tlam

  END FUNCTION alamnormint

  SUBROUTINE alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, moment)
    INTEGER, INTENT(IN) :: dim_in, dim_out, moment
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    INTEGER :: ifail_slatec

    REAL(KIND=DBL) :: lamin
    REAL(KIND=DBL) :: Tlam, Rlam1, Rlam2
    INTEGER :: i, first_marker
    REAL(KIND=DBL), DIMENSION(ntheta) :: theta,Tint,Rint1, Rint2

    ERRORSTOP(dim_out /= 1 .OR. dim_in /= 1, 'Wrong in/out dimensions for alamint')

    ERRORSTOP(moment < 1 .OR. moment > 5, 'not a known moment of alamint')
    ifail = 0
    lamin = f_in(1)

    theta=(/((i * pi/(ntheta-1)),i=0,ntheta-1)/) !set theta = [0,pi]
    Tint = 2./SQRT(1-lamin*(1+2.*epsilon(plam)*SIN(theta/2.)**2))

    !MARKER - User provided identifier for debugging purposes
    ! There are 5 alam ints + 1 normalization int
    ! There are 3 integrals per alamint
    first_marker = 1 + (moment - 1) * 3

    ifail_slatec=0
    CALL davint(theta,Tint,ntheta,0.,pi,Tlam,ifail_slatec,first_marker)
    CALL debug_msg_alamint(ifail_slatec, moment, plam, verbose, 'Tlam', ifail)

    Rint1=1./SQRT(1.-lamin*(1.+2.*epsilon(plam)*SIN(theta/2.)**2))

    ifail_slatec=0
    CALL davint(theta,Rint1,ntheta,0.,pi,Rlam1,ifail_slatec,first_marker+1)
    CALL debug_msg_alamint(ifail_slatec, moment, plam, verbose, 'Rlam1', ifail)

    Rint2=Rint1 * SQRT(1.-lamin*(1.+2.*epsilon(plam)*SIN(theta/2.)**2))**moment

    ifail_slatec = 0
    CALL davint(theta,Rint2,ntheta,0.,pi,Rlam2,ifail_slatec,first_marker+2)
    CALL debug_msg_alamint(ifail_slatec, moment, plam, verbose, 'Rlam2', ifail)

    f_out = Rlam2/Rlam1*1./(4.*pi)*Tlam

  END SUBROUTINE alamint

  SUBROUTINE alam1int(dim_in, f_in, f_data, dim_out, f_out, ifail)
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    CALL alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, 1)
  END SUBROUTINE alam1int

  SUBROUTINE alam2int(dim_in, f_in, f_data, dim_out, f_out, ifail)
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    CALL alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, 2)
  END SUBROUTINE alam2int

  SUBROUTINE alam3int(dim_in, f_in, f_data, dim_out, f_out, ifail)
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    CALL alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, 3)
  END SUBROUTINE alam3int

  SUBROUTINE alam4int(dim_in, f_in, f_data, dim_out, f_out, ifail)
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    CALL alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, 4)
  END SUBROUTINE alam4int

  SUBROUTINE alam5int(dim_in, f_in, f_data, dim_out, f_out, ifail)
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    CALL alamint(dim_in, f_in, f_data, dim_out, f_out, ifail, 5)
  END SUBROUTINE alam5int

  SUBROUTINE debug_msg_alamint(ifail_slatec, moment, plam, verbose, integrand_name, ifail_func)
    INTEGER, INTENT(IN) :: ifail_slatec, moment, plam
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) ::integrand_name
    INTEGER, INTENT(OUT) :: ifail_func
    IF (ifail_slatec /= 1 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,I0,A,A,A,I7)") 'ifail_slatec = ',ifail_slatec,&
        &'. Abnormal termination of alam', moment, ' solution ', integrand_name, ' integration at p=',plam
      ifail_func = 1
    ENDIF
  END SUBROUTINE debug_msg_alamint

  REAL(KIND=DBL) FUNCTION sin2thint(krr)
!!! Integrand for average of sin^2(theta/2) over eigenfunction used in V_par averages
    REAL(KIND=DBL), INTENT(in) :: krr
    ! Local variables
    REAL(KIND=DBL)    :: normgs,gau

    normgs = normkr * SQRT((REAL(mwidth)**2 - AIMAG(mwidth)**2)) / SQRT(pi) * &
         EXP(-AIMAG(mshift)**2/(REAL(mwidth)**2-AIMAG(mwidth)**2))
    gau = EXP(-0.5_DBL*(krr*normkr*mwidth)**2 - ci*krr*normkr*mshift)   !!definition of the eigenfun in k-space

    sin2thint= normgs * ABS(gau)**2. * SIN(distan(plam,nulam)*krr*normkr/2)**2

  END FUNCTION sin2thint

  LOGICAL FUNCTION outside_contour(p, nu, j, oldsolin, ommaxin)
    INTEGER, INTENT(IN) :: p, nu, j
    COMPLEX(KIND=DBL), DIMENSION(:,:,:), INTENT(IN) :: oldsolin
    COMPLEX(KIND=DBL), DIMENSION(:,:), INTENT(IN) :: ommaxin
    outside_contour = (ABS(AIMAG(oldsolin(p,nu,j))) > ABS(AIMAG(ommaxin(p,nu)))) .OR. &
         (ABS(REAL(oldsolin(p,nu,j))) > ABS(REAL(ommaxin(p,nu))))
  END FUNCTION




END MODULE calcroutines
