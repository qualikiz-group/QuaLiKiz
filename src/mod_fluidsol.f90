! At the moment there is a mix of various fluid solutions here as testing needs to be done

! From old Qlk_Jon version, ana_solflu
! New rigorous version: jon_fluidsol populates jon_solflu, jon_modewidth, and jon_modeshift. Testing needs to be done

MODULE mod_fluidsol
#include "preprocessor.inc"
  USE kind
  USE datmat
  USE datcal
  USE mod_integration
  IMPLICIT NONE

CONTAINS
  SUBROUTINE gen_fluidsol(p, nu, elec, rotation, Machilocal, Auilocal, gammaElocal)
    !Constructs the various coefficients used in the advanced fluid solver with rotation.
    !Carries out the direct pitch angle integrations of various frequency and geometric terms
    INTEGER, INTENT(IN) :: p, nu
    LOGICAL , INTENT(IN) :: rotation, elec
    REAL(KIND=DBL), DIMENSION(nions), INTENT(IN), OPTIONAL :: Machilocal, Auilocal
    REAL(KIND=DBL), INTENT(IN), OPTIONAL :: gammaElocal
    COMPLEX(KIND=DBL) :: dw2,sol,width,width2,width4,shift, A0,A1,A2,A3,B0,B1,U1,U2,U3,D1,ww0,ww2
    COMPLEX(KIND=DBL), DIMENSION(2) :: width1vec,width2vec
    REAL(KIND=DBL) :: fc2,ft2,norm,ktheta,fk,fk2,Wv4,lam
    INTEGER :: j, degpoly
    COMPLEX(KIND=DBL), DIMENSION(:), ALLOCATABLE :: poly
    COMPLEX(KIND=DBL), DIMENSION(:), ALLOCATABLE :: polysol
    REAL(KIND=DBL), DIMENSION(:), ALLOCATABLE :: warray
    REAL(KIND=DBL), DIMENSION(nions) :: kbar,Wd1i0,Wd1i1,Wd1i2,Wd2i,Wd3i0,Wd3i1
    REAL(KIND=DBL) :: kbare
    REAL(KIND=DBL), DIMENSION(nions) :: WvT1i,WvT21i,Wv1i,Wv2i0,Wv2i1,Wv4i,nwdi !for ions
    COMPLEX(KIND=DBL), DIMENSION(nions) :: banhat,icoef, rhohati
    COMPLEX(KIND=DBL) :: rhohate
    REAL(KIND=DBL) :: Wd1e,Wd2e,WvT1e,WvT21e,Wv1e,Wv2e0,nwde !for electrons
    REAL(KIND=DBL) :: V1,V2, cputime1, cputime2,gamEunnorm
    INTEGER, DIMENSION(1) :: iloc

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_1d
    REAL(KIND=DBL), DIMENSION(1) :: fluid_lbound, fluid_ubound, fk_ubound
    REAL(KIND=DBL), DIMENSION(1) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(1) :: fk_fdata
    REAL(KIND=DBL), DIMENSION(2) :: fluid_fdata
    INTEGER :: ifailloc

    CHARACTER(len=100) :: msg ! buffer for error messages

    ! from lib/src/slatec/cpqr79.f
    EXTERNAL CPQR79

    ERRORSTOP(rotation .and. elec, 'Electrons should be always rotationless')

    if (elec) then
      degpoly = ndegpoly
    else
      CALL CPU_TIME(cputime1)
      degpoly = ndegpoly + 1
    endif
    allocate(poly(degpoly))
    allocate(polysol(degpoly - 1))
    allocate(warray(2*degpoly*(degpoly-1)))

    !Set integration limits
    fluid_lbound = 0.0d0
    fk_ubound = 1.0d0 !- barelyavoid
    fluid_ubound = 1 - 2 * epsilon(p)

    dim_out = 1
    dim_in = 1
    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT

    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
    CLASS IS (hcubature_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
      opts_1d%minEval = 0
      opts_1d%norm = 1
      opts_1d%reqAbsError = absacc1
    END SELECT

    ktheta = ntor(p,nu)*qx(p)/(Rmin(p)*x(p))

    if (elec) then
      kbare = ktheta*ABS(smag(p))/(qx(p)*Ro(p))*cthe(p)
    else
      kbar(:) = ktheta*ABS(smag(p))/(qx(p)*Ro(p))*cthi(p, :)
    endif
    nwde = -nwg*Tex(p)
    nwdi(:) = -nwg*(-Tix(p,:)/Zi(p,:)) !opposite to the usual definition do to a reversal of the sign in the analytic derivation of the formulas

    pFFk=p !to pass radial coordinate into integrand functions which can only have one argument

    ifailloc = 1
    fk_fdata(1) = 1 ! n
    CALL integration(dim_out, fknint, dim_in, fluid_lbound, fk_ubound, opts_1d, int_res, int_err, fk_fdata, ifailloc)
    CALL debug_msg_pnuion('fk', ifailloc, verbose, p, nu, ion)
    fk = int_res(1)

    if (.not. elec) then
      ifailloc = 1
      fk_fdata(1) = 2
      CALL integration(dim_out, fknint, dim_in, fluid_lbound, fk_ubound, opts_1d, int_res, int_err, fk_fdata, ifailloc)
      CALL debug_msg_pnuion('fk2', ifailloc, verbose, p, nu, ion)
      fk2 = int_res(1)
    endif

    ifailloc = 1
    fluid_fdata(1) = 0
    fluid_fdata(2) = 17
    CALL integration(dim_out, fluidint, dim_in, fluid_lbound, fluid_ubound, opts_1d, int_res, int_err, fluid_fdata, ifailloc)
    CALL debug_msg_pnuion('norm', ifailloc, verbose, p, nu, ion)
    norm = int_res(1)

!!$    fc2 = fc(p)
!!$    ft2 = ft(p)
    fc2 = norm
    ft2 = 1-fc2

    ifailloc = 1
    fluid_fdata(1) = 1
    fluid_fdata(2) = 18
    CALL integration(dim_out, fluidint, dim_in, fluid_lbound, fluid_ubound, opts_1d, int_res, int_err, fluid_fdata, ifailloc)
    CALL debug_msg_pnuion('lam', ifailloc, verbose, p, nu, ion)
    lam = int_res(1)

    ifailloc = 1
    fluid_fdata(1) = 2
    fluid_fdata(2) = 19
    CALL integration(dim_out, fluidint, dim_in, fluid_lbound, fluid_ubound, opts_1d, int_res, int_err, fluid_fdata, ifailloc)
    CALL debug_msg_pnuion('V1', ifailloc, verbose, p, nu, ion)
    V1 = int_res(1)

    ifailloc = 1
    fluid_fdata(1) = 3
    fluid_fdata(2) = 20
    CALL integration(dim_out, fluidint, dim_in, fluid_lbound, fluid_ubound, opts_1d, int_res, int_err, fluid_fdata, ifailloc)
    CALL debug_msg_pnuion('V2', ifailloc, verbose, p, nu, ion)
    V2 = int_res(1)

    ifailloc = 1
    fluid_fdata(1) = 4
    fluid_fdata(2) = 21
    CALL integration(dim_out, fluidint, dim_in, fluid_lbound, fluid_ubound, opts_1d, int_res, int_err, fluid_fdata, ifailloc)
    CALL debug_msg_pnuion('Wv4', ifailloc, verbose, p, nu, ion)
    Wv4 = int_res(1)

    Wd1e = nwde*Ane(p)
    Wd2e = nwde*Ate(p)

    Wv1e = nwde*(2.-lam/fc2)
    Wv2e0 = nwde*( (smag(p)-alphax(p)-0.5)*(2.-lam/fc2))
    WvT1e = nwde*fk

    if (elec) then
      rhohate = 1. - (ktheta**2*Rhoe(p)**2)/2.

      IF (REAL(rhohate) < 0.25) rhohate = CMPLX(((ktheta**4)*(Rhoe(p))**4)/64,0.)

      !calculate polynomial coefficients
      poly(1) = -1. - Tex(p)/Tix(p,1)*Zeffx(p) + rhohate !C2 in notes
      poly(2) = (Wd1e*ft2*rhohate-1.5*WvT1e)+fc2*Wd1e*rhohate-1.5*fc2*Wv1e !C1 in notes
      poly(3) = -1.5*(Wd1e+Wd2e)*WvT1e - 1.5*fc2*(Wd1e+Wd2e)*Wv1e !C0 in notes
    else
      
      WvT21e = nwde**2*fk2

      Wd1i0(:) = nwdi(:)*Ani(p,:)
      Wd2i(:) = nwdi(:)*(Ati(p,:))
      if (rotation) then
        Wd1i1(:) = -2.*nwdi(:)*Machilocal(:)*Auilocal(:)
        Wd1i2(:) = nwdi(:)*Ati(p,:)*Machilocal(:)**2 
        Wd3i0(:) = 2.*nwdi(:)*Auilocal(:)
        Wd3i1(:) = -2.*nwdi(:)*Machilocal(:)*Ati(p,:)
      else
        Wd1i1(:) = 0
        Wd1i2(:) = 0
        Wd3i0(:) = 0
        Wd3i1(:) = 0
      endif

      Wv1i(:) = nwdi(:)*(2.-lam/fc2)
      Wv2i0(:) = nwdi(:)*( (smag(p)-alphax(p)-0.5)*(2.-lam/fc2))
      Wv2i1(:) = -nwdi(:)*lam/(2.*fc2)*epsilon(p)
      Wv4i(:) = nwdi(:)**2*Wv4
      WvT1i(:) = nwdi(:)*fk
      WvT21i(:) = nwdi(:)**2*fk2


      ! set ion coefficients
      icoef(:) = ninorm(p,:)*Zi(p,:)**2*Tex(p)/Tix(p,:)
      rhohati(:) = 1. - ((ktheta**2)*(Rhoi(p,:))**2)/2.

      DO j=1,nions
         IF (REAL(rhohati(j)) < 0.25) rhohati(j) = CMPLX(((ktheta**4)*(Rhoi(p,j))**4)/64,0.)
      ENDDO

      WHERE ( (Tex(p)/Tix(p,:) < 1./5.) .OR. (ion_type(p,:) > 1) ) icoef(:) = 0.
      msg = 'icoef == 0 for all ions in mod_fluidsol. There should be at least one non-fast ion (Ti/Te < 5)'
      ERRORSTOP(ALL(ABS(icoef) < epsS), msg)

      !Polynomial coefficients
      !With self consistent ordering
      !Without rotation or d/dx^2 as per perturbative solution

      poly(1) = -1. + ft2 + SUM( icoef*(-1.+ rhohati )) !C3 in notes
      poly(2) = Wd1e * ft2 - 1.5*WvT1e+SUM( icoef*rhohati*( ft2*Wd1i0 - 1.5*WvT1i + &
           & fc2*Wd1i0 - 1.5*fc2*Wv1i))

      poly(3) = -1.5*WvT1e*(Wd1e+Wd2e)+ 1.5*WvT21e + SUM(icoef*( -1.5*WvT1i*(Wd1i0+Wd2i)*rhohati + 1.5*WvT21i -  &
           & 1.5*fc2*(Wd1i0+Wd2i)*Wv1i*rhohati + 15./4.*Wv4i )) !C1 in notes

      poly(4) = 1.5*WvT21e*(Wd1e+Wd2e) +SUM(icoef*(1.5*WvT21i*(Wd1i0+Wd2i) + &  !C0 in notes
         & 15./4.*(Wd1i0+2.*Wd2i)*Wv4i ))
    endif


    !**FIND ROOT OF POLYNOMIAL**
    CALL CPQR79(degpoly - 1,poly,polysol,ifailloc,warray)
    IF (ifailloc /= 0) THEN
       IF (verbose .EQV. .TRUE.) WRITE(stderr,"(A,I0,A,I7,A,I0)") 'ifailloc = ',ifailloc,&
            &'. Abnormal termination of fluid solution CPQR root finder at p=',p,', nu=',nu
    ENDIF

    !find and set most unstable mode
    iloc = MAXLOC(AIMAG(polysol))
    sol = polysol(iloc(1))

    !WIDTH TERMS
    !self-consistent ordering
    if (elec) then
      A2 = 1.5*kbare**2*V1*(sol+Wd1e+Wd2e)
      A1 = 1.5*kbare**2*V2*distan(p,nu)**2*(sol+Wd1e+Wd2e)
      A0 = ft2*rhohate*(sol**3 + sol**2*Wd1e)*de(p)**2/2. + &
           fc2*(sol**3 + sol**2*Wd1e)*Rhoe(p)**2/2. + &
           & fc2*(1.5*sol**2*Wv2e0*distan(p,nu)**2 + 1.5*sol*(Wd1e+Wd2e)*distan(p,nu)**2*Wv2e0)
      width4 = -A0/A2

      !TESTING DEBUGGING
      !WRITE(*,*) width4
      !WRITE(*,*) (-A1 + SQRT(A1**2-4.*A0*A2))/A0
      !WRITE(*,*) (-A1 - SQRT(A1**2-4.*A0*A2))/A0
    else
      A3 = SUM(icoef*(ft2*di(p,:)**2/2. + fc2*Rhoi(p,:)**2/2.))

      A2 = SUM(icoef*( (ft2*(Wd1i0+Wd1i1)-1.5*WvT1i)*di(p,:)**2/2. + Wd1i0*fc2*Rhoi(p,:)**2/2. + 1.5*fc2*distan(p,nu)**2*Wv2i0))

      A1 = SUM(icoef*( (1.5*(Wd1i0+Wd2i)*Wv2i0*fc2*distan(p,nu)**2 - 1.5*(Wd1i0+Wd2i)*WvT1i*di(p,:)**2/2.)))

      B1 = SUM(icoef* 1.5*V1 *kbar**2)

      B0 = SUM(icoef*1.5*V1*(Wd1i0+Wd2i)*kbar**2)

      ww0 = sol**3*A3+sol**2*A2+sol*A1
      ww2 = B0+B1*sol
      width4 = -ww0/ww2
    endif

    width2vec(1) = ABS(width4)**0.5*EXP(ci*(ATAN(AIMAG(width4)/REAL(width4))/2. + 2.*pi*0./2.))
    width2vec(2) = ABS(width4)**0.5*EXP(ci*(ATAN(AIMAG(width4)/REAL(width4))/2. + 2.*pi*1./2.))

    IF (REAL(width2vec(1)) > 0) THEN
       width2=width2vec(1)
    ELSE
       width2=width2vec(2)
    ENDIF

    width1vec(1) = ABS(width2)**0.5*EXP(ci*(ATAN(AIMAG(width2)/REAL(width2))/2. + 2.*pi*0./2.))
    width1vec(2) = ABS(width2)**0.5*EXP(ci*(ATAN(AIMAG(width2)/REAL(width2))/2. + 2.*pi*1./2.))

    IF (REAL(width1vec(1)) > 0) THEN
       width=width1vec(1)
    ELSE
       width=width1vec(2)
    ENDIF

    if (.not. elec .and. rotation) then
      ! set tuning factor in width
      width4 = width4*widthtuneITG
      width2 = width2 * widthtuneITG**0.5
      width =  width * widthtuneITG**0.25
      IF (REAL(width) < 0.6*distan(p,nu)) THEN 
         !width=width*0.66*distan(p,nu)/REAL(width)
         width=CMPLX(0.6*distan(p,nu),0.)
         width2=width**2
         width4=width**4
      ENDIF
    endif

    !Update values for outside of module
    omeflu = sol
    mwidth = width

    if (.not. elec) then
      if (rotation) then
        !set ion coefficients
        banhat(:) = 1. - (di(p,:)**2)/(2.*width**2)
        dw2 = (distan(p,nu)/width)**2

        !! calculate shift using solved width and eigenvalue solution
        !self consistent ordering
        U1 = (1.-ft2)*3.*ktheta*sol**2 - 2.*ktheta*Wd1e*sol*ft2 + 3.*WvT1e*ktheta*sol+1.5*ktheta*WvT1e*(Wd1e+Wd2e) + &
             & SUM(icoef*( ( (1.-ft2*banhat)*3.*ktheta*sol**2 - 2.*ktheta*sol*(Wd1i0*banhat+Wd1i1)*ft2 + &
             & 3.*ktheta*WvT1i*sol+1.5*ktheta*WvT1i*(Wd1i0+Wd2i)) + &
             & (-fc2*3.*ktheta*sol**2 + 3.*ktheta*sol*(Wv1i+Wv2i0*dw2)*fc2 - 2.*fc2*(Wd1i0+Wd1i1)*sol*ktheta + & 
             & 1.5*ktheta*fc2*(Wd2i+Wd1i0)*(Wv1i+Wv2i0*dw2))))     

        U2 = SUM(icoef*3.*(kbar*sol**2*V1+kbar*sol*(Wd1i0+Wd2i)*V1)*Machilocal(:))

        U3 = SUM(icoef*1.5*kbar*sol*V1*( Wd3i0+Wd3i1))

        D1  = SUM(icoef*(-di(p,:)**2/width**4*ft2*(sol**3+Wd1i0*sol**2))) + &
             & SUM(icoef*(-3.*sol**2*distan(p,nu)**2/width**4*Wv2i0*fc2 - 3.*sol*(Wd1i0+Wd2i)*Wv2i0*distan(p,nu)**2/width**4*fc2))

        gamEunnorm=gammaElocal*cref(p)/Ro(p)
        shift = -(gamEunnorm*U1+U2+U3)/D1
        mshift = shift
        ! Ordering should maintain shift < width. Pathalogical cases break this, so for code stability we constrain mshift:
        IF (ABS(REAL(mshift))>REAL(mwidth)) mshift=CMPLX(SIGN(REAL(mwidth),REAL(mshift)),AIMAG(mshift))
        IF (ABS(AIMAG(mshift))>REAL(mwidth)) mshift=CMPLX(REAL(mshift),SIGN(REAL(mwidth),AIMAG(mshift)))
      else
        mshift = 0. ! By definition

        ! tuning factor for width
        width = width*widthtuneITG**0.25
        IF (REAL(width) < 0.6*distan(p,nu)) width=CMPLX(0.6*distan(p,nu),0.)
        mwidth = width
      endif
    else
      mshift = 0
      ! tuning factor for width
      width = width*widthtuneETG**0.25
      IF (REAL(width) < 0.6*distan(p,nu)) width=CMPLX(0.6*distan(p,nu),0.)
      mwidth = width
    endif

    if (.not. elec) then
      CALL CPU_TIME(cputime2)
    endif

  END SUBROUTINE gen_fluidsol

  SUBROUTINE jon_fluidsol(p,nu, Machilocal, Auilocal, gammaElocal)
    !Constructs the various coefficients used in the advanced fluid solver with rotation.
    !Carries out the direct pitch angle integrations of various frequency and geometric terms
    INTEGER, INTENT(IN) :: p, nu
    REAL(KIND=DBL), DIMENSION(nions), INTENT(IN) :: Machilocal, Auilocal
    REAL(KIND=DBL), INTENT(IN) :: gammaElocal

    call gen_fluidsol(p, nu, .FALSE., .TRUE., Machilocal, Auilocal, gammaElocal)
  END SUBROUTINE jon_fluidsol

  SUBROUTINE jon_fluidsol_ele(p,nu)
    !Constructs the various coefficients used in the advanced fluid solver for ky>2 electron terms.
    !Carries out the direct pitch angle integrations of various frequency and geometric terms
    INTEGER, INTENT(IN) :: p, nu

    call gen_fluidsol(p, nu, .TRUE., .FALSE.)
  END SUBROUTINE jon_fluidsol_ele

  SUBROUTINE jon_fluidsol_norot(p,nu)
    !Constructs the various coefficients used in the advanced fluid solver with rotation.
    !Sets all rotation related terms to zero
    !Carries out the direct pitch angle integrations of various frequency and geometric terms

    INTEGER, INTENT(IN) :: p, nu
    call gen_fluidsol(p, nu, .FALSE., .FALSE.)
  END SUBROUTINE jon_fluidsol_norot


  SUBROUTINE fknint(dim_in, f_in, f_data, dim_out, f_out, ifail)
    !! integrand for <f(k)^n>, the pinch angle integration of the vertical drift ^2 frequency for trapped particles
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail

    REAL(KIND=DBL) :: kk
    INTEGER :: n
    REAL(KIND=DBL)    :: ya, k2
    REAL(KIND=DBL)    :: fki, Eg, Kg

    ERRORSTOP(dim_in > 1, 'fknint input should be 1D')
    ERRORSTOP(dim_out > 1, 'fknint output should be 1D')

    kk = f_in(1)
    n = INT(f_data(1))
    k2 = kk*kk
    ! The term weighting the vertical drift of the trapped (fk) is calculated
    ! The formulation with elliptic integrals is used
    ya = 1.-k2
    Kg = ceik(ya)
    Eg = ceie(ya)

    fki = -1. + (smag(pFFk)*4. + 4./3. * alphax(pFFk)) * &
         &     (k2-1.+Eg/Kg) + 2.*Eg/Kg *(1-4./3. * k2 * alphax(pFFk))

    f_out(1) = fki**n*Kg*kk
    ifail = 0
    END SUBROUTINE fknint

  SUBROUTINE fluidint(dim_in, f_in, f_data, dim_out, f_out, ifail)
    !! integrand for <f(k)^n>, the pinch angle integration of the vertical drift ^2 frequency for trapped particles
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail

    REAL(KIND=DBL) :: lamin
    INTEGER :: caseflag, marker
    REAL(KIND=DBL) :: Tlam
    INTEGER :: ifailloc,i
    REAL(KIND=DBL), DIMENSION(ntheta) :: theta,Tint

    ! From lib/src/slatec/davint.f
    EXTERNAL davint

    ERRORSTOP(dim_in > 1, 'fluidint input should be 1D')
    ERRORSTOP(dim_out > 1, 'fluidint output should be 1D')

    lamin = f_in(1)
    caseflag = INT(f_data(1))
    marker = INT(f_data(2))

    theta=(/((i * pi/(ntheta-1)),i=0,ntheta-1)/) !set theta = [0,pi]
    Tint = 2./SQRT(1-lamin*(1+2.*epsilon(pFFk)*SIN(theta/2.)**2))

    ifailloc=0
    CALL davint(theta,Tint,ntheta,0.,pi,Tlam,ifailloc,marker) !calculate transit time

    IF (ifailloc /= 1) THEN
       IF (verbose .EQV. .TRUE.) WRITE(stderr,"(A,I0,A,I7)") 'ifailloc = ',ifailloc,&
            &'. Abnormal termination of fluid solution T integration at p=',pFFk
    ENDIF

    SELECT CASE (caseflag)
    CASE (0)
       f_out(1) = 1./(4.*pi)*Tlam
    CASE (1)
       f_out(1) = 1./(4.*pi)*Tlam*lamin
    CASE (2)
       f_out(1) =  1./(4.*pi)*Tlam*(1.-lamin)
    CASE (3)
       f_out(1) = -1./(4.*pi)*Tlam*lamin*epsilon(pFFk)/2.
    CASE (4)
       f_out(1) = 1./(4.*pi)*Tlam*(2.-lamin)**2
    END SELECT

    ifail = 0
  END SUBROUTINE fluidint

  SUBROUTINE ana_fluidsol( p, nu, gammasol )  
    ! Simple solution for fluid growth rate, based on maximum growth rate from all possible 
    ! ionic or electron slab or interchange analytial growth rates
    ! NOTE: only provides imaginary part
    INTEGER, INTENT(IN)  :: p, nu
    REAL(KIND=DBL), INTENT(OUT) :: gammasol

    ! Variables locales
    REAL(KIND=DBL) :: Lpe,wge,wpe,wne, Vte, de
    REAL(KIND=DBL) :: Ls,kteta, rhoeffsum, deff, ceffsum, ceff, tauoverZbar, gameffsum,gameff
    REAL(KIND=DBL) :: interion,interel,slabion,slabel,inter,slab
    REAL(KIND=DBL), DIMENSION(nions) :: Lpi,wgi,wpi,wni

    INTEGER :: i

    ! Define length and time scales of problem
    kteta = ntor(p,nu) * ktetasn(p)
    Ls = Ro(p)*qx(p)/MAX(ABS(smag(p)),0.1)
    wge = wg(p)*Tex(p) 
    wpe = wge * (Ate(p) + Ane(p))
    wne = wge * Ane(p)
    Lpe = Ro(p) / MAX(Ate(p)+Ane(p),1.)
    DO i = 1,nions
       Lpi(i) = Ro(p) / MAX(Ati(p,i)+Ani(p,i),1.)
       wgi(i) = -wg(p)*Tix(p,i)/Zi(p,i)
       wpi(i) = wgi(i) * (Ati(p,i) + Ani(p,i))
       wni(i) = wgi(i) * Ani(p,i)
    ENDDO
    rhoeffsum = 0
    ceffsum = 0
    tauoverZbar = 0
    gameffsum = 0
    DO i = 1,nions
       IF (ion_type(p,i) == 1) THEN
          rhoeffsum  = rhoeffsum + tau(p,i)*ninorm(p,i)*Zi(p,i)**2. * Rhoi(p,i)**2. !This is rhoeff**2
          ceffsum  = ceffsum + tau(p,i)*ninorm(p,i)*Zi(p,i)**2. * 2*Tex(p)*1d3*qe/mi(p,i)
          tauoverZbar  = tauoverZbar + 1/tau(p,i)*ninorm(p,i)*Lpe/Lpi(i)
          gameffsum = gameffsum + tau(p,i)*ninorm(p,i)*Zi(p,i)**2 * wpi(i)*wgi(i)
       ENDIF
    ENDDO
    Vte = SQRT(2*Tex(p)*1d3*qe/me)
    de = qx(p)/SQRT(( 2*epsilon(p) )) * Rhoe(p)
    deff = SQRT(rhoeffsum*(1+ft(p)/fc(p)*(qx(p)**2)/(2*epsilon(p))))
    !WRITE(*,*) 'deff1 = ',deff
    !deff = SQRT(deffsum*(1+ft(p)/fc(p)/(qx(p)**2)*(2*epsilon(p))))
    !WRITE(*,*) 'deff2 = ',deff,qx(p)**2/2/epsilon(p)
    !WRITE(*,*) Ro(p),Ls/qx(p)*(1/tauoverzbar*ft(p)+1)*SQRT(2*epsilon(p)/(ft(p)*fc(p)))
    ceff = SQRT(ceffsum)
    gameff = SQRT(gameffsum / tauoverZbar)

    IF ( ETG_flag(nu) .EQV. .FALSE. ) THEN !ITG/TEM case
       interel = SQRT(ABS((ft(p)+tauoverzbar)*wge*wpe/fc(p)))/wg(p)
       interion = SQRT(ABS((ft(p)+tauoverzbar)*wgi(1)*wpi(1)/fc(p)) )/wg(p)
       slabel = SQRT( ABS(tauoverzbar*ntor(p,nu)*wpe*kteta*deff*ceff/(2*Ls))  ) / (ntor(p,nu) * wg(p))
       slabion = SQRT( ABS(tauoverzbar*ntor(p,nu)*wpi(1)*kteta*deff*ceff/(2*Ls))  ) / (ntor(p,nu) * wg(p))
       !gammasol = MAX( MAX(interel,interion),MAX(slabel,slabion) )
        gammasol = MAX(interel,interion )
      !DEBUGGING
       !WRITE(*,*) 'ITG/TEM ',interel, interion, slabel, slabion, gammasol
    ELSE !ETG case
       inter = SQRT(ABS(wpe*wge/wg(p)**2 / (tau(p,1)*Zeffx(p))))
       slab = SQRT(ABS(ntor(p,nu)*wpe*qx(p)*kteta*Vte*Rhoe(p)/(2*Ls) / (tau(p,1)*Zeffx(p))))/(ntor(p,nu)*wg(p))
       gammasol = MAX(inter,slab)
       !DEBUGGING
       !WRITE(*,*) 'ETG ',inter,slab,gammasol
    ENDIF

  END SUBROUTINE ana_fluidsol

END MODULE mod_fluidsol
