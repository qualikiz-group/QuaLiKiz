module mod_qualikiz
  !! QuaLiKiz - quasilinear gyrokinetic calculation of growth rates and transport fluxes !
  !!                                                                                     !
  !! Clarisse Bourdelle, Chantal Passeron, 			                      !
  !! Claude Fourment, Giorgio Regnoli,                                                   !
  !! Alessandro Casati, Pierre Cottier, Jonathan Citrin 				      !	
  !!								                      ! 
  !! PARALLEL VERSION                                                                    !
  contains

SUBROUTINE qualikiz(sizes, in_regular, &
                    output_meth_0, &
                    output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                    output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                    primi_meth_0, &
                    output_meth_1, &
                    output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                    output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                    primi_meth_1, &
                    output_meth_2, &
                    output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                    output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                    primi_meth_2, &
                    in_newt, runcounterin)

  !BRIEF EXPLANATION OF MODULES
  !
  !kind defines the kind numbers and output/error units
  !mod_io_managment contains all the subroutines and functions used for file IO
  !mod_make_io contains all routines for (de)allocating, defining, and writing input and output data
  !datmat contains all shared data 
  !datcal contains all shared parameters
  !calcroutines contains all the main calculation routines
  !asymmetry calculates the poloidally dependent quantities relevant for heavy impurities
  !saturation calculates the nonlinear saturation rule and makes the final output

  USE kind
  USE mod_make_io
  USE datmat
  USE datcal
  USE calcroutines  
  USE asymmetry
  USE mod_saturation

  IMPLICIT NONE

  !EXTERNAL phieq

  !Local data dictionary.
  !
  !Time measuring variables
  REAL(kind=DBL) :: tpstot, timetot, timetot2
  INTEGER :: time1, time2, time3, time4, freq
  !MPI variables:
  !TotTask: Total number of Tasks (radial*wavenumber coordinates)
  INTEGER,DIMENSION(MPI_STATUS_SIZE) :: status
  INTEGER, DIMENSION(:), ALLOCATABLE :: wavenum,radcoord
  logical :: run_with_oldsol

  INTEGER :: j,ierror,nproc,TotTask, myrank, ETGind
  integer, optional, intent(in) :: runcounterin
  type(qlk_sizes), intent(in) :: sizes
  type(qlk_in_regular), intent(in) :: in_regular
  type(qlk_in_newt), optional, intent(in) :: in_newt

  type(qlk_output_meth_0)       , intent(out) :: output_meth_0
  type(qlk_output_meth_0_sep_0) , intent(out) :: output_meth_0_sep_0_SI , output_meth_0_sep_0_GB
  type(qlk_output_meth_0_sep_1) , optional, intent(out) :: output_meth_0_sep_1_SI, output_meth_0_sep_1_GB
  type(qlk_output_meth_1)       , optional, intent(out) :: output_meth_1
  type(qlk_output_meth_1_sep_0) , optional, intent(out) :: output_meth_1_sep_0_SI, output_meth_1_sep_0_GB
  type(qlk_output_meth_1_sep_1) , optional, intent(out) :: output_meth_1_sep_1_SI, output_meth_1_sep_1_GB
  type(qlk_output_meth_2)       , optional, intent(out) :: output_meth_2
  type(qlk_output_meth_2_sep_0) , optional, intent(out) :: output_meth_2_sep_0_SI, output_meth_2_sep_0_GB
  type(qlk_output_meth_2_sep_1) , optional, intent(out) :: output_meth_2_sep_1_SI, output_meth_2_sep_1_GB
  type(qlk_primi_meth_0)        , optional, intent(out) :: primi_meth_0
  type(qlk_primi_meth_1)        , optional, intent(out) :: primi_meth_1
  type(qlk_primi_meth_2)        , optional, intent(out) :: primi_meth_2

  ! final output arrays following saturation rule
  !REAL(KIND=DBL), DIMENSION(dimx)  :: pfe_SIout,efe_SIout
  !REAL(KIND=DBL), DIMENSION(dimx,nions) :: pfi_SIout,efi_SIout,vfi_SIout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeETG_SIout,efeETG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeTEM_SIout,efeTEM_GBout,pfeTEM_SIout,pfeTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: dfeTEM_SIout,vteTEM_SIout,vceTEM_SIout,dfeTEM_GBout,vteTEM_GBout,vceTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeTEM_SIout,veneTEM_SIout,veceTEM_SIout,chieeTEM_GBout,veneTEM_GBout,veceTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeITG_SIout,efeITG_GBout,pfeITG_SIout,pfeITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: dfeITG_SIout,vteITG_SIout,vceITG_SIout,dfeITG_GBout,vteITG_GBout,vceITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeETG_SIout,veneETG_SIout,veceETG_SIout,chieeETG_GBout,veneETG_GBout,veceETG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeITG_SIout,veneITG_SIout,veceITG_SIout,chieeITG_GBout,veneITG_GBout,veceITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: efiTEM_SIout,efiTEM_GBout,pfiTEM_SIout,pfiTEM_GBout,vfiTEM_SIout,vfiTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: dfiTEM_SIout,vtiTEM_SIout,vciTEM_SIout,vriTEM_SIout,dfiTEM_GBout,vtiTEM_GBout,vciTEM_GBout,vriTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: chieiTEM_SIout,veniTEM_SIout,veciTEM_SIout,veriTEM_SIout,chieiTEM_GBout,veniTEM_GBout,veciTEM_GBout,veriTEM_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: efiITG_SIout,efiITG_GBout,pfiITG_SIout,pfiITG_GBout,vfiITG_SIout,vfiITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: dfiITG_SIout,vtiITG_SIout,vciITG_SIout,vriITG_SIout,dfiITG_GBout,vtiITG_GBout,vciITG_GBout,vriITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: chieiITG_SIout,veniITG_SIout,veciITG_SIout,veriITG_SIout,chieiITG_GBout,veniITG_GBout,veciITG_GBout,veriITG_GBout
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeETG_SIouttmp,efeETG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeETG_SIouttmp,veneETG_SIouttmp,veceETG_SIouttmp,chieeETG_GBouttmp,veneETG_GBouttmp,veceETG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeTEM_SIouttmp,pfeTEM_SIouttmp,efeTEM_GBouttmp,pfeTEM_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: dfeTEM_SIouttmp,vteTEM_SIouttmp,vceTEM_SIouttmp,dfeTEM_GBouttmp,vteTEM_GBouttmp,vceTEM_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeTEM_SIouttmp,veneTEM_SIouttmp,veceTEM_SIouttmp,chieeTEM_GBouttmp,veneTEM_GBouttmp,veceTEM_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: efeITG_SIouttmp,pfeITG_SIouttmp,efeITG_GBouttmp,pfeITG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: dfeITG_SIouttmp,vteITG_SIouttmp,vceITG_SIouttmp,dfeITG_GBouttmp,vteITG_GBouttmp,vceITG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:) :: chieeITG_SIouttmp,veneITG_SIouttmp,veceITG_SIouttmp,chieeITG_GBouttmp,veneITG_GBouttmp,veceITG_GBouttmp

  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: efiTEM_SIouttmp,efiTEM_GBouttmp,pfiTEM_SIouttmp,pfiTEM_GBouttmp,vfiTEM_SIouttmp,vfiTEM_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: dfiTEM_SIouttmp,vtiTEM_SIouttmp,vciTEM_SIouttmp,dfiTEM_GBouttmp,vtiTEM_GBouttmp,vciTEM_GBouttmp,vriTEM_SIouttmp,vriTEM_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: chieiTEM_SIouttmp,veniTEM_SIouttmp,veciTEM_SIouttmp,chieiTEM_GBouttmp,veniTEM_GBouttmp,veciTEM_GBouttmp,veriTEM_SIouttmp,veriTEM_GBouttmp

  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: efiITG_SIouttmp,efiITG_GBouttmp,pfiITG_SIouttmp,pfiITG_GBouttmp,vfiITG_SIouttmp,vfiITG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: dfiITG_SIouttmp,vtiITG_SIouttmp,vciITG_SIouttmp,vriITG_SIouttmp,dfiITG_GBouttmp,vtiITG_GBouttmp,vciITG_GBouttmp,vriITG_GBouttmp
  REAL(KIND=DBL), ALLOCATABLE, DIMENSION(:,:) :: chieiITG_SIouttmp,veniITG_SIouttmp,veciITG_SIouttmp,chieiITG_GBouttmp,veniITG_GBouttmp,veciITG_GBouttmp,veriITG_SIouttmp,veriITG_GBouttmp



  !__________________END OF LOCAL DATA DICTIONARY__________________________

  !DEBUGGING
  !CHARACTER(len=20) :: fmtn

  ! -- MPI Initialisation -- !
  CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
  CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)

  CALL SYSTEM_CLOCK(time1)

  ! Make the input (including derived quantities)
  CALL make_input(sizes, in_regular)

  ! set optional input
  IF (PRESENT(runcounterin)) THEN
     runcounter=runcounterin
  ELSE
     runcounter = 0
  ENDIF

  ! Run with old solution
  IF (runcounter < in_regular%maxruns .and. runcounter /= 0) THEN
     IF (verbose .and. myrank == 0) THEN
        WRITE(stdout,"(A)") 'Running QuaLiKiz skipping full contour integrations and starting Newton iteration &
          & using previous solution as first guess'
     ENDIF
     run_with_oldsol = .TRUE.
  ELSE
     IF (verbose .and. myrank == 0) THEN
        WRITE(stdout,"(A)") 'Running full QuaLiKiz with full contour integration'
     ENDIF
     run_with_oldsol = .FALSE.
  ENDIF

  IF (run_with_oldsol) THEN
     allocate(oldsol(sizes%dimx, sizes%dimn, sizes%numsols))
     allocate(oldfdsol(sizes%dimx, sizes%dimn, sizes%numsols))
     oldsol = in_newt%oldsol
     oldfdsol = in_newt%oldfdsol
  ENDIF

  !Check sanity of input (these can be much expanded and set as a separate subroutine)
  IF (myrank == 0) THEN
     IF ( (onlyion .EQV. .TRUE.) .AND. (onlyelec .EQV. .TRUE.) ) THEN
        WRITE(stderr,*) 'onlyion and onlyelec both set to true! Abandon ship...'
        CALL mpi_abort(mpi_comm_world, -1, ierror)
     ENDIF

     IF ( (rot_flag < 0) .OR. (rot_flag > 2) ) THEN
        WRITE(stderr,*) 'rot_flag must be between 0 and 2! Abandon ship...'
        CALL mpi_abort(mpi_comm_world, -1, ierror)
     ENDIF

     !Find index of first ETG-scale mode (if it exists)
     ETGind=0
     DO j=1,dimn
        IF (kthetarhos(j) > ETGk) THEN         
           ETGind = j
           EXIT
        ENDIF
     ENDDO

     !Check kthetarhos grid for sanity
     !Case 1: 1 mode only (ETG or ITG). Do nothing. mod_saturation can handle this
     !Case 2: Only 2 ETG modes. Crash gracefully. Otherwise davint will crash in mod_saturation
     !Case 3: No modes in gap. Crash gracefully. A mode is needed between 2<kthetarhos<5 for robust davint integration
     IF ( (dimn > 1) .AND. ( ETGind > 0 ) .AND. ( dimn - ETGind == 1  ) ) THEN
        WRITE(stderr,*) 'ETG scales cannot contain only 2 values, due to spectral integration constraint! Abandon ship...'
        CALL mpi_abort(mpi_comm_world, -1, ierror)
     ENDIF
     IF ( (dimn > 1) .AND. ( ETGind > 0 ) ) THEN
        IF ( kthetarhos(ETGind) > 5 )  THEN
           WRITE(stderr,*) 'For robust spectral integration in ETG scales, at least one mode in the ITG-ETG gap is necessary (2<kthetarhos<5)! Abandon ship...'
           CALL mpi_abort(mpi_comm_world, -1, ierror)
        ENDIF
     ENDIF
  ENDIF

  CALL MPI_Barrier(mpi_comm_world,ierror)

  !Allocation and initialization of calculated arrays (named "output")
  CALL allocate_output() !subroutine found in mod_make_io
  CALL init_asym() !subroutine in datcal. Initializes variables used for Fried-Conte function asymptotic expansions


  CALL calcphi() !calculate poloidal density asymmetries due to rotation and temp anisotropy
  CALL calcdphi() !calculate radial and poloidal gradient of 2D electrostatic potential
  CALL calccoefs() !calculate e# LFS to FSA transport coefficients, FSA R/Ln, FSA n, asym factor, and also 2D R/Ln for all ion species
  !The poloidal asymmetry terms take around 10ms to calculate

  weidcount = 0 !Initialize dispersion relation function call counter. Used for debugging purposes
  Nsolrat = 0   !Initializes count of failed solutions (see calcroutines). Rarely occurs.

  !Total number of jobs to run
  TotTask=dimx*dimn

  !Initialize output related arrays
  IF (myrank == 0) THEN
     IF(ALLOCATED(wavenum) .EQV. .FALSE.) ALLOCATE(wavenum(TotTask))
     IF(ALLOCATED(radcoord) .EQV. .FALSE.) ALLOCATE(radcoord(TotTask))
  END IF

  CALL MPI_Barrier(mpi_comm_world,ierror)

  !! NOW THE MAGIC HAPPENS!! This subroutine is contained below
  !! Distributes tasks to all processors and calculates output

  CALL DistriTask(TotTask,nproc,myrank, in_regular)

  CALL MPI_Barrier(mpi_comm_world,ierror)

  IF (myrank==0) THEN
     IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A)") '*** Collecting output'
  ENDIF

  !Consolidate all arrays into rank=0 for QL flux integrals and output
  IF (nproc > 1) THEN
     IF (myrank==0) CALL SYSTEM_CLOCK(time3)
     CALL collectarrays()
  ENDIF
  CALL MPI_Barrier(mpi_comm_world,ierror)

  CALL allocate_endoutput()
  call allocatenewoutput(sizes, in_regular, &
                       output_meth_0, &
                       output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                       output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                       output_meth_1, &
                       output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                       output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                       output_meth_2, &
                       output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                       output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                       primi_meth_0, &
                       primi_meth_1, &
                       primi_meth_2)

  !If rank0, then carry out the saturation rules and output final results. This will soon be parallelized too. Trivial over dimx
  IF (myrank==0) THEN 
     CALL SYSTEM_CLOCK(time4)
     CALL SYSTEM_CLOCK(count_rate=freq)
     IF (nproc > 1) THEN
        timetot = REAL(time4-time3) / REAL(freq)
        IF (verbose .EQV. .TRUE.) WRITE(stdout,*)
        IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,F11.3,A)") 'Profiling: First MPI_AllReduce time = ',timetot,' s'  !final write
     ENDIF
     CALL SYSTEM_CLOCK(time2)
     CALL SYSTEM_CLOCK(count_rate=freq)
     timetot = REAL(time2-time1) / REAL(freq)
     IF (verbose .EQV. .TRUE.) WRITE(stdout,*)
     IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,F11.3,A)") 'Profiling: Hurrah! All eigenmodes calculated! Time = ',timetot,' s'  !final write

     IF (verbose .EQV. .TRUE.) WRITE(stdout,*)     
     IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A)") '*** Calculating nonlinear saturation rule'

     CALL SYSTEM_CLOCK(time1)
  ENDIF

  IF (separateflux .EQV. .TRUE.) THEN
     IF (myrank==0) THEN
        IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A)") '*** separateflux=T ,  NL saturation rule for separate modes also calculated'
        IF (verbose .EQV. .TRUE.) WRITE(stdout,*)     
     ENDIF
     CALL saturation(1) !set 0 for including all modes, 1 for only ITG, 2 for only TEM, 3 for only ETG
     allocate(efeITG_GBout(dimx))
     allocate(pfeITG_GBout(dimx))

     allocate(efiITG_GBout(dimx, nions))
     allocate(pfiITG_GBout(dimx, nions))
     allocate(vfiITG_GBout(dimx, nions))

     allocate(efeITG_SIout(dimx))
     allocate(pfeITG_SIout(dimx))

     allocate(efiITG_SIout(dimx, nions))
     allocate(pfiITG_SIout(dimx, nions))
     allocate(vfiITG_SIout(dimx, nions))

     allocate(efeITG_GBouttmp(dimx))
     allocate(pfeITG_GBouttmp(dimx))

     allocate(efiITG_GBouttmp(dimx, nions))
     allocate(pfiITG_GBouttmp(dimx, nions))
     allocate(vfiITG_GBouttmp(dimx, nions))

     allocate(efeITG_SIouttmp(dimx))
     allocate(pfeITG_SIouttmp(dimx))

     allocate(efiITG_SIouttmp(dimx, nions))
     allocate(pfiITG_SIouttmp(dimx, nions))
     allocate(vfiITG_SIouttmp(dimx, nions))


     efeITG_GBout = eef_GB
     pfeITG_GBout = epf_GB

     efiITG_GBout = ief_GB
     pfiITG_GBout = ipf_GB
     vfiITG_GBout = ivf_GB

     efeITG_SIout = eef_SI
     pfeITG_SIout = epf_SI

     efiITG_SIout = ief_SI
     pfiITG_SIout = ipf_SI
     vfiITG_SIout = ivf_SI

     if (in_regular%phys_meth /= 0) then
       allocate(dfeITG_GBout(dimx))
       allocate(vteITG_GBout(dimx))
       allocate(vceITG_GBout(dimx))

       allocate(dfiITG_GBout(dimx, nions))
       allocate(vtiITG_GBout(dimx, nions))
       allocate(vciITG_GBout(dimx, nions))
       allocate(vriITG_GBout(dimx, nions))

       allocate(dfeITG_SIout(dimx))
       allocate(vteITG_SIout(dimx))
       allocate(vceITG_SIout(dimx))

       allocate(dfiITG_SIout(dimx, nions))
       allocate(vtiITG_SIout(dimx, nions))
       allocate(vciITG_SIout(dimx, nions))
       allocate(vriITG_SIout(dimx, nions))

       allocate(dfeITG_GBouttmp(dimx))
       allocate(vteITG_GBouttmp(dimx))
       allocate(vceITG_GBouttmp(dimx))

       allocate(dfiITG_GBouttmp(dimx, nions))
       allocate(vtiITG_GBouttmp(dimx, nions))
       allocate(vciITG_GBouttmp(dimx, nions))
       allocate(vriITG_GBouttmp(dimx, nions))

       allocate(dfeITG_SIouttmp(dimx))
       allocate(vteITG_SIouttmp(dimx))
       allocate(vceITG_SIouttmp(dimx))

       allocate(dfiITG_SIouttmp(dimx, nions))
       allocate(vtiITG_SIouttmp(dimx, nions))
       allocate(vciITG_SIouttmp(dimx, nions))
       allocate(vriITG_SIouttmp(dimx, nions))

       dfeITG_GBout = dfe_GB
       vteITG_GBout = vte_GB
       vceITG_GBout = vce_GB

       dfiITG_GBout = dfi_GB
       vtiITG_GBout = vti_GB
       vciITG_GBout = vci_GB
       vriITG_GBout = vri_GB

       dfeITG_SIout = dfe_SI
       vteITG_SIout = vte_SI
       vceITG_SIout = vce_SI

       dfiITG_SIout = dfi_SI
       vtiITG_SIout = vti_SI
       vciITG_SIout = vci_SI
       vriITG_SIout = vri_SI
     endif

     if (in_regular%phys_meth == 2) then
       allocate(veneITG_GBout(dimx))
       allocate(veceITG_GBout(dimx))

       allocate(veniITG_GBout(dimx, nions))
       allocate(veciITG_GBout(dimx, nions))
       allocate(veriITG_GBout(dimx, nions))

       allocate(chieeITG_GBout(dimx))
       allocate(chieiITG_GBout(dimx, nions))

       allocate(veneITG_SIout(dimx))
       allocate(veceITG_SIout(dimx))

       allocate(veniITG_SIout(dimx, nions))
       allocate(veciITG_SIout(dimx, nions))
       allocate(veriITG_SIout(dimx, nions))

       allocate(chieeITG_SIout(dimx))
       allocate(chieiITG_SIout(dimx, nions))

       allocate(veneITG_GBouttmp(dimx))
       allocate(veceITG_GBouttmp(dimx))

       allocate(veniITG_GBouttmp(dimx, nions))
       allocate(veciITG_GBouttmp(dimx, nions))
       allocate(veriITG_GBouttmp(dimx, nions))

       allocate(chieeITG_GBouttmp(dimx))
       allocate(chieiITG_GBouttmp(dimx, nions))

       allocate(veneITG_SIouttmp(dimx))
       allocate(veceITG_SIouttmp(dimx))

       allocate(veniITG_SIouttmp(dimx, nions))
       allocate(veciITG_SIouttmp(dimx, nions))
       allocate(veriITG_SIouttmp(dimx, nions))

       allocate(chieeITG_SIouttmp(dimx))
       allocate(chieiITG_SIouttmp(dimx, nions))


       veneITG_GBout  = vene_GB
       veceITG_GBout  = vece_GB

       veniITG_GBout  = veni_GB
       veciITG_GBout  = veci_GB
       veriITG_GBout  = veri_GB

       chieeITG_GBout = chiee_GB
       chieiITG_GBout = chiei_GB

       veneITG_SIout  = vene_SI
       veceITG_SIout  = vece_SI

       veniITG_SIout  = veni_SI
       veciITG_SIout  = veci_SI
       veriITG_SIout  = veri_SI

       chieeITG_SIout = chiee_SI
       chieiITG_SIout = chiei_SI
     endif

     CALL saturation(2) !set 0 for including all modes, 1 for only ITG, 2 for only TEM, 3 for only ETG
     allocate(efeTEM_GBout(dimx))
     allocate(pfeTEM_GBout(dimx))

     allocate(efiTEM_GBout(dimx, nions))
     allocate(pfiTEM_GBout(dimx, nions))
     allocate(vfiTEM_GBout(dimx, nions))

     allocate(efeTEM_SIout(dimx))
     allocate(pfeTEM_SIout(dimx))

     allocate(efiTEM_SIout(dimx, nions))
     allocate(pfiTEM_SIout(dimx, nions))
     allocate(vfiTEM_SIout(dimx, nions))

     allocate(efeTEM_GBouttmp(dimx))
     allocate(pfeTEM_GBouttmp(dimx))

     allocate(efiTEM_GBouttmp(dimx, nions))
     allocate(pfiTEM_GBouttmp(dimx, nions))
     allocate(vfiTEM_GBouttmp(dimx, nions))

     allocate(efeTEM_SIouttmp(dimx))
     allocate(pfeTEM_SIouttmp(dimx))

     allocate(efiTEM_SIouttmp(dimx, nions))
     allocate(pfiTEM_SIouttmp(dimx, nions))
     allocate(vfiTEM_SIouttmp(dimx, nions))


     efeTEM_GBout = eef_GB
     pfeTEM_GBout = epf_GB

     efiTEM_GBout = ief_GB
     pfiTEM_GBout = ipf_GB
     vfiTEM_GBout = ivf_GB

     efeTEM_SIout = eef_SI
     pfeTEM_SIout = epf_SI

     efiTEM_SIout = ief_SI
     pfiTEM_SIout = ipf_SI
     vfiTEM_SIout = ivf_SI

     if (in_regular%phys_meth /= 0) then
       allocate(dfeTEM_GBout(dimx))
       allocate(vteTEM_GBout(dimx))
       allocate(vceTEM_GBout(dimx))

       allocate(dfiTEM_GBout(dimx, nions))
       allocate(vtiTEM_GBout(dimx, nions))
       allocate(vciTEM_GBout(dimx, nions))
       allocate(vriTEM_GBout(dimx, nions))

       allocate(dfeTEM_SIout(dimx))
       allocate(vteTEM_SIout(dimx))
       allocate(vceTEM_SIout(dimx))

       allocate(dfiTEM_SIout(dimx, nions))
       allocate(vtiTEM_SIout(dimx, nions))
       allocate(vciTEM_SIout(dimx, nions))
       allocate(vriTEM_SIout(dimx, nions))

       allocate(dfeTEM_GBouttmp(dimx))
       allocate(vteTEM_GBouttmp(dimx))
       allocate(vceTEM_GBouttmp(dimx))

       allocate(dfiTEM_GBouttmp(dimx, nions))
       allocate(vtiTEM_GBouttmp(dimx, nions))
       allocate(vciTEM_GBouttmp(dimx, nions))
       allocate(vriTEM_GBouttmp(dimx, nions))

       allocate(dfeTEM_SIouttmp(dimx))
       allocate(vteTEM_SIouttmp(dimx))
       allocate(vceTEM_SIouttmp(dimx))

       allocate(dfiTEM_SIouttmp(dimx, nions))
       allocate(vtiTEM_SIouttmp(dimx, nions))
       allocate(vciTEM_SIouttmp(dimx, nions))
       allocate(vriTEM_SIouttmp(dimx, nions))

       dfeTEM_GBout = dfe_GB
       vteTEM_GBout = vte_GB
       vceTEM_GBout = vce_GB

       dfiTEM_GBout = dfi_GB
       vtiTEM_GBout = vti_GB
       vciTEM_GBout = vci_GB
       vriTEM_GBout = vri_GB

       dfeTEM_SIout = dfe_SI
       vteTEM_SIout = vte_SI
       vceTEM_SIout = vce_SI

       dfiTEM_SIout = dfi_SI
       vtiTEM_SIout = vti_SI
       vciTEM_SIout = vci_SI
       vriTEM_SIout = vri_SI
     endif

     if (in_regular%phys_meth == 2) then
       allocate(veneTEM_GBout(dimx))
       allocate(veceTEM_GBout(dimx))

       allocate(veniTEM_GBout(dimx, nions))
       allocate(veciTEM_GBout(dimx, nions))
       allocate(veriTEM_GBout(dimx, nions))

       allocate(chieeTEM_GBout(dimx))
       allocate(chieiTEM_GBout(dimx, nions))

       allocate(veneTEM_SIout(dimx))
       allocate(veceTEM_SIout(dimx))

       allocate(veniTEM_SIout(dimx, nions))
       allocate(veciTEM_SIout(dimx, nions))
       allocate(veriTEM_SIout(dimx, nions))

       allocate(chieeTEM_SIout(dimx))
       allocate(chieiTEM_SIout(dimx, nions))

       allocate(veneTEM_GBouttmp(dimx))
       allocate(veceTEM_GBouttmp(dimx))

       allocate(veniTEM_GBouttmp(dimx, nions))
       allocate(veciTEM_GBouttmp(dimx, nions))
       allocate(veriTEM_GBouttmp(dimx, nions))

       allocate(chieeTEM_GBouttmp(dimx))
       allocate(chieiTEM_GBouttmp(dimx, nions))

       allocate(veneTEM_SIouttmp(dimx))
       allocate(veceTEM_SIouttmp(dimx))

       allocate(veniTEM_SIouttmp(dimx, nions))
       allocate(veciTEM_SIouttmp(dimx, nions))
       allocate(veriTEM_SIouttmp(dimx, nions))

       allocate(chieeTEM_SIouttmp(dimx))
       allocate(chieiTEM_SIouttmp(dimx, nions))


       veneTEM_GBout  = vene_GB
       veceTEM_GBout  = vece_GB

       veniTEM_GBout  = veni_GB
       veciTEM_GBout  = veci_GB
       veriTEM_GBout  = veri_GB

       chieeTEM_GBout = chiee_GB
       chieiTEM_GBout = chiei_GB

       veneTEM_SIout  = vene_SI
       veceTEM_SIout  = vece_SI

       veniTEM_SIout  = veni_SI
       veciTEM_SIout  = veci_SI
       veriTEM_SIout  = veri_SI

       chieeTEM_SIout = chiee_SI
       chieiTEM_SIout = chiei_SI
     endif
  ENDIF
  ALLOCATE(efeETG_SIouttmp(dimx))
  ALLOCATE(efeETG_GBouttmp(dimx))
  ALLOCATE(efeETG_SIout(dimx))
  ALLOCATE(efeETG_GBout(dimx))
  efeETG_SIouttmp = 0
  efeETG_SIout = 0
  efeETG_GBouttmp = 0
  efeETG_GBout = 0
  CALL saturation(0) !set 0 for including all modes, 1 for only ITG, 2 for only TEM
  efeETG_SIout = eefETG_SI
  efeETG_GBout = eefETG_GB
  if ((in_regular%phys_meth == 2) .and. (in_regular%separateflux == 1)) then
    ALLOCATE(chieeETG_SIouttmp(dimx))
    ALLOCATE(veneETG_SIouttmp(dimx))
    ALLOCATE(veceETG_SIouttmp(dimx))
    ALLOCATE(chieeETG_SIout(dimx))
    ALLOCATE(veneETG_SIout(dimx))
    ALLOCATE(veceETG_SIout(dimx))

    ALLOCATE(chieeETG_GBouttmp(dimx))
    ALLOCATE(veneETG_GBouttmp(dimx))
    ALLOCATE(veceETG_GBouttmp(dimx))
    ALLOCATE(chieeETG_GBout(dimx))
    ALLOCATE(veneETG_GBout(dimx))
    ALLOCATE(veceETG_GBout(dimx))

    chieeETG_SIouttmp = 0
    veneETG_SIouttmp = 0
    veceETG_SIouttmp = 0
    chieeETG_SIout = 0
    veneETG_SIout = 0
    veceETG_SIout = 0
    chieeETG_GBouttmp = 0
    veneETG_GBouttmp = 0
    veceETG_GBouttmp = 0
    chieeETG_GBout = 0
    veneETG_GBout = 0
    veceETG_GBout = 0

    chieeETG_SIout = chieeETG_SI
    veneETG_SIout = veneETG_SI
    veceETG_SIout = veceETG_SI

    chieeETG_GBout = chieeETG_GB
    veneETG_GBout = veneETG_GB
    veceETG_GBout = veceETG_GB
  endif

  IF (myrank==0) THEN
     CALL SYSTEM_CLOCK(time2)
     CALL SYSTEM_CLOCK(count_rate=freq)
     timetot2 = REAL(time2-time1) / REAL(freq)
     IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,F11.3,A)") 'Profiling: saturation rule calculation time = ',timetot2,' s'  
     WRITE(stdout,"(A,F7.3,A)") 'Hurrah! QuaLiKiz Job completed! Total time = ',timetot+timetot2,' s'  !final write

     OPEN(unit=900, file="lastruntime.qlk", action="write", status="replace")
     WRITE(900,"(A,F7.3,A)") 'Last completed run time = ',timetot+timetot2,' s'  !final write
     CLOSE(900)

!!!DEBUGGING FOR FLUID SOLUTION
!!$     WRITE(fmtn,'(A,I0, A)') '(',dimn,'G15.7)'
!!$     OPEN(unit=900, file="output/primitive/rjonsolflu.dat", action="write", status="replace")
!!$     WRITE(900,fmtn) ((REAL(jon_solflu(i,j)),j=1,dimn),i=1,dimx) ; CLOSE(900)
!!$     OPEN(unit=900, file="output/primitive/ijonsolflu.dat", action="write", status="replace")
!!$     WRITE(900,fmtn) ((AIMAG(jon_solflu(i,j)),j=1,dimn),i=1,dimx) ; CLOSE(900)
  ENDIF
  !DEBUG
!!$
!!$    OPEN(unit=900, file="FLRep.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') ((FLRep(i,j),j=1,dimn),i=1,dimx) ;  CLOSE(900)
!!$
!!$    OPEN(unit=900, file="FLRip.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') (((FLRip(i,j,k),j=1,dimn),i=1,dimx),k=1,nions) ;  CLOSE(900)
!!$
!!$    OPEN(unit=900, file="rmodewidth.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') ((REAL(modewidth(i,j)),j=1,dimn),i=1,dimx) ;  CLOSE(900)
!!$
!!$    OPEN(unit=900, file="rmodeshift.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') ((REAL(modeshift(i,j)),j=1,dimn),i=1,dimx) ;  CLOSE(900)
!!$
!!$    OPEN(unit=900, file="imodewidth.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') ((AIMAG(modewidth(i,j)),j=1,dimn),i=1,dimx) ;  CLOSE(900)
!!$
!!$    OPEN(unit=900, file="imodeshift.dat", action="write", status="replace")
!!$    WRITE(900,'(16G15.7)') ((AIMAG(modeshift(i,j)),j=1,dimn),i=1,dimx) ;  CLOSE(900)

  IF (myrank==0) CALL SYSTEM_CLOCK(time3)

  CALL reduceoutput() ! spread all output to all cores

  call setoutput(in_regular, in_newt, &
                 output_meth_0, &
                 output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                 output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                 output_meth_1, &
                 output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                 output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                 output_meth_2, &
                 output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                 output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                 primi_meth_0, &
                 primi_meth_1, &
                 primi_meth_2)

  !messy setting separated flux output if they exist

  if (separateflux .EQV. .TRUE.) THEN
    CALL MPI_AllReduce(efeITG_GBout  , efeITG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfeITG_GBout  , pfeITG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efiITG_GBout  , efiITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfiITG_GBout  , pfiITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(vfiITG_GBout  , vfiITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efeITG_SIout  , efeITG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfeITG_SIout  , pfeITG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efiITG_SIout  , efiITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(vfiITG_SIout  , vfiITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfiITG_SIout  , pfiITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efeTEM_GBout  , efeTEM_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfeTEM_GBout  , pfeTEM_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efiTEM_GBout  , efiTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfiTEM_GBout  , pfiTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(vfiTEM_GBout  , vfiTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efeTEM_SIout  , efeTEM_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfeTEM_SIout  , pfeTEM_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(efiTEM_SIout  , efiTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(vfiTEM_SIout  , vfiTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(pfiTEM_SIout  , pfiTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    if (in_regular%phys_meth /= 0) then
      CALL MPI_AllReduce(dfeITG_GBout  , dfeITG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vteITG_GBout  , vteITG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vceITG_GBout  , vceITG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfiITG_GBout  , dfiITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vtiITG_GBout  , vtiITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vciITG_GBout  , vciITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vriITG_GBout  , vriITG_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfeITG_SIout  , dfeITG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vteITG_SIout  , vteITG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vceITG_SIout  , vceITG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfiITG_SIout  , dfiITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vtiITG_SIout  , vtiITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vciITG_SIout  , vciITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vriITG_SIout  , vriITG_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfeTEM_GBout  , dfeTEM_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vteTEM_GBout  , vteTEM_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vceTEM_GBout  , vceTEM_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfiTEM_GBout  , dfiTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vtiTEM_GBout  , vtiTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vciTEM_GBout  , vciTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vriTEM_GBout  , vriTEM_GBouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfeTEM_SIout  , dfeTEM_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vteTEM_SIout  , vteTEM_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vceTEM_SIout  , vceTEM_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(dfiTEM_SIout  , dfiTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vtiTEM_SIout  , vtiTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vciTEM_SIout  , vciTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(vriTEM_SIout  , vriTEM_SIouttmp  , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    endif

    if (in_regular%phys_meth == 2) then
      CALL MPI_AllReduce(veneITG_GBout , veneITG_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veceITG_GBout , veceITG_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veniITG_GBout , veniITG_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veciITG_GBout , veciITG_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veriITG_GBout , veriITG_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieeITG_GBout, chieeITG_GBouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieiITG_GBout, chieiITG_GBouttmp, dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veneITG_SIout , veneITG_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veceITG_SIout , veceITG_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veniITG_SIout , veniITG_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veciITG_SIout , veciITG_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veriITG_SIout , veriITG_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieeITG_SIout, chieeITG_SIouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(chieiITG_SIout, chieiITG_SIouttmp, dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veneTEM_GBout , veneTEM_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veceTEM_GBout , veceTEM_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veniTEM_GBout , veniTEM_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veciTEM_GBout , veciTEM_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veriTEM_GBout , veriTEM_GBouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieeTEM_GBout, chieeTEM_GBouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieiTEM_GBout, chieiTEM_GBouttmp, dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veneTEM_SIout , veneTEM_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veceTEM_SIout , veceTEM_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(veniTEM_SIout , veniTEM_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veciTEM_SIout , veciTEM_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(veriTEM_SIout , veriTEM_SIouttmp , dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

      CALL MPI_AllReduce(chieeTEM_SIout, chieeTEM_SIouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
      CALL MPI_AllReduce(chieiTEM_SIout, chieiTEM_SIouttmp, dimx*nions, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    endif
  endif

  CALL MPI_AllReduce(efeETG_SIout  , efeETG_SIouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
  CALL MPI_AllReduce(efeETG_GBout  , efeETG_GBouttmp  , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

  if ((in_regular%phys_meth == 2) .and. (in_regular%separateflux == 1)) then
    CALL MPI_AllReduce(chieeETG_SIout, chieeETG_SIouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(veneETG_SIout , veneETG_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(veceETG_SIout , veceETG_SIouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)

    CALL MPI_AllReduce(chieeETG_GBout, chieeETG_GBouttmp, dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(veneETG_GBout , veneETG_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
    CALL MPI_AllReduce(veceETG_GBout , veceETG_GBouttmp , dimx      , MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_world, ierror)
  endif

  IF (in_regular%separateflux == 1) THEN
    output_meth_0_sep_1_GB%efeITG = efeITG_GBouttmp
    output_meth_0_sep_1_GB%pfeITG = pfeITG_GBouttmp

    output_meth_0_sep_1_GB%efiITG = efiITG_GBouttmp
    output_meth_0_sep_1_GB%pfiITG = pfiITG_GBouttmp
    output_meth_0_sep_1_GB%vfiITG = vfiITG_GBouttmp

    output_meth_0_sep_1_SI%efeITG = efeITG_SIouttmp
    output_meth_0_sep_1_SI%pfeITG = pfeITG_SIouttmp

    output_meth_0_sep_1_SI%efiITG = efiITG_SIouttmp
    output_meth_0_sep_1_SI%pfiITG = pfiITG_SIouttmp
    output_meth_0_sep_1_SI%vfiITG = vfiITG_SIouttmp

    output_meth_0_sep_1_GB%efeTEM = efeTEM_GBouttmp
    output_meth_0_sep_1_GB%pfeTEM = pfeTEM_GBouttmp

    output_meth_0_sep_1_GB%efiTEM = efiTEM_GBouttmp
    output_meth_0_sep_1_GB%pfiTEM = pfiTEM_GBouttmp
    output_meth_0_sep_1_GB%vfiTEM = vfiTEM_GBouttmp

    output_meth_0_sep_1_SI%efeTEM = efeTEM_SIouttmp
    output_meth_0_sep_1_SI%pfeTEM = pfeTEM_SIouttmp

    output_meth_0_sep_1_SI%efiTEM = efiTEM_SIouttmp
    output_meth_0_sep_1_SI%pfiTEM = pfiTEM_SIouttmp
    output_meth_0_sep_1_SI%vfiTEM = vfiTEM_SIouttmp

    output_meth_0_sep_1_SI%normalization = 0
    output_meth_0_sep_1_GB%normalization = 1

    if (in_regular%phys_meth /= 0) then
      output_meth_1_sep_1_GB%dfeITG = dfeITG_GBouttmp
      output_meth_1_sep_1_GB%vteITG = vteITG_GBouttmp
      output_meth_1_sep_1_GB%vceITG = vceITG_GBouttmp

      output_meth_1_sep_1_GB%dfiITG = dfiITG_GBouttmp
      output_meth_1_sep_1_GB%vtiITG = vtiITG_GBouttmp
      output_meth_1_sep_1_GB%vciITG = vciITG_GBouttmp
      output_meth_1_sep_1_GB%vriITG = vriITG_GBouttmp

      output_meth_1_sep_1_SI%dfeITG = dfeITG_SIouttmp
      output_meth_1_sep_1_SI%vteITG = vteITG_SIouttmp
      output_meth_1_sep_1_SI%vceITG = vceITG_SIouttmp

      output_meth_1_sep_1_SI%dfiITG = dfiITG_SIouttmp
      output_meth_1_sep_1_SI%vtiITG = vtiITG_SIouttmp
      output_meth_1_sep_1_SI%vciITG = vciITG_SIouttmp
      output_meth_1_sep_1_SI%vriITG = vriITG_SIouttmp

      output_meth_1_sep_1_GB%dfeTEM = dfeTEM_GBouttmp
      output_meth_1_sep_1_GB%vteTEM = vteTEM_GBouttmp
      output_meth_1_sep_1_GB%vceTEM = vceTEM_GBouttmp

      output_meth_1_sep_1_GB%dfiTEM = dfiTEM_GBouttmp
      output_meth_1_sep_1_GB%vtiTEM = vtiTEM_GBouttmp
      output_meth_1_sep_1_GB%vciTEM = vciTEM_GBouttmp
      output_meth_1_sep_1_GB%vriTEM = vriTEM_GBouttmp

      output_meth_1_sep_1_SI%dfeTEM = dfeTEM_SIouttmp
      output_meth_1_sep_1_SI%vteTEM = vteTEM_SIouttmp
      output_meth_1_sep_1_SI%vceTEM = vceTEM_SIouttmp

      output_meth_1_sep_1_SI%dfiTEM = dfiTEM_SIouttmp
      output_meth_1_sep_1_SI%vtiTEM = vtiTEM_SIouttmp
      output_meth_1_sep_1_SI%vciTEM = vciTEM_SIouttmp
      output_meth_1_sep_1_SI%vriTEM = vriTEM_SIouttmp

      output_meth_1_sep_1_SI%normalization = 0
      output_meth_1_sep_1_GB%normalization = 1
    endif

    if (in_regular%phys_meth == 2) then
      output_meth_2_sep_1_GB%veneITG  = veneITG_GBouttmp
      output_meth_2_sep_1_GB%veceITG  = veceITG_GBouttmp

      output_meth_2_sep_1_GB%veniITG  = veniITG_GBouttmp
      output_meth_2_sep_1_GB%veciITG  = veciITG_GBouttmp
      output_meth_2_sep_1_GB%veriITG  = veriITG_GBouttmp

      output_meth_2_sep_1_GB%chieeITG = chieeITG_GBouttmp
      output_meth_2_sep_1_GB%chieiITG = chieiITG_GBouttmp

      output_meth_2_sep_1_SI%veneITG  = veneITG_SIouttmp
      output_meth_2_sep_1_SI%veceITG  = veceITG_SIouttmp

      output_meth_2_sep_1_SI%veniITG  = veniITG_SIouttmp
      output_meth_2_sep_1_SI%veciITG  = veciITG_SIouttmp
      output_meth_2_sep_1_SI%veriITG  = veriITG_SIouttmp

      output_meth_2_sep_1_SI%chieeITG = chieeITG_SIouttmp
      output_meth_2_sep_1_SI%chieiITG = chieiITG_SIouttmp

      output_meth_2_sep_1_GB%veneTEM  = veneTEM_GBouttmp
      output_meth_2_sep_1_GB%veceTEM  = veceTEM_GBouttmp

      output_meth_2_sep_1_GB%veniTEM  = veniTEM_GBouttmp
      output_meth_2_sep_1_GB%veciTEM  = veciTEM_GBouttmp
      output_meth_2_sep_1_GB%veriTEM  = veriTEM_GBouttmp

      output_meth_2_sep_1_GB%chieeTEM = chieeTEM_GBouttmp
      output_meth_2_sep_1_GB%chieiTEM = chieiTEM_GBouttmp

      output_meth_2_sep_1_SI%veneTEM  = veneTEM_SIouttmp
      output_meth_2_sep_1_SI%veceTEM  = veceTEM_SIouttmp

      output_meth_2_sep_1_SI%veniTEM  = veniTEM_SIouttmp
      output_meth_2_sep_1_SI%veciTEM  = veciTEM_SIouttmp
      output_meth_2_sep_1_SI%veriTEM  = veriTEM_SIouttmp

      output_meth_2_sep_1_SI%chieeTEM = chieeTEM_SIouttmp
      output_meth_2_sep_1_SI%chieiTEM = chieiTEM_SIouttmp


      output_meth_2_sep_1_GB%veneETG  = veneETG_GBouttmp
      output_meth_2_sep_1_GB%veceETG  = veceETG_GBouttmp
      output_meth_2_sep_1_GB%chieeETG = chieeETG_GBouttmp

      output_meth_2_sep_1_SI%veneETG  = veneETG_SIouttmp
      output_meth_2_sep_1_SI%veceETG  = veceETG_SIouttmp
      output_meth_2_sep_1_SI%chieeETG = chieeETG_SIouttmp

      output_meth_2_sep_1_SI%normalization = 0
      output_meth_2_sep_1_GB%normalization = 1
    endif
  endif

    output_meth_0_sep_0_SI%efeETG = efeETG_SIouttmp
    output_meth_0_sep_0_GB%efeETG = efeETG_GBouttmp

  IF (myrank==0) THEN 
     CALL SYSTEM_CLOCK(time4)
     CALL SYSTEM_CLOCK(count_rate=freq)
     timetot = REAL(time4-time3) / REAL(freq)
     IF (verbose .EQV. .TRUE.) WRITE(stdout,*)
     IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,F11.3,A)") 'Profiling: Second MPI_AllReduce time = ',timetot,' s'

  ENDIF


  CALL deallocate_endoutput()     

  CALL deallocate_all()

  !Deallocate optional oldsol
  !IF (PRESENT(oldsolin)) DEALLOCATE(oldsol)
  !IF (PRESENT(oldfdsolin)) DEALLOCATE(oldfdsol)
  IF (run_with_oldsol) THEN
     deallocate(oldsol)
     deallocate(oldfdsol)
  ENDIF

CONTAINS 

  SUBROUTINE DistriTask(NumTasks,numprocs,rank, in_regular)
    IMPLICIT NONE
    !     include 'mpif.h'
    INTEGER,DIMENSION(:),ALLOCATABLE :: Request,OK,Finish
    INTEGER,INTENT(IN) :: NumTasks,numprocs,rank
    type(qlk_in_regular), intent(in) :: in_regular
    REAL(kind=DBL) :: tps
    INTEGER :: iradcoord,iwavenum, Task, NoTask, iloop, ierr
    INTEGER :: one=1, minusone=-1 
    INTEGER :: my_first, my_last, my_numtasks, max_per_proc, my_max
    LOGICAL :: Complete = .FALSE.
    LOGICAL :: Complete0 = .FALSE.
    LOGICAL :: ressend = .FALSE.
    LOGICAL, DIMENSION(:), ALLOCATABLE :: resready

    !Initialize for all procs
    Task = 0
    NoTask = 0
    tpstot=0 !initialize time

300 FORMAT(1x,'rank ',I5,' NoTask ',I7,' time ',F10.3,', total time ',F10.3,' (p,nu)=(',I7,',',I3,')') 

    ! Simple no openMP version, for sanity checking
    ! Split the work evenly, assuming equal work per task
    simple: IF (in_regular%simple_mpi_only == 1) THEN

       max_per_proc = ceiling(real(NumTasks,DBL)/real(numprocs,DBL))
       my_first = rank * max_per_proc + 1
       my_max = (rank + 1) * max_per_proc
       my_last = min(my_max,NumTasks) 
       my_numtasks = my_last - my_first + 1     

303    FORMAT(1x,'rank: ',I4,', first: ',I4,', last: ',I4,', myno: ',I4,', max_pp: ',I4,', mymax: ',I4)
       IF (verbose .EQV. .TRUE.) write(stdout,303) rank, my_first, my_last, my_numtasks, max_per_proc, my_max

       DO Task = my_first, my_last
          NoTask = Task
          tps=MPI_Wtime()
          calltimeinit=MPI_Wtime() ! for timing inside routines
          timeoutflag = .FALSE.
          iwavenum=(NoTask-1)/(dimx) + 1
          iradcoord=MOD((NoTask-1),dimx) + 1

          !Calculate mode width and shift according to Citrin model. omeflu, mwidth, and mshift are written inside the subroutines. Only call if rhomin<rho<rhomax
          IF ( ( rho(iradcoord) >= rhomin ) .AND. ( rho(iradcoord) <= rhomax) ) THEN !check if rho is within the defined range, and run eigenvalue solver if so
             CALL calc(iradcoord, iwavenum)
          ENDIF
          IF ( (timeoutflag .EQV. .TRUE.) .AND. (verbose .EQV. .TRUE.)) WRITE(stdout,'(A,I7,A,I3)') 'Timeout recorded at (p,nu)=',iradcoord,',',iwavenum
          tps=MPI_Wtime()-tps
          tpstot=tpstot+tps  
          IF (verbose .EQV. .TRUE.) THEN
             WRITE(stdout,300) rank,NoTask,tps,tpstot,iradcoord,iwavenum
          ENDIF

       END DO

       ! Master-slave work distribution depending on both MPI and OpenMP
    ELSE

       ! Code for Master processor
       IF (rank==0) THEN
          ! Initialization phase for Master arrays. 

          IF(ALLOCATED(Request) .EQV. .FALSE.) ALLOCATE(Request(numprocs-1))
          IF(ALLOCATED(resready) .EQV. .FALSE.)ALLOCATE(resready(numprocs-1))
          IF(ALLOCATED(OK) .EQV. .FALSE.) ALLOCATE(OK(numprocs-1))
          IF(ALLOCATED(Finish) .EQV. .FALSE.) ALLOCATE(Finish(0:numprocs-1))
          Finish(:) = 0
          resready(:)=.FALSE.
          ! Initialize OMP
          CALL OMP_SET_DYNAMIC(.FALSE.) 
          CALL OMP_SET_NUM_THREADS(2)
          !       WRITE(*,*) 'threads=',omp_get_num_threads()
          !$OMP PARALLEL
          !$OMP SECTIONS

          !$OMP SECTION
          !****************************************************
          !****** Distributor thread on Master processor ******
          !****************************************************
          ! Master receives an integer "1" from each of the slaves in an unblocked receive.
          ! This sign of life from the slaves is checked in the while loop below
          DO iloop=1,numprocs-1
             CALL MPI_Irecv(OK(iloop),1,MPI_INTEGER,iloop,100+iloop,MPI_COMM_WORLD,Request(iloop),ierr)
          ENDDO

          !This loop distributes tasks to slaves. The +numprocs is necessary since Task will
          !increment one extra time for each slave processor
          DO WHILE (Task<=NumTasks+numprocs-1)

             DO iloop=1,numprocs-1
                !If the slave processor number iloop still has tasks to do, then carry on
                IF (Finish(iloop) /= 1) THEN               
                   !Check if processor iloop is ready to proceed 
                   !(if it's the first task or completed its previous task)
                   CALL MPI_Test(Request(iloop),complete,status,ierr)
                   IF (Complete) THEN 
                      IF (resready(iloop)) THEN
                         !     CALL getresults(iloop,iloop)                                      
                      ENDIF
                      Task=Task+1 !Prepare next task number
                      resready(iloop)=.TRUE. !results will now be saved before next distribution
                      IF (Task<=NumTasks) THEN
                         !A valid task number is sent from the Master to the slave
                         CALL MPI_SSend(Task,1,MPI_INTEGER,iloop,100+iloop,MPI_COMM_WORLD,ierr)
                         !Following MPI_Irecv, when the task is completed, 
                         !the next MPI_Test will provide Complete=.TRUE. for iloop
                         CALL MPI_Irecv(OK(iloop),1,MPI_INTEGER,iloop,100+iloop,MPI_COMM_WORLD,Request(iloop),ierr)
                         wavenum(Task)=(Task-1)/(dimx) + 1
                         radcoord(Task)=MOD((Task-1),dimx) + 1                
                      ELSE ! No more tasks. Send "-1" to slave, signalling that the tasks are done
                         CALL MPI_SSend(minusone,1,MPI_INTEGER,iloop,100+iloop,MPI_COMM_WORLD,ierr)
                         Finish(iloop) = 1
                      ENDIF
                   ENDIF
                ENDIF
             ENDDO

             !  Manage thread timeshare

             !Now coordinate with 2nd worker thread on Master processor,
             !which is also carrying out tasks. 

             !$OMP FLUSH(Complete0)
             IF (( Finish(0) /= 1 ) .AND. Complete0) THEN         
                !If 2nd thread initialized or completed previous task, then increment task
                !and switch flag so that 2nd thread will launch a new task
                Task = Task + 1
                IF (Task <= NumTasks) THEN
                   NoTask = Task
                   Complete0 = .FALSE.
                ELSE
                   Finish(0) = 1
                ENDIF
             ENDIF
          ENDDO

          !$OMP SECTION
          !**************************************
          !***Worker thread on Master processor***
          !**************************************
          complete0=.TRUE. 
          DO
             !$OMP FLUSH(Finish)
             IF (Finish(0)==1) EXIT
             !$OMP FLUSH(complete0)
             IF ( .NOT. Complete0) THEN            
                tps=MPI_Wtime()
                calltimeinit=MPI_Wtime() ! for timing inside routines
                timeoutflag = .FALSE.
                iwavenum=(NoTask-1)/(dimx) + 1
                iradcoord=MOD((NoTask-1),dimx) + 1
                IF ( ( rho(iradcoord) >= rhomin ) .AND. ( rho(iradcoord) <= rhomax) ) THEN !check if rho is within the defined range, and run eigenvalue solver if so
                   CALL calc(iradcoord, iwavenum)
                ENDIF
                IF ( (timeoutflag .EQV. .TRUE.) .AND. (verbose .EQV. .TRUE.)) WRITE(stdout,'(A,I7,A,I3)') 'Timeout recorded at (p,nu)=',iradcoord,',',iwavenum
                tps=MPI_Wtime()-tps
                tpstot=tpstot+tps  
                IF (verbose .EQV. .TRUE.) THEN
                   WRITE(stdout,300) rank,NoTask,tps,tpstot,iradcoord,iwavenum
                ENDIF
                Complete0 = .TRUE.
             ENDIF
          ENDDO
          !$OMP END SECTIONS
          !$OMP END PARALLEL
       ELSE

          !********************
          !****** SLAVES ******
          !********************

          !The slaves run tasks until they receive NoTask=-1 from the coordinating Master
          DO WHILE (NoTask /= -1)
             !The slaves notify the Master that they are ready for a new task
             CALL MPI_SSend(one,1,MPI_INTEGER,0,100+rank,MPI_COMM_WORLD,ierr)
             IF (ressend) THEN
                ! CALL sendresults(iradcoord,iwavenum,rank) !copy over output to rank0 which will have full arrays for later             
             ENDIF
             !The slaves receive their task number back from the Master.
             CALL MPI_Recv(NoTask,1,MPI_INTEGER,0,100+rank,MPI_COMM_WORLD,status,ierr)
             IF (NoTask /= -1) THEN
                !If a task is assigned, the slaves compute the task. The specific task to
                !be carried out (i.e. wavenumber and 'radius') depends on the value of NoTask.
                tps=MPI_Wtime()
                calltimeinit=MPI_Wtime() ! for timing inside routines
                timeoutflag = .FALSE.
                iwavenum=(NoTask-1)/(dimx) + 1
                iradcoord=MOD((NoTask-1),dimx) + 1
                IF ( ( rho(iradcoord) >= rhomin ) .AND. ( rho(iradcoord) <= rhomax) ) THEN !check if rho is within the defined range, and run eigenvalue solver if so
                   CALL calc(iradcoord, iwavenum)
                ENDIF              
                IF ((timeoutflag .EQV. .TRUE.) .AND. (verbose .EQV. .TRUE.)) WRITE(stdout,'(A,I7,A,I3)') 'Timeout recorded at (p,nu)=',iradcoord,',',iwavenum
                ressend=.TRUE.
                tps=MPI_Wtime()-tps
                tpstot=tpstot+tps
                IF (verbose .EQV. .TRUE.) THEN
                   WRITE(stdout,301) rank,NoTask,tps,tpstot,iradcoord,iwavenum
                ENDIF
301             FORMAT(1x,'rank ',I5,' NoTask ',I7,' time ',F10.3,', total time ',F10.3,' (p,nu)=(',I7,',',I3,')')
             ENDIF
          ENDDO
       ENDIF

    END IF simple

  END SUBROUTINE DistriTask

  SUBROUTINE allocatenewoutput(sizes, in_regular, &
                       output_meth_0, &
                       output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                       output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                       output_meth_1, &
                       output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                       output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                       output_meth_2, &
                       output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                       output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                       primi_meth_0, &
                       primi_meth_1, &
                       primi_meth_2)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular

    type(qlk_output_meth_0)       , intent(inout) :: output_meth_0
    type(qlk_output_meth_0_sep_0) , intent(inout) :: output_meth_0_sep_0_SI , output_meth_0_sep_0_GB
    type(qlk_output_meth_0_sep_1) , optional, intent(inout) :: output_meth_0_sep_1_SI , output_meth_0_sep_1_GB
    type(qlk_output_meth_1)       , optional, intent(inout) :: output_meth_1
    type(qlk_output_meth_1_sep_0) , optional, intent(inout) :: output_meth_1_sep_0_SI , output_meth_1_sep_0_GB
    type(qlk_output_meth_1_sep_1) , optional, intent(inout) :: output_meth_1_sep_1_SI , output_meth_1_sep_1_GB
    type(qlk_output_meth_2)       , optional, intent(inout) :: output_meth_2
    type(qlk_output_meth_2_sep_0) , optional, intent(inout) :: output_meth_2_sep_0_SI , output_meth_2_sep_0_GB
    type(qlk_output_meth_2_sep_1) , optional, intent(inout) :: output_meth_2_sep_1_SI , output_meth_2_sep_1_GB
    type(qlk_primi_meth_0)        , optional, intent(inout) :: primi_meth_0
    type(qlk_primi_meth_1)        , optional, intent(inout) :: primi_meth_1
    type(qlk_primi_meth_2)        , optional, intent(inout) :: primi_meth_2

    call allocate_output_meth_0       ( sizes%dimx, sizes%dimn, sizes%nions, ntheta, numecoefs, numicoefs, output_meth_0)
    call allocate_output_meth_0_sep_0 ( sizes%dimx, sizes%dimn, sizes%nions, sizes%numsols, output_meth_0_sep_0_SI)
    call allocate_output_meth_0_sep_0 ( sizes%dimx, sizes%dimn, sizes%nions, sizes%numsols, output_meth_0_sep_0_GB)
    if (in_regular%separateflux == 1) then
      call allocate_output_meth_0_sep_1 ( sizes%dimx, sizes%nions, output_meth_0_sep_1_SI)
      call allocate_output_meth_0_sep_1 ( sizes%dimx, sizes%nions, output_meth_0_sep_1_GB)
    endif
    call allocate_primi_meth_0        ( sizes%dimx, sizes%dimn ,sizes%nions  ,sizes%numsols, primi_meth_0)
    if (in_regular%phys_meth /= 0) then
      call allocate_output_meth_1       ( sizes%dimx, sizes%nions, output_meth_1)
      call allocate_output_meth_1_sep_0 ( sizes%dimx, sizes%nions, output_meth_1_sep_0_SI)
      call allocate_output_meth_1_sep_0 ( sizes%dimx, sizes%nions, output_meth_1_sep_0_GB)
      if (in_regular%separateflux == 1) then
        call allocate_output_meth_1_sep_1 ( sizes%dimx, sizes%nions, output_meth_1_sep_1_SI)
        call allocate_output_meth_1_sep_1 ( sizes%dimx, sizes%nions, output_meth_1_sep_1_GB)
      endif
      call allocate_primi_meth_1        ( sizes%dimx, sizes%dimn, sizes%nions, sizes%numsols, primi_meth_1)
    endif
    if (in_regular%phys_meth == 2) then
      call allocate_output_meth_2       ( sizes%dimx, sizes%nions, output_meth_2)
      call allocate_output_meth_2_sep_0 ( sizes%dimx, sizes%nions, output_meth_2_sep_0_SI)
      call allocate_output_meth_2_sep_0 ( sizes%dimx, sizes%nions, output_meth_2_sep_0_GB)
      if (in_regular%separateflux == 1) then
        call allocate_output_meth_2_sep_1 ( sizes%dimx, sizes%nions, output_meth_2_sep_1_SI)
        call allocate_output_meth_2_sep_1 ( sizes%dimx, sizes%nions, output_meth_2_sep_1_GB)
      endif
      call allocate_primi_meth_2        ( sizes%dimx, sizes%dimn, sizes%nions, sizes%numsols, primi_meth_2)
    endif
  END SUBROUTINE allocatenewoutput

  SUBROUTINE setoutput(debug_regular, debug_newt, &
                       output_meth_0, &
                       output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                       output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                       output_meth_1, &
                       output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                       output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                       output_meth_2, &
                       output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                       output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                       primi_meth_0, &
                       primi_meth_1, &
                       primi_meth_2)
    type(qlk_in_regular), intent(in) :: debug_regular
    type(qlk_in_newt), intent(in) :: debug_newt

    type(qlk_output_meth_0)       , intent(inout) :: output_meth_0
    type(qlk_output_meth_0_sep_0) , intent(inout) :: output_meth_0_sep_0_SI , output_meth_0_sep_0_GB
    type(qlk_output_meth_0_sep_1) , optional, intent(inout) :: output_meth_0_sep_1_SI , output_meth_0_sep_1_GB
    type(qlk_output_meth_1)       , optional, intent(inout) :: output_meth_1
    type(qlk_output_meth_1_sep_0) , optional, intent(inout) :: output_meth_1_sep_0_SI , output_meth_1_sep_0_GB
    type(qlk_output_meth_1_sep_1) , optional, intent(inout) :: output_meth_1_sep_1_SI , output_meth_1_sep_1_GB
    type(qlk_output_meth_2)       , optional, intent(inout) :: output_meth_2
    type(qlk_output_meth_2_sep_0) , optional, intent(inout) :: output_meth_2_sep_0_SI , output_meth_2_sep_0_GB
    type(qlk_output_meth_2_sep_1) , optional, intent(inout) :: output_meth_2_sep_1_SI , output_meth_2_sep_1_GB
    type(qlk_primi_meth_0)        , optional, intent(inout) :: primi_meth_0
    type(qlk_primi_meth_1)        , optional, intent(inout) :: primi_meth_1
    type(qlk_primi_meth_2)        , optional, intent(inout) :: primi_meth_2

    output_meth_0_sep_0_SI%gam = gam_SI
    output_meth_0_sep_0_SI%ome = ome_SI

    output_meth_0_sep_0_SI%pfe = epf_SI
    output_meth_0_sep_0_SI%efe = eef_SI
    output_meth_0_sep_0_SI%pfi = ipf_SI
    output_meth_0_sep_0_SI%efi = ief_SI
    output_meth_0_sep_0_SI%vfi = ivf_SI
    output_meth_0_sep_0_SI%normalization = 0


    output_meth_0_sep_0_GB%gam = gam_GB
    output_meth_0_sep_0_GB%ome = ome_GB

    output_meth_0_sep_0_GB%pfe = epf_GB
    output_meth_0_sep_0_GB%efe = eef_GB
    output_meth_0_sep_0_GB%pfi = ipf_GB
    output_meth_0_sep_0_GB%efi = ief_GB
    output_meth_0_sep_0_GB%vfi = ivf_GB

    output_meth_0_sep_0_GB%normalization = 1

    output_meth_0%pfe_cm = epf_cm
    output_meth_0%efe_cm = eef_cm
    output_meth_0%pfi_cm = ipf_cm
    output_meth_0%efi_cm = ief_cm
    output_meth_0%vfi_cm = ivf_cm

    output_meth_0%ecoefs = ecoefs
    output_meth_0%npol = npol
    output_meth_0%cftrans = cftrans

    output_meth_0%Nustar = Nustar
    output_meth_0%Zeff = Zeffx
    output_meth_0%phi = phi

    primi_meth_0%Lcirce = Lcirce
    primi_meth_0%Lpiege = Lpiege
    primi_meth_0%Lecirce = Lecirce
    primi_meth_0%Lepiege = Lepiege
    primi_meth_0%Lcirci = Lcirci
    primi_meth_0%Lpiegi = Lpiegi
    primi_meth_0%Lecirci = Lecirci
    primi_meth_0%Lepiegi = Lepiegi
    primi_meth_0%Lvcirci = Lvcirci
    primi_meth_0%Lvpiegi = Lvpiegi


    primi_meth_0%fdsol = fdsol
    primi_meth_0%modewidth = modewidth
    primi_meth_0%modeshift = modeshift
    primi_meth_0%sol = sol
    primi_meth_0%solflu = solflu
    primi_meth_0%ntor = ntor
    primi_meth_0%distan = distan
    primi_meth_0%kperp2 = kperp2
    primi_meth_0%kymaxETG = krmmuETG
    primi_meth_0%kymaxITG = krmmuITG


    ! TODO: check if these still exist
    solflu_SI = solflu_SI
    solflu_GB = solflu_GB

    IF (debug_regular%phys_meth /= 0) THEN

      primi_meth_1%Lcircgte = Lcircgte
      primi_meth_1%Lpieggte = Lpieggte
      primi_meth_1%Lcircgne = Lcircgne
      primi_meth_1%Lpieggne = Lpieggne
      primi_meth_1%Lcircce = Lcircce
      primi_meth_1%Lpiegce = Lpiegce

      primi_meth_1%Lcircgti = Lcircgti
      primi_meth_1%Lpieggti = Lpieggti
      primi_meth_1%Lcircgni = Lcircgni
      primi_meth_1%Lpieggni = Lpieggni
      primi_meth_1%Lcircgui = Lcircgui
      primi_meth_1%Lpieggui = Lpieggui
      primi_meth_1%Lcircci = Lcircci
      primi_meth_1%Lpiegci = Lpiegci

      output_meth_1_sep_0_SI%dfe = dfe_SI
      output_meth_1_sep_0_SI%vte = vte_SI
      output_meth_1_sep_0_SI%vce = vce_SI

      output_meth_1_sep_0_SI%dfi = dfi_SI
      output_meth_1_sep_0_SI%vti = vti_SI
      output_meth_1_sep_0_SI%vci = vci_SI
      output_meth_1_sep_0_SI%vri = vri_SI
      output_meth_1_sep_0_SI%normalization = 0

      output_meth_1_sep_0_GB%dfe = dfe_GB
      output_meth_1_sep_0_GB%vte = vte_GB
      output_meth_1_sep_0_GB%vce = vce_GB

      output_meth_1_sep_0_GB%dfi = dfi_GB
      output_meth_1_sep_0_GB%vti = vti_GB
      output_meth_1_sep_0_GB%vci = vci_GB
      output_meth_1_sep_0_GB%vri = vri_GB
      output_meth_1_sep_0_GB%normalization = 1

      output_meth_1%cke = cke
      output_meth_1%cki = cki

       IF (debug_regular%phys_meth == 2) THEN
          primi_meth_2%Lecircgte = Lecircgte
          primi_meth_2%Lepieggte = Lepieggte
          primi_meth_2%Lecircgne = Lecircgne
          primi_meth_2%Lepieggne = Lepieggne
          primi_meth_2%Lecircce = Lecircce
          primi_meth_2%Lepiegce = Lepiegce

          primi_meth_2%Lecircgti = Lecircgti
          primi_meth_2%Lepieggti = Lepieggti
          primi_meth_2%Lecircgni = Lecircgni
          primi_meth_2%Lepieggni = Lepieggni
          primi_meth_2%Lecircgui = Lecircgui
          primi_meth_2%Lepieggui = Lepieggui
          primi_meth_2%Lecircci = Lecircci
          primi_meth_2%Lepiegci = Lepiegci

          output_meth_2_sep_0_SI%vene = vene_SI
          output_meth_2_sep_0_SI%chiee = chiee_SI
          output_meth_2_sep_0_SI%vece = vece_SI
          output_meth_2_sep_0_SI%veni = veni_SI
          output_meth_2_sep_0_SI%chiei = chiei_SI
          output_meth_2_sep_0_SI%veci = veci_SI
          output_meth_2_sep_0_SI%veri = veri_SI
          output_meth_2_sep_0_SI%normalization = 0

          output_meth_2_sep_0_GB%vene = vene_GB
          output_meth_2_sep_0_GB%chiee = chiee_GB
          output_meth_2_sep_0_GB%vece = vece_GB
          output_meth_2_sep_0_GB%veni = veni_GB
          output_meth_2_sep_0_GB%chiei = chiei_GB
          output_meth_2_sep_0_GB%veci = veci_GB
          output_meth_2_sep_0_GB%veri = veri_GB
          output_meth_2_sep_0_GB%normalization = 1

          output_meth_2%ceke = ceke
          output_meth_2%ceki = ceki

       ENDIF
    ENDIF

  END SUBROUTINE setoutput

END SUBROUTINE qualikiz
end module mod_qualikiz
