MODULE FLRterms
#include "preprocessor.inc"
  USE kind
  USE datmat
  USE datcal
  USE mod_integration

  IMPLICIT NONE

CONTAINS

  SUBROUTINE makeFLRtermsgeneral(p, nu, rotation)
    !! The scaled Bessel functions are integrated over kr separately, outside the main integrals
    !! Aribitrary normalisation factor (normkr) introduced to optimize kr integration
    !! Note that passing species Bessel functions are included directly in integrand in passints
    INTEGER, INTENT(IN) :: p, nu
    LOGICAL, INTENT(IN) :: rotation
    INTEGER :: ifailloc

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_1d
    REAL(KIND=DBL), DIMENSION(1) :: FLR_lbound, FLR_ubound
    REAL(KIND=DBL), DIMENSION(1) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(2) :: fdata

    if (rotation) then
      FLR_ubound = ABS(2 * pi / (widthhat * normkr))
    else
      FLR_ubound = ABS(2 * pi / (REAL(mwidth) * normkr))
    endif
    FLR_lbound = -FLR_ubound

    dim_out = 1
    dim_in = 1

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT

    opts_1d%reqRelError = epsFLR
    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%maxEval = lw
    CLASS IS (hcubature_options)
      ! hcubature, force abs error to be 0
      opts_1d%maxEval = lw
      opts_1d%minEval = 0
      opts_1d%norm = 1
      opts_1d%reqAbsError = 0
    END SELECT

    !Trapped electrons
    ifailloc = 1
    fdata(1) = 0 ! ion
    fdata(2) = logic2real(rotation)
    CALL integration(dim_out, nFLRsp, dim_in, FLR_lbound, FLR_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnu('J0e2p FLR', ifailloc, verbose, p, nu)
    Joe2p = int_res(1)

    DO ion = 1,nions
      ifailloc = 1
      fdata(1) = ion
      fdata(2) = logic2real(rotation)
      CALL integration(dim_out, nFLRsp, dim_in, FLR_lbound, FLR_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
      CALL debug_msg_pnuion('J0i2p FLR', ifailloc, verbose, p, nu, ion)
      Joi2p(ion) = int_res(1)
    ENDDO

  END SUBROUTINE makeFLRtermsgeneral

  SUBROUTINE makeFLRterms(p,nu)
    !! The scaled Bessel functions are integrated over kr separately, outside the main integrals
    !! Aribitrary normalisation factor (normkr) introduced to optimize kr integration
    !! Note that passing species Bessel functions are included directly in integrand in passints
    INTEGER, INTENT(IN) :: p, nu

    CALL makeFLRtermsgeneral(p, nu, .FALSE.)
  END SUBROUTINE makeFLRterms

  SUBROUTINE makeFLRtermsrot(p,nu)
    !! The scaled Bessel functions are integrated over kr separately, outside the main integrals
    !! Aribitrary normalisation factor (normkr) introduced to optimize kr integration
    !! Note that passing species Bessel functions are included directly in integrand in passints
    INTEGER, INTENT(IN) :: p, nu

    CALL makeFLRtermsgeneral(p, nu, .TRUE.)
  END SUBROUTINE makeFLRtermsrot

  SUBROUTINE nFLRsp(dim_in, f_in, f_data, dim_out, f_out, ifail)
    !! Mutable interface for improved FLR, trapped particles
    !! FLR of trapped particles are calculated from an integration over kr of
    !! Bessel_mod(kr*delta^2) * Bessel_mod(k_perp*rhoLr^2) * eigenfun^2
    !! using k_perp^2 = k_theta^2 + kr^2
    !!
    !! f_in(1) is krr
    !! f_data(1) is nion, 0 for electrons, >0 for nth ion
    !! f_data(2) is rotation, 0 for norot, 1 for rot
    INTEGER, INTENT(IN) :: dim_in, dim_out
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
    INTEGER, INTENT(INOUT) :: ifail
    REAL(KIND=DBL) :: krr
    INTEGER :: nion
    LOGICAL :: rotation
    ! Local variables
    REAL(KIND=DBL)    :: normgs
    REAL(KIND=DBL)    :: var1, var2, var3
    REAL(KIND=DBL)    :: bessm1, bessm2, gau
    REAL(KIND=DBL)    :: ds, Rhos, ktetaRhos
    INTEGER :: ifailloc

    ERRORSTOP(dim_in > 1, 'nFLRsp is one-D in')
    ERRORSTOP(dim_out > 1, 'nFLRsp is one-D out')

    krr = f_in(1)
    nion = INT(f_data(1))
    rotation = real2logic(f_data(2))

    if (nion == 0) then
      ds = de(pnFLR)
      Rhos = Rhoe(pnFLR)
      ktetaRhos = ktetaRhoe
    else
      ds = di(pnFLR,nion)
      Rhos = Rhoi(pnFLR,nion)
      ktetaRhos = ktetaRhoi(nion)
    endif

    ifailloc = 0
    var1 = (normkr * krr * ds)**2   !!argument of 1st Bessel fun
    var2 = (normkr * krr * Rhos)**2 !!1st argument of 2nd Bessel fun
    var3 = ktetaRhos**2             !!2nd argument of 2nd Bessel fun

    bessm1 = BESEI0(var1)
    bessm2 = BESEI0(var2+var3)

    if (rotation) then
      normgs = normkr * SQRT((REAL(mwidth)**2 - AIMAG(mwidth)**2)) / SQRT(pi) * &
           EXP(-AIMAG(mshift)**2/(REAL(mwidth)**2-AIMAG(mwidth)**2))
      gau = EXP(-0.5_DBL*(krr*normkr*mwidth)**2 - ci*krr*normkr*mshift)   !!definition of the eigenfun in k-space
      f_out(1) = normgs * ABS(gau)**2 * bessm1 * bessm2
    else
      normgs = normkr * REAL(mwidth) / SQRT(pi)
      gau = EXP(-0.5_DBL*(krr*normkr*REAL(mwidth))**2)    !!definition of the eigenfun
      f_out(1) = normgs * gau**2 * bessm1 * bessm2
    endif

    ifail = 0
  END SUBROUTINE nFLRsp
END MODULE FLRterms
