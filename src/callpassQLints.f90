MODULE callpassQLints
#include "preprocessor.inc"

  USE kind
  USE datcal
  USE datmat
  USE passints
  USE callpassints
  USE mod_integration

  IMPLICIT NONE

CONTAINS

  SUBROUTINE passQLints( p, nu, omega, rotation, fonctce, fonctci, fonctcgte, fonctcgti, fonctcgne, fonctcgni, &
         fonctcce, fonctcci, fonctece, foncteci, fonctecgte, fonctecgti, fonctecgne, fonctecgni, fonctecce, fonctecci, &
         fonctcgui, fonctecgui)
    !-----------------------------------------------------------
    ! Calculates the passing particle integrals at the omega of the linear solutions
    ! Integrals (with switch) in order to identify particle flux contributions due to An, At and curvature
    ! Includes resonance broadening
    ! To save (some) compuational time, only the (transport relevant) imaginary components are kept
    !-----------------------------------------------------------   
    INTEGER, INTENT(IN)  :: p, nu
    LOGICAL, INTENT(IN) :: rotation
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctce, fonctcgte, fonctcgne, fonctcce, fonctece
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctecgte, fonctecgne, fonctecce
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctci, fonctcgti, fonctcgni, fonctcci, foncteci
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctecgti, fonctecgni, fonctecci
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT), OPTIONAL :: fonctcgui, fonctecgui

    REAL(KIND=DBL), DIMENSION(ndim)    :: a, b
    INTEGER           :: minpts

    REAL(KIND=DBL), DIMENSION(nf) :: intout

    REAL(KIND=DBL)    :: rfonctce, rfonctcgte, rfonctcgne, rfonctcce, rfonctece  
    REAL(KIND=DBL)    :: rfonctecgte, rfonctecgne, rfonctecce 
    REAL(KIND=DBL)    :: ifonctce, ifonctcgte, ifonctcgne, ifonctcce, ifonctece
    REAL(KIND=DBL)    :: ifonctecgte, ifonctecgne, ifonctecce
    REAL(KIND=DBL)    :: intmult
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctci, rfonctcgti, rfonctcgni, rfonctcci, rfoncteci
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctecgti, rfonctecgni, rfonctecci
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctci, ifonctcgti, ifonctcgni, ifonctcci, ifoncteci
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctecgti, ifonctecgni, ifonctecci
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctcgui, rfonctecgui
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctcgui, ifonctecgui

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_2d
    REAL(KIND=DBL), DIMENSION(2) :: int_err
    REAL(KIND=DBL), DIMENSION(4) :: fdata
    INTEGER :: ifailloc
    CHARACTER(len=69) :: msg

    IF (rotation) THEN
      IF (.NOT. present(fonctcgui) .OR. .NOT. present(fonctecgui)) THEN
        msg = 'Please supply fonctcgui, fonctecgui, if rotation .EQ. .TRUE.'
        ERRORSTOP(.TRUE., msg)
      ENDIF
      intmult = 1._DBL
      a(:) = -rkuplim ! with rotation from -inf to +inf
      b(:) =  rkuplim
    ELSE
      intmult = 4._DBL
      a(:) = 0
      b(:) = rkuplim
    ENDIF

    omFFk = omega
    pFFk = p

    dim_out = 2
    dim_in = 2

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag2d_options::opts_2d)
    CASE (1)
      allocate(hcubature_options::opts_2d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in passQLints')
    END SELECT

    SELECT TYPE (opts_2d)
    CLASS IS (nag2d_options)
      opts_2d%reqRelError = relaccQL2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%lenwrk = lenwrk
    CLASS IS (hcubature_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%norm = 2
      opts_2d%reqAbsError = absacc2
    END SELECT

    fdata(1) = -1 ! caseflag
    fdata(2) = -1 ! ion: Do not care
    fdata(3) = logic2real(rotation)
    fdata(4) = 1 ! Do not scale integrals

    !ELECTRON PARTICLE FLUX INTEGRALS
    fdata(2) = 0 ! Electrons
    minpts = 0; ifailloc=1;
    IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
       ifailloc = 1
       CALL integration(dim_out, calc_QL_Fkstarrstare, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
       CALL debug_msg_integration('QL Fkstarrstare', ifailloc, verbose, p, nu, ion, omega)
       intout(1) = 0
    ELSE
       intout(1)=0
       intout(2)=0
    ENDIF
    rfonctce = intmult*intout(1); ifonctce=intmult*intout(2)

    !ADDITIONAL ELECTRON PARTICLE FLUX INTEGRALS
    IF (phys_meth .NE. 0.0) THEN

       minpts = 0; ifailloc=1;
       IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstargte, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstargte', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
       ELSE
          intout(1)=0
          intout(2)=0
       ENDIF
       rfonctcgte = intmult*intout(1); ifonctcgte=intmult*intout(2)

       minpts = 0; ifailloc=1;
       IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstargne, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstargne', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
       ELSE
          intout(1)=0
          intout(2)=0
       ENDIF
       rfonctcgne = intmult*intout(1); ifonctcgne=intmult*intout(2)

       minpts = 0; ifailloc=1;
       IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstarce, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstarce', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
       ELSE
          intout(1)=0
          intout(2)=0
       ENDIF
       rfonctcce = intmult*intout(1); ifonctcce=intmult*intout(2)
!!!
       IF (phys_meth == 2) THEN
          minpts = 0; ifailloc=1;
          IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
             ifailloc = 1
             CALL integration(dim_out, calc_QL_Fekstarrstargte, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
             CALL debug_msg_integration('QL Fekstarrstargte', ifailloc, verbose, p, nu, ion, omega)
             intout(1) = 0
          ELSE
             intout(1)=0
             intout(2)=0
          ENDIF
          rfonctecgte = intmult*intout(1); ifonctecgte=intmult*intout(2)

          minpts = 0; ifailloc=1;
          IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
             ifailloc = 1
             CALL integration(dim_out, calc_QL_Fekstarrstargne, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
             CALL debug_msg_integration('QL Fekstarrstargne', ifailloc, verbose, p, nu, ion, omega)
             intout(1) = 0
          ELSE
             intout(1)=0
             intout(2)=0
          ENDIF
          rfonctecgne = intmult*intout(1); ifonctecgne=intmult*intout(2)

          minpts = 0; ifailloc=1;
          IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
             ifailloc = 1
             CALL integration(dim_out, calc_QL_Fekstarrstarce, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
             CALL debug_msg_integration('QL Fekstarrstarce', ifailloc, verbose, p, nu, ion, omega)
             intout(1) = 0
          ELSE
             intout(1)=0
             intout(2)=0
          ENDIF
          rfonctecce = intmult*intout(1); ifonctecce=intmult*intout(2)
       ELSE
          rfonctecgte = 0.0
          ifonctecgte = 0.0
          rfonctecgne = 0.0
          ifonctecgne = 0.0
          rfonctecce = 0.0
          ifonctecce = 0.0
       ENDIF
    ELSE
       rfonctecgte = 0.0
       ifonctecgte = 0.0
       rfonctecgne = 0.0
       ifonctecgne = 0.0
       rfonctecce = 0.0
       ifonctecce = 0.0

       rfonctcgte = 0.0
       ifonctcgte = 0.0
       rfonctcgne = 0.0
       ifonctcgne = 0.0
       rfonctcce = 0.0
       ifonctcce = 0.0
    ENDIF

    !ELECTRON ENERGY INTEGRALS
    minpts = 0; ifailloc=1;
    IF ( (el_type == 1) .OR. ( (el_type == 3) .AND. (ETG_flag(nu) .EQV. .TRUE.) ) )  THEN
       ifailloc = 1
       CALL integration(dim_out, calc_QL_Fekstarrstare, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
       CALL debug_msg_integration('QL Fekstarrstare', ifailloc, verbose, p, nu, ion, omega)
       intout(1) = 0
    ELSE
       intout(1)=0
       intout(2)=0
    ENDIF
    rfonctece = intmult*intout(1); ifonctece=intmult*intout(2)

    !ION PARTICLE FLUX INTEGRALS
    DO ion=1,nions
       fdata(2) = ion

       ifailloc = 1
       CALL integration(dim_out, calc_QL_Fkstarrstari, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
       CALL debug_msg_integration('QL Fkstarrstari', ifailloc, verbose, p, nu, ion, omega)
       intout(1) = 0
       rfonctci(ion) = intmult*intout(1); ifonctci(ion)=intmult*intout(2)

       !ADDITIONAL ION PARTICLE FLUX INTEGRALS
       IF (phys_meth .NE. 0.0) THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstargti, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstargti', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
          rfonctcgti(ion) = intmult*intout(1); ifonctcgti(ion)=intmult*intout(2)
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstargni, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstargni', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
          rfonctcgni(ion) = intmult*intout(1); ifonctcgni(ion)=intmult*intout(2)

          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fkstarrstarci, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fkstarrstarci', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
          rfonctcci(ion) = intmult*intout(1); ifonctcci(ion)=intmult*intout(2)

          IF (rotation) THEN
             IF ((ABS(Machpar(p)) < epsS) .AND. (ABS(Aupar(p)) < epsS) .AND. (ABS(gammaE(p)) < epsS)) THEN
                intout(1) = 0
                intout(2) = 0
             ELSE
                ifailloc = 1
                CALL integration(dim_out, calc_QL_Fkstarrstargui, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
                CALL debug_msg_integration('QL Fkstarrstargui', ifailloc, verbose, p, nu, ion, omega)
                intout(1) = 0
             ENDIF
             rfonctcgui(ion) = intmult * intout(1)
             ifonctcgui(ion) = intmult * intout(2)
          ENDIF

          IF (phys_meth == 2) THEN
             min_ni: IF (ninorm(p,ion) > min_ninorm) THEN
                ifailloc = 1
                CALL integration(dim_out, calc_QL_Fekstarrstargti, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
                CALL debug_msg_integration('QL Fekstarrstargti', ifailloc, verbose, p, nu, ion, omega)
                intout(1) = 0
                rfonctecgti(ion) = intmult * intout(1)
                ifonctecgti(ion) = intmult * intout(2)

                ifailloc = 1
                CALL integration(dim_out, calc_QL_Fekstarrstargni, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
                CALL debug_msg_integration('QL Fekstarrstargni', ifailloc, verbose, p, nu, ion, omega)
                intout(1) = 0
                rfonctecgni(ion) = intmult * intout(1)
                ifonctecgni(ion) = intmult * intout(2)

                ifailloc = 1
                CALL integration(dim_out, calc_QL_Fekstarrstarci, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
                CALL debug_msg_integration('QL Fekstarrstarci', ifailloc, verbose, p, nu, ion, omega)
                intout(1) = 0
                rfonctecci(ion) = intmult * intout(1)
                ifonctecci(ion) = intmult * intout(2)

                IF (rotation) THEN
                   IF ((ABS(Machpar(p)) < epsS) .AND. (ABS(Aupar(p)) < epsS) .AND. (ABS(gammaE(p)) < epsS)) THEN
                      intout(1) = 0
                      intout(2) = 0
                   ELSE
                      ifailloc = 1
                      CALL integration(dim_out, calc_QL_Fekstarrstargui, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
                      CALL debug_msg_integration('QL Fekstarrstargui', ifailloc, verbose, p, nu, ion, omega)
                      intout(1) = 0
                   ENDIF
                   rfonctecgui(ion) = intmult * intout(1)
                   ifonctecgui(ion) = intmult * intout(2)
                ENDIF
             ELSE min_ni
                rfonctecgti(ion) = 0
                ifonctecgti(ion) = 0
                rfonctecgni(ion) = 0
                ifonctecgni(ion) = 0
                rfonctecci(ion) = 0
                ifonctecci(ion) = 0
                IF (rotation) THEN
                   rfonctecgui(ion) = 0
                   ifonctecgui(ion) = 0
                ENDIF
             ENDIF min_ni
          ELSE
             rfonctecgti(ion) = 0.
             ifonctecgti(ion) = 0.
             rfonctecgni(ion) = 0.
             ifonctecgni(ion) = 0.
             rfonctecci(ion) = 0.
             ifonctecci(ion) = 0.
          ENDIF
       ELSE
          rfonctecgti(ion) = 0.
          ifonctecgti(ion) = 0.
          rfonctecgni(ion) = 0.
          ifonctecgni(ion) = 0.
          rfonctecci(ion) = 0.
          ifonctecci(ion) = 0.

          rfonctcgti(ion) = 0.
          ifonctcgti(ion) = 0.
          rfonctcgni(ion) = 0.
          ifonctcgni(ion) = 0.
          rfonctcci(ion) = 0.
          ifonctcci(ion) = 0.
       ENDIF

       !ION ENERGY INTEGRALS
       IF (ninorm(p,ion) > min_ninorm) THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fekstarrstari, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fekstarrstari', ifailloc, verbose, p, nu, ion, omega)
          intout(1) = 0
          rfoncteci(ion) = intmult * intout(1)
          ifoncteci(ion) = intmult * intout(2)
       ELSE
          rfoncteci(ion) = 0
          ifoncteci(ion) = 0
       ENDIF

    ENDDO

    !The complex forms are reconstructed
    fonctce = rfonctce + ci * ifonctce
    fonctcgte = rfonctcgte + ci * ifonctcgte
    fonctcgne = rfonctcgne + ci * ifonctcgne
    fonctcce = rfonctcce + ci * ifonctcce

    fonctece = rfonctece + ci * ifonctece
    fonctecgte = rfonctecgte + ci * ifonctecgte
    fonctecgne = rfonctecgne + ci * ifonctecgne
    fonctecce = rfonctecce + ci * ifonctecce

    fonctci = rfonctci + ci * ifonctci
    fonctcgti = rfonctcgti + ci * ifonctcgti
    fonctcgni = rfonctcgni + ci * ifonctcgni
    fonctcci =  rfonctcci + ci * ifonctcci
    foncteci = rfoncteci + ci * ifoncteci
    fonctecgti = rfonctecgti + ci * ifonctecgti
    fonctecgni = rfonctecgni + ci * ifonctecgni
    fonctecci =  rfonctecci + ci * ifonctecci

    IF (rotation) THEN
       fonctcgui = rfonctcgui + ci * ifonctcgui
       fonctecgui = rfonctecgui + ci * ifonctecgui
    ENDIF

  END SUBROUTINE passQLints

  SUBROUTINE mompassQLintsrot( p, nu, omega, fonctvci)
    !-----------------------------------------------------------
    ! Calculates the passing particle integrals at the omega of the linear solutions. ANG MOM ONLY
    ! Integrals (with switch) in order to identify particle flux contributions due to An, At and curvature
    ! Includes resonance broadening
    ! To save (some) compuational time, only the (transport relevant) imaginary components are kept
    !-----------------------------------------------------------   
    INTEGER, INTENT(IN)  :: p, nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctvci

    REAL(KIND=DBL), DIMENSION(ndim)    :: a, b

    REAL(KIND=DBL), DIMENSION(nf) :: intout

    REAL(KIND=DBL)    :: intmult=1. ! with rotation need to integrate from -inf to +inf
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctvci
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctvci

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_2d
    REAL(KIND=DBL), DIMENSION(2) :: int_err
    REAL(KIND=DBL), DIMENSION(4) :: fdata
    INTEGER :: ifailloc

    LOGICAL :: tiny_rot

    omFFk = omega
    pFFk = p

    !Integration boundaries
    a(:) = -rkuplim ! with rotation from -inf to +inf
    b(:) =  rkuplim

    dim_out = 2
    dim_in = 2

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag2d_options::opts_2d)
    CASE (1)
      allocate(hcubature_options::opts_2d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in mompassQLintsrot')
    END SELECT

    SELECT TYPE (opts_2d)
    CLASS IS (nag2d_options)
      opts_2d%reqRelError = relaccQL2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%lenwrk = lenwrk
    CLASS IS (hcubature_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%norm = 2
      opts_2d%reqAbsError = absacc2
    END SELECT

    fdata(1) = -1 ! caseflag
    fdata(2) = -1 ! ion: set later
    fdata(3) = logic2real(.TRUE.) !With rotation
    fdata(4) = 1 ! Do not scale integrals


    tiny_rot = (ABS(Machpar(p)) < epsS) .AND. (ABS(Aupar(p)) < epsS) .AND. (ABS(gammaE(p)) < epsS)
    DO ion=1,nions
       fdata(2) = ion
       IF (tiny_rot .OR. (ninorm(p,ion)<min_ninorm)) THEN
          intout(1) = 0
          intout(2) = 0
       ELSE
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fvkstarrstari, dim_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fvkstarrstari', ifailloc, verbose, p, nu, ion, omega)
       ENDIF
       rfonctvci(ion) = 0. 
       ifonctvci(ion) = intmult * intout(2)
    ENDDO

    !The complex forms are reconstructed
    fonctvci = rfonctvci + ci * ifonctvci

  END SUBROUTINE mompassQLintsrot


END MODULE callpassQLints
