MODULE asymmetry
#include "preprocessor.inc"
  USE kind
  USE datmat
  USE datcal
  USE mod_integration
  IMPLICIT NONE

  !Module for calculating density poloidal asymmetries based on centrifugal force and temperature anisotropies (i.e. from heating)
  !Based mostly on work from Angioni POP 2012 and Bilato NF Lett. 2014

CONTAINS

  SUBROUTINE calcphi
    !!Calculates the electric potential along the field line
    !Does so by satisfying the quasineutrality constraint in the presence of rotation and temperature anisotropy
    INTEGER :: j,iflag
    REAL(KIND=DBL) :: guess,Btmp,Ctmp
    REAL(KIND=DBL), DIMENSION(ntheta) :: phicut
    EXTERNAL DFZERO

    DO irad=1,dimx !Loop over radial positions

       !Calculate normalized perpindicular temperatures along field line: Tpar(theta)/Tpar(0). theta=0 is low-field-side.
       !anise and anisi the electron and ion Tperp(0)/Tpar. These are QuaLiKiz input parameters
       !Assumed that Tpare = Tex and Tpari=Tix from QuaLiKiz inputs
       !Formula is based on Bilato 2014
       tpernorme(irad,:) = 1 / ( anise(irad)-(anise(irad)-1) * (1+epsilon(irad)*COS(th)) /(1+epsilon(irad) ) );
       DO j=1,nions
          tpernormi(irad,:,j) = 1 / ( anis(irad,j)-(anis(irad,j)-1) * (1+epsilon(irad)*COS(th)) /(1+epsilon(irad) ) );
       ENDDO

       !Solve the quasineutrality equation at each theta. DFZERO is a root solver routine in lib/src/slatec/slatecroutines.f
       !NOTE: itheta is an effective global variable defined in datmat.f90
       DO itheta=1,ntheta 
          guess = 0._DBL
          iflag=1
          !B (lower limit of solver) C (upper limit of solver), RE (relative error) ,AE (absolute error). All defined in datcal
          Btmp=B
          Ctmp=C
          IF (itheta > 1) THEN !phi(theta=0) is zero by definition
             !Solve quasineutrality! phieq is a function defined below
             CALL DFZERO(phieq,Btmp,Ctmp,guess,RE,AE,iflag)
             phi(irad,itheta)=Btmp
             IF (iflag > 2) THEN
                IF (verbose .EQV. .TRUE.) THEN
                  WRITE(stdout,"(A,I7,A,I2,A,I0)") 'Abnormal termination of DFZERO at ' &
                       // 'p = ',irad,'. itheta = ',itheta, '. iflag = ',iflag
                ENDIF
             ENDIF
          ELSE
             phi(irad,itheta)=0._DBL
          ENDIF
       ENDDO
       !Calculated density profile along the field line for all species, using the just calculated phi
       phicut=phi(irad,:)
       nepol(irad,:)=densprof(phicut,0)
       DO j=1,nions        
          npol(irad,:,j)=densprof(phicut,j)
       ENDDO
    ENDDO
  END SUBROUTINE calcphi

  REAL(KIND=DBL) FUNCTION phieq(x) 
    !!phi is set by quasineutrality constraint at each poloidal position
    !This function defines the quasineutrality equation: sum(Zs*ns(phi))=0 which must be solved for phi
    !ns(phi) is calculated in the dens1 function
    !NOTE: itheta is an effective global variable defined in datmat.f90
    INTEGER :: inum
    REAL(KIND=DBL), INTENT(IN) :: x
    REAL(KIND=DBL) :: phitmp

    phitmp = -dens1(x,th(itheta),0)
    DO inum=1,nions
       IF (ion_type(irad,inum) == 1) THEN
          phitmp=phitmp+Zi(irad,inum)*dens1(x,th(itheta),inum)
       ENDIF
    ENDDO
    phieq = phitmp

  END FUNCTION phieq

  FUNCTION dens1(locphi,theta1,inum)
    !!Calculates density at theta=theta1, for phi=x
    !NOTE: itheta is an effective global variable defined in datmat.f90
    REAL(KIND=DBL), INTENT(IN) :: theta1, locphi
    INTEGER, INTENT(IN) :: inum
    !! inum=0 for electrons, inum>0 for the various ion species
    REAL(KIND=DBL) :: dens1,Rlfs,Rth
    INTEGER :: ix

    REAL(KIND=DBL) :: A
    !! Exponent pre-factor. \(  T_{perp,N,s} * n_s \)
    REAL(KIND=DBL) :: centr_force_bal
    !! Centrifugal force balance \( \omega_{tor}^2 * (R_{th}^2 - R_{lfs}^2) \)
    REAL(KIND=DBL) :: Ts_SI
    !! Species temperature in SI \( 1e3 T_s q_e \)
    REAL(KIND=DBL) :: qs
    !! Charge of species
    REAL(KIND=DBL) :: e4 ! helper variable

    Rlfs=Ro(irad)*(1+epsilon(irad)) !low field side theta=0 major radius
    Rth=Ro(irad)*(1+epsilon(irad)*COS(theta1)) !theta dependent major radius
    ix = NINT(theta1*(ntheta-1)/pi+1)  !find index in theta grid that matches theta1

    !Calculate density depending on input phi, rotation and temp anisotropy
    IF (inum == 0) THEN
      A = tpernorme(irad,ix)*Nex(irad)
      qs = -qe
      centr_force_bal = me/2.*omegator(irad)**2*(Rth**2-Rlfs**2)
      Ts_SI = Tex(irad)*1d3*qe
    ELSE
      A = tpernormi(irad,ix,inum)*Nix(irad,inum)
      qs = Zi(irad,inum)*qe
      centr_force_bal = mi(irad,inum)/2.*omegator(irad)**2*(Rth**2-Rlfs**2)
      Ts_SI = (Tix(irad,inum)*1d3*qe)
    ENDIF
    e4 = -(qs * locphi - centr_force_bal) / Ts_SI
    ! Protect against overflow
    if (e4 >= 700._DBL) then
      dens1 = A * EXP(700._DBL)
    else
      dens1 = A * EXP(e4)
    endif
  END FUNCTION dens1

  FUNCTION densprof(phi,inum)
    !!Calculates density profiles for species inum (0 electrons, >=1 for ions), and for an electron potential profile phi
    REAL(KIND=DBL), DIMENSION(ntheta), INTENT(IN) :: phi
    INTEGER, INTENT(IN) :: inum
    REAL(KIND=DBL), DIMENSION(ntheta) :: densprof, Rth
    REAL(KIND=DBL) :: Rlfs

    Rlfs=Ro(irad)*(1+epsilon(irad))
    Rth(:)=Ro(irad)*(1+epsilon(irad)*COS(th))
    IF (inum == 0) THEN
       densprof=tpernorme(irad,:)*Nex(irad)*EXP(-(-qe*phi - me/2.*omegator(irad)**2*(Rth(:)**2-Rlfs**2))/(Tex(irad)*1d3*qe));
    ELSE
       densprof=tpernormi(irad,:,inum)*Nix(irad,inum)&
            &*EXP(-(Zi(irad,inum)*qe*phi - mi(irad,inum)/2.*omegator(irad)**2*(Rth(:)**2-Rlfs**2))/(Tix(irad,inum)*1d3*qe));
    ENDIF
  END FUNCTION densprof

  SUBROUTINE calcdphi
    !Calculates dphi/dr and dphi/dtheta needed for poloidal asymmetry effect calculations.
    !Does so by solving quasineutrality of gradients: sum_j*(R/Lnj * n_j * Z_j) = 0
    !For a known phi, this solution can be done analytically
    REAL(KIND=DBL), DIMENSION(ntheta) :: Rlfs,Rth,nom,denom,Ee,dtpernormedr,phicut
    REAL(KIND=DBL), DIMENSION(ntheta,nions) :: dtpernormidr, Ei
    INTEGER :: j

    DO irad = 1,dimx
       phicut=phi(irad,:) !Assumes that phi (global variable in datmat) already calculated beforehand with calcphi routine
       Rlfs=Ro(irad)*(1+epsilon(irad)) !low field side major radius
       Rth =Ro(irad)*(1+epsilon(irad)*COS(th)) !theta dependent major radius

       Ee(:)=-qe*phi(irad,:)-me/2.*omegator(irad)**2*(Rth**2-Rlfs**2)  !electron electric and rotational kinetic potential

       dtpernormedr(:) = 2*SIN(th/2.)**2*tpernorme(irad,:)**2/(Ro(irad)*(1+epsilon(irad))**2)*&
            &(1-anise(irad)-danisedr(irad)*epsilon(irad)*Ro(irad)*(1+epsilon(irad))) !Derivative of Tperpe(theta)/Tperpe(0)

       DO j=1,nions
          Ei(:,j)=Zi(irad,j)*qe*phi(irad,:)-mi(irad,j)/2.*omegator(irad)**2*(Rth**2-Rlfs**2) !ion electric and rotational kinetic potential
          dtpernormidr(:,j) = 2*SIN(th/2.)**2*tpernormi(irad,:,j)**2/(Ro(irad)*(1+epsilon(irad))**2)*&
               &(1-anis(irad,j)-danisdr(irad,j)*epsilon(irad)*Ro(irad)*(1+epsilon(irad))) !Derivative of Tperpi(theta)/Tperpi(0)
       ENDDO

       !Calculate dphi/dr from analytical solution of quasineutrality of gradients
       !nom (nominator) and denom (denominator) contain sums over species. dphi/dr = nom/denom
       nom(:) = -densprof(phicut,0)*(-Ane(irad)-Ee(:)*Ate(irad)/(Tex(irad)*qe*1d3) + & 
            & Ro(irad)*me*omegator(irad)*domegatordr(irad)/(Tex(irad)*qe*1d3)*(Rth**2-Rlfs**2) + me*Ro(irad)*omegator(irad)**2/ &
            (Tex(irad)*qe*1d3)*(Rth*COS(th)-Rlfs) +Ro(irad)*dtpernormedr(:)/tpernorme(irad,:))

       denom(:) = densprof(phicut,0)*qe/(Tex(irad)*qe*1d3)*Ro(irad)

       DO j=1,nions
          IF (ion_type(irad,j) == 1) THEN
             nom(:) = nom(:) +  Zi(irad,j)*densprof(phicut,j)*(-Ani(irad,j)-Ei(:,j)*Ati(irad,j)/(Tix(irad,j)*qe*1d3) + &
                  & Ro(irad)*mi(irad,j)*omegator(irad)*domegatordr(irad)/(Tix(irad,j)*qe*1d3)*(Rth**2-Rlfs**2) + &
                  & mi(irad,j)*Ro(irad)*omegator(irad)**2/(Tix(irad,j)*qe*1d3)*(Rth*COS(th)-Rlfs) &
                  &  +Ro(irad)*dtpernormidr(:,j)/tpernormi(irad,:,j))

             denom(:) = denom(:) +  Zi(irad,j)**2*densprof(phicut,j)*qe/(Tix(irad,j)*qe*1d3)*Ro(irad)            
          ENDIF
       ENDDO

       dphidr(irad,:)=nom(:)/denom(:)

       !Calculate theta derivative of theta with 5 point difference formula
       !Uses phifunc function which calculates phi at arbitrary theta (needed for the numeric differentiation)
       DO j=1,ntheta
          dphidth(irad,j) = ( phifunc(th(j)-2.*dtheta)-8.*phifunc(th(j)-dtheta) + 8.*phifunc(th(j)+dtheta) - &
               phifunc(th(j)+2.*dtheta) ) /(12.*dtheta)
       ENDDO

    ENDDO

  END SUBROUTINE calcdphi

  REAL(KIND=DBL) FUNCTION phifunc(x)
    !!Function for calculating phi at arbitrary theta for the numerical theta differentiation for dphidth
    !!quasineutrality equation at arbitrary theta defined in phieqfunc
    REAL(KIND=DBL), INTENT(IN) :: x !input arbitrary theta
    INTEGER :: j,iflag
    REAL(KIND=DBL) :: guess,Btmp,Ctmp,tpernormefunc
    REAL(KIND=DBL), DIMENSION(nions) :: tpernormifunc
    EXTERNAL DFZERO

    tpernormefunc = 1 / ( anise(irad)-(anise(irad)-1) * (1+epsilon(irad)*COS(x)) /(1+epsilon(irad) ) + epsD);
    DO j=1,nions
       tpernormifunc(j) = 1 / ( anis(irad,j)-(anis(irad,j)-1) * (1+epsilon(irad)*COS(x)) /(1+epsilon(irad) ) + epsD);
    ENDDO

    guess = 0._DBL
    iflag=1
    Btmp=B
    Ctmp=C
    thetapass= x  !thetapass is a global variable defined in datmat. Used to pass around the angle between the functions used in the calculation

    CALL DFZERO(phieqfunc,Btmp,Ctmp,guess,RE,AE,iflag)
    IF (iflag > 2) THEN
       IF (verbose .EQV. .TRUE.) WRITE(stdout,"(A,I2,A,I2,A,I0)") 'Abnormal termination of DFZERO in phifunc at ' &
            // 'p = ',irad,'. itheta = ',itheta, '. iflag = ',iflag
    ENDIF

    phifunc = Btmp

  END FUNCTION phifunc

  REAL(KIND=DBL) FUNCTION phieqfunc(x) 
    !phi is set by quasineutrality constraint at an arbitrary theta (used for the theta differentiation)
    INTEGER :: inum
    REAL(KIND=DBL), INTENT(IN) :: x
    REAL(KIND=DBL) :: phitmp

    phitmp = -dens1(x,thetapass,0)
    DO inum=1,nions
       IF (ion_type(irad,inum) == 1) THEN
          phitmp=phitmp+Zi(irad,inum)*dens1(x,thetapass,inum)
       ENDIF
    ENDDO
    phieqfunc = phitmp

  END FUNCTION phieqfunc

  SUBROUTINE calccoefs
    !calculates the e0-e6 coefficients used for transforming from low field side (LFS) to flux surface average (FSA)
    !transport coefficients and gradients. Done for each species. Also calcuates the poloidal profile of R/Ln for each species
    REAL(KIND=DBL) :: theta1
    INTEGER :: ith,ifailloc, ecoef_num

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_1d
    REAL(KIND=DBL), DIMENSION(1) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(2) :: fdata
    REAL(KIND=DBL), DIMENSION(1) :: thmin, thmax
    character(len=8) :: msg

    dim_in = 1
    dim_out = 1
    !limits for integration
    thmin = 0._DBL
    thmax = pi

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT

    opts_1d%reqRelError = epsFLR
    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%maxEval = lw2
    CLASS IS (hcubature_options)
      ! hcubature, force abs error to be 0
      ! TODO: Double check if epsFLR is not to strict for hcub
      opts_1d%maxEval = lw2
      opts_1d%minEval = 0
      opts_1d%norm = 1
      opts_1d%reqAbsError = 0
    END SELECT

    fdata(2) = 0 ! No flux surface avarage

    DO irad=1,dimx !loop over radial locations

       DO ion=0,nions !loop over species in plasma (0=electrons, >=1 for ions)

          DO ith=1,ntheta !loop over theta grid
             theta1=th(ith)

             !Calculate theta dependent R/Ln using the non-flux-surface averaged e#1 coefficients.
             !These coefficients are defined in GKW manual appendix B, and generalized here to include
             !the temperature anisotropy
             IF (ion == 0) THEN
                Anepol(irad,ith) = Ane(irad) - ( -Ate(irad)*e11(theta1) - e21(theta1) + &
                     & Ro(irad)**2*me*omegator(irad)**2*Ate(irad)/(2*(Tex(irad)*1d3*qe))*e31(theta1) + &
                     &  Ro(irad)**3*me*omegator(irad)*domegatordr(irad)/(Tex(irad)*qe*1d3)*e31(theta1) + &
                     &  Ro(irad)**2*me*omegator(irad)**2/(2*(Tex(irad)*1d3*qe))*e41(theta1) +e61(theta1) )/e01(theta1)
             ELSE

                Anipol(irad,ith,ion) = Ani(irad,ion) - ( -Ati(irad,ion)*e11(theta1) - e21(theta1) + &
                     & Ro(irad)**2*mi(irad,ion)*omegator(irad)**2*Ati(irad,ion)/(2*(Tix(irad,ion)*1d3*qe))*e31(theta1) + &
                     &  Ro(irad)**3*mi(irad,ion)*omegator(irad)*domegatordr(irad)/(Tix(irad,ion)*qe*1d3)*e31(theta1) + &
                     &  Ro(irad)**2*mi(irad,ion)*omegator(irad)**2/(2*(Tix(irad,ion)*1d3*qe))*e41(theta1) +e61(theta1) )/e01(theta1)
             ENDIF
          ENDDO

          DO ecoef_num = 0, 9
             ifailloc = 1
             fdata(1) = ecoef_num
             CALL integration(dim_out, calc_ecoef_integral, dim_in, thmin, thmax, opts_1d, int_res, int_err, fdata, ifailloc)
             WRITE(msg, '(A, I0)') 'ecoef ', ecoef_num
             CALL debug_msg_pion(msg, ifailloc, verbose, irad, ion)
             ecoefs(irad,ion,ecoef_num + 1) = int_res(1)
          ENDDO

          !R/Ln (defined as R*<dn/dr>/<n>)
          IF (ion == 0) THEN
             ecoefs(irad,ion,11) = Ane(irad) - ( -Ate(irad)*ecoefs(irad,ion,2) - ecoefs(irad,ion,3) + &
                  & Ro(irad)**2*me*omegator(irad)**2*Ate(irad)/(2*(Tex(irad)*1d3*qe))*ecoefs(irad,ion,4) + &
                  &  Ro(irad)**3*me*omegator(irad)*domegatordr(irad)/(Tex(irad)*qe*1d3)*ecoefs(irad,ion,4) + &
                  &  Ro(irad)**2*me*omegator(irad)**2/(2*(Tex(irad)*1d3*qe))*ecoefs(irad,ion,5) +ecoefs(irad,ion,7)) / &
                  ecoefs(irad,ion,1)
          ELSE

             ecoefs(irad,ion,11) = Ani(irad,ion) - ( -Ati(irad,ion)*ecoefs(irad,ion,2) - ecoefs(irad,ion,3) + &
                  & Ro(irad)**2*mi(irad,ion)*omegator(irad)**2*Ati(irad,ion)/(2*(Tix(irad,ion)*1d3*qe))*ecoefs(irad,ion,4) + &
                  &  Ro(irad)**3*mi(irad,ion)*omegator(irad)*domegatordr(irad)/(Tix(irad,ion)*qe*1d3)*ecoefs(irad,ion,4) + &
                  &  Ro(irad)**2*mi(irad,ion)*omegator(irad)**2/(2*(Tix(irad,ion)*1d3*qe))*ecoefs(irad,ion,5) +ecoefs(irad,ion,7))&
                  & / ecoefs(irad,ion,1)

          ENDIF

          !<n>
          IF (ion == 0) THEN
             ecoefs(irad,ion,12) = Nex(irad)*ecoefs(irad,ion,1)
          ELSE
             ecoefs(irad,ion,12) = Nix(irad,ion)*ecoefs(irad,ion,1)
          ENDIF

          !Asymmetry factor: (nLFS-nHFS)/<n> 
          ecoefs(irad,ion,13)=(dens1(phi(irad,1),0._DBL,ion)-dens1(phi(irad,ntheta),pi,ion))/ecoefs(irad,ion,12)

       ENDDO
    ENDDO

  END SUBROUTINE calccoefs

  REAL(KIND=DBL) FUNCTION calc_ecoef(x, ecoef_num, flux_surface_average) RESULT(ecoef)
     !! Calculate a specific e-coefficient at each poloidal position
     !! e7 (new definition= Z*qe/T * s/eps*th*dphidth) used in the effective R/Ln in the QL flux integral
     !! e8 (new definition= (1+eps*cos(th))(cos(th)+s*th*sin(th))) 
     !! e9 (like e4 but without Rth)
     !!
     !! \(P\): tpernorme
     !! \(e^{-E_s/T_s}\): exp_E_T
     !! \(\Theta\): x

     REAL(KIND=DBL), INTENT(IN) :: x
     INTEGER, INTENT(IN) :: ecoef_num
     LOGICAL, INTENT(IN) :: flux_surface_average
     REAL(KIND=DBL) :: Rlfs,Rth
     REAL(KIND=DBL) :: dRlfsdr,dRthdr
     REAL(KIND=DBL) :: dtpernormdr, tpernorm
     INTEGER :: ix

     REAL(KIND=DBL) :: Etot, T_SI, exp_E_T, P_exp_E_T, qs, flux_surface_averager, T_s
     REAL(KIND=DBL) :: danissdr, aniss

     ix = NINT(x*(ntheta-1)/pi+1)

     Rlfs = Ro(irad)*(1+epsilon(irad))
     Rth =  Ro(irad)*(1+epsilon(irad)*COS(x)) !R(theta)
     SELECT CASE (ecoef_num)
     CASE (0,1,2,3,7,8)
        ! No extra variables needed
     CASE (5)
        ! always 0 in Hamada coordinates
     CASE (4, 9)
        dRlfsdr = 1
        dRthdr = COS(x)
     CASE (6)
        IF (ion == 0) THEN
           danissdr = danisedr(irad)
           aniss = anise(irad)
        ELSE
           danissdr = danisdr(irad,ion)
           aniss = anis(irad,ion)
        END IF
        dtpernormdr = -2*SIN(x/2.)**2*(epsilon(irad)*(1+epsilon(irad))*danissdr+(aniss-1)/Ro(irad))/ &
              &(aniss*(epsilon(irad)-epsilon(irad)*COS(x))+epsilon(irad)*COS(x)+1)**2
     CASE DEFAULT
        ERRORSTOP(.TRUE., 'Unknown ecoef number requested')
     END SELECT

     IF (ion == 0) THEN
        Etot = -qe*phi(irad,ix)-me/2.*omegator(irad)**2*(Rth**2-Rlfs**2)
        T_s = Tex(irad)
        tpernorm = tpernorme(irad,ix)
        qs = - qe
     ELSE
        Etot = Zi(irad,ion)*qe*phi(irad,ix)-mi(irad,ion)/2.*omegator(irad)**2*(Rth**2-Rlfs**2)
        T_s = Tix(irad,ion)
        tpernorm = tpernormi(irad,ix,ion)
        qs = Zi(irad,ion) * qe
     END IF
     T_SI = T_s * qe * 1d3
     exp_E_T = EXP(-Etot/T_SI)
     P_exp_E_T =  tpernorm * exp_E_T

     SELECT CASE (ecoef_num)
     CASE (0)
        ecoef = P_exp_E_T
     CASE (1)
        ecoef = (qs / T_SI) * phi(irad,ix) * P_exp_E_T
     CASE (2)
        ecoef = Ro(irad) * (qs / T_SI) * dphidr(irad,ix) * P_exp_E_T
     CASE (3)
        ecoef = (1 / Ro(irad)**2) * (Rth**2 - Rlfs**2) * P_exp_E_T
     CASE (4)
        ecoef = (2 / Ro(irad)) * (Rth * dRthdr - Rlfs * dRlfsdr) * P_exp_E_T
     CASE (5)
        ecoef = 0
     CASE (6)
        ecoef = Ro(irad) * dtpernormdr * exp_E_T
     CASE (7)
        ecoef = P_exp_E_T * (smag(irad) / epsilon(irad) * x) * (qs / T_SI) * dphidth(irad,ix)
     CASE (8)
        ecoef = P_exp_E_T * (1 + epsilon(irad) * COS(x)) * (COS(x) + smag(irad) * x * SIN(x))
        ! TODO: Remove if statement after check of documentation + regression
        if (ion == 0) THEN
           ecoef = -ecoef
        END IF
     CASE (9)
        ecoef = (2 / Ro(irad)) * (-Rlfs * dRlfsdr) * P_exp_E_T
     CASE DEFAULT
        ERRORSTOP(.TRUE., 'Unknown ecoef number requested')
     END SELECT

     ecoef = ecoef / pi

     IF (flux_surface_average) THEN
        flux_surface_averager = (1+epsilon(irad)*COS(x))*EXP(-1./2.*(widthhat/distan(irad,inu)*x)**2)
        ecoef = ecoef * flux_surface_averager
        IF ((ix == 2) .AND. (ecoef < epsS)) THEN
           ecoef = epsS
        END IF
     END IF
  END FUNCTION calc_ecoef

  REAL(KIND=DBL) FUNCTION e01(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 0, .false.)
  END FUNCTION e01

  REAL(KIND=DBL) FUNCTION e11(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 1, .false.)
  END FUNCTION e11

  REAL(KIND=DBL) FUNCTION e21(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 2, .false.)
  END FUNCTION e21

  REAL(KIND=DBL) FUNCTION e31(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 3, .false.)
  END FUNCTION e31

  REAL(KIND=DBL) FUNCTION e41(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 4, .false.)
  END FUNCTION e41

  REAL(KIND=DBL) FUNCTION e51(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 5, .false.)
  END FUNCTION e51

  REAL(KIND=DBL) FUNCTION e61(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 6, .false.)
  END FUNCTION e61

  REAL(KIND=DBL) FUNCTION e71(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 7, .false.)
  END FUNCTION e71

  REAL(KIND=DBL) FUNCTION e81(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
        ecoef = calc_ecoef(x, 8, .false.)
  END FUNCTION e81

  REAL(KIND=DBL) FUNCTION e91(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 9, .false.)
  END FUNCTION e91

  REAL(KIND=DBL) FUNCTION e01d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 0, .true.)
  END FUNCTION e01d

  REAL(KIND=DBL) FUNCTION e11d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 1, .true.)
  END FUNCTION e11d

  REAL(KIND=DBL) FUNCTION e21d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 2, .true.)
  END FUNCTION e21d

  REAL(KIND=DBL) FUNCTION e31d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 3, .true.)
  END FUNCTION e31d

  REAL(KIND=DBL) FUNCTION e41d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 4, .true.)
  END FUNCTION e41d

  REAL(KIND=DBL) FUNCTION e51d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 5, .true.)
  END FUNCTION e51d

  REAL(KIND=DBL) FUNCTION e61d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 6, .true.)
  END FUNCTION e61d

  REAL(KIND=DBL) FUNCTION e71d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 7, .true.)
  END FUNCTION e71d

  REAL(KIND=DBL) FUNCTION e81d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 8, .true.)
  END FUNCTION e81d

  REAL(KIND=DBL) FUNCTION e91d(x) RESULT(ecoef)
     REAL(KIND=DBL), INTENT(IN) :: x
     ecoef = calc_ecoef(x, 9, .true.)
  END FUNCTION e91d

  SUBROUTINE calc_ecoef_integral(dim_in, f_in, f_data, dim_out, f_out, ifail)
     !! Mutable interface to calculate specific e-coefficients at each poloidal position
     !! f_in(1) is x
     !! f_data(1) can be used to select a specific ecoef
     !! f_data(2) is 0 for per-radial-location, 2 for flux surface average
     INTEGER, INTENT(IN) :: dim_in, dim_out
     REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
     REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
     REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
     INTEGER, INTENT(INOUT) :: ifail

     REAL(KIND=DBL) :: x, ecoef
     INTEGER :: ecoef_num
     LOGICAL :: flux_surface_average

     ERRORSTOP(dim_in /= 1, 'dim_in for ecoef integral /= 1')
     ERRORSTOP(dim_out /= 1, 'dim_out for ecoef integral /= 1')

     x = f_in(1)
     ecoef_num = f_data(1)
     flux_surface_average = real2logic(f_data(2))

     ecoef = calc_ecoef(x, ecoef_num, flux_surface_average)
     f_out(1) = ecoef

     ifail = 0
  END SUBROUTINE calc_ecoef_integral

  SUBROUTINE FSAnorm(dim_in, f_in, f_data, dim_out, f_out, ifail)
     !! Calculates the normalization coefficient for the wavefunction weighted FSA used in makeecoefsgau
     !! f_in(1) is x
     INTEGER, INTENT(IN) :: dim_in, dim_out
     REAL(KIND=DBL), DIMENSION(:), INTENT(IN) :: f_in
     REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
     REAL(KIND=DBL), DIMENSION(:), INTENT(OUT) :: f_out
     INTEGER, INTENT(INOUT) :: ifail

     REAL(KIND=DBL) :: x, ix, res

     ERRORSTOP(dim_in /= 1, 'dim_in for FSAnorm /= 1')
     ERRORSTOP(dim_out /= 1, 'dim_out for FSAnorm /= 1')

     x = f_in(1)

     ix = NINT(x*(ntheta-1)/pi+1) 

     res = 1./pi*(1+epsilon(irad)*COS(x))*EXP(-1./2.*(widthhat/distan(irad,inu)*x)**2)
     IF ((ix == 2) .AND. (res < epsS)) THEN
        res = epsS
     ENDIF
     f_out(1) = res

     ifail = 0
  END SUBROUTINE FSAnorm

  SUBROUTINE makeecoefsgau(p,nu)
    INTEGER, INTENT(IN) :: p, nu
    REAL(KIND=DBL) :: intnorm
    INTEGER :: ifailloc
    INTEGER :: ecoef_num

    INTEGER :: dim_out, dim_in
    class(integration_options), allocatable :: opts_1d
    REAL(KIND=DBL), DIMENSION(1) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(2) :: fdata
    REAL(KIND=DBL), DIMENSION(1) :: thmin, thmax
    character(len=12) :: msg

    dim_in = 1
    dim_out = 1
    !limits for integration
    thmin=0._DBL
    thmax=pi

    irad= p
    inu = nu

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in calcroutines')
    END SELECT

    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw2
    CLASS IS (hcubature_options)
      ! hcubature, force abs error to be 0
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw2
      opts_1d%minEval = 0
      opts_1d%norm = 1
      opts_1d%reqAbsError = 0
    END SELECT

    fdata(2) = 1 ! Flux surface avarage

    !Calculate integration norm
    ifailloc = 1
    CALL integration(dim_out, FSAnorm, dim_in, thmin, thmax, opts_1d, int_res, int_err, fdata, ifailloc)
    CALL debug_msg_pnuion('e-coef gau normalization', ifailloc, verbose, p, nu, ion)
    intnorm = int_res(1)

    !Calculate the averages including full FSA and the Gaussian width weighting

    DO ion=0,nions !loop over species in plasma (0=electrons, >=1 for ions)
       DO ecoef_num = 0, 9
          ifailloc = 1
          fdata(1) = ecoef_num
          CALL integration(dim_out, calc_ecoef_integral, dim_in, thmin, thmax, opts_1d, int_res, int_err, fdata, ifailloc)
          WRITE(msg, '(A, I0)') 'ecoef gau ', ecoef_num
          CALL debug_msg_pnuion(msg, ifailloc, verbose, p, nu, ion)
          ecoefsgau(irad,inu,ion,ecoef_num) = int_res(1)
       ENDDO
    ENDDO

    !Normalize all calculated quantities with the integration norm
    ecoefsgau(irad,inu,:,:)=ecoefsgau(irad,inu,:,:)/intnorm

  END SUBROUTINE makeecoefsgau


END MODULE asymmetry
