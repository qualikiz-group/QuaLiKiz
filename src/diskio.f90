MODULE diskio
#include "preprocessor.inc"

  USE kind
  USE mpi
  use qlk_version, only: QLK_CLOSEST_RELEASE, QLK_GITBRANCHNAME, QLK_GITSHAKEY, QLK_COMPILER_VERSION, &
       QLK_REPOSITORY_ROOT, QLK_HOSTFULL

  IMPLICIT NONE

  INTERFACE writevar
     MODULE PROCEDURE &
          writevar_0d, &
          writevar_0d_integer, &
          writevar_1d, &
          writevar_2d, &
          writevar_2d_complex, &
          writevar_2d_integer, &
          writevar_3d, &
          writevar_3d_complex, &
          writevar_4d, &
          writevar_4d_complex
  END INTERFACE

  INTERFACE readvar
     MODULE PROCEDURE &
          readvar_0d, &
          readvar_1d, &
          readvar_2d, &
          readvar_3d_txt
  END INTERFACE


CONTAINS
  SUBROUTINE open_file_in(filename,lengthin,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: myunit, lengthin

    IF (INDEX(filename, '.dat') > 0) THEN
        CALL open_file_in_txt(filename, lengthin, myunit)
    ELSE
        CALL open_file_in_bin(filename, lengthin, myunit)
    ENDIF
  END SUBROUTINE open_file_in

  SUBROUTINE open_file_in_bin(filename,lengthin,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: myunit
    INTEGER :: lengthin

    OPEN( unit=myunit, &
         file=filename, &
         access='direct', &
         form='unformatted', &
         status='old', &
         recl=lengthin)
  END SUBROUTINE open_file_in_bin

  SUBROUTINE open_file_in_txt(filename,lengthin,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: myunit, lengthin

    OPEN( unit=myunit, &
         file=filename, &
         access='sequential', &
         form='formatted', &
         status='old', &
         recl=lengthin)
  END SUBROUTINE open_file_in_txt

  SUBROUTINE open_file_out_txt(filename,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: myunit
    INTEGER :: rc

    OPEN(unit=myunit, &
         file=filename, &
         access='sequential', &
         form='formatted', &
         status='replace', &
         iostat=rc)
    ERRORSTOP(rc /= 0, "Could not open file")
  END SUBROUTINE open_file_out_txt

  SUBROUTINE writevar_0d(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    REAL, INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER :: myunit, ierror
    INTEGER :: myrank, nproc
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    ERRORSTOP(ierror /= 0, "Could not communicate with MPI")
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    ERRORSTOP(ierror /= 0, "Could not communicate with MPI")
    !write(stdout,'(A,A,A,I15,A,I15,A,I15)') "Testing if to write filename=", filename, " fileno=",fileno, " myrank=",myrank," nproc=",nproc
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        !IF (.NOT. force_write_local) THEN
        !    WRITE(stdout, *) MOD(fileno, nproc) == myrank
        !    WRITE(stdout, '(A,I5,A,I5,A,F15.0)') 'rank: ', myrank, ' fileno: ', fileno, ' time: ', omp_get_wtime()
        !ENDIF
        myunit = 700 + myrank
        !write(stdout,*) "Writing with myunit=",myunit
        CALL open_file_out_txt(filename,myunit)
        WRITE(unit=myunit,fmt='(' // varformat // ')') mold
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_0d

  SUBROUTINE writevar_0d_integer(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    INTEGER, INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER :: myunit, ierror
    INTEGER :: nproc, myrank
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    ERRORSTOP(ierror /= 0, "Could not communicate with MPI")
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    ERRORSTOP(ierror /= 0, "Could not communicate with MPI")
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        CALL open_file_out_txt(filename,myunit)
        WRITE(unit=myunit,fmt='(' // varformat // ')') mold
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_0d_integer

  SUBROUTINE writevar_1d(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    REAL, DIMENSION(:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER :: myunit, nproc, myrank, ierror
    INTEGER :: numcols
    CHARACTER(LEN=30) :: rowfmt
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        numcols = 1 !Write out 1D array as a single column
        CALL open_file_out_txt(filename,myunit)
        WRITE(rowfmt,'(A,I0,A)') '(',numcols,varformat // ')'
        WRITE(unit=myunit,fmt=rowfmt) mold
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_1d

  SUBROUTINE writevar_2d(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    REAL, DIMENSION(:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER           :: numcols,numrows,i,j
    CHARACTER(LEN=30) :: rowfmt 
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        numcols = SIZE(mold,2)
        numrows = SIZE(mold,1)
        CALL open_file_out_txt(filename,myunit)
        WRITE(rowfmt,'(A,I0,A)') '(',numcols,varformat // ')'
        WRITE(myunit,rowfmt) ((mold(i,j),j=1,numcols),i=1,numrows)
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_2d

  SUBROUTINE writevar_2d_integer(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    INTEGER, DIMENSION(:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER           :: numcols,numrows,i,j
    CHARACTER(LEN=30) :: rowfmt
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        numcols = SIZE(mold,2)
        numrows = SIZE(mold,1)
        CALL open_file_out_txt(filename,myunit)
        WRITE(rowfmt,'(A,I0,A)') '(',numcols, varformat // ')'
        WRITE(myunit,rowfmt) ((mold(i,j),j=1,numcols),i=1,numrows)
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_2d_integer

  SUBROUTINE writevar_2d_complex(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    COMPLEX(kind=DBL), DIMENSION(:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno

    CHARACTER(LEN=:), ALLOCATABLE :: dirname,basename,suffix
    INTEGER :: dirsep, sufsep
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        dirsep=index(filename, '/', BACK=.TRUE.)
        sufsep=index(filename, '.', BACK=.TRUE.)

        dirname=filename(1:dirsep)
        basename=filename(dirsep+1:sufsep-1)
        suffix=filename(sufsep+1:)

        CALL writevar_2d(dirname // 'r' // basename // '.' // suffix, &
             REAL(mold),varformat,myrank,force_write=.TRUE.)
        CALL writevar_2d(dirname // 'i' // basename // '.' // suffix, &
             AIMAG(mold),varformat,myrank,force_write=.TRUE.)
     ENDIF
  END SUBROUTINE writevar_2d_complex

  SUBROUTINE writevar_3d(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    REAL, DIMENSION(:,:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER           :: numcols,numrows,numpages,i,j,k
    CHARACTER(LEN=30) :: rowfmt
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        numpages = SIZE(mold,3)
        numcols = SIZE(mold,2)
        numrows = SIZE(mold,1)
        CALL open_file_out_txt(filename,myunit)
        WRITE(rowfmt,'(A,I0,A)') '(',numcols,varformat // ')'
        WRITE(myunit,rowfmt) (((mold(i,j,k),j=1,numcols),i=1,numrows),k=1,numpages)
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_3d

  SUBROUTINE writevar_3d_complex(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    COMPLEX(kind=DBL), DIMENSION(:,:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    CHARACTER(LEN=:), ALLOCATABLE :: dirname,basename,suffix
    INTEGER :: dirsep, sufsep
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        dirsep=index(filename, '/', BACK=.TRUE.)
        sufsep=index(filename, '.', BACK=.TRUE.)

        dirname=filename(1:dirsep)
        basename=filename(dirsep+1:sufsep-1)
        suffix=filename(sufsep+1:)

        CALL writevar_3d(dirname // 'r' // basename // '.' // suffix, &
             REAL(mold),varformat,myrank,force_write=.TRUE.)
        CALL writevar_3d(dirname // 'i' // basename // '.' // suffix, &
             AIMAG(mold),varformat,myrank,force_write=.TRUE.)
     ENDIF
  END SUBROUTINE writevar_3d_complex

  SUBROUTINE writevar_4d(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    REAL, DIMENSION(:,:,:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    INTEGER           :: numcols,numrows,numpages,numhpages,i,j,k,l
    CHARACTER(LEN=30) :: rowfmt
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        numhpages = SIZE(mold,4)
        numpages = SIZE(mold,3)
        numcols = SIZE(mold,2)
        numrows = SIZE(mold,1)
        CALL open_file_out_txt(filename,myunit)
        WRITE(rowfmt,'(A,I0,A)') '(',numcols,varformat // ')'
        WRITE(myunit,rowfmt) ((((mold(i,j,k,l),j=1,numcols),i=1,numrows), &
            k=1,numpages),l=1,numhpages)
        CLOSE(unit=myunit)
    ENDIF
  END SUBROUTINE writevar_4d

  SUBROUTINE writevar_4d_complex(filename, mold, varformat, fileno, force_write)
    CHARACTER(len=*), INTENT(IN) :: filename, varformat
    COMPLEX(kind=DBL), DIMENSION(:,:,:,:), INTENT(IN) :: mold
    INTEGER, INTENT(IN) :: fileno
    CHARACTER(LEN=:), ALLOCATABLE :: dirname,basename,suffix
    INTEGER :: dirsep, sufsep
    INTEGER :: myunit, nproc, myrank, ierror
    LOGICAL, OPTIONAL :: force_write
    LOGICAL :: force_write_local

    IF (.NOT. PRESENT(force_write)) THEN
        force_write_local = .FALSE.
    ELSE
        force_write_local = force_write
    ENDIF

    CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
    CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)
    IF ((MOD(fileno, nproc) == myrank) .OR. force_write_local) THEN
        myunit = 700 + myrank
        dirsep=index(filename, '/', BACK=.TRUE.)
        sufsep=index(filename, '.', BACK=.TRUE.)

        dirname=filename(1:dirsep)
        basename=filename(dirsep+1:sufsep-1)
        suffix=filename(sufsep+1:)

        CALL writevar_4d(dirname // 'r' // basename // '.' // suffix, &
             REAL(mold),varformat,myrank,force_write=.TRUE.)
        CALL writevar_4d(dirname // 'i' // basename // '.' // suffix, &
             AIMAG(mold),varformat,myrank,force_write=.TRUE.)
     ENDIF
  END SUBROUTINE writevar_4d_complex

  REAL(kind=DBL) FUNCTION readvar_0d(filename, dummy, ktype,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: ktype, myunit
    REAL(kind=DBL), INTENT(IN) ::dummy
    INTEGER :: lengthin,istat
    INQUIRE(iolength=lengthin) dummy
    CALL open_file_in_bin(filename,lengthin,myunit)
    READ(unit=myunit,rec=1,iostat=istat) readvar_0d
    CLOSE(unit=myunit)
    IF ( istat /= 0 ) THEN
       WRITE(stderr,100) filename, istat
100    FORMAT('PROBLEM IN READING INPUT FILE ',A,'. IOSTAT = ',I6)
       !CALL mpi_abort(mpi_comm_world,-1)
    ENDIF
  END FUNCTION readvar_0d

  FUNCTION readvar_1d(filename,dummy,ktype,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: ktype, myunit
    REAL(kind=DBL), DIMENSION(:), INTENT(IN) ::dummy
    INTEGER :: lengthin,istat
    REAL(KIND=DBL), DIMENSION(SIZE(dummy,1)) :: readvar_1d

    INQUIRE(iolength=lengthin) dummy
    CALL open_file_in_bin(filename,lengthin,myunit)
    READ(unit=myunit,rec=1,IOSTAT=istat) readvar_1d
    CLOSE(unit=myunit)
    IF ( istat /= 0) THEN
       WRITE(stderr,100) filename, istat
100    FORMAT('PROBLEM IN READING INPUT FILE ',A,'. IOSTAT = ',I6)
       !CALL mpi_abort(mpi_comm_world,-1)
    ENDIF
  END FUNCTION readvar_1d

  FUNCTION readvar_2d(filename,dummy,ktype,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: ktype, myunit
    REAL(kind=DBL), DIMENSION(:,:), INTENT(IN) ::dummy
    INTEGER :: lengthin,istat
    REAL(KIND=DBL), DIMENSION(SIZE(dummy,1), SIZE(dummy,2)) :: readvar_2d

    INQUIRE(iolength=lengthin) dummy
    CALL open_file_in_bin(filename,lengthin,myunit)
    READ(unit=myunit,rec=1,IOSTAT=istat) readvar_2d
    CLOSE(unit=myunit)
    IF ( istat /= 0) THEN
       WRITE(stderr,100) filename, istat
100    FORMAT('PROBLEM IN READING INPUT FILE ',A,'. IOSTAT = ',I6)
       !CALL mpi_abort(mpi_comm_world,-1)
    ENDIF
  END FUNCTION readvar_2d

  FUNCTION readvar_3d_txt(filename,dummy,ktype,myunit)
    CHARACTER(len=*), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: ktype, myunit
    REAL(kind=DBL), DIMENSION(:,:,:), INTENT(IN) ::dummy
    INTEGER :: lengthin,istat, i, j, k
    REAL(KIND=DBL), DIMENSION(SIZE(dummy,1), SIZE(dummy,2), SIZE(dummy,3)) :: readvar_3d_txt

    INQUIRE(iolength=lengthin) dummy
    CALL open_file_in_txt(filename,lengthin,myunit)
    READ(unit=myunit, IOSTAT=istat, fmt=*) (((readvar_3d_txt(i,j,k),j=1, SIZE(dummy, 2)),i=1, SIZE(dummy, 1)),k=1,SIZE(dummy, 3))
    IF ( istat /= 0) THEN
       WRITE(stderr,100) filename, istat
100    FORMAT('PROBLEM IN READING INPUT FILE ',A,'. IOSTAT = ',I6)
       !CALL mpi_abort(mpi_comm_world,-1)
    ENDIF
  END FUNCTION readvar_3d_txt

  subroutine outputdebug(sizes, in_regular)
    CHARACTER(len=:), ALLOCATABLE :: debugdir
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    CHARACTER(len=20) :: myfmt, myint
    INTEGER :: fileno

    myint='I15'
    myfmt='G16.7E3'
    debugdir='debug/'

    fileno = 0
    CALL writevar(debugdir // 'dimx.dat'                 , sizes%dimx                 , myint , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'dimn.dat'                 , sizes%dimn                 , myint , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'nions.dat'                , sizes%nions                , myint , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'numsols.dat'              , sizes%numsols              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'phys_meth.dat'            , in_regular%phys_meth            , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'coll_flag.dat'            , in_regular%coll_flag            , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'rot_flag.dat'             , in_regular%rot_flag             , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'verbose.dat'              , in_regular%verbose              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'separateflux.dat'         , in_regular%separateflux         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'write_primi.dat'          , in_regular%write_primi          , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'kthetarhos.dat'           , in_regular%kthetarhos           , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'x.dat'                    , in_regular%x                    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'rho.dat'                  , in_regular%rho                  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ro.dat'                   , in_regular%Ro                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Rmin.dat'                 , in_regular%Rmin                 , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Bo.dat'                   , in_regular%Bo                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'q.dat'                    , in_regular%q                    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'smag.dat'                 , in_regular%smag                 , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'alpha.dat'                , in_regular%alpha                , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Machtor.dat'              , in_regular%Machtor              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Autor.dat'                , in_regular%Autor                , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Machpar.dat'              , in_regular%Machpar              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Aupar.dat'                , in_regular%Aupar                , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'gammaE.dat'               , in_regular%gammaE               , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Te.dat'                   , in_regular%Te                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'ne.dat'                   , in_regular%Ne                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ate.dat'                  , in_regular%Ate                  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ane.dat'                  , in_regular%Ane                  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'typee.dat'                , in_regular%el_type              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ai.dat'                   , in_regular%Ai                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Zi.dat'                   , in_regular%Zi                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ti.dat'                   , in_regular%Ti                   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'normni.dat'               , in_regular%normni               , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ati.dat'                  , in_regular%Ati                  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'Ani.dat'                  , in_regular%Ani                  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'typei.dat'                , in_regular%ion_type             , myint , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'maxpts.dat'               , in_regular%maxpts               , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'maxruns.dat'              , in_regular%maxruns              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'relacc1.dat'          , in_regular%relacc1          , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'relacc2.dat'          , in_regular%relacc2          , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'timeout.dat'              , in_regular%timeout              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'ETGmult.dat'              , in_regular%ETGmult              , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'collmult.dat'             , in_regular%collmult             , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'simple_mpi_only.dat'      , in_regular%simple_mpi_only      , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'integration_routine.dat'  , in_regular%integration_routine  , myint , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'absacc2.dat'  , in_regular%absacc2, myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'absacc1.dat' , in_regular%absacc1 , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'rhomin.dat'  , in_regular%rhomin, myfmt , fileno)
    fileno=fileno+1
    CALL writevar(debugdir // 'rhomax.dat' , in_regular%rhomax, myfmt , fileno)
    fileno=fileno+1
  end subroutine outputdebug

  subroutine outputascii(sizes, in_regular, &
                      output_meth_0, &
                      output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                      output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                      primi_meth_0, &
                      output_meth_1, &
                      output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                      output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                      primi_meth_1, &
                      output_meth_2, &
                      output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                      output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                      primi_meth_2)

    CHARACTER(len=20) :: fmtxrow,fmtcftrans
    INTEGER :: fileno
    CHARACTER(len=:), ALLOCATABLE :: primitivedir, outputdir, debugdir

    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular

    type(qlk_output_meth_0)       , intent(in) :: output_meth_0
    type(qlk_output_meth_0_sep_0) , intent(in) :: output_meth_0_sep_0_SI , output_meth_0_sep_0_GB
    type(qlk_output_meth_0_sep_1) , optional, intent(in) :: output_meth_0_sep_1_SI, output_meth_0_sep_1_GB
    type(qlk_output_meth_1)       , optional, intent(in) :: output_meth_1
    type(qlk_output_meth_1_sep_0) , optional, intent(in) :: output_meth_1_sep_0_SI, output_meth_1_sep_0_GB
    type(qlk_output_meth_1_sep_1) , optional, intent(in) :: output_meth_1_sep_1_SI, output_meth_1_sep_1_GB
    type(qlk_output_meth_2)       , optional, intent(in) :: output_meth_2
    type(qlk_output_meth_2_sep_0) , optional, intent(in) :: output_meth_2_sep_0_SI, output_meth_2_sep_0_GB
    type(qlk_output_meth_2_sep_1) , optional, intent(in) :: output_meth_2_sep_1_SI, output_meth_2_sep_1_GB
    type(qlk_primi_meth_0)        , optional, intent(in) :: primi_meth_0
    type(qlk_primi_meth_1)        , optional, intent(in) :: primi_meth_1
    type(qlk_primi_meth_2)        , optional, intent(in) :: primi_meth_2

    WRITE(fmtxrow,'(A,I0,A)') '(', sizes%dimx,'G15.7)'
    WRITE(fmtcftrans,'(A)') '(7G15.7)'

    fileno = 0
    IF (in_regular%write_primi == 1) THEN
      primitivedir='output/primitive/'
      call output_primi_meth_0(sizes, in_regular, primitivedir, primi_meth_0)
      IF (in_regular%phys_meth /= 0) THEN
         call output_primi_meth_1(sizes, in_regular, primitivedir, primi_meth_1)
         IF (in_regular%phys_meth == 2) THEN
           call output_primi_meth_2(sizes, in_regular, primitivedir, primi_meth_2)
         ENDIF
      ENDIF
    ENDIF

    debugdir = 'debug/'
    outputdir = 'output/'

    call output_output_meth_0(sizes, in_regular, outputdir, debugdir, output_meth_0)
    call output_output_meth_0_sep_0(sizes, in_regular, outputdir, output_meth_0_sep_0_GB)
    call output_output_meth_0_sep_0(sizes, in_regular, outputdir, output_meth_0_sep_0_SI)
    IF (in_regular%separateflux==1) THEN
      call output_output_meth_0_sep_1(sizes, in_regular, outputdir, output_meth_0_sep_1_GB)
      call output_output_meth_0_sep_1(sizes, in_regular, outputdir, output_meth_0_sep_1_SI)
    ENDIF


    IF (in_regular%phys_meth /= 0) THEN
       call output_output_meth_1(sizes, in_regular, outputdir, output_meth_1)

      call output_output_meth_1_sep_0(sizes, in_regular, outputdir, output_meth_1_sep_0_GB)
      call output_output_meth_1_sep_0(sizes, in_regular, outputdir, output_meth_1_sep_0_SI)


       IF (in_regular%separateflux == 1) THEN
         call output_output_meth_1_sep_1(sizes, in_regular, outputdir, output_meth_1_sep_1_SI)
         call output_output_meth_1_sep_1(sizes, in_regular, outputdir, output_meth_1_sep_1_GB)
       ENDIF

       IF (in_regular%phys_meth == 2) THEN
          call output_output_meth_2(sizes, in_regular, outputdir, output_meth_2)
          call output_output_meth_2_sep_0(sizes, in_regular, outputdir, output_meth_2_sep_0_SI)
          call output_output_meth_2_sep_0(sizes, in_regular, outputdir, output_meth_2_sep_0_GB)

          IF (in_regular%separateflux == 1) THEN
            call output_output_meth_2_sep_1(sizes, in_regular, outputdir, output_meth_2_sep_1_SI)
            call output_output_meth_2_sep_1(sizes, in_regular, outputdir, output_meth_2_sep_1_GB)
          ENDIF
       ENDIF
    ENDIF

  END SUBROUTINE outputascii

  subroutine output_primi_meth_2(sizes, in_regular, dir, primi_meth_2)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_primi_meth_2), intent(in) :: primi_meth_2

    CHARACTER(len=20) :: myfmt
    integer :: fileno

    fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'Lecircgne.dat' , primi_meth_2%Lecircgne , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepieggne.dat' , primi_meth_2%Lepieggne , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecircgte.dat' , primi_meth_2%Lecircgte , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepieggte.dat' , primi_meth_2%Lepieggte , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecircce.dat'  , primi_meth_2%Lecircce  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepiegce.dat'  , primi_meth_2%Lepiegce  , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir // 'Lecircgni.dat' , primi_meth_2%Lecircgni , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepieggni.dat' , primi_meth_2%Lepieggni , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecircgui.dat' , primi_meth_2%Lecircgui , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepieggui.dat' , primi_meth_2%Lepieggui , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecircgti.dat' , primi_meth_2%Lecircgti , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepieggti.dat' , primi_meth_2%Lepieggti , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecircci.dat'  , primi_meth_2%Lecircci  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepiegci.dat'  , primi_meth_2%Lepiegci  , myfmt , fileno)
    fileno=fileno+1
  end subroutine output_primi_meth_2

  subroutine output_primi_meth_1(sizes, in_regular, dir, primi_meth_1)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_primi_meth_1), intent(in) :: primi_meth_1

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'Lcircgne.dat' , primi_meth_1%Lcircgne , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpieggne.dat' , primi_meth_1%Lpieggne , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcircgte.dat' , primi_meth_1%Lcircgte , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpieggte.dat' , primi_meth_1%Lpieggte , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcircce.dat'  , primi_meth_1%Lcircce  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpiegce.dat'  , primi_meth_1%Lpiegce  , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir // 'Lcircgni.dat' , primi_meth_1%Lcircgni , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpieggni.dat' , primi_meth_1%Lpieggni , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcircgui.dat' , primi_meth_1%Lcircgui , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpieggui.dat' , primi_meth_1%Lpieggui , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcircgti.dat' , primi_meth_1%Lcircgti , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpieggti.dat' , primi_meth_1%Lpieggti , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcircci.dat'  , primi_meth_1%Lcircci  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpiegci.dat'  , primi_meth_1%Lpiegci  , myfmt , fileno)
    fileno=fileno+1

  end subroutine output_primi_meth_1

  subroutine output_primi_meth_0(sizes, in_regular, dir, primi_meth_0)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_primi_meth_0), intent(in) :: primi_meth_0

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'solflu.dat'    , primi_meth_0%solflu    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'kymaxITG.dat'  , primi_meth_0%kymaxITG , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'kymaxETG.dat'  , primi_meth_0%kymaxETG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'distan.dat'    , primi_meth_0%distan    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'kperp2.dat'    , primi_meth_0%kperp2    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'modewidth.dat' , primi_meth_0%modewidth , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'modeshift.dat' , primi_meth_0%modeshift , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'ntor.dat'      , primi_meth_0%ntor      , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'sol.dat'       , primi_meth_0%sol       , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'fdsol.dat'     , primi_meth_0%fdsol     , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcirce.dat'    , primi_meth_0%Lcirce    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpiege.dat'    , primi_meth_0%Lpiege    , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecirce.dat'   , primi_meth_0%Lecirce   , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepiege.dat'   , primi_meth_0%Lepiege   , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir // 'Lcirci.dat'  , primi_meth_0%Lcirci  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpiegi.dat'  , primi_meth_0%Lpiegi  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lcirci.dat'  , primi_meth_0%Lcirci  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lpiegi.dat'  , primi_meth_0%Lpiegi  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lecirci.dat' , primi_meth_0%Lecirci , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lepiegi.dat' , primi_meth_0%Lepiegi , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lvcirci.dat' , primi_meth_0%Lvcirci , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'Lvpiegi.dat' , primi_meth_0%Lvpiegi , myfmt , fileno)
    fileno=fileno+1

  end subroutine output_primi_meth_0

  subroutine output_output_meth_0(sizes, in_regular, dir, dir_debug, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir, dir_debug
    type(qlk_output_meth_0), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'pfe_cm.dat'         , output%pfe_cm         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfi_cm.dat'         , output%pfi_cm         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efe_cm.dat'         , output%efe_cm         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efi_cm.dat'         , output%efi_cm         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vfi_cm.dat'         , output%vfi_cm         , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir // 'ecoefs.dat'         , output%ecoefs         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'npol.dat'           , output%npol           , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'cftrans.dat'        , output%cftrans        , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir_debug // 'phi.dat'      , TRANSPOSE(output%phi) , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir_debug // 'Nustar.dat'   , output%Nustar         , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir_debug // 'Zeff.dat'     , output%Zeff           , myfmt , fileno)
    fileno=fileno+1

  end subroutine output_output_meth_0

  subroutine output_output_meth_0_sep_0(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_0_sep_0), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'gam' // suffix // '.dat', output%gam, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'ome' // suffix // '.dat', output%ome, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfe' // suffix // '.dat', output%pfe, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efe' // suffix // '.dat', output%efe, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efeETG' // suffix // '.dat', output%efeETG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfi' // suffix // '.dat', output%pfi, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efi' // suffix // '.dat', output%efi, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vfi' // suffix // '.dat', output%vfi, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_0_sep_0

  subroutine output_output_meth_0_sep_1(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_0_sep_1), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'efiITG' // suffix // '.dat', output%efiITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efiTEM' // suffix // '.dat', output%efiTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfiITG' // suffix // '.dat', output%pfiITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfiTEM' // suffix // '.dat', output%pfiTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vfiITG' // suffix // '.dat', output%vfiITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vfiTEM' // suffix // '.dat', output%vfiTEM, myfmt, fileno)
    fileno=fileno+1

    CALL writevar(dir // 'efeITG' // suffix // '.dat', output%efeITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'efeTEM' // suffix // '.dat', output%efeTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfeITG' // suffix // '.dat', output%pfeITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'pfeTEM' // suffix // '.dat', output%pfeTEM, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_0_sep_1

  subroutine output_output_meth_1(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_1), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'cke.dat', output%cke, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'cki.dat', output%cki, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_1

  subroutine output_output_meth_1_sep_0(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_1_sep_0), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'dfe' // suffix // '.dat', output%dfe, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vte' // suffix // '.dat', output%vte, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vce' // suffix // '.dat', output%vce, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'dfi' // suffix // '.dat', output%dfi, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vti' // suffix // '.dat', output%vti, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vri' // suffix // '.dat', output%vri, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vci' // suffix // '.dat', output%vci, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_1_sep_0

  subroutine output_output_meth_1_sep_1(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_1_sep_1), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'dfeITG' // suffix // '.dat', output%dfeITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vteITG' // suffix // '.dat', output%vteITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vceITG' // suffix // '.dat', output%vceITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'dfeTEM' // suffix // '.dat', output%dfeTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vteTEM' // suffix // '.dat', output%vteTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vceTEM' // suffix // '.dat', output%vceTEM, myfmt, fileno)
    fileno=fileno+1

    CALL writevar(dir // 'dfiITG' // suffix // '.dat', output%dfiITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vtiITG' // suffix // '.dat', output%vtiITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vciITG' // suffix // '.dat', output%vciITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vriITG' // suffix // '.dat', output%vriITG, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'dfiTEM' // suffix // '.dat', output%dfiTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vtiTEM' // suffix // '.dat', output%vtiTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vciTEM' // suffix // '.dat', output%vciTEM, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vriTEM' // suffix // '.dat', output%vriTEM, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_1_sep_1

  subroutine output_output_meth_2(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_2), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0

    myfmt='G16.7E3'

    CALL writevar(dir // 'ceke.dat', output%ceke, myfmt, fileno)
    fileno=fileno+1
    CALL writevar(dir // 'ceki.dat', output%ceki, myfmt, fileno)
    fileno=fileno+1

  end subroutine output_output_meth_2

  subroutine output_output_meth_2_sep_0(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_2_sep_0), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'vene' // suffix // '.dat'  , output%vene  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'chiee' // suffix // '.dat' , output%chiee , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'vece' // suffix // '.dat'  , output%vece  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veni' // suffix // '.dat'  , output%veni  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veri' // suffix // '.dat'  , output%veri  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'chiei' // suffix // '.dat' , output%chiei , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veci' // suffix // '.dat'  , output%veci  , myfmt , fileno)
    fileno=fileno+1
  end subroutine output_output_meth_2_sep_0

  subroutine output_output_meth_2_sep_1(sizes, in_regular, dir, output)
    type(qlk_sizes), intent(in) :: sizes
    type(qlk_in_regular), intent(in) :: in_regular
    character(len=*), intent(in) :: dir
    type(qlk_output_meth_2_sep_1), intent(in) :: output

    CHARACTER(len=20) :: myfmt
    integer :: fileno = 0
    character(len=3) :: suffix

    if (output%normalization == 0) then
      suffix = '_SI'
    elseif (output%normalization == 1) then
      suffix = '_GB'
    else
      ERRORSTOP(.TRUE., 'Unknown normalization')
    endif

    myfmt='G16.7E3'

    CALL writevar(dir // 'chieiITG' // suffix // '.dat' , output%chieiITG , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veniITG' // suffix // '.dat'  , output%veniITG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veciITG' // suffix // '.dat'  , output%veciITG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veriITG' // suffix // '.dat'  , output%veriITG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'chieiTEM' // suffix // '.dat' , output%chieiTEM , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veniTEM' // suffix // '.dat'  , output%veniTEM  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veciTEM' // suffix // '.dat'  , output%veciTEM  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veriTEM' // suffix // '.dat'  , output%veriTEM  , myfmt , fileno)
    fileno=fileno+1

    CALL writevar(dir // 'chieeETG' // suffix // '.dat' , output%chieeETG , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veneETG' // suffix // '.dat'  , output%veneETG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veceETG' // suffix // '.dat'  , output%veceETG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'chieeTEM' // suffix // '.dat' , output%chieeTEM , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veneTEM' // suffix // '.dat'  , output%veneTEM  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veceTEM' // suffix // '.dat'  , output%veceTEM  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'chieeITG' // suffix // '.dat' , output%chieeITG , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veneITG' // suffix // '.dat'  , output%veneITG  , myfmt , fileno)
    fileno=fileno+1
    CALL writevar(dir // 'veceITG' // suffix // '.dat'  , output%veceITG  , myfmt , fileno)
    fileno=fileno+1
  end subroutine output_output_meth_2_sep_1

  subroutine write_compile_info()

    character(len=:), allocatable :: filename
    character(len=:), allocatable :: debugdir
    integer :: fileno = 0

    debugdir='debug/'
    filename = 'compile_info.csv'

    open(unit=fileno, file=debugdir // filename)
    write(fileno, *) 'QLK_REPOSITORY_ROOT, ', QLK_REPOSITORY_ROOT
    write(fileno, *) 'QLK_GITBRANCHNAME, ', QLK_GITBRANCHNAME
    write(fileno, *) 'QLK_GITSHAKEY, ', QLK_GITSHAKEY
    write(fileno, *) 'QLK_HOSTFULL, ', QLK_HOSTFULL
    write(fileno, *) 'QLK_CLOSEST_RELEASE, ', QLK_CLOSEST_RELEASE
    write(fileno, *) 'QLK_COMPILER_VERSION, ', QLK_COMPILER_VERSION
    close(unit=fileno)
  end subroutine write_compile_info


END MODULE diskio
