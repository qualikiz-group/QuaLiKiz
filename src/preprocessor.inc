#if defined __PGI
#define ERRORSTOP(cond, msg) if(cond) then; write(stderr,*) msg; STOP; endif;
#elif defined __INTEL_COMPILER
#define ERRORSTOP(cond, msg) if(cond) then; write(stderr,*) msg; STOP; endif;
#else
#define ERRORSTOP(cond, msg) if(cond) then; write(stderr,*) msg; ERROR STOP; endif;
#endif
