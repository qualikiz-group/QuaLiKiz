MODULE calltrapQLints
#include "preprocessor.inc"

  USE kind
  USE datcal
  USE datmat
  USE mod_integration
  USE trapints
  USE calltrapints

  IMPLICIT NONE

CONTAINS

  SUBROUTINE trapQLints( p, nu, omega, rotation, fonctpe, fonctpi, fonctpgte, fonctpgti, fonctpgne, &
         fonctpgni, fonctpce, fonctpci, fonctepe, fonctepi, fonctepgte, fonctepgti, fonctepgne, fonctepgni, fonctepce, fonctepci, &
         fonctpgui, fonctepgui)
    !-----------------------------------------------------------
    ! Trapped particle integrals at the omega of the linear solutions
    ! Double integral done for trapped electrons due to collisions
    ! Integrals (with switch) in order to identify particle flux contributions due to An, At and curvature
    ! To save some computational time, only the imaginary component is kept - relevant for transport
    !----------------------------------------------------------- 
    LOGICAL, INTENT(IN) :: rotation
    INTEGER, INTENT(IN)  :: p,nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctpe, fonctpgte, fonctpgne, fonctpce, fonctepe
    COMPLEX(KIND=DBL), INTENT(OUT) :: fonctepgte, fonctepgne, fonctepce
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctpi, fonctpgti, fonctpgni, fonctpci, fonctepi
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctepgti, fonctepgni, fonctepci
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT), OPTIONAL ::  fonctpgui, fonctepgui

    INTEGER, PARAMETER :: lwloc=50000 !default 10000
    INTEGER :: minpts

    REAL(KIND=DBL), DIMENSION(ndim) :: a,b
    REAL(KIND=DBL) :: cc, dd

    REAL(KIND=DBL)    :: rfonctpe, rfonctpgte, rfonctpgne, rfonctpce, rfonctepe
    REAL(KIND=DBL)    :: rfonctepgte, rfonctepgne, rfonctepce
    REAL(KIND=DBL)    :: ifonctpe, ifonctpgte, ifonctpgne, ifonctpce, ifonctepe
    REAL(KIND=DBL)    :: ifonctepgte, ifonctepgne, ifonctepce

    REAL(KIND=DBL), DIMENSION(nions) :: rfonctpi, rfonctpgti, rfonctpgni, rfonctpci, rfonctepi
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctepgti, rfonctepgni, rfonctepci
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctpi, ifonctpgti, ifonctpgni, ifonctpci, ifonctepi
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctepgti, ifonctepgni, ifonctepci
    REAL(KIND=DBL), DIMENSION(nions) :: rfonctpgui, ifonctpgui, rfonctepgui, ifonctepgui

    INTEGER :: dim_out, dim_in, dim_2d_in
    class(integration_options), allocatable :: opts_1d, opts_2d
    REAL(KIND=DBL), DIMENSION(1) :: int_1d_lbound, int_1d_ubound
    REAL(KIND=DBL), DIMENSION(2) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(5) :: fdata

    REAL(KIND=DBL), DIMENSION(nf) :: intout
    INTEGER :: ifailloc
    CHARACTER(len=73) :: msg
    ! iFFeki hcub
    ! FFekirot hcub

    IF (rotation) THEN
      IF (.NOT. present(fonctpgui) .OR. .NOT. present(fonctepgui) ) THEN
        msg = 'Please supply fonctpgui, fonctepgui, if rotation .EQ. .TRUE.'
        ERRORSTOP(.TRUE., msg)
      ENDIF
    ENDIF

    omFFk = omega
    pFFk = p

    !! Integration bounds
    !! a,b are for the 1D ions. 
    a(1) = 0.0d0
    b(1) = 1.0d0 - barelyavoid
    a(2) = 0.0d0
    b(2) = vuplim

    cc = a(1)
    dd = b(1)

    dim_out = 2
    dim_in = 1
    int_1d_lbound = a(1)
    int_1d_ubound = b(1)

    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
      allocate(nag2d_options::opts_2d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
      allocate(hcubature_options::opts_2d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in trapQLints')
    END SELECT

    SELECT TYPE (opts_2d)
    CLASS IS (nag2d_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%lenwrk = lenwrk
    CLASS IS (hcubature_options)
      opts_2d%reqRelError = relacc2
      opts_2d%maxEval = maxpts
      opts_2d%minEval = 0
      opts_2d%norm = 2
      opts_2d%reqAbsError = absacc2
    END SELECT

    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%reqRelError = relaccQL1
      opts_1d%maxEval = lw
    CLASS IS (hcubature_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
      opts_1d%minEval = 0
      opts_1d%norm = 2
      opts_1d%reqAbsError = absacc1
    END SELECT


    dim_2d_in = 2

    fdata(1) = -1 ! caseflag
    fdata(2) = -1 ! ion: Do not care
    fdata(3) = logic2real(rotation)
    fdata(4) = 0 ! Do not sum over ions
    fdata(5) = 1 ! Do not scale integrals for now

    DO ion=1,nions
      ! ION PARTICLE FLUX
      ifailloc = 1
      fdata(2) = ion
      CALL integration(dim_out, calc_QL_FFki, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
      CALL debug_msg_integration('QL FFki', ifailloc, verbose, p, nu, ion, omega)
      rfonctpi(ion) = 0
      ifonctpi(ion) = int_res(2)

      ! ADDITIONAL ION INTEGRALS TO SEPARATE TRANSPORT COMPONENTS
      IF (phys_meth .NE. 0.0) THEN

        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFkgti, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFkgti', ifailloc, verbose, p, nu, ion, omega)
        rfonctpgti(ion) = 0
        ifonctpgti(ion) = int_res(2)

        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFkgni, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFkgni', ifailloc, verbose, p, nu, ion, omega)
        rfonctpgni(ion) = 0
        ifonctpgni(ion) = int_res(2)

        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFkci, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFkci', ifailloc, verbose, p, nu, ion, omega)
        rfonctpci(ion) = 0
        ifonctpci(ion) = int_res(2)

        IF (rotation) THEN
          ifailloc = 1
          CALL integration(dim_out, calc_QL_Fekgti, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL Fekgti', ifailloc, verbose, p, nu, ion, omega)
          rfonctpgui(ion) = 0
          ifonctpgui(ion) = int_res(2)
        ENDIF
        IF (phys_meth == 2) THEN
          IF (ninorm(p,ion) > min_ninorm) THEN
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgti, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, &
                 int_res, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgti', ifailloc, verbose, p, nu, ion, omega)
            ifonctepgti(ion) = int_res(2)

            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgni, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, &
                 int_res, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgni', ifailloc, verbose, p, nu, ion, omega)
            ifonctepgni(ion) = int_res(2)

            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekci, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, &
                 int_res, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekci', ifailloc, verbose, p, nu, ion, omega)
            ifonctepci(ion) = int_res(2)
          ELSE
            ifonctepgti(ion) = 0
            ifonctepgni(ion) = 0
            ifonctepci(ion) = 0
          ENDIF
          rfonctepgti(ion) = 0
          rfonctepgni(ion) = 0
          rfonctepci(ion) = 0

          IF (rotation) THEN
            IF (ninorm(p,ion) > min_ninorm) THEN
              ifailloc = 1
              CALL integration(dim_out, calc_QL_FFekgui, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, &
                   int_res, int_err, fdata, ifailloc)
              CALL debug_msg_integration('QL FFekgui', ifailloc, verbose, p, nu, ion, omega)
              ifonctepgui(ion) = int_res(2)
            ELSE
              ifonctepgui(ion) = 0
            ENDIF
            rfonctepgui(ion) = 0
          ENDIF
        ELSE
          rfonctepgti(ion) = 0
          ifonctepgti(ion) = 0
          rfonctepgni(ion) = 0
          ifonctepgni(ion) = 0
          rfonctepci(ion) = 0
          ifonctepci(ion) = 0
          IF (rotation) THEN
            rfonctepgui(ion) = 0
            ifonctepgui(ion) = 0
          ENDIF
        ENDIF
      ELSE
        rfonctpgti(ion) = 0
        ifonctpgti(ion) = 0
        rfonctpgni(ion) = 0
        ifonctpgni(ion) = 0
        rfonctpci(ion) = 0
        ifonctpci(ion) = 0
        rfonctepgti(ion) = 0
        ifonctepgti(ion) = 0
        rfonctepgni(ion) = 0
        ifonctepgni(ion) = 0
        rfonctepci(ion) = 0
        ifonctepci(ion) = 0
      ENDIF

      ! ION ENERGY FLUX

      ifailloc=1
      IF (ninorm(p,ion) > min_ninorm) THEN
        ifailloc = 1
        ! CALL integration(dim_out, calc_QL_FFeki, dim_in, int_1d_lbound, int_1d_ubound, opts_1d_hard_for_pcub, int_res, int_err, fdata, ifailloc)
        CALL integration(dim_out, calc_QL_FFeki, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFeki', ifailloc, verbose, p, nu, ion, omega)
        ifonctepi(ion) = int_res(2)
      ELSE
        ifonctepi(ion) = 0
      ENDIF
      rfonctepi(ion) = 0

    ENDDO



    ! 2D INTEGRALS FOR ELECTRONS
    fdata(2) = 0
    !Only calculate nonadiabatic part for electrons if el_type == 1
    IF (el_type == 1) THEN 

      IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFke, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFke', ifailloc, verbose, p, nu, ion, omega)

      ELSE ! Collisionless simulation, revert to faster single integral
        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFke, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFke nocoll', ifailloc, verbose, p, nu, ion, omega)
      ENDIF
    ELSE
      intout(1) = 0
      intout(2) = 0
    ENDIF
    rfonctpe = intout(1)
    ifonctpe = intout(2)

    !   !!ADDITIONAL ELECTRON INTEGRALS TO SEPARATE TRANSPORT COMPONENTS
    IF (phys_meth .NE. 0.0) THEN
      !Only calculate nonadiabatic part for electrons if el_type == 1
      IF (el_type == 1) THEN 
        IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkgte, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkgte', ifailloc, verbose, p, nu, ion, omega)

        ELSE ! Collisionless simulation, revert to faster single integral
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkgte, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkgte nocoll', ifailloc, verbose, p, nu, ion, omega)

        ENDIF
      ELSE
        intout(1) = 0
        intout(2) = 0
      ENDIF
      rfonctpgte = intout(1)
      ifonctpgte = intout(2)

      minpts = 0; ifailloc=1;
      !Only calculate nonadiabatic part for electrons if el_type == 1
      IF (el_type == 1) THEN 
        IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkgne, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkgne', ifailloc, verbose, p, nu, ion, omega)
        ELSE ! Collisionless simulation, revert to faster single integral
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkgne, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkgne nocoll', ifailloc, verbose, p, nu, ion, omega)
        ENDIF

      ELSE
        intout(1) = 0
        intout(2) = 0
      ENDIF
      rfonctpgne = intout(1)
      ifonctpgne = intout(2)

      !Only calculate nonadiabatic part for electrons if el_type == 1
      IF (el_type == 1) THEN
        IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral 
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkce, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkce', ifailloc, verbose, p, nu, ion, omega)
        ELSE ! Collisionless simulation, revert to faster single integral
          ifailloc = 1
          CALL integration(dim_out, calc_QL_FFkce, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
          CALL debug_msg_integration('QL FFkce nocoll', ifailloc, verbose, p, nu, ion, omega)
        ENDIF

      ELSE
        intout(1) = 0
        intout(2) = 0
      ENDIF
      rfonctpce = intout(1)
      ifonctpce = intout(2)

      IF (phys_meth == 2) THEN
        minpts = 0; ifailloc=1;
        !Only calculate nonadiabatic part for electrons if el_type == 1
        IF (el_type == 1) THEN 
          IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgte, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgte', ifailloc, verbose, p, nu, ion, omega)
          ELSE ! Collisionless simulation, revert to faster single integral
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgte, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgte nocoll', ifailloc, verbose, p, nu, ion, omega)
          ENDIF
        ELSE
          intout(1) = 0
          intout(2) = 0
        ENDIF
        rfonctepgte = intout(1)
        ifonctepgte = intout(2)

        minpts = 0; ifailloc=1;
        !Only calculate nonadiabatic part for electrons if el_type == 1
        IF (el_type == 1) THEN 
          IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgne, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgne', ifailloc, verbose, p, nu, ion, omega)
          ELSE ! Collisionless simulation, revert to faster single integral
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekgne, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekgne nocoll', ifailloc, verbose, p, nu, ion, omega)
            ifailloc=1
          ENDIF
        ELSE
          intout(1) = 0
          intout(2) = 0
        ENDIF
        rfonctepgne = intout(1)
        ifonctepgne = intout(2)

        !Only calculate nonadiabatic part for electrons if el_type == 1
        IF (el_type == 1) THEN
          IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral 
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekce, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekce', ifailloc, verbose, p, nu, ion, omega)
          ELSE ! Collisionless simulation, revert to faster single integral
            ifailloc = 1
            CALL integration(dim_out, calc_QL_FFekce, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
            CALL debug_msg_integration('QL FFekce nocoll', ifailloc, verbose, p, nu, ion, omega)
          ENDIF
        ELSE
          intout(1) = 0
          intout(2) = 0
        ENDIF
        rfonctepce = intout(1)
        ifonctepce = intout(2)
      ELSE
        rfonctepgte = 0
        ifonctepgte = 0
        rfonctepgne = 0
        ifonctepgne = 0
        rfonctepce = 0
        ifonctepce = 0
      ENDIF
    ELSE
      rfonctpgte = 0
      ifonctpgte = 0
      rfonctpgne = 0
      ifonctpgne = 0
      rfonctpce = 0
      ifonctpce = 0
      rfonctepgte = 0
      ifonctepgte = 0
      rfonctepgne = 0
      ifonctepgne = 0
      rfonctepce = 0
      ifonctepce = 0
    ENDIF

    !ELECTRON ENERGY FLUX

    minpts = 0; ifailloc=1;
    !Only calculate nonadiabatic part for electrons if el_type == 1
    IF (el_type == 1) THEN 
      IF ( ABS(coll_flag) > epsD) THEN ! Collisional simulation, do double integral
        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFeke, dim_2d_in, a, b, opts_2d, intout, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFeke', ifailloc, verbose, p, nu, ion, omega)
      ELSE ! Collisionless simulation, revert to faster single integral
        ifailloc = 1
        CALL integration(dim_out, calc_QL_FFeke, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, intout, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFeke nocoll', ifailloc, verbose, p, nu, ion, omega)
      ENDIF
    ELSE
      intout(1) = 0
      intout(2) = 0
    ENDIF
    rfonctepe = intout(1)
    ifonctepe = intout(2)

    !The complex forms are reconstructed
    fonctpe = rfonctpe + ci * ifonctpe
    fonctpi(:) = rfonctpi(:) + ci * ifonctpi(:)

    fonctpgte = rfonctpgte + ci * ifonctpgte
    fonctpgti(:) = rfonctpgti(:) + ci * ifonctpgti(:)

    fonctpgne = rfonctpgne + ci * ifonctpgne
    fonctpgni(:) = rfonctpgni(:) + ci * ifonctpgni(:)

    fonctpce = rfonctpce + ci * ifonctpce
    fonctpci(:) = rfonctpci(:) + ci * ifonctpci(:)

    fonctepe = rfonctepe + ci * ifonctepe
    fonctepi(:) = rfonctepi(:) + ci * ifonctepi(:)

    fonctepgte = rfonctepgte + ci * ifonctepgte
    fonctepgti(:) = rfonctepgti(:) + ci * ifonctepgti(:)

    fonctepgne = rfonctepgne + ci * ifonctepgne
    fonctepgni(:) = rfonctepgni(:) + ci * ifonctepgni(:)

    fonctepce = rfonctepce + ci * ifonctepce
    fonctepci(:) = rfonctepci(:) + ci * ifonctepci(:)

    IF (rotation) THEN
      fonctpgui(:) = rfonctpgui(:) + ci * ifonctpgui(:)
      fonctepgui(:) = rfonctepgui(:) + ci * ifonctepgui(:)
    ENDIF


  END SUBROUTINE trapQLints

  SUBROUTINE momtrapQLintsrot( p, nu, omega, fonctvpi)
    !-----------------------------------------------------------
    ! Trapped particle integrals at the omega of the linear solutions. ONLY FOR ANGULAR MOMENTUM
    ! Double integral done for trapped electrons due to collisions
    ! Integrals (with switch) in order to identify particle flux contributions due to An, At and curvature
    ! To save some computational time, only the imaginary component is kept - relevant for transport
    !----------------------------------------------------------- 
    INTEGER, INTENT(IN)  :: p,nu
    COMPLEX(KIND=DBL), INTENT(IN)  :: omega
    COMPLEX(KIND=DBL), DIMENSION(:), INTENT(OUT) :: fonctvpi

    INTEGER, PARAMETER :: lwloc=50000 !default 10000

    REAL(KIND=DBL), DIMENSION(ndim) :: a,b

    REAL(KIND=DBL), DIMENSION(nions) :: rfonctvpi
    REAL(KIND=DBL), DIMENSION(nions) :: ifonctvpi

    INTEGER :: ifailloc

    INTEGER :: dim_out, dim_in
  class(integration_options), allocatable :: opts_1d
    REAL(KIND=DBL), DIMENSION(1) :: int_1d_lbound, int_1d_ubound
    REAL(KIND=DBL), DIMENSION(2) :: int_res, int_err
    REAL(KIND=DBL), DIMENSION(5) :: fdata

    LOGICAL :: tiny_rot
    
    omFFk = omega
    pFFk = p

    !! Integration bounds
    !! a,b are for the 1D ions
    a(1) = 0.0d0
    b(1) = 1.0d0 - barelyavoid
    a(2) = 0.0d0
    b(2) = vuplim

    dim_out = 2
    dim_in = 1
    int_1d_lbound = a(1)
    int_1d_ubound = b(1)
    
      
    SELECT CASE (integration_routine)
    CASE (0)
      allocate(nag1d_options::opts_1d)
    CASE (1)
      allocate(hcubature_options::opts_1d)
    CASE DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration routine specified in momtrapQLints')
    END SELECT
    
    SELECT TYPE (opts_1d)
    CLASS IS (nag1d_options)
      opts_1d%reqRelError = relaccQL1
      opts_1d%maxEval = lw
    CLASS IS (hcubature_options)
      opts_1d%reqRelError = relacc1
      opts_1d%maxEval = lw
      opts_1d%minEval = 0
      opts_1d%norm = 2
      opts_1d%reqAbsError = absacc1
    END SELECT


    fdata(1) = -1 ! caseflag
    fdata(2) = -1 ! ion: Do not care
    fdata(3) = logic2real(.TRUE.) ! Rotation
    fdata(4) = logic2real(.FALSE.) ! Sum over ions?
    fdata(5) = 1 ! Do not scale integrals for now

    tiny_rot = (ABS(Machpar(p)) < epsS) .AND. (ABS(Aupar(p)) < epsS) .AND. (ABS(gammaE(p)) < epsS)    
    ! ION ang mom FLUX
    DO ion=1,nions
      fdata(2) = ion
      ifailloc=1
      IF (tiny_rot .OR. (ninorm(p,ion)<min_ninorm)) THEN      
        rfonctvpi(ion) = 0
        ifonctvpi(ion) = 0
      ELSE
        CALL integration(dim_out, calc_QL_FFvki, dim_in, int_1d_lbound, int_1d_ubound, opts_1d, int_res, int_err, fdata, ifailloc)
        CALL debug_msg_integration('QL FFvkirot', ifailloc, verbose, p, nu, ion, omega)
        rfonctvpi(ion) = 0
        ifonctvpi(ion) = int_res(2)
      ENDIF
 
    ENDDO

    !The complex forms are reconstructed

    fonctvpi(:) = rfonctvpi(:) + ci * ifonctvpi(:)


  END SUBROUTINE momtrapQLintsrot


END MODULE calltrapQLints
