PROGRAM qlk_standalone
#include "preprocessor.inc"
  !Standalone driver for qualikiz. Detailed code info in call_qualikiz subroutine header

  !$     use omp_lib
  USE kind
  USE diskio
  USE mpi
  use mod_qualikiz, only: qualikiz

  IMPLICIT NONE

  INTERFACE
    subroutine run_qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        output_meth_1, &
                        output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                        output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                        primi_meth_1, &
                        output_meth_2, &
                        output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                        output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                        primi_meth_2, &
                        in_newt, runcounterin)
      use kind
      IMPLICIT NONE

      type(qlk_sizes), intent(in) :: sizes
      type(qlk_in_regular), intent(in) :: in_regular

      type(qlk_output_meth_0)       , intent(out) :: output_meth_0
      type(qlk_output_meth_0_sep_0) , intent(out) :: output_meth_0_sep_0_SI , output_meth_0_sep_0_GB
      type(qlk_output_meth_0_sep_1) , optional, intent(out) :: output_meth_0_sep_1_SI, output_meth_0_sep_1_GB
      type(qlk_output_meth_1)       , optional, intent(out) :: output_meth_1
      type(qlk_output_meth_1_sep_0) , optional, intent(out) :: output_meth_1_sep_0_SI, output_meth_1_sep_0_GB
      type(qlk_output_meth_1_sep_1) , optional, intent(out) :: output_meth_1_sep_1_SI, output_meth_1_sep_1_GB
      type(qlk_output_meth_2)       , optional, intent(out) :: output_meth_2
      type(qlk_output_meth_2_sep_0) , optional, intent(out) :: output_meth_2_sep_0_SI, output_meth_2_sep_0_GB
      type(qlk_output_meth_2_sep_1) , optional, intent(out) :: output_meth_2_sep_1_SI, output_meth_2_sep_1_GB
      type(qlk_primi_meth_0)        , optional, intent(out) :: primi_meth_0
      type(qlk_primi_meth_1)        , optional, intent(out) :: primi_meth_1
      type(qlk_primi_meth_2)        , optional, intent(out) :: primi_meth_2

      type(qlk_in_newt), optional, intent(in) :: in_newt
      integer, optional, intent(in) :: runcounterin
    end subroutine
  end interface

  !Local data dictionary.

  !Time measuring variables
  REAL(kind=DBL) :: timetot, datainittime
  INTEGER :: time1, time2, time3,time4,freq
  CHARACTER(len=20) :: myfmt, myint

  !MPI variables:
  INTEGER :: ierror, nproc, myrank, provided

  type(qlk_sizes) :: sizes
  type(qlk_in_regular) :: in_regular
  type(qlk_in_newt) :: in_newt

  type(qlk_output_meth_0)       :: output_meth_0
  type(qlk_output_meth_0_sep_0) :: output_meth_0_sep_0_SI, output_meth_0_sep_0_GB
  type(qlk_output_meth_0_sep_1) :: output_meth_0_sep_1_SI, output_meth_0_sep_1_GB
  type(qlk_output_meth_1)       :: output_meth_1
  type(qlk_output_meth_1_sep_0) :: output_meth_1_sep_0_SI, output_meth_1_sep_0_GB
  type(qlk_output_meth_1_sep_1) :: output_meth_1_sep_1_SI, output_meth_1_sep_1_GB
  type(qlk_output_meth_2)       :: output_meth_2
  type(qlk_output_meth_2_sep_0) :: output_meth_2_sep_0_SI, output_meth_2_sep_0_GB
  type(qlk_output_meth_2_sep_1) :: output_meth_2_sep_1_SI, output_meth_2_sep_1_GB
  type(qlk_primi_meth_0)        :: primi_meth_0
  type(qlk_primi_meth_1)        :: primi_meth_1
  type(qlk_primi_meth_2)        :: primi_meth_2

  logical :: oldsol_loaded = .false.


  ! Poloidal asymmetry variables
  ! Ion density along field line, used was asymmetries are present. 
  ! 1st dimension is radius. 2nd is theta (ntheta points from [0,pi]), 3rd is ion
  INTEGER, PARAMETER :: ntheta = 64
  INTEGER, PARAMETER :: numecoefs = 13
  INTEGER, PARAMETER :: numicoefs = 7

  REAL, PARAMETER :: epsD = 1d-14
  INTEGER :: runcounter ! used for counting runs inside integrated modelling applications for deciding to recalculate all or just jump to newton based on old solutions

  LOGICAL :: exist1, exist2, exist3, exist4, exist5 !used for checking for existence of files

  !DEBUGGING
  CHARACTER(len=20) :: fmtn
  INTEGER :: myunit

  ! We init a thread with MPI_INIT_THREAD(REQUIRED, PROVIDED, IERROR)
  !MPI_THREAD_SINGLE
  !    Only one thread will execute.
  !MPI_THREAD_FUNNELED
  !    If the process is multithreaded, only the thread that called MPI_Init_thread will make MPI calls.
  !MPI_THREAD_SERIALIZED
  !    If the process is multithreaded, only one thread will make MPI library calls at one time.
  !MPI_THREAD_MULTIPLE
  !    If the process is multithreaded, multiple threads may call MPI at once with no restrictions.
  CALL mpi_init_thread(MPI_THREAD_FUNNELED,provided,ierror)
  CALL mpi_comm_size(mpi_comm_world,nproc,ierror)
  CALL mpi_comm_rank(mpi_comm_world,myrank,ierror)

  myunit = 700+myrank !set a unique file unit per CPU

  ! Begin time measurement
  CALL SYSTEM_CLOCK(time1)

  ! Read input and initialize all arrays
  CALL data_init(sizes, in_regular, in_newt, oldsol_loaded)

  IF (myrank==0) THEN
     WRITE(stdout,*) ' _________________________________________________________________________________ '
     WRITE(stdout,*) '                                     QUALIKIZ ' // QLK_CLOSEST_RELEASE
     WRITE(stdout,*) '  gyrokinetic calculation of linear growth rates and quasilinear transport fluxes  '
     WRITE(stdout,*) ' _________________________________________________________________________________ '
     WRITE(stdout,*) ' '
     IF (in_regular%verbose >= 1) THEN
       WRITE(stdout, *) 'QUALIKIZ REPOSITORY STATUS DURING COMPILATION'
       WRITE(stdout, *) '---------------------------------------------'
       WRITE(stdout, *) 'From GIT repository:     ' // QLK_REPOSITORY_ROOT
       WRITE(stdout, *) 'On GIT branch:           ' // QLK_GITBRANCHNAME
       WRITE(stdout, *) 'Last commit SHA1-key:    ' // QLK_GITSHAKEY
       WRITE(stdout, *) 'Hostname at compilation: ' // QLK_HOSTFULL
       WRITE(stdout, *) 'Compiler version:        ' // QLK_COMPILER_VERSION
       WRITE(stdout,*) ' '
     ENDIF
     WRITE(stdout,'(A,I10)') ' Amount of parallel processors = ', nproc
     WRITE(stdout,'(A,I10)') ' Number of radial or scan points (dimx) =', sizes%dimx
     WRITE(stdout,'(A,I10)') ' Number of wavenumbers (dimn)  = ', sizes%dimn
     WRITE(stdout,*) ' '

     CALL SYSTEM_CLOCK(time2)
     CALL SYSTEM_CLOCK(count_rate=freq)
     datainittime = REAL(time2-time1) / REAL(freq)
  ENDIF

!!!! CALL THE CALCULATION PHASE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  no_sepflux: IF (in_regular%separateflux == 0) THEN
     has_oldsol: IF (oldsol_loaded) THEN !Call with optional old solution input
        IF (in_regular%phys_meth == 0) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 1) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        output_meth_1=output_meth_1, &
                        output_meth_1_sep_0_SI=output_meth_1_sep_0_SI, output_meth_1_sep_0_GB=output_meth_1_sep_0_GB, &
                        primi_meth_1=primi_meth_1, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 2) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        output_meth_1=output_meth_1, &
                        output_meth_1_sep_0_SI=output_meth_1_sep_0_SI, output_meth_1_sep_0_GB=output_meth_1_sep_0_GB, &
                        primi_meth_1=primi_meth_1, &
                        output_meth_2=output_meth_2, &
                        output_meth_2_sep_0_SI=output_meth_2_sep_0_SI, output_meth_2_sep_0_GB=output_meth_2_sep_0_GB, &
                        primi_meth_2=primi_meth_2, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF


     ELSE has_oldsol !Don't call with optional old solution input
        IF (in_regular%phys_meth == 0) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 1) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        output_meth_1=output_meth_1, &
                        output_meth_1_sep_0_SI=output_meth_1_sep_0_SI, output_meth_1_sep_0_GB=output_meth_1_sep_0_GB, &
                        primi_meth_1=primi_meth_1, &
                        runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 2) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        primi_meth_0=primi_meth_0, &
                        output_meth_1=output_meth_1, &
                        output_meth_1_sep_0_SI=output_meth_1_sep_0_SI, output_meth_1_sep_0_GB=output_meth_1_sep_0_GB, &
                        primi_meth_1=primi_meth_1, &
                        output_meth_2=output_meth_2, &
                        output_meth_2_sep_0_SI=output_meth_2_sep_0_SI, output_meth_2_sep_0_GB=output_meth_2_sep_0_GB, &
                        primi_meth_2=primi_meth_2, &
                        runcounterin=runcounter)
        ENDIF
     ENDIF has_oldsol
  ELSE no_sepflux !with separateflux==1
     has_oldsol2: IF (oldsol_loaded) THEN !Call with optional old solution input
        IF (in_regular%phys_meth == 0) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 1) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        output_meth_1, &
                        output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                        output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                        primi_meth_1, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 2) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        output_meth_1, &
                        output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                        output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                        primi_meth_1, &
                        output_meth_2, &
                        output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                        output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                        primi_meth_2, &
                        in_newt=in_newt, runcounterin=runcounter)
        ENDIF

     ELSE has_oldsol2 !Don't call with optional old solution input
        IF (in_regular%phys_meth == 0) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 1) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        output_meth_1, &
                        output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                        output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                        primi_meth_1, &
                        runcounterin=runcounter)
        ENDIF

        IF (in_regular%phys_meth == 2) THEN
          call qualikiz(sizes, in_regular, &
                        output_meth_0, &
                        output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                        output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                        primi_meth_0, &
                        output_meth_1, &
                        output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                        output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                        primi_meth_1, &
                        output_meth_2, &
                        output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                        output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                        primi_meth_2, &
                        runcounterin=runcounter)
        ENDIF
     ENDIF has_oldsol2

  ENDIF no_sepflux
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  IF (myrank == 0) CALL SYSTEM_CLOCK(time3)
  call outputascii(sizes, in_regular, &
                      output_meth_0, &
                      output_meth_0_sep_0_SI, output_meth_0_sep_0_GB, &
                      output_meth_0_sep_1_SI, output_meth_0_sep_1_GB, &
                      primi_meth_0, &
                      output_meth_1, &
                      output_meth_1_sep_0_SI, output_meth_1_sep_0_GB, &
                      output_meth_1_sep_1_SI, output_meth_1_sep_1_GB, &
                      primi_meth_1, &
                      output_meth_2, &
                      output_meth_2_sep_0_SI, output_meth_2_sep_0_GB, &
                      output_meth_2_sep_1_SI, output_meth_2_sep_1_GB, &
                      primi_meth_2)

  IF (myrank == 0) THEN
     CALL SYSTEM_CLOCK(time4)
     CALL SYSTEM_CLOCK(count_rate=freq)
     timetot = REAL(time4-time3) / REAL(freq)
     WRITE(stdout,"(A,F11.3,A)") 'Profiling: input read and data initialization time = ',datainittime,' s'  
     WRITE(stdout,"(A,F11.3,A)") 'Profiling: output write time = ',timetot,' s'  
  ENDIF

  CALL MPI_Barrier(mpi_comm_world,ierror)

  !WRITE(stdout,*) 'Z function was called ',weidcount,' times' 
  !WRITE(stdout,*)
  !WRITE(stdout,"(A,I0,A,I0)") '*** time: ',timetot, 'seconds, for rank = ',myrank 
  !WRITE(stdout,"(A,I0)") '*** End of job for rank ',myrank
  !IF (myrank==0) WRITE(stdout,"(A,I0,A)") 'We have missed ',Nsolrat,' eigenvalues.'

  !Deallocating all
  !CALL deallocate_all()

  IF (myrank==0) THEN 
     CALL SYSTEM_CLOCK(time2)
     CALL SYSTEM_CLOCK(count_rate=freq)
     timetot = REAL(time2-time1) / REAL(freq)
     WRITE(stdout,"(A,F11.3,A)") 'Profiling: Hurrah! Job completed! Total time = ',timetot,' s'  !final write
     WRITE(stdout,*)
     !Write time of run to disk
     OPEN(unit=900, file="lastruntime.dat", action="write", status="replace")
     WRITE(900,"(A,F11.3,A)") 'Last completed run time = ',timetot,' s'  !final write
     CLOSE(900)
  ENDIF

  ! MPI finalization
  CALL mpi_finalize(ierror)

CONTAINS 

  SUBROUTINE data_init(sizes, in_regular, in_newt, oldsol_loaded)
    use mod_make_io, only: allocate_qlk_in_regular, allocate_qlk_in_newt, zeroout_qlk_in_regular, zeroout_qlk_in_newt
    !Parallel read data, allocate input and output arrays

    type(qlk_sizes), intent(out) :: sizes
    type(qlk_in_regular), intent(out) :: in_regular
    type(qlk_in_newt), intent(out) :: in_newt
    logical, intent(out) :: oldsol_loaded
    INTEGER, PARAMETER :: ktype = 1 ! BINARY FILES
    INTEGER :: fileno,ierr
    REAL(kind=DBL) :: dummy !dummy variable for obtaining input. Must be real for readvar

    REAL(kind=DBL), DIMENSION(:), ALLOCATABLE :: dummyn
    REAL(kind=DBL), DIMENSION(:), ALLOCATABLE :: dummyx
    REAL(kind=DBL), DIMENSION(:,:), ALLOCATABLE :: dummyxnions
    REAL(kind=DBL), DIMENSION(:,:,:), ALLOCATABLE :: dummyxnnumsol

    REAL(kind=DBL), DIMENSION(:,:,:), ALLOCATABLE :: oldisol, oldifdsol
    REAL(kind=DBL), DIMENSION(:,:,:), ALLOCATABLE :: oldrsol, oldrfdsol

    CHARACTER(len=:), ALLOCATABLE :: debugdir, inputdir, primitivedir
    ! READING INPUT ARRAYS FROM BINARY FILES

    inputdir = 'input/'

    fileno = 0

    ! p{1} Size of radial or scan arrays
    sizes%dimx = 0
    IF (myrank == mod(fileno, nproc)) sizes%dimx = INT(readvar(inputdir // 'dimx.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{2} Size of wavenumber arrays
    sizes%dimn = 0
    IF (myrank == mod(fileno, nproc)) sizes%dimn = INT(readvar(inputdir // 'dimn.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{3} Number of ions in system
    sizes%nions = 0
    IF (myrank == mod(fileno, nproc)) sizes%nions = INT(readvar(inputdir // 'nions.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{9} Number of total saught after solutions
    sizes%numsols = 0
    IF (myrank == mod(fileno, nproc)) sizes%numsols = INT(readvar(inputdir // 'numsols.bin', dummy, ktype, myunit))
    fileno=fileno+1


    fileno=0
    call MPI_Bcast(sizes%dimx, 1, MPI_INTEGER, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
    call MPI_Bcast(sizes%dimn, 1, MPI_INTEGER, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
    call MPI_Bcast(sizes%nions, 1, MPI_INTEGER, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
    call MPI_Bcast(sizes%numsols, 1, MPI_INTEGER, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
    !print *, 'rank ', myrank, sizes%dimx, sizes%dimn, sizes%nions, sizes%numsols, in_regular%maxruns

    call allocate_qlk_in_regular(sizes%dimx, sizes%dimn, sizes%nions, in_regular)
    call zeroout_qlk_in_regular(in_regular)
    allocate(dummyn(sizes%dimn))
    allocate(dummyx(sizes%dimx))
    allocate(dummyxnions(sizes%dimx, sizes%nions))
    allocate(dummyxnnumsol(sizes%dimx, sizes%dimn, sizes%numsols))

    fileno=0 ! Reset the counter, new parallel part!

    ! p{12} Number of runs before runcounter resets
    IF (myrank == mod(fileno, nproc)) in_regular%maxruns = INT(readvar(inputdir // 'maxruns.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{4} Flag for calculating decomposition of particle and heat transport into diffusive and convective components
    IF (myrank == mod(fileno, nproc)) in_regular%phys_meth = INT(readvar(inputdir // 'phys_meth.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{5} Flag for including collisions
    IF (myrank == mod(fileno, nproc)) in_regular%coll_flag = INT(readvar(inputdir // 'coll_flag.bin', dummy, ktype, myunit))
    fileno=fileno+1

    IF (myrank == mod(fileno, nproc)) in_regular%write_primi = INT(readvar(inputdir // 'write_primi.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{6} Flag for including rotation
    IF (myrank == mod(fileno, nproc)) in_regular%rot_flag = INT(readvar(inputdir // 'rot_flag.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{7} Flag for verbose output
    IF (myrank == mod(fileno, nproc)) in_regular%verbose = INT(readvar(inputdir // 'verbose.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{8} Flag for separate mode flux output
    IF (myrank == mod(fileno, nproc)) in_regular%separateflux = INT(readvar(inputdir // 'separateflux.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{10} 1D integral accuracy of NAG routines
    IF (myrank == mod(fileno, nproc)) in_regular%relacc1 = readvar(inputdir // 'relacc1.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! p{11} 2D integral accuracy of NAG routines
    IF (myrank == mod(fileno, nproc)) in_regular%relacc2 = readvar(inputdir // 'relacc2.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! p{13} Maximum number of integrand evaluations in 2D integration routine
    IF (myrank == mod(fileno, nproc)) in_regular%maxpts = INT(readvar(inputdir // 'maxpts.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{14} Timeout seconds for a given solution search
    IF (myrank == mod(fileno, nproc)) in_regular%timeout = readvar(inputdir // 'timeout.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! p{15} Multiplier for ETG saturation rule (default 1. Mostly for testing)
    IF (myrank == mod(fileno, nproc)) in_regular%ETGmult = readvar(inputdir // 'ETGmult.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! p{16} Multiplier for collisionality (default 1. Mostly for testing)
    IF (myrank == mod(fileno, nproc)) in_regular%collmult = readvar(inputdir // 'collmult.bin', dummy, ktype, myunit)
    fileno=fileno+1

    IF (myrank == mod(fileno, nproc)) in_regular%simple_mpi_only = INT(readvar(inputdir // 'simple_mpi_only.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{18} Toroidal wave-number grid
    IF (myrank == mod(fileno, nproc)) in_regular%kthetarhos = readvar(inputdir // 'kthetarhos.bin', dummyn, ktype, myunit)
    fileno=fileno+1

    ! p{19} Normalised radial coordinate (midplane radius)
    IF (myrank == mod(fileno, nproc)) in_regular%x = readvar(inputdir // 'x.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{20} Normalised radial coordinate (midplane radius)
    IF (myrank == mod(fileno, nproc)) in_regular%rho = readvar(inputdir // 'rho.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{21} <Ro> major radius
    IF (myrank == mod(fileno, nproc)) in_regular%Ro = readvar(inputdir // 'Ro.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{22} <a> minor radius
    IF (myrank == mod(fileno, nproc)) in_regular%Rmin = readvar(inputdir // 'Rmin.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{23} B(rho) magnetic field
    IF (myrank == mod(fileno, nproc)) in_regular%Bo = readvar(inputdir // 'Bo.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{24} q(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%q = readvar(inputdir // 'q.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{25} s(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%smag = readvar(inputdir // 'smag.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{26} alpha(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%alpha = readvar(inputdir // 'alpha.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{27} Machtor(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%Machtor = readvar(inputdir // 'Machtor.bin', dummyx, ktype, myunit)
    fileno=fileno+1

!!$    WHERE(ABS(Machtor) < epsD) Machtor = epsD

    ! p{28} Autor(rho) profile
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%Autor = readvar(inputdir // 'Autor.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%Autor) < epsD) in_regular%Autor = epsD
    ENDIF
    fileno=fileno+1

    ! p{29} Machpar(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%Machpar = readvar(inputdir // 'Machpar.bin', dummyx, ktype, myunit)
    fileno=fileno+1
!!$    WHERE(ABS(Machpar) < epsD) Machpar = epsD

    ! p{30} Aupar(rho) profile
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%Aupar = readvar(inputdir // 'Aupar.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%Aupar) < epsD) in_regular%Aupar = epsD
    ENDIF
    fileno=fileno+1

    ! p{31} gammaE(rho) profile
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%gammaE = readvar(inputdir // 'gammaE.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%gammaE) < epsD) in_regular%gammaE = epsD
    ENDIF
    fileno=fileno+1

    ! p{32} Te(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%Te = readvar(inputdir // 'Te.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{33} ne(rho) profile
    IF (myrank == mod(fileno, nproc)) in_regular%Ne = readvar(inputdir // 'ne.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{34} R/LTe(rho) profile
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%Ate = readvar(inputdir // 'Ate.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%Ate) < epsD) in_regular%Ate = epsD
    ENDIF
    fileno=fileno+1

    ! p{35} R/Lne(rho) profile
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%Ane = readvar(inputdir // 'Ane.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%Ane) < epsD) in_regular%Ane = epsD
       ! TODO: check, this process does not nessecaraly have Ate!
       WHERE(in_regular%Ane + in_regular%Ate < epsD) in_regular%Ane = in_regular%Ane + epsD
    ENDIF
    fileno=fileno+1

    ! p{36} Flag for adiabatic electrons
    IF (myrank == mod(fileno, nproc)) in_regular%el_type = INT(readvar(inputdir // 'typee.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! p{37} Species temp anisotropy at LFS. Zero is electrons
    IF (myrank == mod(fileno, nproc)) in_regular%anise = readvar(inputdir // 'anise.bin', dummyx, ktype, myunit)
    fileno=fileno+1

    ! p{38} Species temp anisotropy at LFS. Zero is electrons
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%danisedr = readvar(inputdir // 'danisdre.bin', dummyx, ktype, myunit)
       WHERE(ABS(in_regular%danisedr) < epsD) in_regular%danisedr = epsD
    ENDIF
    fileno=fileno+1

    ! p{39} Ti(rho) profiles
    IF (myrank == mod(fileno, nproc)) in_regular%Ti = readvar(inputdir // 'Ti.bin', dummyxnions, ktype, myunit)
    fileno=fileno+1

    ! p{40} ni/ne (rho) profiles
    IF (myrank == mod(fileno, nproc)) in_regular%normni = readvar(inputdir // 'normni.bin', dummyxnions, ktype, myunit)
    fileno=fileno+1

    ! p{41} R/LTi(rho) profiles
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%Ati = readvar(inputdir // 'Ati.bin', dummyxnions, ktype, myunit)
       WHERE(ABS(in_regular%Ati) < epsD) in_regular%Ati = epsD
    ENDIF
    fileno=fileno+1

    ! p{42} R/Lni(rho) profiles
    IF (myrank == mod(fileno, nproc)) THEN 
       in_regular%Ani = readvar(inputdir // 'Ani.bin', dummyxnions, ktype, myunit)
       WHERE(ABS(in_regular%Ani) < epsD) in_regular%Ani = epsD
       ! TODO: check, this process does not nessecaraly have Ati!
       WHERE(in_regular%Ani + in_regular%Ati < epsD) in_regular%Ani = in_regular%Ani + epsD
    ENDIF
    fileno=fileno+1

    ! p{43} Ion types
    IF (myrank == mod(fileno, nproc)) in_regular%ion_type = INT(readvar(inputdir // 'typei.bin', dummyxnions, ktype, myunit))
    fileno=fileno+1

    ! p{44} Species temp anisotropy at LFS. Zero is electrons
    IF (myrank == mod(fileno, nproc)) in_regular%anis = readvar(inputdir // 'anisi.bin', dummyxnions, ktype, myunit)
    fileno=fileno+1

    ! p{45} Species temp anisotropy at LFS. Zero is electrons
    IF (myrank == mod(fileno, nproc)) THEN
       in_regular%danisdr = readvar(inputdir // 'danisdri.bin', dummyxnions, ktype, myunit)
       WHERE(ABS(in_regular%danisdr) < epsD) in_regular%danisdr = epsD
    ENDIF
    fileno=fileno+1

    ! p{46} Main ion mass
    IF (myrank == mod(fileno, nproc)) in_regular%Ai = readvar(inputdir // 'Ai.bin', dummyxnions, ktype, myunit)
    fileno=fileno+1

    ! p{47} Main ion charge
    IF (myrank == mod(fileno, nproc)) in_regular%Zi = readvar(inputdir // 'Zi.bin', dummyxnions, ktype, myunit)
    fileno=fileno+1

    ! Style of integration routine to use. 0: NAG, 1: cubature
    IF (myrank == mod(fileno, nproc)) in_regular%integration_routine = INT(readvar(inputdir // 'integration_routine.bin', dummy, ktype, myunit))
    fileno=fileno+1

    ! Low accuracy absolute accuracy of cubature integration
    IF (myrank == mod(fileno, nproc)) in_regular%absacc2 = readvar(inputdir // 'absacc2.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! High accuracy absolute accuracy of cubature integration
    IF (myrank == mod(fileno, nproc)) in_regular%absacc1 = readvar(inputdir // 'absacc1.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! Min of calculation depending on rho
    IF (myrank == mod(fileno, nproc)) in_regular%rhomin = readvar(inputdir // 'rhomin.bin', dummy, ktype, myunit)
    fileno=fileno+1

    ! Max of calculation depending on rho
    IF (myrank == mod(fileno, nproc)) in_regular%rhomax = readvar(inputdir // 'rhomax.bin', dummy, ktype, myunit)
    fileno=fileno+1

    runcounter = 9999999
    IF (myrank == mod(fileno, nproc)) THEN
       INQUIRE(file="runcounter.dat", EXIST=exist1)
       IF (exist1) THEN
          OPEN(myunit, file="runcounter.dat", status="old", action="read")
          READ(myunit,*) runcounter;  CLOSE(myunit)
       ELSE
          runcounter = 0 !First run
       END IF
    ENDIF
    fileno=fileno+1

    WRITE(fmtn,'(A,I0, A)') '(', sizes%dimn,'G16.7)'

    !Now do MPIAllReduce to inputs. We need runcounter for potential next stage, so
    !the reduce operations are split
    fileno = 0
    call MPI_Bcast(in_regular%maxruns, 1, MPI_INTEGER, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%phys_meth            , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%coll_flag            , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%write_primi          , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%rot_flag             , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%verbose              , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%separateflux         , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%relacc1              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%relacc2              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%maxpts               , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1

    CALL MPI_Bcast(in_regular%timeout              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%ETGmult              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%collmult             , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%simple_mpi_only      , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%kthetarhos           , sizes%dimn       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%x                    , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%rho                  , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ro                   , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Rmin                 , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Bo                   , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%q                    , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%smag                 , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%alpha                , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Machtor              , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Autor                , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Machpar              , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Aupar                , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%gammaE               , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Te                   , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ne                   , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ate                  , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ane                  , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%el_type              , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%anise                , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%danisedr             , sizes%dimx       , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ti                   , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%normni               , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ati                  , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ani                  , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%ion_type             , sizes%dimx*sizes%nions , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%anis                 , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%danisdr              , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Ai                   , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%Zi                   , sizes%dimx*sizes%nions , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%integration_routine  , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%absacc2              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%absacc1              , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%rhomin               , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(in_regular%rhomax               , 1          , MPI_DOUBLE_PRECISION ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1
    CALL MPI_Bcast(runcounter                      , 1          , MPI_INTEGER          ,  mod(fileno, nproc), mpi_comm_world , ierr); fileno=fileno+1

    IF (runcounter >= in_regular%maxruns) THEN !Reset if we're at our maximum number of runs
       WRITE(stdout,'(A,I2,A,I2,A)') 'Maxruns reached with runcounter=', runcounter, ', in_regular%maxruns=', in_regular%maxruns, ' resetting to 0'
       runcounter = 0 !First run
    ENDIF

    IF (runcounter /= 0) THEN
       ! Read and write runcounter input to decide course of action in calcroutines (full solution or start from previous solution)
       INQUIRE(file="output/primitive/rsol.dat", EXIST=exist2)
       INQUIRE(file="output/primitive/isol.dat", EXIST=exist3)
       INQUIRE(file="output/primitive/rfdsol.dat", EXIST=exist4)
       INQUIRE(file="output/primitive/ifdsol.dat", EXIST=exist5)
       IF (.NOT. (exist2 .AND. exist3 .AND. exist4 .AND. exist5)) THEN
          WRITE(stdout,'(A)') 'Warning! runcounter /= 0 but not all old solutions exists. Resetting runcounter!'
          runcounter = 0
       ENDIF
    ENDIF

    IF (runcounter /= 0) THEN !load old rsol and isol if we're not doing a reset run
       ALLOCATE( oldrsol(  sizes%dimx , sizes%dimn , sizes%numsols) ); oldrsol = 0
       ALLOCATE( oldisol(  sizes%dimx , sizes%dimn , sizes%numsols) ); oldisol = 0
       ALLOCATE( oldrfdsol(sizes%dimx , sizes%dimn , sizes%numsols) ) ; oldrfdsol = 0
       ALLOCATE( oldifdsol(sizes%dimx , sizes%dimn , sizes%numsols) ); oldifdsol = 0
       primitivedir = 'output/primitive/'
       myfmt = 'G16.7E3'
       fileno = 0
       IF (myrank == fileno) THEN
           oldrsol = readvar(primitivedir // 'rsol.dat', dummyxnnumsol, ktype, myunit)
           CALL writevar(primitivedir // 'rsol_old.dat', oldrsol, myfmt, myunit)
       ENDIF
       fileno=fileno+1; IF (fileno==nproc) fileno=0 

       IF (myrank == fileno) THEN
           oldisol = readvar(primitivedir // 'isol.dat', dummyxnnumsol, ktype, myunit)
       ENDIF
       fileno=fileno+1; IF (fileno==nproc) fileno=0 

       IF (myrank == fileno) THEN
           oldrfdsol = readvar(primitivedir // 'rfdsol.dat', dummyxnnumsol, ktype, myunit)
       ENDIF
       fileno=fileno+1; IF (fileno==nproc) fileno=0 

       IF (myrank == fileno) THEN
           oldifdsol = readvar(primitivedir // 'ifdsol.dat', dummyxnnumsol, ktype, myunit)
       ENDIF
       fileno=fileno+1; IF (fileno==nproc) fileno=0 

       fileno = 0
       CALL MPI_Bcast(oldrsol, sizes%dimx*sizes%dimn*sizes%numsols,   MPI_DOUBLE_PRECISION, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
       CALL MPI_Bcast(oldisol, sizes%dimx*sizes%dimn*sizes%numsols,   MPI_DOUBLE_PRECISION, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
       CALL MPI_Bcast(oldrfdsol, sizes%dimx*sizes%dimn*sizes%numsols, MPI_DOUBLE_PRECISION, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1
       CALL MPI_Bcast(oldifdsol, sizes%dimx*sizes%dimn*sizes%numsols, MPI_DOUBLE_PRECISION, mod(fileno, nproc), mpi_comm_world, ierr); fileno=fileno+1

       call allocate_qlk_in_newt(sizes%dimx, sizes%dimn, sizes%numsols, in_newt)
       call zeroout_qlk_in_newt(in_newt)

       in_newt%oldsol = CMPLX(oldrsol,oldisol)
       in_newt%oldfdsol = CMPLX(oldrfdsol,oldifdsol)

       DEALLOCATE( oldrsol )
       DEALLOCATE( oldisol )
       DEALLOCATE( oldrfdsol )
       DEALLOCATE( oldifdsol )

       oldsol_loaded = .true.
     ELSE
       oldsol_loaded = .false.
    ENDIF

    IF (myrank == 0) THEN
       OPEN(unit=700, file="runcounter.dat", status="replace", action="write") !Replace old runcounter with new runcounter
       WRITE(700,*) runcounter + 1 ; CLOSE(700)
    ENDIF

    !DEBUGGING WRITE OUT INPUT TO ASCII FILE
    call outputdebug(sizes, in_regular)
    call write_compile_info()
    fileno=fileno+1; IF (fileno==nproc) fileno=0
    CALL MPI_Barrier(mpi_comm_world,ierror)
    ERRORSTOP(ierror /= 0, "MPI Barrier Failed")


  END SUBROUTINE data_init

END PROGRAM qlk_standalone
