MODULE mod_integration
#include "preprocessor.inc"
  USE kind
  USE hcub
  USE datmat, ONLY: global_verbosity => verbose

  IMPLICIT NONE

  TYPE integration_options
    INTEGER :: maxEval
    REAL(KIND=DBL) :: reqRelError
  END TYPE integration_options

  TYPE, EXTENDS(integration_options) :: cubature_options
    INTEGER :: norm
    INTEGER :: minEval
    REAL(KIND=DBL) :: reqAbsError
  END TYPE cubature_options

  TYPE, EXTENDS(cubature_options) :: hcubature_options
  END TYPE hcubature_options

  TYPE, EXTENDS(cubature_options) :: pcubature_options
  END TYPE pcubature_options

  TYPE, EXTENDS(integration_options) :: nag1d_options
  END TYPE nag1d_options

  TYPE, EXTENDS(integration_options) :: nag2d_options
    INTEGER :: minEval
    INTEGER :: lenwrk
  END TYPE nag2d_options

CONTAINS

  SUBROUTINE integration(dim_out, f, dim_in, int_lbound, int_ubound, opts, result, result_err, f_data, ifail)
    !! General interface for integrating arbitrary integrants with predefined integration routines.
    !! Accepts multivariable vector functions
    !! $$\int\cdots\int_\boldsymbol{D} \vec{f}(x_1, x_2, \ldots, x_n) dx_1 \cdots dx_n$$
    USE datcal, ONLY: general_integrand, logic2real
    PROCEDURE(general_integrand) :: f
    !! Integrant \(\vec{f}\) to be integrated
    INTEGER, INTENT(IN) :: dim_in
    !! Input dimensionality of \(\vec{f}\)
    INTEGER, INTENT(IN) :: dim_out
    !! Output dimensionality of \(\vec{f}\)
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN), CONTIGUOUS :: int_ubound
    !! Upper bound of the integration interval
    REAL(KIND=DBL), DIMENSION(:), INTENT(IN), CONTIGUOUS :: int_lbound
    !! Lower bound of the integration interval
    CLASS(integration_options), INTENT(IN) :: opts
    !! Options specific to the chosen integration routine. Type of opts determines which internal routine is called
    INTEGER, INTENT(INOUT) :: ifail
    !! Failure flag. 0 if no errors occurred during integration
    REAL(KIND=DBL), DIMENSION(:), INTENT(INOUT) :: f_data
    !! Array to pass arbitrary data to be used inside the integrant function
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT), CONTIGUOUS :: result
    !! Calculated integral result
    REAL(KIND=DBL), DIMENSION(:), INTENT(OUT), CONTIGUOUS :: result_err
    !! Estimation of the precision of the integral result. Interpretation of this value depends on the chosen integration routine
    INTEGER :: evals, ifail_cub
#ifdef HAVE_NAG
    EXTERNAL :: d01ahf
    REAL(KIND=DBL) :: d01ahf
    REAL(KIND=DBL) :: a, b
    REAL(KIND=DBL) :: result_1d, error_1d
    INTEGER :: ifail_nag, num_out_dim
    INTEGER, DIMENSION(:), ALLOCATABLE :: ifails
    REAL(KIND=DBL), DIMENSION(:), ALLOCATABLE :: wrkstr
#endif
    CHARACTER(len=100) :: msg

    msg = 'integration lower bound shape different from input shape'
    ERRORSTOP(SIZE(int_lbound) /= dim_in, msg)
    msg = 'integration upper bound shape different from input shape'
    ERRORSTOP(SIZE(int_ubound) /= dim_in, msg)
    SELECT TYPE (opts)
    CLASS IS (hcubature_options)
      ifail_cub = 0
      evals = opts%minEval
      CALL hcubature(dim_out, f, dim_in, int_lbound, int_ubound, evals, opts%maxEval, opts%reqAbsError, opts%reqRelError, opts%norm, result, result_err, f_data, ifail_cub)
      if (ifail_cub /= 0) then
        WRITE(msg,'(A,I0,A)') 'WARNING: hcubature ifail_cub = ', ifail_cub, '. Internal failure. Consider increasing maxpts if this occurs often'
        if (ifail /= 0) then
          WRITE(stderr,'(A,I0,A)') msg
        endif
      endif
      ifail = ifail_cub
    CLASS IS (pcubature_options)
      ERRORSTOP(.TRUE., 'pcub purged from QuaLiKiz after v2.7.0')
    CLASS IS (nag1d_options)
#ifdef HAVE_NAG
      a = int_lbound(1)
      b = int_ubound(1)
      ERRORSTOP(dim_in > 1, 'nag1d_options integation only accepts 1D input')
      ALLOCATE(ifails(dim_out))
      DO num_out_dim = 1, dim_out
        ifail_nag = ifail
        evals = -1
        error_1d = -1
        result_1d = d01ahf(a, b, opts%reqRelError, evals, error_1d, nag_1d_f, opts%maxEval, ifail_nag)
        ERRORSTOP(ifail_nag == -399, 'NAG license error! Exiting')
        ERRORSTOP(ifail_nag == 3, 'NAG d01ahf invalid accuracy requested!')
        result(num_out_dim) = result_1d
        result_err(num_out_dim) = error_1d
        ifails(num_out_dim) = ifail_nag
      ENDDO
      IF (ALL(ifails == 0)) ifail = 0
#else
      ERRORSTOP(.TRUE., 'Trying to call NAG routines, but compiled without NAG. Add -DHAVE_NAG and rebuild mod_integration')
#endif
    CLASS IS (nag2d_options)
#ifdef HAVE_NAG
      ALLOCATE(wrkstr(opts%lenwrk))
      ALLOCATE(ifails(dim_out))
      DO num_out_dim = 1, dim_out
        ifail_nag = ifail
        evals = opts%minEval
        error_1d = -1
        CALL d01fcf(dim_in, int_lbound, int_ubound, evals, opts%maxEval, nag_nd_f, opts%reqRelError, error_1d, &
             opts%lenwrk, wrkstr, result_1d, ifail_nag)
        ERRORSTOP(ifail_nag == -399, 'NAG license error! Exiting')
        ERRORSTOP(ifail_nag == 1, 'NAG d01fcf input error!')
        IF(ifail_nag == 3 .and. logic2real(global_verbosity) >= 1) then
          WRITE(stderr,'(A,I0,A)') 'd01fcf ifail_nag = ', ifail_nag, ', increase wrkstr lenght'
        ENDIF
        result(num_out_dim) = result_1d
        result_err(num_out_dim) = error_1d
        ifails(num_out_dim) = ifail_nag
      ENDDO
      DEALLOCATE(wrkstr)
      IF (ALL(ifails == 0)) ifail = 0
#else
      ERRORSTOP(.TRUE., 'Trying to call NAG routines, but compiled without NAG. Add -DHAVE_NAG and rebuild mod_integration')
#endif
    CLASS DEFAULT
      ERRORSTOP(.TRUE., 'Unknown integration setting class passed')
    END SELECT
#ifdef HAVE_NAG
  CONTAINS
    REAL(KIND=DBL) FUNCTION nag_1d_f(f_in_1d) RESULT(f_out_1d)
      USE kind
      REAL(KIND=DBL), INTENT(IN) :: f_in_1d
      REAL, DIMENSION(dim_in) :: f_in
      REAL, DIMENSION(dim_out) :: f_out
      INTEGER :: ifail_func

      IF (dim_out <= 0) THEN
        ERRORSTOP(.TRUE., 'NAG routines do not accept 0d functions')
      END IF
      f_in = f_in_1d
      CALL f(dim_in, f_in, f_data, dim_out, f_out, ifail_func)
      f_out_1d = f_out(num_out_dim)
    END FUNCTION

    REAL(KIND=DBL) FUNCTION nag_nd_f(dim_in, f_nd_in) RESULT(f_out_nd)
      USE kind
      INTEGER, INTENT(IN) :: dim_in
      REAL(KIND=DBL), DIMENSION(dim_in), INTENT(IN) :: f_nd_in
      REAL, DIMENSION(dim_out) :: f_out
      INTEGER :: ifail_func

      CALL f(dim_in, f_nd_in, f_data, dim_out, f_out, ifail_func)
      ! Limit values of integrand going into NAG
      ! Should rarely happen, but causes NAG iterate indefinetly in some (all?)
      ! cases, for example ifort/12 inside JETTO, see 6ceb24de7af3d19826a868b635d596870c606a83
      f_out_nd = MIN(MAX(f_out(num_out_dim), -1e12), 1e12)
    END FUNCTION
#endif
  END SUBROUTINE integration

  SUBROUTINE debug_msg_pion(f_name, ifail, verbose, p, nion)
    INTEGER, INTENT(IN) :: ifail, p, nion
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) :: f_name
    IF (ifail /= 0 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,A,A,I7,A,I3)") 'ifail = ',ifail, &
        '. Abnormal termination of ', f_name, &
        ', integration at p=', p, ', nion=', nion
    ENDIF
  END SUBROUTINE

  SUBROUTINE debug_msg_pnu(f_name, ifail, verbose, p, nu)
    INTEGER, INTENT(IN) :: ifail, p, nu
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) :: f_name
    IF (ifail /= 0 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,A,A,I7,A,I3)") 'ifail = ',ifail, &
        '. Abnormal termination of ', f_name, &
        ', integration at p=', p, ', nu=', nu
    ENDIF
  END SUBROUTINE

  SUBROUTINE debug_msg_pnuion(f_name, ifail, verbose, p, nu, nion)
    INTEGER, INTENT(IN) :: ifail, p, nu, nion
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) :: f_name
    IF (ifail /= 0 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,A,A,I7,A,I3,A,I3)") 'ifail = ',ifail, &
        '. Abnormal termination of ', f_name, &
        ', integration at p=', p, ', nu=', nu, ', nion=', nion
    ENDIF
  END SUBROUTINE debug_msg_pnuion

  SUBROUTINE debug_msg_integration(f_name, ifail, verbose, p, nu, nion, omega)
    INTEGER, INTENT(IN) :: ifail, p, nu, nion
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) :: f_name
    COMPLEX, INTENT(IN) :: omega
    IF (ifail /= 0 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,A,A,I7,A,I3,A,I3,A,G10.3,A,G10.3,A)") 'ifail = ',ifail, &
        '. Abnormal termination of ', f_name, &
        ', integration at p=', p, ', nu=', nu, ', nion=', nion, &
        ', omegap=(', REAL(omega), ',', AIMAG(omega), ')'
    ENDIF
  END SUBROUTINE debug_msg_integration

  SUBROUTINE debug_msg_pnuomega(f_name, ifail, verbose, p, nu, omega)
    INTEGER, INTENT(IN) :: ifail, p, nu
    LOGICAL, INTENT(IN) :: verbose
    CHARACTER(LEN=*), INTENT(IN) :: f_name
    COMPLEX, INTENT(IN) :: omega
    IF (ifail /= 0 .AND. verbose) THEN
      WRITE(stderr,"(A,I0,A,A,A,I7,A,I3,A,G10.3,A,G10.3,A)") 'ifail = ',ifail, &
        '. Abnormal termination of ', f_name, &
        ', integration at p=', p, ', nu=', nu, &
        ', omegap=(', REAL(omega), ',', AIMAG(omega), ')'
    ENDIF
  END SUBROUTINE
END MODULE mod_integration
