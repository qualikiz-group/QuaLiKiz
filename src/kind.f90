!--------------------------------------------------------------------
!PURPOSE: DEFINE KIND AND STDERR/STDOUT NUMBERS TO USE THROUGHOUT CODE
!--------------------------------------------------------------------

MODULE kind
  ! Define kind numbers
  INTEGER, PARAMETER :: SGL = SELECTED_REAL_KIND(p=6,r=37)
  INTEGER, PARAMETER :: DBL = SELECTED_REAL_KIND(p=12,r=50)

  ! Define units of standard output and standard error
  INTEGER, PARAMETER :: stdout = 6
  INTEGER, PARAMETER :: stderr = 0

  type qlk_output_meth_0
    real(DBL), allocatable :: pfe_cm(:,:)
    real(DBL), allocatable :: pfi_cm(:,:,:)
    real(DBL), allocatable :: efe_cm(:,:)
    real(DBL), allocatable :: efi_cm(:,:,:)
    real(DBL), allocatable :: vfi_cm(:,:,:)
    real(DBL), allocatable :: ecoefs(:,:,:)
    real(DBL), allocatable :: npol(:,:,:)
    real(DBL), allocatable :: cftrans(:,:,:)
    real(DBL), allocatable :: phi(:,:)
    real(DBL), allocatable :: Nustar(:)
    real(DBL), allocatable :: Zeff(:)
  end type qlk_output_meth_0

  type qlk_output_meth_0_sep_0
    real(DBL), allocatable :: gam(:,:,:)
    real(DBL), allocatable :: ome(:,:,:)
    real(DBL), allocatable :: pfe(:)
    real(DBL), allocatable :: pfi(:,:)
    real(DBL), allocatable :: efe(:)
    real(DBL), allocatable :: efi(:,:)
    real(DBL), allocatable :: vfi(:,:)
    real(DBL), allocatable :: efeETG(:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_0_sep_0

  type qlk_output_meth_0_sep_1
    real(DBL), allocatable :: efeITG(:)
    real(DBL), allocatable :: efeTEM(:)
    real(DBL), allocatable :: efiITG(:,:)
    real(DBL), allocatable :: efiTEM(:,:)
    real(DBL), allocatable :: pfeITG(:)
    real(DBL), allocatable :: pfeTEM(:)
    real(DBL), allocatable :: pfiITG(:,:)
    real(DBL), allocatable :: pfiTEM(:,:)
    real(DBL), allocatable :: vfiITG(:,:)
    real(DBL), allocatable :: vfiTEM(:,:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_0_sep_1

  type qlk_output_meth_1
    real(DBL), allocatable :: cke(:)
    real(DBL), allocatable :: cki(:,:)
  end type qlk_output_meth_1

  type qlk_output_meth_1_sep_0
    real(DBL), allocatable :: dfe(:)
    real(DBL), allocatable :: dfi(:,:)
    real(DBL), allocatable :: vte(:)
    real(DBL), allocatable :: vti(:,:)
    real(DBL), allocatable :: vce(:)
    real(DBL), allocatable :: vci(:,:)
    real(DBL), allocatable :: vri(:,:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_1_sep_0

  type qlk_output_meth_1_sep_1
    real(DBL), allocatable :: dfeITG(:)
    real(DBL), allocatable :: dfeTEM(:)
    real(DBL), allocatable :: dfiITG(:,:)
    real(DBL), allocatable :: dfiTEM(:,:)
    real(DBL), allocatable :: vriITG(:,:)
    real(DBL), allocatable :: vriTEM(:,:)
    real(DBL), allocatable :: vteITG(:)
    real(DBL), allocatable :: vteTEM(:)
    real(DBL), allocatable :: vtiITG(:,:)
    real(DBL), allocatable :: vtiTEM(:,:)
    real(DBL), allocatable :: vceITG(:)
    real(DBL), allocatable :: vceTEM(:)
    real(DBL), allocatable :: vciITG(:,:)
    real(DBL), allocatable :: vciTEM(:,:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_1_sep_1

  type qlk_output_meth_2
    real(DBL), allocatable :: ceke(:)
    real(DBL), allocatable :: ceki(:,:)
  end type qlk_output_meth_2

  type qlk_output_meth_2_sep_0
    real(DBL), allocatable :: chiee(:)
    real(DBL), allocatable :: chiei(:,:)
    real(DBL), allocatable :: vece(:)
    real(DBL), allocatable :: veci(:,:)
    real(DBL), allocatable :: vene(:)
    real(DBL), allocatable :: veni(:,:)
    real(DBL), allocatable :: veri(:,:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_2_sep_0

  type qlk_output_meth_2_sep_1
    real(DBL), allocatable :: chieeITG(:)
    real(DBL), allocatable :: chieeTEM(:)
    real(DBL), allocatable :: chieiITG(:,:)
    real(DBL), allocatable :: chieiTEM(:,:)
    real(DBL), allocatable :: veceITG(:)
    real(DBL), allocatable :: veceTEM(:)
    real(DBL), allocatable :: veciITG(:,:)
    real(DBL), allocatable :: veciTEM(:,:)
    real(DBL), allocatable :: veneITG(:)
    real(DBL), allocatable :: veneTEM(:)
    real(DBL), allocatable :: veniITG(:,:)
    real(DBL), allocatable :: veniTEM(:,:)
    real(DBL), allocatable :: veriITG(:,:)
    real(DBL), allocatable :: veriTEM(:,:)
    real(DBL), allocatable :: chieeETG(:)
    real(DBL), allocatable :: veceETG(:)
    real(DBL), allocatable :: veneETG(:)
    integer :: normalization !! Normalization of qlk_output block, 0=SI, 1=GB
  end type qlk_output_meth_2_sep_1

  type qlk_primi_meth_0
    real(DBL), allocatable :: Lcirce(:,:,:)
    real(DBL), allocatable :: Lcirci(:,:,:,:)
    real(DBL), allocatable :: Lecirce(:,:,:)
    real(DBL), allocatable :: Lecirci(:,:,:,:)
    real(DBL), allocatable :: Lepiege(:,:,:)
    real(DBL), allocatable :: Lpiege(:,:,:)
    real(DBL), allocatable :: Lpiegi(:,:,:,:)
    real(DBL), allocatable :: Lepiegi(:,:,:,:)
    real(DBL), allocatable :: Lvcirci(:,:,:,:)
    real(DBL), allocatable :: Lvpiegi(:,:,:,:)
    complex(DBL), allocatable :: fdsol(:,:,:)
    complex(DBL), allocatable :: modeshift(:,:)
    complex(DBL), allocatable :: modewidth(:,:)
    complex(DBL), allocatable :: sol(:,:,:)
    complex(DBL), allocatable :: solflu(:,:)
    real(DBL), allocatable :: ntor(:,:)
    real(DBL), allocatable :: distan(:,:)
    real(DBL), allocatable :: kperp2(:,:)
    real(DBL), allocatable :: kymaxETG(:)
    real(DBL), allocatable :: kymaxITG(:)
  end type qlk_primi_meth_0

  type qlk_primi_meth_1
    real(DBL), allocatable :: Lcircgne(:,:,:)
    real(DBL), allocatable :: Lcircgni(:,:,:,:)
    real(DBL), allocatable :: Lcircgte(:,:,:)
    real(DBL), allocatable :: Lcircgti(:,:,:,:)
    real(DBL), allocatable :: Lcircgui(:,:,:,:)
    real(DBL), allocatable :: Lcircce(:,:,:)
    real(DBL), allocatable :: Lcircci(:,:,:,:)
    real(DBL), allocatable :: Lpiegce(:,:,:)
    real(DBL), allocatable :: Lpiegci(:,:,:,:)
    real(DBL), allocatable :: Lpieggte(:,:,:)
    real(DBL), allocatable :: Lpieggti(:,:,:,:)
    real(DBL), allocatable :: Lpieggui(:,:,:,:)
    real(DBL), allocatable :: Lpieggne(:,:,:)
    real(DBL), allocatable :: Lpieggni(:,:,:,:)
  end type qlk_primi_meth_1

  type qlk_primi_meth_2
    real(DBL), allocatable :: Lecircce(:,:,:)
    real(DBL), allocatable :: Lecircci(:,:,:,:)
    real(DBL), allocatable :: Lecircgne(:,:,:)
    real(DBL), allocatable :: Lecircgni(:,:,:,:)
    real(DBL), allocatable :: Lecircgte(:,:,:)
    real(DBL), allocatable :: Lecircgti(:,:,:,:)
    real(DBL), allocatable :: Lecircgui(:,:,:,:)
    real(DBL), allocatable :: Lepiegce(:,:,:)
    real(DBL), allocatable :: Lepiegci(:,:,:,:)
    real(DBL), allocatable :: Lepieggne(:,:,:)
    real(DBL), allocatable :: Lepieggni(:,:,:,:)
    real(DBL), allocatable :: Lepieggui(:,:,:,:)
    real(DBL), allocatable :: Lepieggte(:,:,:)
    real(DBL), allocatable :: Lepieggti(:,:,:,:)
  end type qlk_primi_meth_2

  type qlk_sizes
    integer :: dimn
    integer :: dimx
    integer :: nions
    integer :: numsols
  end type qlk_sizes

  type qlk_in_regular
    real(DBL), allocatable :: Ane(:)
    real(DBL), allocatable :: Ate(:)
    real(DBL), allocatable :: Aupar(:)
    real(DBL), allocatable :: Autor(:)
    real(DBL), allocatable :: Machpar(:)
    real(DBL), allocatable :: Machtor(:)
    real(DBL), allocatable :: x(:)
    real(DBL), allocatable :: Bo(:)
    real(DBL), allocatable :: gammaE(:)
    real(DBL), allocatable :: ne(:)
    real(DBL), allocatable :: q(:)
    real(DBL), allocatable :: Ro(:)
    real(DBL), allocatable :: Rmin(:)
    real(DBL), allocatable :: smag(:)
    real(DBL), allocatable :: Te(:)
    real(DBL), allocatable :: alpha(:)
    real(DBL), allocatable :: rho(:)

    real(DBL), allocatable :: anise(:)
    real(DBL), allocatable :: danisedr(:)

    real(DBL), allocatable :: kthetarhos(:)

    real(DBL), allocatable :: Ai(:,:)
    real(DBL), allocatable :: Ani(:,:)
    real(DBL), allocatable :: Ati(:,:)
    real(DBL), allocatable :: Zi(:,:)
    real(DBL), allocatable :: typei(:,:)
    real(DBL), allocatable :: normni(:,:)
    real(DBL), allocatable :: Ti(:,:)

    real(DBL), allocatable :: anis(:,:)
    real(DBL), allocatable :: danisdr(:,:)

    integer, allocatable :: ion_type(:,:)

    integer :: el_type
    integer :: coll_flag
    integer :: maxpts
    integer :: maxruns
    integer :: typee
    integer :: separateflux
    integer :: phys_meth
    integer :: verbose
    integer :: integration_routine
    integer :: simple_mpi_only
    integer :: write_primi
    integer :: rot_flag
    real(DBL) :: relacc1
    real(DBL) :: relacc2
    real(DBL) :: absacc1
    real(DBL) :: absacc2
    real(DBL) :: collmult
    real(DBL) :: ETGmult
    real(DBL) :: timeout
    real(DBL) :: rhomin
    real(DBL) :: rhomax
  end type qlk_in_regular

  type qlk_in_newt
    complex(DBL), allocatable, dimension(:,:,:) :: oldsol
    complex(DBL), allocatable, dimension(:,:,:) :: oldfdsol
  end type qlk_in_newt

END MODULE kind
