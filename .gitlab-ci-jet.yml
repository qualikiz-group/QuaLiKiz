# original project: 'qualikiz-group/QuaLiKiz'
# Linting HTTPS API does not work for local includes
# See https://docs.gitlab.com/ee/api/lint.html
# yamllint disable rule:line-length
---
include:
  - local: '.common-ci.yml'

.cache_python: &cache_python
  paths:
    - $PIP_CACHE_DIR
    - $PYTHONTOOLS_VENV_PATH

stages:
  - prepare
  - build
  - prepare_test
  - test
  - doc

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  # Python/pip variables
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PYTHON: python3
  PIP: pip3
  GIT_SUBMODULE_STRATEGY: recursive
  JINTRAC_PYTHON_INSTALL_DIR: "$CI_PROJECT_DIR/.local"
  PYTHONUSERBASE: "$CI_PROJECT_DIR/.local"  # This is $JINTRAC_PYTHON_INSTALL_DIR in JINTRAC
  PYTHONTOOLS_VENV_PATH: "$CI_PROJECT_DIR/venv/qualikiz_tools"
  PYTHONTOOLS_DIR: "$CI_PROJECT_DIR"
  PYTEST_MARK: "not run_qualikiz and not need_casefiles and not qualikiz_src and not need_reffiles"
  PACKAGE: none
  DD_DIRECTORY: none
  # Fortran variables
  HOSTFULL: "freia.jet.uk"
  QLK_HAVE_NAG: 1 # Build with NAG

###############################################
# General Freia JINTRAC environment templates #
###############################################
.prepare_env_freia: &prepare_env_freia |
  # From jetto-sanco before script (a1ad862a03cdb981642878c2baab663945c1cf06)
  echo "Setting up Freia environment"
  export USER=root
  export LOGNAME=root
  source /etc/profile.d/modules.sh
  module use /usr/local/modules/default
  # Debugging
  echo "Printing out Freia environment"
  uname -a # Print current OS for debugging
  export SOCKETS=$(grep ^physical\\sid /proc/cpuinfo | uniq | wc -l)
  export PHYS_CORES_PER_SOCKET=$(grep ^cpu\\scores /proc/cpuinfo | uniq | awk '{print $4}')
  export PHYS_CORES=$(expr $PHYS_CORES_PER_SOCKET \* $SOCKETS)
  export VCORES=$(grep ^processor /proc/cpuinfo | uniq | wc -l)
  printf "CPU $(cat /proc/cpuinfo | grep 'model name' | uniq)\n"
  echo 'physical cores: ' $PHYS_CORES
  echo 'virtual cores: ' $VCORES
  echo `pwd`

.prepare_env_jetto_sanco: &prepare_env_jetto_sanco |
  echo "Setting up JETTO-SANCO environment"
  # From jetto-sanco before script (a1ad862a03cdb981642878c2baab663945c1cf06)
  module load standard dot FUN
  mkdir -p runs
  export PPF=NONE
  export NOBATCH=yes
  #. /home/sim/jintrac/default/sh/load_jintrac default sim && echo "Loading of JINTRAC env worked!" || echo "Loading of JINTRAC env failed"
  # Instead of alias loadjintrac='source /home/sim/jintrac/default/sh/load_jintrac'
  # Also does
  # module load jintrac-pythontools
  # Next two commands are in module load
  # export PATH=$PATH:`pwd`/$JINTRAC_PYTHON_INSTALL_DIR/bin
  # export PYTHONPATH=`pwd`/$JINTRAC_PYTHON_INSTALL_DIR/lib/python3.7/site-packages/:$PYTHONPATH

.destroy_env_jetto_sanco: &destroy_env_jetto_sanco |
  # From jetto-sanco after script (a1ad862a03cdb981642878c2baab663945c1cf06)
  echo "Destroying JETTO-SANCO environment"
  export USER=root
  export LOGNAME=root
  source /etc/profile.d/modules.sh
  module use /usr/local/modules/default
  module load git/2.26.1
  make describe -C /home/sim/jintrac/ci_ifort

.prepare_venv_python: &prepare_venv_python |
  echo "Preparing Python environment"
  # Create a python venv with access to the globally installed packages. This links to setuptools, pip and python, pkg_resources, easy_install directly.
  module unload jintrac-pythontools/freia jintrac-simdb/freia # Contain python/3.7 packages
  module swap python python/3.9
  $PYTHON -m venv --system-site-packages $PYTHONTOOLS_VENV_PATH
  source $PYTHONTOOLS_VENV_PATH/bin/activate
  # Simulate Python3.9 --upgrade-deps flag
  #   --upgrade-deps        Upgrade core dependencies: pip setuptools to the
  #                         latest version in PyPI
  $PYTHON -m pip install --force-reinstall --upgrade pip setuptools wheel
  # Set up a Francis JINTRAC-like user environment
  export PYTHONUSERBASE="${PYTHONUSERBASE:=./.local}"
  export PATH="${PYTHONUSERBASE}/bin":$PATH
  # Give venv PYTONPATH and PATH precedence over pre-defined env ones
  export PATH=$VIRTUAL_ENV/bin:$PATH
  export PYTHONPATH=$VIRTUAL_ENV/lib/python`$PYTHON --version | cut -d' ' -f2 | cut -d'.' -f1-2`/site-packages/:$PYTHONPATH
  export LANG=en # Set language for GUIs
  $PIP install importlib-metadata -U # Fix broken env
  module list
  echo "Done preparing Python environment"

.destroy_env_python: &destroy_env_python |
  echo "Destroying Python VENV contained in '$PYTHONTOOLS_VENV_PATH'"
  rm -rf $PYTHONTOOLS_VENV_PATH

#######################################
# Freia compile environment templates #
#######################################
# Kept as reference. See current JETTO CI for up-to-date version
.prepare_env_pgi: &prepare_env_pgi |
  # From jetto-sanco build-ifort (a1ad862a03cdb981642878c2baab663945c1cf06)
  # module use /home/sim/jintrac/ci_pgi/modules
  # module load jintrac/freia.pgi
  module list
  # module use /home/sim/jintrac/ci_pgi/modules
  # module load jintrac/freia.pgi
  # module unload idl
  # module load idl
  # module list
  # export RUNS_HOME=`pwd`/runs

.prepare_env_intel: &prepare_env_intel |
  module unload pgi # Conflicts with to-be-loaded module 'ifort/12.0'
  # From jetto-sanco build-ifort (a1ad862a03cdb981642878c2baab663945c1cf06)
  module use /home/sim/jintrac/ci_ifort/modules
  module load jintrac/freia.ifort
  module list

.prepare_env_gcc: &prepare_env_gcc |
  module unload pgi # Conflicts with to-be-loaded module 'gfortran/4.8'
  # From jetto-sanco build-ifort (a1ad862a03cdb981642878c2baab663945c1cf06)
  module use /home/sim/jintrac/ci_gfortran/modules
  module load jintrac/freia.gfortran
  module list
  # Old; for reference
  #module unload pgi
  #module use /home/pknight/jintrac/ci_gfortran/modules
  #module load jintrac/freia.gfortran
  #module unload pgi openmpi
  #module load gfortran/4.8 openmpi openmpi-devel
  #module list


  #############################
  # Python workflow templates #
  #############################

  # Unforunately we cannot extend because of JETs older GitLab
  # So we need to fully copy here to mask the unsupported keyswords in the templates
  # We do a JINTRAC-like wheel install
.collect_deps_manually: &collect_deps_manually |
  export BUILD_DEPS=$(cat pyproject.toml | grep requires | cut -d'=' -f2- | tr -d ,\"\' | sed "s/^ \[//" | sed "s/\]$//")
  export RUN_DEP_FILES="requirements_core.txt requirements_gui.txt requirements_test.txt requirements_qlknn.txt"

.print_debugging: &print_debugging |
  $PYTHON --version # Print out python version for debugging
  $PIP --version # Show pip version for debugging
  echo PYTHONPATH=$PYTHONPATH
  echo PATH=$PATH
  echo BUILD_DEPS=$BUILD_DEPS
  echo RUN_DEP_FILES=$RUN_DEP_FILES

install_venv:
  script:
    - cd QuaLiKiz-pythontools
    - *collect_deps_manually
    - *print_debugging
    # Now we install dependencies manually
    - $PIP install --upgrade $BUILD_DEPS  # Install build deps manually
    - $PYTHON setup.py --version  # Print setuptools found version
    - $PYTHON -m pip freeze > pre_install_packages.txt
    - for file in $RUN_DEP_FILES; do echo Installing $file; pip install -r $file; done;  # Install run deps manually
    # Install from wheel
    # Should install almost nothing, as we installed most of it ourselves
    # Install without extras here. We installed these manually.
    # Should be similar to how Francis install the Pythontools in the JET env
    - $PIP install --user --upgrade --cache-dir=$JINTRAC_PYTHON_INSTALL_DIR/pip_cache --no-build-isolation --editable .  # Install local folder in editable mode
    - $PIP freeze > post_install_packages.txt
    # Do basic sanity checking
    - cat qualikiz_tools/version.py  # Check if version file was generated
    - $PYTHON -c "import qualikiz_tools; print(qualikiz_tools.__version__); print(qualikiz_tools.__path__)"  # Try regular import
    # Run the real test, pytest
    - which pytest
    - pytest --version
    - pytest --cov=qualikiz_tools --cov-report=term --cov-report=xml --junit-xml=junit.xml -m "need_casefiles and not run_qualikiz"
    # Test also primitives of what is needed for other "general" jobs
    - cd ..
    - qualikiz_tools create regression --as_batch bash
    - qualikiz_tools input generate regression/quicktest_parameters
  # One pipeline has one artifact. We need both the test report _and_ the installed packages to pass on
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - QuaLiKiz-pythontools/
      - $PYTHONTOOLS_VENV_PATH
      - QuaLiKiz-pythontools/pre_install_packages.txt
      - QuaLiKiz-pythontools/post_install_packages.txt
      - $PYTHONTOOLS_VENV_PATH
      - QuaLiKiz-pythontools/dist/qualikiz_tools*
      - $PYTHONUSERBASE
    reports:
      junit: [QuaLiKiz-pythontools/junit.xml]
      cobertura: [QuaLiKiz-pythontools/coverage.xml]
    when: always
    expire_in: 7 day  # These are only used in quick interactive CI debugging, and in next jobs
  stage: build
  cache:
    <<: *cache_python
    policy: push
  except:
    - documentation
  tags:
    - freia

##############################
# Global tests and templates #
##############################

# Almost all jobs need the default env and a python env
# We use the intel env as default

before_script:
  - *prepare_env_freia
  - *prepare_env_jetto_sanco
  - *prepare_env_intel
  - *prepare_venv_python

after_script:
  - *destroy_env_jetto_sanco


###############
# Build tests #
###############

# Build qualikiz. Only needs a sane env, so overwrite variables and env in extending jobs
.qualikiz_build_freia:
  extends: .qualikiz_build_template
  tags:
    - freia

build_qualikiz_intel:
  extends: .qualikiz_build_freia
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_intel
  variables:
    TOOLCHAIN: intel
    BUILD: release
    TARGET_ENV: default

build_qualikiz_intel_debug:
  extends: .qualikiz_build_freia
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_intel
  variables:
    TOOLCHAIN: intel
    BUILD: debug
    TARGET_ENV: default

build_qualikiz_gcc:
  extends: .qualikiz_build_freia
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_gcc
  variables:
    TOOLCHAIN: gcc
    BUILD: release
    TARGET_ENV: default

build_qualikiz_gcc_debug:
  extends: .qualikiz_build_freia
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_gcc
  variables:
    TOOLCHAIN: gcc
    BUILD: debug
    TARGET_ENV: default


###############
# Run tests   #
###############

# Tests that need just do mpirun
# Needs Pythontools to generate run folders
# Overwrite env, variables, and needs in the extending job
.pythontools_runtest_freia:
  extends: .runtest_template
  tags:
    - freia

# Run test variables, for re-use in variables list
# Each test overwrites before_script depending on the env needed
.run_test_default_variables: &run_test_default_variables
  RUN_CASE: full_flags
  MPI_TASKS: 1
  SIMPLE_MPI_ONLY: 0
  TARGET_ENV: default
  MPIRUN_CMD: mpirun --allow-run-as-root

# QuaLikiz process hangs: OPENMP trouble
# See https://gitlab.com/qualikiz-group/QuaLiKiz/issues/46
runtest_gfortran:
  extends: .pythontools_runtest_freia
  variables:
    <<: *run_test_default_variables
    TOOLCHAIN: gcc
    BUILD: release
    SIMPLE_MPI_ONLY: 1
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_gcc
    - *prepare_venv_python
  needs: ["install_venv", "build_qualikiz_gcc"]

# QuaLikiz process hangs: OPENMP trouble
# See https://gitlab.com/qualikiz-group/QuaLiKiz/issues/46
runtest_gfortran_debug:
  extends: .pythontools_runtest_freia
  variables:
    <<: *run_test_default_variables
    TOOLCHAIN: gcc
    BUILD: debug
    SIMPLE_MPI_ONLY: 1
  before_script:
    - *prepare_env_freia
    - *prepare_env_jetto_sanco
    - *prepare_env_gcc
    - *prepare_venv_python
  needs: ["install_venv", "build_qualikiz_gcc_debug"]

####################
# Regression tests #
####################

# Tests that call test_compare_cases[$CASE_NAME]
.pythontools_regression_freia:
  extends: .pythontools_regression_template
  tags:
    - freia
  needs: ["install_venv", "build_qualikiz_intel"]


.regression_test_default_variables: &regression_test_default_variables
  TOOLCHAIN: intel
  BUILD: release
  TARGET_ENV: default
  MPI_TASKS: 4
  RUN_TIMES: 1
  SIMPLE_MPI_ONLY: ""
  EQUIV_REL_DIFF_BOUND: 0
  ALLOW_RUN_AS_ROOT: "--allow-run-as-root"

quicktest_parameters:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

cyclone_base_case_withrot_withcoll:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

cyclone_base_case_norot_withcoll:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

GASTD_base_case_withcol:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

GASTD_base_case_withcol_sepflux:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

jcitrin_TEM_base_case_nocoll:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

POP2012_caseC_base_case_phys0:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

POP2012_caseC_base_case_phys1:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

POP2012_caseC_base_case_phys2:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

jcitrin_TEM_base_case_withcoll:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

example_extra_impurities:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

example_extra_impurities_type2:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

example_extra_impurities_type3:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

example_extra_impurities_type4:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

# Different momentum transport fluxes with complex mpi
# and/or with multiple cores
# See https://gitlab.com/qualikiz-group/QuaLiKiz/-/issues/75
# Also different for different CPU archs?
jetto_par03:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables
    SIMPLE_MPI_ONLY: --simple-mpi-only
    MPI_TASKS: 1
    EQUIV_REL_DIFF_BOUND: 1e-2

GASTD_nocol_withrot:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

full_flags:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables

full_flags_run_twice:
  extends: .pythontools_regression_freia
  variables:
    <<: *regression_test_default_variables
    RUN_TIMES: 2
