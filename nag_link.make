ifeq ($(QLK_HAVE_NAG),1)
  ifeq ($(LOCALCFG_ACTIVE),docker-debian)
    QLK_EXTRA_LFLAGS_ += -lnag-$(TOOLCHAIN)-$(BUILD)-$(TARGET_ENV)
  endif

  ifeq ($(LOCALCFG_ACTIVE),jet.uk)
    NAG_ROOT = /usr/local/depot/NAGMark24/
    # Use JETTO-style hardlinks to NAG libraries
    ifeq ($(TOOLCHAIN),gcc)
      NAG_INSTALL_DIR=$(NAG_ROOT)/gfortran
      NAG_LIB=$(NAG_INSTALL_DIR)/lib/libnag_nag.a
      NAG=-L$(NAG_INSTALL_DIR)/lib -lnag_nag
      QLK_EXTRA_LFLAGS_  += $(NAG_LIB)
    endif
    ifeq ($(TOOLCHAIN),intel)
      NAG_INSTALL_DIR=$(NAG_ROOT)/ifort
      NAG_LIB=$(NAG_INSTALL_DIR)/lib/libnag_nag.a
      NAG=-L$(NAG_INSTALL_DIR)/lib -lnag_nag -lnag_mkl
      QLK_EXTRA_LFLAGS_ += $(NAG_LIB)
    endif
    ifeq ($(TOOLCHAIN),pgi)
      NAG_INSTALL_DIR = $(NAG_ROOT)/pgi
      NAG_LIB = $(NAG_INSTALL_DIR)/lib/libnag_nag.a
      NAG = -I$(NAG_INSTALL_DIR)/nag_interface_blocks $(NAG_LIB)
      QLK_EXTRA_LFLAGS_ += $(NAG_LIB)
    endif
  endif

  ifeq ($(LOCALCFG_ACTIVE),marconi.cineca.it)
    NAG_DIR=$(shell echo $$NAG_LIB)
    QLK_EXTRA_LFLAGS_ += -L$(shell echo $$NAG_LIB) -lnag_mkl
  endif

  ifneq (,$(filter $(LOCALCFG_ACTIVE),cobra draco))
    QLK_EXTRA_LFLAGS_ += $(NAGFLIB)
  endif

  ifneq (,$(filter $(LOCALCFG_ACTIVE),hpc.l galileo.cineca.it))
    QLK_EXTRA_LFLAGS_ += $(shell eval-pkg-config --libs nag)
  endif

  QLK_EXTRA_FFLAGS += -DHAVE_NAG
endif
