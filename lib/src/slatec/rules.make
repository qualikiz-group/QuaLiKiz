$(call SUBPROJECT_set_local_dir_here)

$(call SUBPROJECT_set_local_fflags, gcc, \
	-ffree-line-length-none \
	-fdefault-real-8 \
	-fdefault-double-8 \
	-Wno-unused-variable \
	-Wno-unused-dummy-argument \
	-Wno-unused-parameter \
	-Wno-maybe-uninitialized \
	-Wno-uninitialized \
	-Wno-compare-reals \
	-Wno-conversion \
	-Wno-array-temporaries \
	-Wno-intrinsic-shadow \
	-cpp \
)

$(call LOCAL_add,\
    cpqr79.f \
    davint.f \
    dsort.f \
    rpqr79.f \
    slatecroutines.f \
)
