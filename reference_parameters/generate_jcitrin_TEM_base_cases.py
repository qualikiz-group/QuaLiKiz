import sys
import os
import inspect
import copy
from collections import OrderedDict

import numpy as np
from IPython import embed

from qualikiz_tools.machine_specific.bash import Run, Batch
from qualikiz_tools.qualikiz_io.inputfiles import QuaLiKizXpoint, QuaLiKizPlan, Electron, Ion, IonList

root = os.path.dirname(__file__)
def generate_base_cases(target_dir=root, qualikiz_plan_base=None):
    """ Generate parameters.json based on the GA standard case, but with TEM triggered
    """
    if qualikiz_plan_base is None:
        ## Load the default parameters.json
        base_json = os.path.join(root, 'jcitrin_TEM_base.json')
        qualikiz_plan_base = QuaLiKizPlan.from_json(base_json)
    xpoint = qualikiz_plan_base['xpoint_base']

    # Some values that depend on other values
    xpoint['x'] = 0.18 * xpoint['Ro'] / xpoint['Rmin']
    xpoint['rho'] = xpoint['x']

    # And some options we might want to mess with
    xpoint['phys_meth'] = 2
    xpoint['rot_flag'] = 0
    xpoint['coll_flag'] = False
    xpoint['verbose'] = 1
    xpoint['separateflux'] = 0
    xpoint['simple_mpi_only'] = True

    # Set some options just in case the defaults change
    xpoint['set_qn_normni'] = False
    xpoint['set_qn_An'] = False
    xpoint['check_qn'] = True
    xpoint['x_eq_rho'] = False
    xpoint['recalc_Nustar'] = False
    xpoint['recalc_Ti_Te_rel'] = False
    xpoint['assume_tor_rot'] = True

    xpoint.set_puretor()

    qualikiz_plan_base.to_json(os.path.join(target_dir, 'jcitrin_TEM_base_case_nocoll.json'))
    xpoint['coll_flag'] = True
    qualikiz_plan_base.to_json(os.path.join(target_dir, 'jcitrin_TEM_base_case_withcoll.json'))
