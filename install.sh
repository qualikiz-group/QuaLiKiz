#!/bin/bash
if [ "$1" == "auto" ]; then
    interactive=false
    echo not interactive
fi

case $HOSTNAME in
  karel-work | karel-macbook | karel-super | karel-differ)
    ARCH=archlinux
    ;;
  rs*)
    ARCH=rsrijnhuizen
    ;;
  edison*)
    ARCH=edison
    ;;
  fusionsim)
    ARCH=fusionsim
    ;;
  freia*)
    ARCH=freia
    ;;
  heimdall*)  #rename of freia, same architecture
    ARCH=freia
    ;;
  cobra*)
    ARCH=cobra
    ;;
  draco*)
    ARCH=draco
    ;;
  andromede*)
    ARCH=andromede
    ;;
  r0*)
    ARCH=marconi
    ;;
  docker-debian*)
    ARCH=docker-debian
    ;;
  r1*)
    ARCH=gateway
    ;;
esac

if [ "$ARCH" == "" ]
then
    echo "Machine not found"
    yno=N
else
    echo Detected machine \'$ARCH\' with root \'$PWD\'. Is this correct? [Y/n]
    if $interactive
    then
        read yno
    fi
    if [ "$yno" == "" ]
    then
      echo Y
      yno=Y
    fi
fi

case $yno in
  Y | y)
    echo Creating and adjusting src/Makefile.inc
    cp $PWD/src/make.inc/Makefile.$ARCH $PWD/src/Makefile.inc
    sed -i '/QLKDIR=*/c\QLKDIR='$PWD $PWD/src/Makefile.inc
    ;;
  N | n)
    echo "Please manually copy src/make.inc/Makefile.* for your arch to src/Makefile.inc and edit QLKDIR and compiler flags"
    exit 1
    ;;
esac
