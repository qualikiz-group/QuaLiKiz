# Python rules
KIND_MAP=$(QLKROOT_)/src/qlk_python_kind_map
F90WRAP_MODULE=QLKFORT
#QLK_PYTHON_MODULES=src/kind.f90 src/datmat.f90 src/datcal.f90 src/mod_make_io.f90
# For qlk_makeflux we need
# kind, datmat, datcal, asymmetry, mod_saturation
# asymmetry: mod_integration, datcal, datmat, kind
# mod_integration: kind, hcub, datcal
# hcub: kind
# mod_saturation: kind, datmat, datcal, mod_make_io
# mod_make_io: kind, datcal, datmat, mpi
#
QLK_PYTHON_WRAP_MODULES=src/kind.f90 src/datmat.f90 src/datcal.f90 src/mod_make_io.f90 src/mod_saturation.f90

#src/hcub.f90
#src/mod_integration.f90
# For qlk_makeflux we need
# asymmetry, mod_saturation
# asymmetry: mod_integration
# mod_integration: hcub
# hcub:
# mod_saturation:
#
#QLK_PYTHON_MODULES+=mod_io_management asymmetrymod_saturation

python: $(PROJECT_SUB_QLK_OBJS) $(KIND_MAP)
	f90wrap -v -m $(F90WRAP_MODULE) $(QLK_PYTHON_WRAP_MODULES) -k $(KIND_MAP)
	mv f90wrap_*.f90 $(BAKEDBUILDDIR)
	f2py-f90wrap -DHAVE_NAG --include-paths $(BAKEDMODDEST) -I$(BAKEDMODDEST) -c -m _$(F90WRAP_MODULE) $(PROJECT_SUB_QLK_OBJS) --f77flags="-fdefault-real-8 -fdefault-double-8 -fopenmp" --f90flags="-fdefault-real-8 -fdefault-double-8 -fopenmp" --opt='-Og' -lgomp --f90exec=$(FC) $(BAKEDBUILDDIR)/f90wrap_*.f90
	mv $(F90WRAP_MODULE).py _$(F90WRAP_MODULE)* $(BAKEDBUILDDIR)
