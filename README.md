# QuaLiKiz
QuaLiKiz is a quasilinear gyrokinetic code rapid enough to ease systematic
interface with experiments. The quasilinear approximation is justified over a
wide range of tokamak core parameters where nonlinear and quasilinear turbulent
fluxes have been shown to agree. QuaLiKiz computes quasilinear gyrokinetic
turbulent heat, particle and angular momentum fluxes. It is coupled in
integrated modelling platforms such as CRONOS and JETTO. QuaLiKiz is available
to all users. It allows for extensive stand-alone interpretative analysis and
for first principle based integrated predictive modelling.

## Install
1. [optional, preferred] [Set up your SSH keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
2. [Clone the repository from GitLab](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)
    and move into the folder

       git clone git@gitlab.com:QuaLiKiz-group/QuaLiKiz.git
       cd QuaLiKiz

    Initialize and update the (optional) tools contained as [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

       git submodule init
       git submodule update

For more tips on git check out the QuaLiKiz
[git for dummies wiki page](https://gitlab.com/qualikiz-group/QuaLiKiz/-/wikis/Some%20%22GIT%20for%20dummies%22%20tips).

3. Compile QuaLiKiz. To install QuaLiKiz you will need a fortran compiler,
    openmpi and openMP library. Aditionally, QuaLiKiz depends on NAG for its
    integration routines. QuaLiKiz uses [TUBS](https://gitlab.com/tci-dev/tubs)
    as build system, and it has many fusion-relevant system settings already
    pre-configured. Just run

   ``` bash
   make
   ````

   The TUBS/TCI summary will show `with localcfg (none)` if your system is
   undefined. If so, you can still build QuaLiKiz by setting your TUBS settings
   accordingly. You can put these settings (and settings to overwrite TUBS
   defaults if your system is known) in a file called `usercfg.make` in the
   QuaLiKiz root. Alternatively, pass these on the command line to `make`. An
   example `usercfg.make` can be found below, any `make` legal constructs are
   allowed.

   ``` make
   ifeq ($(TUBS_CONFIG_PHASE),1)
   TOOLCHAIN=gcc #One of pgi, gcc or intel. The vendor of the compiler used. Needs to be set in CONFIG_PHASE=1, so on command line or with the special if statement in usercfg.make
   else
   FC=mpifort # MPI-enabled wrapper for compiler
   LINK=$(FC) # MPI-enabled wrapper for linker
   VERBOSE=1 # Turn on verbose build messages. 0 to turn them off
   QLK_EXTRA_LFLAGS_=-L/usr/local/depot/NAGMark24/gfortran -lnag_nag # Flags to link to NAG. For some inspiration, look in nag_link.make
   BUILD=release # Build the release version of QuaLiKiz. Can also be 'debug'
   endif
   ```

TUBS puts finished binaries in `bin/`. To be compatible with old workflows, a link to the last build binary is created in the root directory.

## Usage
To run QuaLiKiz, one first has to create a folder `input` with the input
binaries, as well as the folder `output`, `output/primitive` and `debug`. Then,
one needs to generate input binaries. To make this easier, we have developed
tools in [MATLAB](https://gitlab.com/QuaLiKiz-group/QuaLiKiz-matlabtools) and
[Python](https://gitlab.com/QuaLiKiz-group/QuaLiKiz-pythontools) to set up,
validate and plot QuaLiKiz runs. There is a step-by-step guide for a standalone
run with the Pythontools at the [QuaLiKiz wiki](https://gitlab.com/qualikiz-group/QuaLiKiz/wikis/Running%20with%20Pythontools%20in%20standalone).

## Disclaimer
QuaLiKiz is free and open-source software. If you have used QuaLiKiz in your
own work, please cite our latest paper, [J. Citrin et al. PPCF 2017](http://iopscience.iop.org/article/10.1088/1361-6587/aa8aeb).
